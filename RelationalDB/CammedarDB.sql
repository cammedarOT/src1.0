-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Cammedar
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Cammedar
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Cammedar` DEFAULT CHARACTER SET utf8 ;
USE `Cammedar` ;

-- -----------------------------------------------------
-- Table `Cammedar`.`Actions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`Actions` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(80) NULL,
  `EmptySlot1` VARCHAR(45) NULL,
  `EmptySlot2` VARCHAR(45) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`Muscles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`Muscles` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(80) NULL,
  `Funtion` MEDIUMTEXT NULL,
  `Origin` MEDIUMTEXT NULL,
  `Insertion` MEDIUMTEXT NULL,
  `EmptySlot1` VARCHAR(45) NULL,
  `EmptySlot2` VARCHAR(45) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`RigBones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`RigBones` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(80) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`ActionCateogry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`ActionCateogry` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(80) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`MuscleCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`MuscleCategory` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(80) NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`MuscleCategoryParents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`MuscleCategoryParents` (
  `ParentID` INT NULL,
  `ChildID` INT NULL,
  INDEX `_idx` (`ParentID` ASC, `ChildID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`ParentID` , `ChildID`)
    REFERENCES `Cammedar`.`MuscleCategory` (`ID` , `ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`MuscleParents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`MuscleParents` (
  `ParentID` INT NULL,
  `ChildID` INT NULL,
  INDEX `2_idx` (`ChildID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`ParentID`)
    REFERENCES `Cammedar`.`MuscleCategory` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `2`
    FOREIGN KEY (`ChildID`)
    REFERENCES `Cammedar`.`Muscles` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`ActionRigBones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`ActionRigBones` (
  `ActionID` INT NULL,
  `BoneID` INT NULL,
  INDEX `1_idx` (`ActionID` ASC),
  INDEX `2_idx` (`BoneID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`ActionID`)
    REFERENCES `Cammedar`.`Actions` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `2`
    FOREIGN KEY (`BoneID`)
    REFERENCES `Cammedar`.`RigBones` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`ActionMuscles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`ActionMuscles` (
  `ActionID` INT NULL,
  `MuscleID` INT NULL,
  `IsPrimary` TINYINT(1) NULL,
  INDEX `1_idx` (`ActionID` ASC),
  INDEX `2_idx` (`MuscleID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`ActionID`)
    REFERENCES `Cammedar`.`Actions` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `2`
    FOREIGN KEY (`MuscleID`)
    REFERENCES `Cammedar`.`Muscles` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`Accompanying Action`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`Accompanying Action` (
  `ActionID` INT NULL,
  `AccompanyingActionID` INT NULL,
  INDEX `1_idx` (`ActionID` ASC, `AccompanyingActionID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`ActionID` , `AccompanyingActionID`)
    REFERENCES `Cammedar`.`Actions` (`ID` , `ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`ActionEquations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`ActionEquations` (
  `ActionID` INT NULL,
  `EquationID` INT NOT NULL AUTO_INCREMENT,
  `Eqaution` VARCHAR(80) NULL,
  INDEX `1_idx` (`ActionID` ASC),
  PRIMARY KEY (`EquationID`),
  CONSTRAINT `1`
    FOREIGN KEY (`ActionID`)
    REFERENCES `Cammedar`.`Actions` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`States`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`States` (
  `ActionID` INT NULL,
  `StateNumber` VARCHAR(15) NULL,
  `Angle` FLOAT NULL,
  INDEX `1_idx` (`ActionID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`ActionID`)
    REFERENCES `Cammedar`.`Actions` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`StateActions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`StateActions` (
  `PrimaryActionID` INT NULL,
  `StateNumber` VARCHAR(15) NULL,
  `AccompanyingActionID` VARCHAR(45) NULL,
  INDEX `1_idx` (`PrimaryActionID` ASC),
  CONSTRAINT `1`
    FOREIGN KEY (`PrimaryActionID`)
    REFERENCES `Cammedar`.`States` (`ActionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
CONSTRAINT `2`
	FOREIGN KEY (`StateNumber`)  REFERENCES `Cammedar`.`States` (`StateNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- ///////////////////////////////////////////////
-- USER DATA TABLES 

-- -----------------------------------------------------
-- Table `Cammedar`.`Users(byFirebase)`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`Users(byFirebase)` (
  `Identifier` VARCHAR(45) NULL,
  `Created` DATETIME NULL,
  `SignedIn` DATETIME NULL,
  `UserUID` VARCHAR(38) NOT NULL,
  PRIMARY KEY (`UserUID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`UserTypes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`UserTypes` (
  `UserUID` VARCHAR(38) NOT NULL,
  `Type` VARCHAR(45) NULL,
  PRIMARY KEY (`UserUID`),
  CONSTRAINT `fk_UsersData_Users (byFirebase)1`
    FOREIGN KEY (`UserUID`)
    REFERENCES `Cammedar`.`Users(byFirebase)` (`UserUID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`UsersData`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`UsersData` (
  `UserUID` VARCHAR(38) NOT NULL,
  `UniversityEmail` VARCHAR(55) NOT NULL,
  `PersonalEmail` VARCHAR(55) NOT NULL,
  `Department` VARCHAR(45) NOT NULL,
  `ExpectedYearofGraduation` DATE NULL,
  `CellPhone` VARCHAR(13) NOT NULL,
  PRIMARY KEY (`UserUID`),
  CONSTRAINT `fk_table1_Users (byFirebase)1`
    FOREIGN KEY (`UserUID`)
    REFERENCES `Cammedar`.`Users(byFirebase)` (`UserUID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`ConfirmedUsers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`ConfirmedUsers` (
  `confirmed` TINYINT(1) NULL,
  `UserUID` VARCHAR(38) NOT NULL,
  INDEX `fk_ConfirmedUsers_Users (byFirebase)1_idx` (`UserUID` ASC),
  CONSTRAINT `fk_ConfirmedUsers_Users (byFirebase)1`
    FOREIGN KEY (`UserUID`)
    REFERENCES `Cammedar`.`Users(byFirebase)` (`UserUID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`UserSessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`UserSessions` (
  `SessionID` VARCHAR(45) NOT NULL,
  `UserUID` VARCHAR(38) NOT NULL,
  UNIQUE INDEX `SessionID_UNIQUE` (`SessionID` ASC),
  INDEX `fk_UserSessions_Users (byFirebase)1_idx` (`UserUID` ASC),
  CONSTRAINT `fk_UserSessions_Users (byFirebase)1`
    FOREIGN KEY (`UserUID`)
    REFERENCES `Cammedar`.`Users(byFirebase)` (`UserUID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cammedar`.`Sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Cammedar`.`Sessions` (
  `SessionID` VARCHAR(45) NOT NULL,
  `DeltaTimefromProgramStart` TIME NULL,
  `MouseX` INT NULL,
  `MouseY` INT NULL,
  `Event` VARCHAR(45) NULL,
  INDEX `fk_UserSessions_SessionID_idx` (`SessionID` ASC),
  CONSTRAINT `fk_UserSessions_SessionID`
    FOREIGN KEY (`SessionID`)
    REFERENCES `Cammedar`.`UserSessions` (`SessionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

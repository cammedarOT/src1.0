﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class MenuItemStateTextLinker : MonoBehaviour {

    public string enabledText;
    public string disabledText;
    public bool buttonEnabled ;

    private HintPopup hintPopUp;
	[ObfuscateLiterals]
    void Start()
    {
        hintPopUp = this.transform.GetComponent<HintPopup>();
        if (hintPopUp == null)
        {
            Debug.Log(" Can't find a hintPopUp script attached to this object");
        }
        else
        {
            SetText();
        }
    }
    void Update()
    {
    }

    public void StateButtonClickHandler()
    {
        buttonEnabled = !buttonEnabled;
        SetText();


    }
    private void SetText()
    {
        if (buttonEnabled)
            hintPopUp.SetText(disabledText);
        else hintPopUp.SetText(enabledText);
    }
}

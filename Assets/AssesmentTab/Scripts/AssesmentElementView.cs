﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssesmentElementView : MonoBehaviour {

    public Animator rigAnimator;
    public Animator doctorAnimator;
    public GameObject rig3d, animationRig,doctorRig;
    public string animationClip;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayButtonClickListener()
    {
		doctorAnimator.Play (animationClip, -1);
        rigAnimator.Play(animationClip, -1);
    }
}

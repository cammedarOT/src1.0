﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResetPasswordFunction : MonoBehaviour, IPointerClickHandler
{


    public Text emailInput;
    public Networking_Manager net;

    public void OnPointerClick(PointerEventData evd)
    {
        net.ResetPassword(emailInput.text);
    }
}

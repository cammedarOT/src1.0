﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class CursorIconChanger : MonoBehaviour , IPointerClickHandler , IPointerEnterHandler , IPointerExitHandler{
	public Texture2D cursorTexture;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;

	public void OnPointerClick(PointerEventData evd)
	{
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
	}

	public void OnPointerEnter(PointerEventData evd)
	{
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
	}

	public void OnPointerExit(PointerEventData evd)
	{
		Cursor.SetCursor(null, Vector2.zero, cursorMode);
	}
}

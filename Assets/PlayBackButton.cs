﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class PlayBackButton : MonoBehaviour {

    // Use this for initialization
    public GameObject playBackObject;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
    public void Handle()
    {
        playBackObject.GetComponent<PlayBackBehaviour>().StartPlayBack();
    }
}

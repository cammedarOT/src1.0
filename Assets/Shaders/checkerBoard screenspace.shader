﻿//da awel wa7ed kont bgrab fih , da el 3reft mno eni mmkn a3raf mkan l vertex ka pixel feen 
Shader "Assets/Shaders/checkerBoard screenspace"
{
    Properties
    {
        _MyColor ("myColor", Color) = (0,1,0,1)
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            float4 _MyColor;

            void vert (
                float4 vertex : POSITION, // vertex position input
                out float4 outpos : SV_POSITION // clip space position output
                )
            {
                outpos = UnityObjectToClipPos(vertex);
            }

            fixed4 frag (UNITY_VPOS_TYPE screenPos : VPOS) : SV_Target
            {
                // screenPos.xy will contain pixel integer coordinates.
                // use them to implement a checkerboard pattern that skips rendering
                // 4x4 blocks of pixels

                // checker value will be negative for 4x4 blocks of pixels
                // in a checkerboard pattern
                screenPos.xy = floor(screenPos.xy * 0.0612) * 0.5;
                float checker = -frac(screenPos.r + screenPos.g);

                // clip HLSL instruction stops rendering a pixel if value is negative
                clip(checker);

                // for pixels that were kept, read the texture and output it
                return _MyColor;
            }
            ENDCG
        }
    }
}
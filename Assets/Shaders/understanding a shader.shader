﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/understanding a shader" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (0.0, 0.03)) = .005
		_MainTex ("Base (RGB)", 2D) = "white" { }
	}


	//shader code pasted into all further CGPROGRAM blocks in this .shader file
 	//CGINCLUDE 
 	//....
 	//ENDCG

 	////////////////////////// this block of code will behave as if it's pasted inside all CGPROGRAM below
 	// garabt a5odha cut we 7atetha gowa el CGPROGRAM ta7t we eshtghalet 3adi, just 3ashan maykrarhash bas
 	// e3tbrha maktoba ta7t fl CGPROGRAM 	
	CGINCLUDE
		#include "UnityCG.cginc"
		struct appdata {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
		}; 
		struct v2f {
			float4 pos : POSITION;
			float4 color : COLOR;
		};
		uniform float _Outline;
		uniform float4 _OutlineColor;
	 
		v2f vert(appdata v) {
			// just make a copy of incoming vertex data but scaled according to normal direction
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
		 
			float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
			float2 offset = TransformViewToProjection(norm.xy);
		 
			o.pos.xy += offset * o.pos.z * _Outline;
			o.color = _OutlineColor;
			return o;
		}
	ENDCG
 	//////////////////////////


	SubShader {
		// note that a vertex shader is specified here but its using the one above
		Pass {
			Name "OUTLINE"
			Tags { "LightMode" = "Always" }
			Cull Off
			ZWrite Off
			ZTest Always
			ColorMask RGB // alpha not used
 
			// you can choose what kind of blending mode you want for the outline
			Blend SrcAlpha OneMinusSrcAlpha // Normal
			//Blend One One // Additive
			//Blend One OneMinusDstColor // Soft Additive
			//Blend DstColor Zero // Multiplicative
			//Blend DstColor SrcColor // 2x Multiplicative
 
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				 
				half4 frag(v2f i) :COLOR {
					return i.color;
				}
			ENDCG
		}

		// el pass da maktob be tri2a esmaha fixed-function (7aga 3abita mn zman keda 3ashan yb2a sahl abl l surface shader
		// we da link byeshra7ha https://docs.unity3d.com/550/Documentation/Manual/ShaderTut1.html
		Pass {
			Name "BASE"
			ZWrite On
			ZTest LEqual
			//Blend SrcAlpha OneMinusSrcAlpha //da el kan 3amel el blending el m5ali lon l border yfre2 we y5ali lonha ma7roo2
			Material {
				Diffuse [_Color]
				Ambient [_Color]
			}
			Lighting On
			SetTexture [_MainTex] {
				ConstantColor [_Color]
				Combine texture * constant
			}
			SetTexture [_MainTex] {
				Combine previous * primary DOUBLE
			}
		}
	}

	Fallback "Diffuse"
}
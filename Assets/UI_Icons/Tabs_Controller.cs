﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Tabs_Controller : MonoBehaviour , IPointerClickHandler {
	public bool isMusclesTab = false;
	public bool isActionsTab = false;
	public bool isAnimationTab = false;
	public bool isAssessmentTab = false;
	public Button MusclesTab;
	public Button ActionsTab;
	public Button AnimationTab;
	public Button AssessmentTab;
	public GameObject MusclesContent;
	public GameObject ActionsContent;
	public GameObject AnimationContent;
	public GameObject AssessmentContent;
    public GameObject originalRig, animationRig, doctorRig;
	public MenuCollapse collapse;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerClick(PointerEventData evd){
		MusclesTab.interactable = !isMusclesTab;
		ActionsTab.interactable = !isActionsTab;
		AnimationTab.interactable = !isAnimationTab;
		AssessmentTab.interactable = !isAssessmentTab;

		MusclesContent.SetActive(isMusclesTab);
		ActionsContent.SetActive(isActionsTab);
        //A very quick and hacky fix to fix actions tab disappear after going to muscles tab
        if(isActionsTab)
        {
            GameObject actionsTabContainer = ActionsContent.transform.GetChild(0).gameObject;
            actionsTabContainer.GetComponent<ScrollRect>().verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
            //actionsTabContainer.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;
        }
        if(isAssessmentTab)
        {
            GameObject assesmentTabContainer = AssessmentContent.transform.GetChild(0).gameObject;
            assesmentTabContainer.GetComponent<ScrollRect>().verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHide;
            assesmentTabContainer.GetComponent<ScrollRect>().verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
            assesmentTabContainer.GetComponent<ScrollRect>().horizontalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
            assesmentTabContainer.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;
        }
		AnimationContent.SetActive(isAnimationTab);
		AssessmentContent.SetActive(isAssessmentTab);
        doctorRig.SetActive(isAssessmentTab);
        animationRig.SetActive(isAssessmentTab);
        originalRig.SetActive(!isAssessmentTab);
		if (collapse.isOpen ()) {
			var pointer = new PointerEventData(EventSystem.current);
			ExecuteEvents.Execute(collapse.gameObject, pointer, ExecuteEvents.pointerClickHandler);
		}
	}
}

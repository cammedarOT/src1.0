﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class MenuDropDown : MonoBehaviour , IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

	bool menuExpanded = false;
	bool mouseOnObject = false;

	public GameObject menu;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		HideIfClickedOutside (menu);

	}

	public void OnPointerClick(PointerEventData evd){
		
		Debug.Log ("Menu Expanded : " + menuExpanded);
		menu.SetActive (!menuExpanded);
		menuExpanded = !menuExpanded;


	}

	public void OnPointerEnter(PointerEventData eventData){

		mouseOnObject = true;

	}

	public void OnPointerExit(PointerEventData eventData){

		mouseOnObject = false;

	}


	private void HideIfClickedOutside(GameObject toBeHidden) {
		if (Input.GetMouseButton (0) && !toBeHidden.GetComponent<MenuContent>().isInside()) {
			toBeHidden.SetActive (false);
			menuExpanded = false;
		}
	}

	/*
	private void HideIfClickedOutside(GameObject outside, GameObject hide) {
		if (Input.GetMouseButton(0) && outside.activeSelf && 
			!RectTransformUtility.RectangleContainsScreenPoint(
				outside.GetComponent<RectTransform>(), 
				Input.mousePosition, 
				Camera.main)) {
			hide.SetActive(false);
			//menuExpanded = false;
		}

	}
	*/
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // Required when Using UI elements.
using UnityEngine.EventSystems;

public class HintPopup : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public string text;
	private TooltipView tooltip;
	bool active;

	void Start ()
	{

		object[] all = TooltipView.FindObjectsOfTypeAll(typeof (TooltipView));
		foreach(object t in all){
			tooltip = (TooltipView)t;
		}
	}


	void Update()
	{
	}
		


	public void OnPointerEnter(PointerEventData evd){
		//Debug.Log ("Enterrr");
		tooltip.changeText (text);
		tooltip.gameObject.SetActive (true);
		//tooltip.transform.position = Input.mousePosition;

		//button.image.color = newColor;

	}

	public void OnPointerExit(PointerEventData evd){
		//Debug.Log ("Leaveee");
		tooltip.gameObject.SetActive (false);

		//button.image.color = newColor;

	}

    public void SetText(string text)
    {
        this.text = text;
        tooltip.changeText(text);
    }
}
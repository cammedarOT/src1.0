﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuBarController : MonoBehaviour , IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler{
	public bool isFile = false;
	public bool isEdit = false;
	public bool isView = false;
	public bool isHelp = false;
	static bool fileMenuExpanded = false;
	static bool editMenuExpanded = false;
	static bool viewMenuExpanded = false;
	static bool helpMenuExpanded = false;
	bool mouseOnObject = false;

	public GameObject FileMenu;
	public GameObject EditMenu;
	public GameObject ViewMenu;
	public GameObject HelpMenu;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		HideIfClickedOutside(FileMenu);
		HideIfClickedOutside(EditMenu);
		HideIfClickedOutside(ViewMenu);
		HideIfClickedOutside(HelpMenu);


	}

	public void OnPointerClick(PointerEventData evd){
		/*if (isFile && fileMenuExpanded) {
			FileMenu.SetActive (false);
			fileMenuExpanded = false;
		} else {
			FileMenu.SetActive (isFile);
			fileMenuExpanded = isFile;
		}
			
		if (isEdit && editMenuExpanded) {
			EditMenu.SetActive (false);
			editMenuExpanded = false;
		} else {
			EditMenu.SetActive (isEdit);
			editMenuExpanded = isEdit;
		}

		if (isView && viewMenuExpanded) {
			ViewMenu.SetActive (false);
			viewMenuExpanded = false;
		} else {
			ViewMenu.SetActive (isView);
			viewMenuExpanded = isView;
		}

		if (isHelp && helpMenuExpanded) {
			HelpMenu.SetActive (false);
			helpMenuExpanded = false;
		} else {
			HelpMenu.SetActive (isHelp);
			helpMenuExpanded = isHelp;
		}*/
		FileMenu.SetActive (isFile);
		fileMenuExpanded = isFile;
		EditMenu.SetActive (isEdit);
		editMenuExpanded = isEdit;
		ViewMenu.SetActive (isView);
		viewMenuExpanded = isView;
		HelpMenu.SetActive (isHelp);
		helpMenuExpanded = isHelp;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		
		
		if ((fileMenuExpanded&&!isFile) || (editMenuExpanded&&!isEdit) ||
			(viewMenuExpanded&&!isView) || (helpMenuExpanded&&!isHelp)) {

			var pointer = new PointerEventData (EventSystem.current);
			ExecuteEvents.Execute (this.gameObject, pointer, ExecuteEvents.pointerClickHandler);

		}
		mouseOnObject = true;

	}

	public void OnPointerExit(PointerEventData eventData){

		mouseOnObject = false;

	}

	private void HideIfClickedOutside(GameObject toBeHidden) {
		if (Input.GetMouseButton (0) && !toBeHidden.GetComponent<MenuContent>().isInside()) {
			toBeHidden.SetActive (false);
			fileMenuExpanded = false;
			editMenuExpanded = false;
			viewMenuExpanded = false;
			helpMenuExpanded = false;
		}
	}

	/*
	private void HideIfClickedOutside(GameObject panel) {
		if (Input.GetMouseButton(0) && panel.activeSelf && 
			!RectTransformUtility.RectangleContainsScreenPoint(
				panel.GetComponent<RectTransform>(), 
				Input.mousePosition, 
				Camera.main)) {
			panel.SetActive(false);
			fileMenuExpanded = false;
			editMenuExpanded = false;
			viewMenuExpanded = false;
			helpMenuExpanded = false;
		}

	}*/
    //TODO : Move this to an actall button controllre
    public void OnResetButtonClicked()
    {
        Utils.Reset();
    }

    private void OnGUI()
    {
        Event e = Event.current;
        if (e.control && e.keyCode == KeyCode.R)
        {
            Utils.Reset();
        }
    }

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TooltipView : MonoBehaviour {

	//public string text;
	public Text textObject;
	bool PosChanged = false;

    void Update()
    {
        this.transform.position = Input.mousePosition + new Vector3(30, -10, 0);
    }

	public void changeText(string text)
	{
		if (!textObject.text.Equals (text)) {
			textObject.text = text;
            //this.transform.Translate(this.transform.position - (Input.mousePosition + new Vector3(10, 500, 0)));
            PosChanged = false;
		}

	}

	public void updatePosition(){
		if (!PosChanged) {
			this.transform.position = Input.mousePosition + new Vector3(20, -10, 0);
			PosChanged = true;
		}
	}

	/*public bool IsActive {
		get {
			return gameObject.activeSelf;
		}
	}
	//public CanvasGroup tooltip;
	public UnityEngine.UI.Text tooltipText;

	void Awake() {
		instance = this;
		HideTooltip();
	}

	public void ShowTooltip(string text, Vector3 pos) {
		this.transform.position = pos;
		if (tooltipText.text != text)
			tooltipText.text = text;
		this.gameObject.SetActive(true);
	}

	public void HideTooltip() {
		gameObject.SetActive(false);
	}

	// Standard Singleton Access 
	private static TooltipView instance;
	public static TooltipView Instance
	{
		get
		{
			if (instance == null)
				instance = GameObject.FindObjectOfType<TooltipView>();
			return instance;
		}
	}
	*/
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public static class ColorScheme{

	public static bool isDark = true;
	//Main Colors
	public static Color red = hexToColor("D83710FF");
	public static Color green = hexToColor("A0D583FF");

	public static Color imageColors = hexToColor("FFFFFFFF");

	public static Color sceneBackground = hexToColor("161616FF");
	public static Color windowBackground = hexToColor("303036FF");

	public static Color menuCollapse = hexToColor("24242CFF");

	public static Color enable_text = hexToColor("E2E2E2FF");
	public static  Color disable_text = hexToColor("B3B3B3FF");

	public static  Color toolbarBackground = hexToColor("5C5C5CFF");
	public static  Color controlBoxBackground = hexToColor("313137C8");
	public static  Color controlBoxListBackground = hexToColor("1D1D20BB");
	public static  Color groupPanelBackground = hexToColor("36363CBB");



	//Enable TriggerButton 
	public static Color enableNormalColor_TriggerButton = hexToColor("5F5F6FFF");
	public static Color enableHighlightedColor_TriggerButton = hexToColor("3B3B47FF");
	public static Color enablePressedColor_TriggerButton = enableNormalColor_TriggerButton;

	//Disable TriggerButton
	public static Color disableNormalColor_TriggerButton = windowBackground;
	public static Color disableHighlightedColor_TriggerButton = hexToColor("575767FF");
	public static Color disablePressedColor_TriggerButton = windowBackground;

	public static Color disabledColor_TriggerButton = hexToColor("898989FF");



	//Enable Button 
	public static Color enableNormalColor_Button = windowBackground;
	public static Color enableHighlightedColor_Button = hexToColor("4C4C52FF");
	public static Color enablePressedColor_Button = hexToColor("6E6F7CFF");
	//Disable Button
	public static Color disableNormalColor_Button = hexToColor("1E1E22FF");
	public static Color disableHighlightedColor_Button = windowBackground;
	public static Color disablePressedColor_Button = enableHighlightedColor_Button;

	public static Color disabledColor_Button = disableNormalColor_Button;

	//Tabs
	public static Color normalColor_Tab = windowBackground;
	public static Color highlightedColor_Tab = enableHighlightedColor_Button;
	public static Color pressedColor_Tab = enablePressedColor_Button;
	public static Color disabledColor_Tab = pressedColor_Tab;








	public static void brightTheme()
	{
		isDark = false;
		imageColors = hexToColor("8B8B8BFF");

		sceneBackground = hexToColor("E9E9E9FF");
		windowBackground = hexToColor("CECEC8FF");

		menuCollapse = hexToColor("DBDBD3FF");

		enable_text = hexToColor("2A2A2AFF");
		disable_text = hexToColor("696969FF");

		toolbarBackground = hexToColor("A3A3A3FF");
		controlBoxBackground = hexToColor("CECEC8C8");
		controlBoxListBackground = hexToColor("E2E2DFBB");
		groupPanelBackground = hexToColor("C3C3BCFF");


		//Enable TriggerButton
		enableNormalColor_TriggerButton = windowBackground;
		enableHighlightedColor_TriggerButton = groupPanelBackground;
		enablePressedColor_TriggerButton = windowBackground;

		//Disable TriggerButton 
		disableNormalColor_TriggerButton = hexToColor("969685FF");
		disableHighlightedColor_TriggerButton = hexToColor("B2B2A6FF");
		disablePressedColor_TriggerButton = enableNormalColor_TriggerButton;

		disabledColor_TriggerButton = hexToColor("898989FF");



		//Enable Button 
		enableNormalColor_Button = windowBackground;
		enableHighlightedColor_Button = hexToColor("BFBFB8FF");
		enablePressedColor_Button = hexToColor("B6B6B2FF");
		//Disable Button
		disableNormalColor_Button = hexToColor("969685FF");
		disableHighlightedColor_Button = windowBackground;
		disablePressedColor_Button = enableHighlightedColor_Button;

		disabledColor_Button = disableNormalColor_Button;

		//Tabs
		normalColor_Tab = windowBackground;
		highlightedColor_Tab = enableHighlightedColor_Button;
		pressedColor_Tab = enablePressedColor_Button;
		disabledColor_Tab = pressedColor_Tab;
		
	}


	public static void darkTheme()
	{
		isDark = true;
		imageColors = hexToColor("FFFFFFFF");

		sceneBackground = hexToColor("161616FF");
		windowBackground = hexToColor("303036FF");

		menuCollapse = hexToColor("24242CFF");

		enable_text = hexToColor("E2E2E2FF");
		disable_text = hexToColor("B3B3B3FF");

		toolbarBackground = hexToColor("5C5C5CFF");
		controlBoxBackground = hexToColor("313137C8");
		controlBoxListBackground = hexToColor("1D1D20BB");
		groupPanelBackground = hexToColor("36363CBB");


		//Enable TriggerButton 
		enableNormalColor_TriggerButton = hexToColor("5F5F6FFF");
		enableHighlightedColor_TriggerButton = hexToColor("3B3B47FF");
		enablePressedColor_TriggerButton = enableNormalColor_TriggerButton;

		//Disable TriggerButton
		disableNormalColor_TriggerButton = windowBackground;
		disableHighlightedColor_TriggerButton = hexToColor("575767FF");
		disablePressedColor_TriggerButton = windowBackground;

		disabledColor_TriggerButton = hexToColor("898989FF");



		//Enable Button 
		enableNormalColor_Button = windowBackground;
		enableHighlightedColor_Button = hexToColor("4C4C52FF");
		enablePressedColor_Button = hexToColor("6F6F7CFF");
		//Disable Button
		disableNormalColor_Button = hexToColor("1E1E22FF");
		disableHighlightedColor_Button = windowBackground;
		disablePressedColor_Button = enableHighlightedColor_Button;

		disabledColor_Button = disableNormalColor_Button;

		//Tabs
		normalColor_Tab = windowBackground;
		highlightedColor_Tab = enableHighlightedColor_Button;
		pressedColor_Tab = enablePressedColor_Button;
		disabledColor_Tab = pressedColor_Tab;

	}

	private static void swap(Color first, Color second)
	{
		Color temp = first;
		first = second;
		second = temp;
	}



	static ColorBlock cb;

	static void swap(Sprite s1, Sprite s2){
		/*Sprite temp = s1;
		s1 = s2;
		s2 = temp;*/
		//Debug.Log (s1 + " , " + s2 + "  Swapped");
	}

	public static void updateSceneColors()
	{
		object[] buttons = GameObject.FindObjectsOfTypeAll(typeof (Button));
		foreach (object b in buttons)
		{
			
			Button button = (Button) b;
			if (button.name != "Cube_Front" && button.name != "Cube_Top" && button.name != "Cube_Left" &&
				button.name != "Cam_Reset" && button.name != "Cam_More" && button.name != "Cam_Pan" &&
				button.name != "Cam_Orbit" ) 
			{

				/*
				TriggerButton tb = button.GetComponent<TriggerButton> ();
				if (tb != null) {
					tb.swapSprites ();
					//Debug.Log (tb.enableSprite + " , " + tb.disableSprite + "  Swapped");
				}

				ToolbarButton tlb = button.GetComponent<ToolbarButton> ();
				if (tlb != null) {
					tlb.swapSprites ();
					//Debug.Log (tlb.enableSprite + " , " + tlb.disableSprite + "  Swapped");
					//swap (tlb.enableSprite, tlb.disableSprite);
				}
				*/


				cb = button.colors;
				cb.normalColor = ColorScheme.enableNormalColor_Button;
				cb.pressedColor = ColorScheme.enablePressedColor_Button;
				cb.highlightedColor = ColorScheme.enableHighlightedColor_Button;
				cb.disabledColor = ColorScheme.disabledColor_Button;
				button.colors = cb;
			}
		}

		object[] triggerButtons = TriggerButton.FindObjectsOfTypeAll(typeof (TriggerButton));

		foreach (object b in triggerButtons)
		{
			TriggerButton d = (TriggerButton) b;
			Button button = d.GetComponent<Button> ();
			cb = button.colors;
			cb.normalColor = ColorScheme.enableNormalColor_TriggerButton;
			cb.pressedColor = ColorScheme.enablePressedColor_TriggerButton;
			cb.highlightedColor = ColorScheme.enableHighlightedColor_TriggerButton;
			cb.disabledColor = ColorScheme.disabledColor_TriggerButton;
			button.colors = cb;
		}


		object[] tabButtons = Tabs_Controller.FindSceneObjectsOfType(typeof (Tabs_Controller));

		foreach (object b in tabButtons)
		{
			Tabs_Controller d = (Tabs_Controller) b;
			Button button = d.GetComponent<Button> ();

			cb = button.colors;
			cb.normalColor = ColorScheme.normalColor_Tab;
			cb.pressedColor = ColorScheme.pressedColor_Tab;
			cb.highlightedColor = ColorScheme.highlightedColor_Tab;
			cb.disabledColor = ColorScheme.disabledColor_Tab;
			button.colors = cb;
		}


		//Text
		object[] texts = GameObject.FindObjectsOfTypeAll(typeof (Text));
		foreach (object b in texts)
		{
			Text d = (Text) b;
			d.color = enable_text;

		}

		//Menu Collapse
		object[] collapse = GameObject.FindObjectsOfTypeAll(typeof (MenuCollapse));
		foreach (object b in collapse)
		{
			MenuCollapse d = (MenuCollapse) b;
			Button button = d.GetComponent<Button> ();

			cb = button.colors;
			cb.normalColor = ColorScheme.menuCollapse;
			cb.pressedColor = ColorScheme.enablePressedColor_Button;
			cb.highlightedColor = ColorScheme.enableHighlightedColor_Button;
			cb.disabledColor = ColorScheme.disabledColor_Button;
			button.colors = cb;

		}


		//Images
		object[] images = GameObject.FindObjectsOfTypeAll(typeof (Image));
		foreach (object b in images)
		{
			Image img = (Image) b;
			Debug.Log (img);
			if(img.GetComponent<Button>() == null)
				img.color = imageColors;

			if (img.name.Equals ("Toolbar"))
				img.color = toolbarBackground;
			else if(img.name.Equals ("Sidebar"))
				img.color = windowBackground;
			else if(img.name.Equals ("LowerBar"))
				img.color = groupPanelBackground;
			else if(img.name.Equals ("LR_Panel"))
				img.color = groupPanelBackground;
			else if(img.name.Equals ("Control_Panel"))
				img.color = groupPanelBackground;
			else if(img.name.Equals ("Actions_Panel"))
				img.color = controlBoxListBackground;
			else if(img.name.Equals ("Muscles_Panel"))
				img.color = controlBoxListBackground;
			else if(img.name.Equals ("Action_Controller"))
				img.color = controlBoxBackground;
			else if(img.name.Equals ("Muscle_Controller"))
				img.color = controlBoxBackground;

		}

		/*
		GameObject panel;

		panel =  GameObject.Find("Toolbar");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = toolbarBackground;

		panel =  GameObject.Find("Sidebar");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = windowBackground;

		//panel =  GameObject.Find("LowerBar");
		panel = FindObjectWithName("LowerBar");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = groupPanelBackground;

		panel =  FindObjectWithName("LR_Panel");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = groupPanelBackground;

		panel =  FindObjectWithName("Control_Panel");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = groupPanelBackground;

		panel =  FindObjectWithName("Actions_Panel");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = controlBoxListBackground;

		panel =  FindObjectWithName("Muscles_Panel");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = controlBoxListBackground;

		panel =  FindObjectWithName("Action_Controller");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = controlBoxBackground;

		panel =  FindObjectWithName("Muscle_Controller");
		Debug.Log (panel);
		panel.GetComponent<Image> ().color = controlBoxBackground;
*/
		//Background
		Camera.main.backgroundColor = sceneBackground;



	}



	public static GameObject FindObjectWithName(string name)
	{
		object[] all = GameObject.FindObjectsOfTypeAll(typeof (GameObject));
		foreach(object t in all){
			GameObject r = (GameObject)t;
			if(r.name == name){
				return r;
			}
		}
		return null;
	}


	private static Color hexToColor(string hex)
	{
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		if(hex.Length == 8){
			a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r,g,b,a);
	}
}


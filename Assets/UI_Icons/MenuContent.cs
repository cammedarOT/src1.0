﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuContent : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler{

	bool inside = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void OnPointerEnter(PointerEventData eventData)
	{
		inside = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		inside = false;
	}

	public bool isInside()
	{
		return inside;
	}
}

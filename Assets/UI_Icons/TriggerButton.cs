﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class TriggerButton : MonoBehaviour , IPointerClickHandler {
	public bool enable = true;
	Button thisButton;
	ColorBlock cb;
	public Image img;
	public Sprite enableSprite;
	public Sprite disableSprite;
	// Use this for initialization
	void Start () {
		
		thisButton = this.GetComponent<Button> ();

		if(enable){
			enableButton ();
		}
		else
		{
			disableButton ();
		}


	}


	public void swapSprites()
	{
		Sprite tmp = enableSprite;
		enableSprite = disableSprite;
		disableSprite = tmp;
		Start ();
	}


	public void enableButton()
	{
		cb = thisButton.colors;
		cb.normalColor = ColorScheme.enableNormalColor_TriggerButton;
		cb.pressedColor = ColorScheme.enablePressedColor_TriggerButton;
		cb.highlightedColor = ColorScheme.enableHighlightedColor_TriggerButton;
		thisButton.colors = cb;

		enable = true;
		img.sprite = enableSprite;

	}

	public void disableButton()
	{
		cb = thisButton.colors;
		cb.normalColor = ColorScheme.disableNormalColor_TriggerButton;
		cb.pressedColor = ColorScheme.disablePressedColor_TriggerButton;
		cb.highlightedColor = ColorScheme.disableHighlightedColor_TriggerButton;
		thisButton.colors = cb;

		enable = false;
		img.sprite = disableSprite;


	}

	public void OnPointerClick(PointerEventData evd){

        IntermediateFunction();
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
	}


    //2ee el 5ra elly b3mlo da :( 
    public void IntermediateFunction()
    {
        if (!enable)
        {
            enableButton();
        }
        else
        {
            disableButton();
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using Cammedar.Analytics;

public class ToolbarButton : MonoBehaviour , IPointerClickHandler {

	public bool isTriggable = false;
	public bool enable = true;
	Button thisButton;
	ColorBlock cb;
	public Image img;
	public Sprite enableSprite;
	public Sprite disableSprite;

	// Use this for initialization
	void Start () {
		thisButton = this.GetComponent<Button> ();
		cb = thisButton.colors;
		cb.normalColor = ColorScheme.enableNormalColor_Button;
		cb.pressedColor = ColorScheme.enablePressedColor_Button;
		cb.highlightedColor = ColorScheme.enableHighlightedColor_Button;
		cb.disabledColor = ColorScheme.disabledColor_Button;
		thisButton.colors = cb;

		if (enable) {
			enableButton ();
		} else {
			disableButton ();
		} 

		
	}

	public void swapSprites()
	{
		Sprite tmp = enableSprite;
		enableSprite = disableSprite;
		disableSprite = tmp;
	}


	public void enableButton()
	{
		enable = true;
		thisButton.interactable = true;
		if (img)
		img.sprite = enableSprite;
	}

	public void disableButton()
	{
		
		enable = false;
		thisButton.interactable = false;
		img.sprite = disableSprite;

	}

	public void OnPointerClick(PointerEventData evd){
		if (isTriggable) {
			cb = thisButton.colors;
			if (!enable) {
				enable = true;
				img.sprite = enableSprite;
				cb.normalColor = ColorScheme.enableNormalColor_Button;
				cb.pressedColor = ColorScheme.enablePressedColor_Button;
				cb.highlightedColor = ColorScheme.enableHighlightedColor_Button;
            } else {
				enable = false;
				img.sprite = disableSprite;
				cb.normalColor = ColorScheme.disableNormalColor_Button;
				cb.pressedColor = ColorScheme.disablePressedColor_Button;
				cb.highlightedColor = ColorScheme.disableHighlightedColor_Button;
            } 
			thisButton.colors = cb;
		}

		EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
	}

	/// <summary>
	/// toolbar buttons functionalities
	/// </summary>
	public void _Toolbar_Functional_run(){
		//remember to add the onclick in the GUI
		GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<setAllFunctionalityFunc>().setAllMuscles(!enable);
		//hna mfrod atfi el open boxes/windows
		GameObject openWindows = GameObject.FindGameObjectWithTag("hoverWindows");
		int windowsCount = openWindows.transform.childCount;
		while ( windowsCount > 0){
			windowsCount--;
			Destroy(openWindows.transform.GetChild(0).gameObject);
		}
	}

	public void _Toolbar_Reset_run(){
        Utils.Reset();
	}

	public void _Toolbar_Visible_run(){
		if (!enable)
			Utils.setMusclesVisibility (true);
		else
			Utils.setMusclesVisibility (false);
	}

}

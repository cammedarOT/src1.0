﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DarkTheme : MonoBehaviour , IPointerClickHandler {

	public void OnPointerClick(PointerEventData evd){
		Debug.Log ("Dark Theme Clicked");
		if(ColorScheme.isDark)
			ColorScheme.brightTheme ();
		else
			ColorScheme.darkTheme ();
		
		ColorScheme.updateSceneColors ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectElement : MonoBehaviour , IPointerClickHandler {
	public Image selectionBar;
	bool selected = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnPointerClick(PointerEventData evd){
		selected = !selected;
		selectionBar.gameObject.SetActive (selected);
	}
}

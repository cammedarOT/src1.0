﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingWindow : MonoBehaviour , IDragHandler {

	public GameObject window;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnDrag(PointerEventData eventData)
	{
        if (window)
            window.transform.position = Input.mousePosition;
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
public class SignUpFunc : MonoBehaviour , IPointerClickHandler {
	[ObfuscateLiterals]
	public void OnPointerClick(PointerEventData evd)
	{
		Screen.SetResolution (debuglogloop.width, debuglogloop.heigth, debuglogloop.full);
		SceneManager.LoadScene ("SignUp", LoadSceneMode.Single);
	}

}

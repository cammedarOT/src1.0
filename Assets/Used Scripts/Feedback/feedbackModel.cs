﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class feedbackModel : MonoBehaviour {

	/*
	 * feedback Panel abstraction class: 
	 */
	private float posClosed;
	private float posOpened;
	private float targetX;
	private Vector3 pos;
	float X;

	public bool open = false;
	private int sendCount = 0;
	private bool sendCountExceeded = false;
	public Networking_Manager net; //to send the email
	public InputField writtenText;
	public Button sendButton;
	public GameObject feedbackmessagebox;


	void Start(){
		posClosed = this.transform.position.x; 
		posOpened = 0;
		X = posClosed;
		//sendButton.interactable = false;
	}

	//TODO this should be converted to a coroutine
	void Update () {
		if (!open) targetX = posClosed;
		if (open) targetX = posOpened;
		X = Mathf.Lerp (X, targetX, 0.15f);
		pos.x = X; pos.y = this.transform.position.y; pos.z = this.transform.position.z;
		this.transform.position = pos;
	}
	/*
	 * Behaviour: send email using the net manager only @sendCount less than ? 
	 * and call increase sendCount
	 */ 
	public void sendFeedback(){
		net.sendFeedback (writtenText.text);
		writtenText.text = string.Empty;
		incrementSendCount ();
		showThankYouMessage ();
		togglePanelViewMode ();
	}

	/*
	 * Behaviour: increase send count by one , and if sendCount reaches (x) it disables the send button and shows a message
	 * and sets @sendCountExceeded to true
	 */ 
	private void incrementSendCount(){
		sendCount++;
	}

	private void showThankYouMessage(){
		feedbackmessagebox.SetActive (true);
	}

	/*
	 * Behaviour: closes the panel (hide it) if it was open, and toggles the @open boolean 
	 * or opens the panel (show it) if it was closed, and toggles the @open boolean
	 */ 
	public void togglePanelViewMode(){
		open = !open;
	}	

	/*
	 * this function gets called by the controller on TextInputValueCHanged event
	 * Behaviour: it checks if the text has characters
	 * if lenght == 0 -> disable send button
	 * if not , check if @sendCountExceeded was false to enable the send button
	 */ 
	public void checkIfhasText(){
		if (writtenText.text.Length == 0)
			sendButton.interactable = false;
		else 
			sendButton.interactable = true;
			
	}
}

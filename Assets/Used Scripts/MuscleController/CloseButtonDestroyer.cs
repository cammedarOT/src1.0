﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseButtonDestroyer : MonoBehaviour {

    public void Handle()
    {
        this.transform.parent.GetComponent<ControllerAnalyticsRemover>().Remove();
        GameObject.Destroy(this.transform.parent.gameObject);
    }
}

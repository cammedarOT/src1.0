﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;

//Called when destroying Action or Muscle Controllers
//Used to remove these controllers from the analytics
public class ControllerAnalyticsRemover : MonoBehaviour
{

    private List<GameObject> analyzableObjects;

    public void Start()
    {
        analyzableObjects = new List<GameObject>();
        ChildTraverser(this.gameObject);
    }
    public void Update()
    {
        analyzableObjects.Clear();
        ChildTraverser(this.gameObject);
    }
    private void ChildTraverser(GameObject gameObject)
    {
        if (gameObject.GetComponent<AnalyzableUnityObject>() != null && gameObject.GetComponent<AnalyzableUnityObject>().Converter != null)
        {
            analyzableObjects.Add(gameObject);
        }
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if (IsValidGameObject(gameObject.transform.GetChild(i).gameObject))
            {
                ChildTraverser(gameObject.transform.GetChild(i).gameObject);
            }

        }
    }
    private bool IsValidGameObject(GameObject gameObject)
    {
        return gameObject.transform.childCount > 0 || gameObject.GetComponent<AnalyzableUnityObject>() != null;
    }

    public void Remove()
    {
	        foreach (var gameObject in analyzableObjects)
        {
            gameObject.GetComponent<AnalyzableUnityObject>().UnRegisterObject();
        }
    }
}

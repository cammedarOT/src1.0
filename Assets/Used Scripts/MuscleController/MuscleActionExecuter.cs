﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;
public class MuscleActionExecuter : MonoBehaviour {
    //This script is used to execute a certain action
    public actionObjectScript action;
    public muscleControllersData_Managed muscle;
    private float contractionValue = 0.0f;
    private bool playButtonEnabled = false; 
	// Use this for initialization
	void Start () {
		
	}
	[ObfuscateLiterals]
    // Update is called once per frame
    void Update()
    {
        if (playButtonEnabled)
        {
			if (!Utils.ExecuteAction(action, contractionValue += 0.01f) || contractionValue > 1.0f)
            {
                contractionValue = 0.0f;
                playButtonEnabled = false;
               
            }
        }
    }
	[ObfuscateLiterals]
    public void playButtonClickListener()
    {
        Utils.ResetMotion();
        playButtonEnabled = !playButtonEnabled;
        Utils.SetAllMuscles(false);
		muscle.setDysfunctional(false);
        GameObject sideBar = GameObject.FindGameObjectWithTag("SideBar");
        sideBar.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<populateActionsTab> ().calculateDisfunc ();
        sideBar.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<populateActionsTab> ().calculateActionAngles ();
    }

    public void Destroy()
    {
        Utils.ExecuteAction(action, 0.0f);
    }
}

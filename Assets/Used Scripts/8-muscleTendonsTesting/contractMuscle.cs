﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Disclaimer:
 * these tendon muscle scripts are not a final version
 * they are just for testing so they don't have a heirerchy 
 */

public class contractMuscle : MonoBehaviour {

	public Transform origin;
	public Transform tendon1insertion;
	public Transform tendon2insertion;
	public Transform insertion;

	public Transform center;
	float radius;
	float distance;
	Vector3 offset;
	Vector3 vector;
	void Start(){
		radius = Mathf.Abs (Vector3.Distance (insertion.position, center.position));
	}

	void Update () {
		/*these movements must be in order from the origin to insertion
		 * and need to be referenced from the start pos with a delta (increasing step) to be able to return to muscles
 		   starting position
 		 * and need to have different contraction rates (graph) per tendon like openSim: watch openSim actions 
 		 * and last point needs to rotate in a circle arround the bone center (it can't move any closer than that)
 		 * -- PROBLEM! having the look at code is not a perfect solution as it will lead to problems when having more than one muscles
 		 * -- acting on the same bone (look at 2 objects?) so force vectors must be calculated (ask hassan)
		*/
		tendon1insertion.position = Vector3.MoveTowards (tendon1insertion.position, origin.position, 0.0025f);
		tendon2insertion.position = Vector3.MoveTowards (tendon2insertion.position, tendon1insertion.position, 0.0035f);
		insertion.position = Vector3.MoveTowards (insertion.position, tendon2insertion.position, 0.04f);


		//---------- LimitDistance part to make last point move in a circle arround bone center
		offset = insertion.position - center.position;
		Vector3 clampedPos = center.position + Vector3.ClampMagnitude(offset, radius); //da 3ashan law be3ed
		distance = Mathf.Abs (Vector3.Distance (clampedPos, center.position));
		if (distance < radius) {
			vector = clampedPos - center.position; vector = vector.normalized; vector *= radius;
			clampedPos = center.position + vector;
		} insertion.position = clampedPos;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* Disclaimer:
 * these tendon muscle scripts are not a final version
 * they are just for testing so they don't have a heirerchy 
 */
[ExecuteInEditMode]
public class drawTendonVectors : MonoBehaviour {

	public Transform origin;
	public Transform tendon1insertion;
	public Transform tendon2insertion;
	public Transform insertion;

	void OnDrawGizmos () {
		Gizmos.color = Color.red;
		Gizmos.DrawLine(origin.position, tendon1insertion.position);
		Gizmos.DrawLine(tendon1insertion.position, tendon2insertion.position);
		Gizmos.DrawLine(tendon2insertion.position, insertion.position);
		Gizmos.color = Color.green;
		DrawArrow.ForGizmo (insertion.position, tendon2insertion.position - insertion.position, 1.0f);
	}
}

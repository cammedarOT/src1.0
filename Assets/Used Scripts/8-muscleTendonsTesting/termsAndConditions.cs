﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class termsAndConditions : MonoBehaviour {

	public Button registerButton;
	public Button registerButton2;
	public Toggle termsAndConditionsToggle;

	public void toggleValueChanged(){
		registerButton.interactable = termsAndConditionsToggle.isOn;
		registerButton2.interactable = termsAndConditionsToggle.isOn;
	}
}

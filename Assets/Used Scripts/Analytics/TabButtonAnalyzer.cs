﻿using System.Collections;
using System.Collections.Generic;
using Cammedar.Analytics;

public class TabButtonAnalyzer : AnalyzableButton {
    public string tabName;
    public override string ID
    {
        get
        {
            return tabName;
        }

        set
        {
            tabName = value;
        }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<Tabs_Controller>().OnPointerClick(null);
    }
}

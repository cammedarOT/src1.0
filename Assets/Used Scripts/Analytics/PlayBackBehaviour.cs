﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Cammedar.Analytics
{
    public class PlayBackBehaviour : MonoBehaviour
    {

        private Analyzer analyzer;
        private bool playBackInProgress;
        private int firstFrame;
        private Pair<EventData, AnalyzableUnityObject> currentEventNode;
        // Use this for initialization
        void Start()
        {
            analyzer = Analyzer.Instance;
            playBackInProgress = false;
        }

        // Update is called once per frame
        void Update()
        {
            if(playBackInProgress)
            {
                if(Time.frameCount - firstFrame >= currentEventNode.First.FrameCount)
                {
                    currentEventNode.Second.Simulate();
                    GetNextEvent();
                }
            }
        }

        public void StartPlayBack()
        {
            firstFrame = Time.frameCount;
            playBackInProgress = true;
            GetNextEvent();
        }
        private void GetNextEvent()
        {
            currentEventNode = analyzer.GetNextEvent();
            if (currentEventNode == null)
            {
                playBackInProgress = false;
            }
        }
    }
}
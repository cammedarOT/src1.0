﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class ActionPanelPlayButtonAnalyzer : AnalyzableButton {
    private string id;
    public override string ID
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }
    public override void Start()
    {
        id = this.transform.parent.GetComponentInParent<ActionView>().actionName.text + "Play Button";
        base.Start();
    }
    public override void Simulate()
    {
        base.Simulate();
        this.transform.parent.GetComponentInParent<ActionView>().ActionControllerMenuPlayButtonListener();
    }
}

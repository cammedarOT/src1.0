﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class MusclePanelOriginAnalyzer : AnalyzableButton
{
    public override string ID
    {
        get
        {
            return "Muscle Panel Origin";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Start()
    {
        base.Start();
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<OriginInsertionButton>().OnPointerClick(null);

    }
}

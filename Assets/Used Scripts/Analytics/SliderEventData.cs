﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cammedar.Analytics
{
    public class SliderEventData:EventData
    {

        private float sliderValue;
        public float SliderValue { get { return sliderValue; } set { sliderValue = value; } }

    }
}

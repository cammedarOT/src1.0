﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cammedar.Analytics
{
    public class AnalyticsConverter
    {
        /*
         *  This class is resposnible for reproducing the events list in the analyzer
         *  Please note that the analytics system consists of two parts, one for registering and the other for reproducing
         * 
         */
        private Dictionary<String, AnalyzableUnityObject> objects;

        private AnalyticsConverter()
        {
            objects = new Dictionary<string, AnalyzableUnityObject>();
        }

        private static AnalyticsConverter instance;
        public static AnalyticsConverter Instance { get { return instance ?? (instance = new AnalyticsConverter()); } }

        public void RegisterNewObject(AnalyzableUnityObject node)
        {
            if (objects.ContainsKey(node.ID))
            {
                throw new InvalidOperationException();
            }
            else
            {
                objects.Add(node.ID, node);
            }
        }
        public void RemoveObject(AnalyzableUnityObject node)
        {
            if (!objects.ContainsKey(node.ID))
            {
                throw new InvalidOperationException();
            }
            else
            {
                objects.Remove(node.ID);
            }
        }
    }
}

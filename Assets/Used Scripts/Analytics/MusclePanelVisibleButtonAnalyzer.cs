﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class MusclePanelVisibleButtonAnalyzer : AnalyzableButton {

    public override string ID
    {
        get
        {
            return "Muscle Panel Visible Button";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<boxVisibilityTogglePublisher>().OnPointerClick(null);
        this.GetComponent<TriggerButton>().OnPointerClick(null);
    }
}

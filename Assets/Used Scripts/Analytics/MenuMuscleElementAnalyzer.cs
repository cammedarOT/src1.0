﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;   
using Cammedar.Analytics;
using UnityEngine.EventSystems;

public class MenuMuscleElementAnalyzer : AnalyzableUnityObject, IPointerClickHandler
{
    private string id;
    public override string ID
    {
        get
        {
            return id;
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        RegisterEvent();
    }

    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<menuItemClickPublisher>().OnPointerClick(null);
    }
    public override void Start()
    {
        id = GetComponent<Text>().text;
        base.Start();
        
    }
}

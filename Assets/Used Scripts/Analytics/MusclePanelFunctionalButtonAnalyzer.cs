﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class MusclePanelFunctionalButtonAnalyzer : AnalyzableButton
{
    public override string ID
    {
        get
        {
            return "Muscle Panel Functional Button";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<boxDysfunctionTogglePublisher>().OnPointerClick(null);
        this.GetComponent<TriggerButton>().OnPointerClick(null);
    }
}

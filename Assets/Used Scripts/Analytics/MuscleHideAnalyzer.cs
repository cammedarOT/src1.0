﻿using System.Collections;
using System.Collections.Generic;
using Cammedar.Analytics;
using UnityEngine.UI;

public class MuscleHideAnalyzer : AnalyzableButton
{
    public override string ID
    {
        get
        {
            return "Muscle Hide Button";
        }
        set { ID = value; }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<ToolbarButton>()._Toolbar_Visible_run();
        this.GetComponent<ToolbarButton>().OnPointerClick(null);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class ActionControllerSliderAnalyzer : AnalyzableSlider {
    private string id;
    public override string ID
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }
    public override void Start()
    {
        id = "Action Controller Slider :" + this.transform.parent.GetComponentInParent<ActionView>().actionName.text;
        base.Start();
    }
}

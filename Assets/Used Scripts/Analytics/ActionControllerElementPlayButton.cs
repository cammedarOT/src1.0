﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
using UnityEngine.UI;

public class ActionControllerElementPlayButton : AnalyzableButton {
    private string id;
    public override string ID
    {
        get
        {
            return id;
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Start()
    {
        id = "Action Controller Element play Button: " + this.GetComponentInParent<ActionControllerElementView>().elementName.text;
        base.Start();
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponentInParent<ActionControllerElementView>().ActionElementPlayButtonClickListener();
    }
}

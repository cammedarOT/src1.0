﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Cammedar.Analytics;

public class MenuActionElementAnalyzer : AnalyzableUnityObject , IPointerClickHandler {
    private string id;
    public override string ID
    {
        get
        {
            return id;
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }

    public override void Start()
    {
        id = GetComponentInParent<actionObjectScript>().actionName;
        base.Start();
         
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<actionsItemClickPublisher>().OnPointerClick(null);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        RegisterEvent();
    }
}

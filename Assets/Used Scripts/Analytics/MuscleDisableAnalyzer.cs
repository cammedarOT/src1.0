﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Cammedar.Analytics;
public class MuscleDisableAnalyzer : AnalyzableButton
{
    public override string ID
    {
        get
        {
            return "Muscles Disable Button";
        }

        set
        {
            ID = value;
        }
    }

    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<ToolbarButton>()._Toolbar_Functional_run();
        this.GetComponent<ToolbarButton>().OnPointerClick(null);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class ActionTabSliderAnalyzer : AnalyzableSlider {
    public override string ID
    {
        get
        {
            return "Action Tab Slider";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }

}

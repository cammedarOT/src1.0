﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class SearchAnalyzer : AnalyzableButton
{

    public override string ID
    {
        get
        {
            return "Search Button";
        }
        set { ID = value; }
    }


    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<ToolbarButton>().OnPointerClick(null);
    }

}

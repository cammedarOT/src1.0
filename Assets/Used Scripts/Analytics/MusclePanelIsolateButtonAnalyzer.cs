﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class MusclePanelIsolateButtonAnalyzer : AnalyzableButton {
    public override string ID
    {
        get
        {
            return "Muscle Panel Isolate Button";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<boxIsolateTogglePublisher>().OnPointerClick(null);
        this.GetComponent<TriggerButton>().OnPointerClick(null);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
namespace Cammedar.Analytics
{
    public abstract class AnalyzableButton : AnalyzableUnityObject
    {
        public override void Start()
        {
            base.Start();
            this.GetComponent<Button>().onClick.AddListener(RegisterEvent);
        }

        public void OnDestroy()
        {
            this.GetComponent<Button>().onClick.RemoveListener(RegisterEvent);
            
        }
    }
}

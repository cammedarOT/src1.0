﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;

public class MusclePanelCloseButtonAnalyzer : AnalyzableButton {
    private string id = "Muscle Panel Close Button";
    public override string ID
    {
        get
        {
            return id ;
        }

        set
        {
            id = value ;
        }
    }


    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<CloseButtonDestroyer>().Handle();
    }
}

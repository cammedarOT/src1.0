﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cammedar.Analytics
{
    public class EventData
    {
        private int frameCount;
        public int FrameCount { get { return frameCount; } set { frameCount = value; } }

        private string message;
        public string Message { get { return message; } set { message = value; } }

        private string dataBaseMessage;
        public string DataBaseMessage { get { return dataBaseMessage; } set { dataBaseMessage = value; } }

        public EventData (string ID)
        {
            message = "This is an event From the Object with ID = " + ID + "\n";
        }
        public EventData() { }
    }
}

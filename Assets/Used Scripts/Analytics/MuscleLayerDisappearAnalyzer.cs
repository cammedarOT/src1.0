﻿using Cammedar.Analytics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuscleLayerDisappearAnalyzer : AnalyzableButton
{
    public override string ID
    {
        get
        {
            return "Layer Down Button";
        }
        set { ID = value; }
    }

    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<LayerChooseButton>().OnLayerDecreaseButtonClick();
    }
}

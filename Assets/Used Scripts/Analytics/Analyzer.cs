﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Cammedar.Analytics
{
    public class Analyzer
    {
        private const string SCRIPTS_OBJECT_TAG = "ScriptManager";
        private List<Pair<EventData,AnalyzableUnityObject>> events;
        private int eventCounter;
        private Networking_Manager networkManager;
        private Analyzer()
        {
            events = new List<Pair<EventData, AnalyzableUnityObject>>();
            eventCounter = 0;
            networkManager = GameObject.FindGameObjectWithTag(SCRIPTS_OBJECT_TAG).GetComponent<Networking_Manager>();
        }
        private static Analyzer instance;
        public static Analyzer Instance { get
            {
                if (instance == null)
                    instance = new Analyzer();
                return instance;
            } }

        public void RegisterEvent(Pair<EventData, AnalyzableUnityObject> node)
        {
            events.Add(node);
            networkManager.addEvent(node.First.DataBaseMessage);

        }
        public Pair<EventData,AnalyzableUnityObject> GetNextEvent()
        {
            return eventCounter ==events.Count ? null : events.ElementAt(eventCounter++);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class ActionPanelCloseButtonAnalyzer : AnalyzableButton {
    public override string ID
    {
        get
        {
            return "Action Panel Close Button";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Simulate()
    {
        base.Simulate();
        GetComponent<CloseButtonDestroyer>().Handle();
    }
}

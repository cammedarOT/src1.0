﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Cammedar.Analytics
{
    public abstract class AnalyzableUnityObject : MonoBehaviour
    {
        //ID for debugging purposes
        public abstract string ID { get; set; }
        private EventData data;

        public EventData Data
        { get { return data; } set { data = value; } }

        protected Analyzer analyzer;
        protected AnalyticsConverter converter;
        public AnalyticsConverter Converter { get { return converter; } set { throw new System.InvalidOperationException(); } }
        public AnalyzableUnityObject()
        {
            data = new EventData(ID);
        }
        public virtual void PrepareEventData()
        {
            Data.FrameCount = Time.frameCount;
            Data.DataBaseMessage = "<<Frame Count: " + Data.FrameCount + ">" + "<ID: " + ID + ">>";
        }
        public virtual void RegisterEvent()
        {
            PrepareEventData();
            Debug.Log("Registering" + ID);
            analyzer.RegisterEvent(new Pair<EventData, AnalyzableUnityObject>(Data, this));
        }

        public virtual void Simulate()
        {
            Debug.Log("Simulating" + ID);
        }
        public virtual void Start()
        {
            analyzer = Analyzer.Instance;
            converter = AnalyticsConverter.Instance;
            converter.RegisterNewObject(this);
        }
        public virtual void UnRegisterObject()
        {
            converter.RemoveObject(this);
        }
    }
}

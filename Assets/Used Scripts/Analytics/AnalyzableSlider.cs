﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace Cammedar.Analytics
{
    //Must Be attached to a slider
    public abstract class AnalyzableSlider : AnalyzableUnityObject
    {
        private float sliderValue;
        public float SliderValue { set { sliderValue = value; } get { return sliderValue; } }
        public override void PrepareEventData()
        {
            Data = new SliderEventData();
            Data.FrameCount = Time.frameCount;
            ((SliderEventData)Data).SliderValue = this.GetComponent<Slider>().value;
            Data.DataBaseMessage = "<<Frame Count: " + Data.FrameCount + ">" + "<ID: " + ID + ">"+"<Slider Value : "+ this.GetComponent<Slider>().value + ">>";
        }

        public override void Simulate()
        {
            base.Simulate();
            this.GetComponent<Slider>().value = sliderValue;
        }
    }
}

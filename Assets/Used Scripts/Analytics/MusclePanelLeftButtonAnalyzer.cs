﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;

public class MusclePanelLeftButtonAnalyzer : AnalyzableButton {
    public string buttonName;
    public override string ID
    {
        get
        {
            return buttonName;
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }

    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<TriggerButton>().OnPointerClick(null);
    }

}

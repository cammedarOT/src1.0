﻿using System.Collections;
using System.Collections.Generic;
using Cammedar.Analytics;

public class MuscleLayerAppearAnalyzer : AnalyzableButton {

    public override string ID
    {
        get
        {
            return "Layer Up Button";
        }
        set { ID = value; }
    }

    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<LayerChooseButton>().OnLayerIncreaseButtonClick();
        this.GetComponent<ToolbarButton>().OnPointerClick(null);
    }
}

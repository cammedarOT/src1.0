﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Cammedar.Analytics;
using UnityEngine.EventSystems;
public class ActionControllerElementText : AnalyzableUnityObject,IPointerClickHandler {
    private string id;
    public override string ID
    {
        get
        {
            return id;
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Start()
    {
        id ="Action Controller Element: "+this.GetComponent<Text>().text;
        base.Start();
    }
    
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponentInParent<ActionControllerElementView>().OnPointerClick(null);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        RegisterEvent();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
public class MusclePanelInsertionAnalyzer : AnalyzableButton {

    public override string ID
    {
        get
        {
            return "Muscle Panel Insertion";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<OriginInsertionButton>().OnPointerClick(null);

    }
}

﻿using Cammedar.Analytics;
using UnityEngine.UI;

public class ResetAnalyzer : AnalyzableButton
{
    public override string ID
    {
        get
        {
            return "Reset Button";
        }
        set { ID = value; }
    }
    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<ToolbarButton>()._Toolbar_Reset_run();
        this.GetComponent<ToolbarButton>().OnPointerClick(null);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cammedar.Analytics;
using UnityEngine.UI;

public class MusclePanelActionsTabButtonAnalyzer : AnalyzableButton {
    public override string ID
    {
        get
        {
            return "Muscle Panel Actions Tab Button";
        }

        set
        {
            throw new System.InvalidOperationException();
        }
    }

    public override void Simulate()
    {
        base.Simulate();
        this.GetComponent<Button>().onClick.RemoveListener(RegisterEvent);
        this.GetComponent<Button>().onClick.Invoke();
        this.GetComponent<Button>().onClick.AddListener(RegisterEvent);
    }
}

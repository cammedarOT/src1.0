﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OriginInsertionButton : MonoBehaviour, IPointerClickHandler {
	public bool isOrigin;
	muscleControllersData_Managed muscle;

	// Use this for initialization
	OriginInsertionManager originInsertionManager;
	private const string SCRIPT_OBJECT_TAG = "ScriptManager";
	void Start () {
		originInsertionManager =  GameObject.FindGameObjectWithTag(SCRIPT_OBJECT_TAG).GetComponent<OriginInsertionManager>();
	}
		
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setMuscle(muscleControllersData_Managed m)
	{
		this.muscle = m;
	}

	public void OnPointerClick(PointerEventData evd){
		if(isOrigin)
			originInsertionManager.invertOrigin(muscle);
		else
			originInsertionManager.invertInsertion(muscle);
	}
    public void OnDestroy()
    {
        originInsertionManager.hideOrigin(muscle);
        originInsertionManager.hideInsertion(muscle);
    }
}

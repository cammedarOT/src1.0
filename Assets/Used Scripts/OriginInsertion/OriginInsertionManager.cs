﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginInsertionManager : MonoBehaviour {

	public Material muscleMaterial;
	public GameObject rig;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void invertOrigin(muscleControllersData_Managed muscle)
	{
		muscle.GetComponent<Renderer> ().material = muscleMaterial;
		GameObject origin = getOriginInsertionObject (muscle, true);
		origin.SetActive (true);
		origin.GetComponent<SkinnedMeshRenderer> ().enabled = !origin.GetComponent<SkinnedMeshRenderer> ().enabled;
	}

	public void invertInsertion(muscleControllersData_Managed muscle)
	{
		muscle.GetComponent<Renderer> ().material = muscleMaterial;
		//muscle.GetComponent<boxReferenceObject> ();
		GameObject insertion = getOriginInsertionObject (muscle, false);
		insertion.SetActive (true);
		insertion.GetComponent<SkinnedMeshRenderer> ().enabled = !insertion.GetComponent<SkinnedMeshRenderer> ().enabled;
	}

	public void showOrigin(muscleControllersData_Managed muscle)
	{
		muscle.GetComponent<Renderer> ().material = muscleMaterial;
		GameObject origin = getOriginInsertionObject (muscle, true);
        if (origin != null)
        {
            origin.SetActive(true);
            origin.GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
	}

	public void hideOrigin(muscleControllersData_Managed muscle)
	{
		GameObject origin = getOriginInsertionObject (muscle, true);
        if (origin != null)
        {
            origin.SetActive(false);
            origin.GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
	}

	public void showInsertion(muscleControllersData_Managed muscle)
	{
		muscle.GetComponent<Renderer> ().material = muscleMaterial;
		GameObject origin = getOriginInsertionObject (muscle, false);
        if (origin == null)
            return;
		origin.SetActive (true);
		origin.GetComponent<SkinnedMeshRenderer> ().enabled = true;
	}
	public void hideInsertion(muscleControllersData_Managed muscle)
	{
		GameObject origin = getOriginInsertionObject (muscle, false);
        if (origin == null)
            return;
		origin.SetActive (false);
		origin.GetComponent<SkinnedMeshRenderer> ().enabled = false;
	}

	GameObject getOriginInsertionObject(muscleControllersData_Managed muscle, bool origin)
	{
		string prefix = "i_";
		if (origin)
			prefix = "o_";
		string objectName = prefix + muscle.name;
        // A fix I don't understand, bu it works
        if (rig != null)
            return rig.gameObject.transform.Find(objectName).gameObject;
        return null;
	}
}

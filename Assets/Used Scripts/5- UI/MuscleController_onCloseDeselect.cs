﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;

public class MuscleController_onCloseDeselect : MonoBehaviour {
	private UI_manager Manager;
	private GameObject musclesTabContent;
	private GameObject reference;

	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
		musclesTabContent = GameObject.FindGameObjectWithTag ("MusclesTabContentPanel");
	}

	public void onCloseDeselctMuscle(){
		deselectthisObject(); //@reference
	}

	private void deselectthisObject(){

		reference = this.gameObject.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript>().reference;
		for (int i = 0; i < musclesTabContent.transform.childCount; i++) { 
			menuObjectScript m = musclesTabContent.transform.GetChild (i).GetComponent<menuObjectScript> ();
			if (m != null)
			if (m.reference != null)
			if (m.reference.gameObject == reference) {
				Manager.deSelectMenuObject (musclesTabContent.transform.GetChild (i).gameObject);
				break;
			}
		}
	}
}

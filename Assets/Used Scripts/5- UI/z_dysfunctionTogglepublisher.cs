﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class z_dysfunctionTogglepublisher : MonoBehaviour , IPointerClickHandler{
	bool first = true;
	public UI_manager Manager;


	// a el 3abat da, msh 3aref leeh onstart msh shghala we di eshtghalet
	// nafs el code gowa (mn gher first di tb3an)
	[ObfuscateLiterals]
	void Update(){
		if (first) {
			first = false;
			Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
			//Debug.Log (Manager.ToString ());
		}
	}
	public void OnPointerClick(PointerEventData evd){
		Manager.dysfunctionalToggleChanged (this.transform.parent.parent.GetComponent<menuObjectScript> ().reference, this.gameObject.GetComponent<Toggle> ().isOn);
	}
}

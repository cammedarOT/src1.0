﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class boxVisibilityTogglePublisher : MonoBehaviour  , IPointerClickHandler{

	public UI_manager Manager;
	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
	}

	public void OnPointerClick(PointerEventData evd){
		Manager.visibilityToggleChanged (this.transform.parent.parent.GetComponent<boxReferenceObject> ().reference
			.GetComponent<menuObjectScript>().reference
			, this.gameObject.GetComponent<TriggerButton> ().enable);
	}
}

﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
public class optionsMenuButtonsfunctions : MonoBehaviour {

	UI_manager Manager;
	public Transform cameraCenter;
	public newCameraRotation cameraScript;
	public CameraManager cameraManager;
	[ObfuscateLiterals]
	void Start(){
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
	}

	[ObfuscateLiterals]
	////FILE //////////////// 
	public void _save(){
		Debug.Log ("save");
	}
	[ObfuscateLiterals]
	public void _load(){
		Debug.Log ("load");
	}
	[ObfuscateLiterals]
	public void _screenshot(){
		Debug.Log ("screenshot");
	}
	[ObfuscateLiterals]
	public void _record(){
		Debug.Log ("record");
	}
	[ObfuscateLiterals]
	public void _exit(){
		Application.Quit ();
	}
	[ObfuscateLiterals]
	////EDIT ////////////////
	public void _undo(){
		Debug.Log ("undo");
	}
	[ObfuscateLiterals]
	public void _redo(){
		Debug.Log ("redo");
	}
	[ObfuscateLiterals]
	public void _reset(){
		Manager.deselectAll ();
		Manager.closeAllBoxes ();
		//Debug.Log ("reset");
		GameObject [] array;
		array = GameObject.FindGameObjectsWithTag ("object");
		foreach (GameObject go in array) {
			muscleControllersData_Managed mc = go.GetComponent<muscleControllersData_Managed> ();
			if (mc != null)
				mc.sliderValue = 0;
		}
		//di msh shghala lesa
		Manager.closeOpenMenu ();

	}
	[ObfuscateLiterals]
	public void _reset_camera(){
		cameraCenter.position = new Vector3 (-4.55f, 1.66f, -3.8f);
		cameraCenter.rotation = Quaternion.identity;

		//cameraCenter.GetChild (0).transform.position = new Vector3 (0.1199f, 0.2099f, -6.99f);
		cameraScript.reset();
	}

	////VIEW////////////////////////
	public void _singleView(){
		cameraManager.setSingleView ();
	}
	public void _doubleView(){
		cameraManager.setDoubleView ();
	}
	public void _quadView(){
		cameraManager.setQuadView ();
	}
	[ObfuscateLiterals]
	////PREFERENCES //////////////// 
	public void _globalLightClick(){
		Debug.Log ("Global light");
	}
	[ObfuscateLiterals]
	public void _cameralight(){
		Debug.Log ("camera light");
	}
	[ObfuscateLiterals]
	public void _qualitysettings(){
		Debug.Log ("quality settings");
	}
	[ObfuscateLiterals]
	public void _backgroundcolor(){
		Debug.Log ("background color");
	}

	////HELP //////////////// 
	public void _language(){
		Debug.Log ("language");
	}
	public void _buylicense(){
		Debug.Log ("buy license");
	}
	public void _contactus(){
		Debug.Log ("contact us");
	}
	public void _help(){
		Debug.Log ("help");
	}

	[ObfuscateLiterals]
	////PREFERENCES->QUALITY SETTINGS //////////////// 
	public void __removeglitches(){
		Debug.Log ("remove glitches");
	}
	//public void
}

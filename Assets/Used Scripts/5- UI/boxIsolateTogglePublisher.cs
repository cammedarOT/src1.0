﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class boxIsolateTogglePublisher : MonoBehaviour  , IPointerClickHandler{

	public UI_manager Manager;
	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
	}
	[ObfuscateLiterals]
	public void OnPointerClick(PointerEventData evd){
		Manager.IsolateToggleChanged (this.transform.parent.parent.GetComponent<boxReferenceObject> ().reference
			.GetComponent<menuObjectScript>().reference
			, this.gameObject.GetComponent<TriggerButton> ().enable);
	}
}

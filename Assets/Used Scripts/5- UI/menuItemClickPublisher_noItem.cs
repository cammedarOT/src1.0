﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
using Beebyte.Obfuscator;
public class menuItemClickPublisher_noItem : MonoBehaviour, IPointerClickHandler {

	bool first = true;
	public UI_manager Manager;

	//for a click on the right panel in an empty area


	[ObfuscateLiterals]
	void Update(){
		if (first) {
			first = false;
			Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
			//Debug.Log (Manager.ToString ());
		}
	}

	public void OnPointerClick(PointerEventData evd){
		Manager.deselectAll();
	}


}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class optionsMenuItemEventsHandler : MonoBehaviour,IPointerEnterHandler, IPointerExitHandler {

	bool first = true;
	public UI_manager Manager;

	Image img = null;
	// m3rfsh leeh l alwan 3abita kda , msh btshtghal gher bl 0. , laken el 255 msh 
	// bteshtghal 
	Color dark = new Color (0.854f,0.854f,0.854f,1);
	Color lihght = new Color (1, 1, 1, 1);
	[ObfuscateLiterals]
	void Update(){
		if (first) {
			first = false;
			Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
			//Debug.Log (Manager.ToString ());
		}

	}

	public void OnPointerEnter(PointerEventData evd){
		Manager.ignoreTheNormalclosingClick (true);
		if (img == null)
			img =  this.transform.GetComponent<Image>();
		img.color = dark;
	}
	public void OnPointerExit(PointerEventData evd){
		Manager.ignoreTheNormalclosingClick (false);
		img.color = lihght;
	}
}

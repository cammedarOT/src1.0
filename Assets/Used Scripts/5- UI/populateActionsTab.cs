﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Beebyte.Obfuscator;
//[System.Serializable] 
public class action{
	public string name;
	public int ID;
	public List <muscleControllersData_Managed> mc = new List <muscleControllersData_Managed>();
    public string primaryMotion;
    public List<muscleControllersData_Managed> primaryMuscles = new List<muscleControllersData_Managed>();
    public List<muscleControllersData_Managed> secondaryMuscles = new List<muscleControllersData_Managed>();
    public string secondaryMotion,secondaryMotionType;
    public List<muscleControllersData_Managed> secondaryMotionMuscles = new List<muscleControllersData_Managed>();
    public Transform one;
	public Transform two;
	public List <action> children = new List <action>();

	public List <CurveStruct> muscleMovers;

	public Vector3 centerCameraPos;

	public float distanceLelObject = 0; //da 3shan a7seb ldistance washof na2es ad a wa7rak l zoom
	// factor le odam l msafa el fadla (aw le wara :'(
	public float xRotation;
	public float yRotation;
};

//[System.Serializable] //[ExecuteInEditMode]
public class populateActionsTab : MonoBehaviour {

	public GameObject Rigg3D;
	public GameObject prefab_actionObject;
	public GameObject musclesMenuParent; //da l gameobject el parent 3l menuObject bta3et l muscles
	public GameObject curvesDataBase;

	public List <action> Actions = new List <action>();
	public bool once = true;
	[ObfuscateLiterals]
	void Start(){
		Actions.Clear ();
		clearTheList ();
		CreateTheActionsObjects ();
		populateTheActionsMenu ();
		calculateDisfunc (); //debug ybni ;+:D
		calculateActionAngles ();
		//TODO
		//getMusclesforeachAction ();
	}
	[ObfuscateLiterals]
	void Update () {
		if (once) {
			once = false;
			Actions.Clear ();
			clearTheList ();
			CreateTheActionsObjects ();
			populateTheActionsMenu ();
		}
	}
	[ObfuscateLiterals]
	//TODO bokra ha3ml 7tet el camera views lel actions el awel , b3d keda lel muscles fl heriarchy kda
	private void CreateTheActionsObjects(){

		//////////////////////////////////// PARENT
		action Head_and_neck = new action();
		Head_and_neck.name = "Head and neck";
		Actions.Add (Head_and_neck);
		//////////
		action mandible_elevation = new action();
		mandible_elevation.name = "mandible elevation";
		Actions.Add (mandible_elevation);
		action mandible_depression = new action();
		mandible_depression.name = "mandible depression";
		Actions.Add (mandible_depression);
		action mandible_protraction = new action();
		mandible_protraction.name = "mandible protraction";
		Actions.Add (mandible_protraction);
		action mandible_retraction = new action();
		mandible_retraction.name = "mandible retraction";
		Actions.Add (mandible_retraction);
		action neck_flexion = new action();
		neck_flexion.name = "neck flexion";
		Actions.Add (neck_flexion);
		action neck_extension = new action();
		neck_extension.name = "neck extension";
		Actions.Add (neck_extension);
		action neck_lateral_flexion = new action();
		neck_lateral_flexion.name = "neck lateral flexion";
		Actions.Add (neck_lateral_flexion);
		action head_rotation_ipsilateral = new action();
		head_rotation_ipsilateral.name = "head rotation ipsilateral";
		Actions.Add (head_rotation_ipsilateral);
		action head_rotation_contralateral = new action();
		head_rotation_contralateral.name = "head rotation contralateral";
		Actions.Add (head_rotation_contralateral);
		Head_and_neck.children.Add (mandible_elevation);
		Head_and_neck.children.Add (mandible_depression);
		Head_and_neck.children.Add (mandible_protraction);
		Head_and_neck.children.Add (mandible_retraction);
		Head_and_neck.children.Add (neck_flexion);
		Head_and_neck.children.Add (neck_extension);
		Head_and_neck.children.Add (neck_lateral_flexion);
		Head_and_neck.children.Add (head_rotation_ipsilateral);
		Head_and_neck.children.Add (head_rotation_contralateral);
		////////////// END PARENT

		//////////////////////////////////// PARENT
		action Shoulder = new action();
		Shoulder.name = "Shoulder";
		Actions.Add (Shoulder);
		////////
		action shoulder_flexion = new action();
		shoulder_flexion.name = "shoulder flexion";
		shoulder_flexion.one = GameObject.Find ("humerus_L").transform;
		shoulder_flexion.mc.Add(getMuscle("Deltoid__anterior_head","L"));
		shoulder_flexion.mc.Add(getMuscle("Pectoralis_major","L"));
		shoulder_flexion.mc.Add(getMuscle("Coracobrachialis","L"));
		shoulder_flexion.mc.Add(getMuscle("Biceps_brachii__long_head","L"));
        shoulder_flexion.primaryMotion = "Glenohumeral Joint";
        shoulder_flexion.primaryMuscles.Add(getMuscle("Deltoid__anterior_head", "L"));
        shoulder_flexion.secondaryMuscles.Add(getMuscle("Pectoralis_major", "L"));
        shoulder_flexion.secondaryMuscles.Add(getMuscle("Coracobrachialis", "L"));
		shoulder_flexion.secondaryMuscles.Add(getMuscle("Biceps_brachii__long_head", "L"));
        shoulder_flexion.secondaryMotion = "Scapulothoracic Motion";
        shoulder_flexion.secondaryMotionType = "Upward Rotation";
        shoulder_flexion.secondaryMotionMuscles.Add(getMuscle("Serratus_anterior", "L"));
        shoulder_flexion.secondaryMotionMuscles.Add(getMuscle("Trapezius_upper", "L"));
        shoulder_flexion.secondaryMotionMuscles.Add(getMuscle("Trapezius_middle", "L"));
        shoulder_flexion.secondaryMotionMuscles.Add(getMuscle("Trapezius_lower", "L"));
        shoulder_flexion.xRotation = 5.0f;
		shoulder_flexion.yRotation = 93.75f;
		shoulder_flexion.distanceLelObject = 6.2f;
		shoulder_flexion.centerCameraPos = new Vector3 (-6.3f, 2.7f, -3.9f);
        shoulder_flexion.muscleMovers = getCurveStructOfAction (shoulder_flexion.name);
        Actions.Add (shoulder_flexion);

		action shoulder_extension = new action();
		shoulder_extension.name = "shoulder extension";
		shoulder_extension.one = GameObject.Find ("humerus_L").transform;
		shoulder_extension.mc.Add(getMuscle("Latissimus_dorsi","L"));
		shoulder_extension.mc.Add(getMuscle("Teres_major","L")); 
		shoulder_extension.mc.Add(getMuscle("Deltoid__posterior_head","L"));
		shoulder_extension.mc.Add(getMuscle("Pectoralis_major","L"));
		shoulder_extension.mc.Add(getMuscle("Triceps__lateral_head","L"));
        shoulder_extension.primaryMotion = "Glenohumeral Joint";
        shoulder_extension.primaryMuscles.Add(getMuscle("Latissimus_dorsi", "L"));
        shoulder_extension.primaryMuscles.Add(getMuscle("Teres_major", "L"));
        shoulder_extension.primaryMuscles.Add(getMuscle("", "L"));
        shoulder_extension.secondaryMuscles.Add(getMuscle("Pectoralis_major", "L"));
        shoulder_extension.secondaryMuscles.Add(getMuscle("Triceps__lateral_head", "L"));
        shoulder_extension.secondaryMotion = "Scapulothoracic Motion";
        shoulder_extension.secondaryMotionType = "Downward Rotation";
        shoulder_extension.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        shoulder_extension.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        shoulder_extension.secondaryMotionMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        shoulder_extension.secondaryMotionMuscles.Add(getMuscle("Levator_scapulae", "L"));
        shoulder_extension.xRotation = 8.75f;
		shoulder_extension.yRotation = -35f;
		shoulder_extension.distanceLelObject = 6.9f;
		shoulder_extension.centerCameraPos = new Vector3 (-6.0f, 2.0f, -4.1f);
		Actions.Add (shoulder_extension);

		action shoulder_horizontal_abduction = new action();
		shoulder_horizontal_abduction.name = "shoulder horizontal abduction";
		shoulder_horizontal_abduction.one = GameObject.Find ("humerus_L").transform; //was shoulder_L
		shoulder_horizontal_abduction.mc.Add(getMuscle("Deltoid__posterior_head","L"));
		shoulder_horizontal_abduction.mc.Add(getMuscle("Teres_minor","L"));
		shoulder_horizontal_abduction.mc.Add(getMuscle("Infraspinatus","L"));
        shoulder_horizontal_abduction.primaryMotion = "Glenohumeral Joint";
        shoulder_horizontal_abduction.primaryMuscles.Add(getMuscle("Deltoid__posterior_head", "L"));
        shoulder_horizontal_abduction.secondaryMuscles.Add(getMuscle("Teres_minor", "L"));
        shoulder_horizontal_abduction.secondaryMuscles.Add(getMuscle("Infraspinatus", "L"));
        shoulder_horizontal_abduction.secondaryMotion = "Scapulothoracic Motion";
        shoulder_horizontal_abduction.secondaryMotionType = "Retraction";
        shoulder_horizontal_abduction.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        shoulder_horizontal_abduction.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        shoulder_horizontal_abduction.secondaryMotionMuscles.Add(getMuscle("Trapezius_middle", "L"));
        shoulder_horizontal_abduction.xRotation = 5f;
		shoulder_horizontal_abduction.yRotation = 15f;
		shoulder_horizontal_abduction.distanceLelObject = 4.4f;
		shoulder_horizontal_abduction.centerCameraPos = new Vector3 (-6.11f, 2.1f, -3.8f);
		Actions.Add (shoulder_horizontal_abduction);

		action shoulder_horizontal_adduction = new action();
		shoulder_horizontal_adduction.name = "shoulder horizontal adduction";
		shoulder_horizontal_adduction.one = GameObject.Find ("humerus_L").transform; //was shoulder_L
		shoulder_horizontal_adduction.mc.Add(getMuscle("Pectoralis_major","L"));
		shoulder_horizontal_adduction.mc.Add(getMuscle("Deltoid__anterior_head","L"));
		shoulder_horizontal_adduction.mc.Add(getMuscle("Coracobrachialis","L"));
        shoulder_horizontal_adduction.primaryMotion = "Glenohumeral Joint";
        shoulder_horizontal_adduction.primaryMuscles.Add(getMuscle("Pectoralis_major", "L"));
        shoulder_horizontal_adduction.secondaryMuscles.Add(getMuscle("Deltoid__anterior_head", "L"));
        shoulder_horizontal_adduction.secondaryMuscles.Add(getMuscle("Coracobrachialis", "L"));
        shoulder_horizontal_adduction.secondaryMotion = "Scapulothoracic Motion";
        shoulder_horizontal_adduction.secondaryMotionType = "Protraction";
        shoulder_horizontal_adduction.secondaryMotionMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        shoulder_horizontal_adduction.secondaryMotionMuscles.Add(getMuscle("Serratus_anterior", "L"));
        shoulder_horizontal_adduction.xRotation = -11.75f;
		shoulder_horizontal_adduction.yRotation = 16f;
		shoulder_horizontal_adduction.distanceLelObject = 4.7f;
		shoulder_horizontal_adduction.centerCameraPos = new Vector3 (-5.8f, 2.1f, -3.8f);
		Actions.Add (shoulder_horizontal_adduction);

		action shoulder_abduction = new action();
		shoulder_abduction.name = "shoulder abduction";
		shoulder_abduction.one = GameObject.Find ("humerus_L").transform;
		shoulder_abduction.mc.Add(getMuscle("Deltoid__middle_head","L")); 
		shoulder_abduction.mc.Add(getMuscle("Supraspinatus","L")); 
		shoulder_abduction.mc.Add(getMuscle("Deltoid__anterior_head","L"));
		shoulder_abduction.mc.Add(getMuscle("Deltoid__posterior_head","L"));
		shoulder_abduction.mc.Add(getMuscle("Biceps_brachii__long_head","L"));
        shoulder_abduction.primaryMotion = "Glenohumeral Joint";
        shoulder_abduction.primaryMuscles.Add(getMuscle("Deltoid__middle_head", "L"));
        shoulder_abduction.primaryMuscles.Add(getMuscle("Supraspinatus", "L"));
        shoulder_abduction.secondaryMuscles.Add(getMuscle("Deltoid__anterior_head", "L"));
        shoulder_abduction.secondaryMuscles.Add(getMuscle("Deltoid__posterior_head", "L"));
		shoulder_abduction.secondaryMuscles.Add(getMuscle("Biceps_brachii__long_head", "L"));
        shoulder_abduction.secondaryMotion = "Scapulothoracic Motion";
        shoulder_abduction.secondaryMotionType = "Upward Rotation";
        shoulder_abduction.secondaryMotionMuscles.Add(getMuscle("Serratus_anterior", "L"));
        shoulder_abduction.secondaryMotionMuscles.Add(getMuscle("Trapezius_upper", "L"));
        shoulder_abduction.secondaryMotionMuscles.Add(getMuscle("Trapezius_middle", "L"));
        shoulder_abduction.secondaryMotionMuscles.Add(getMuscle("Trapezius_lower", "L"));
        shoulder_abduction.xRotation = 8.75f;
		shoulder_abduction.yRotation = 29f;
		shoulder_abduction.distanceLelObject = 6.3f;
		shoulder_abduction.centerCameraPos = new Vector3 (-6.2f, 2.2f, -4.0f);
		Actions.Add (shoulder_abduction);

		action shoulder_adduction = new action();
		shoulder_adduction.name = "shoulder adduction";
		shoulder_adduction.one = GameObject.Find ("humerus_L").transform;
		shoulder_adduction.mc.Add(getMuscle("Pectoralis_major","L"));
		shoulder_adduction.mc.Add (getMuscle ("Latissimus_dorsi", "L"));
		shoulder_adduction.mc.Add(getMuscle("Teres_major","L"));
		shoulder_adduction.mc.Add(getMuscle("Coracobrachialis","L"));
        shoulder_adduction.primaryMotion = "Glenohumeral Joint";
        shoulder_adduction.primaryMuscles.Add(getMuscle("Pectoralis_major", "L"));
        shoulder_adduction.primaryMuscles.Add(getMuscle("Latissimus_dorsi", "L"));
        shoulder_adduction.primaryMuscles.Add(getMuscle("Teres_major", "L"));
        shoulder_adduction.secondaryMuscles.Add(getMuscle("Coracobrachialis", "L"));
        shoulder_adduction.secondaryMotion = "Scapulothoracic Motion";
        shoulder_adduction.secondaryMotionType = "Downward Rotation";
        shoulder_adduction.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        shoulder_adduction.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        shoulder_adduction.secondaryMotionMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        shoulder_adduction.secondaryMotionMuscles.Add(getMuscle("Levator_scapulae", "L"));
        shoulder_adduction.xRotation = 9.5f;
		shoulder_adduction.yRotation = 29.5f;
		shoulder_adduction.distanceLelObject = 7f;
		shoulder_adduction.centerCameraPos = new Vector3 (-6f, 1.8f, -3.8f);
		Actions.Add (shoulder_adduction);

		action shoulder_internal_rotation = new action();
		shoulder_internal_rotation.name = "shoulder internal rotation";
		shoulder_internal_rotation.one = GameObject.Find ("humerus_L").transform;
		shoulder_internal_rotation.mc.Add(getMuscle("Subscapularis","L")); 
		shoulder_internal_rotation.mc.Add(getMuscle("Teres_major","L"));
		shoulder_internal_rotation.mc.Add(getMuscle("Pectoralis_major","L"));
		shoulder_internal_rotation.mc.Add(getMuscle("Latissimus_dorsi","L")); 
		shoulder_internal_rotation.mc.Add(getMuscle("Deltoid__anterior_head","L"));
        shoulder_internal_rotation.primaryMotion = "Glenohumeral Joint";
        shoulder_internal_rotation.primaryMuscles.Add(getMuscle("Subscapularis", "L"));
        shoulder_internal_rotation.secondaryMuscles.Add(getMuscle("Teres_major", "L"));
        shoulder_internal_rotation.secondaryMuscles.Add(getMuscle("Pectoralis_major", "L"));
        shoulder_internal_rotation.secondaryMuscles.Add(getMuscle("Latissimus_dorsi", "L"));
        shoulder_internal_rotation.secondaryMuscles.Add(getMuscle("Deltoid__anterior_head", "L"));
        shoulder_internal_rotation.secondaryMotion = "Scapulothoracic Motion";
        shoulder_internal_rotation.secondaryMotionType = "Protraction";
        shoulder_internal_rotation.secondaryMotionMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        shoulder_internal_rotation.secondaryMotionMuscles.Add(getMuscle("Serratus_anterior", "L"));
        shoulder_internal_rotation.xRotation = 15f;
		shoulder_internal_rotation.yRotation = 41f;
		shoulder_internal_rotation.distanceLelObject = 4.9f;
		shoulder_internal_rotation.centerCameraPos = new Vector3 (-6.1f, 1.3f, -3.9f);
        shoulder_internal_rotation.muscleMovers = getCurveStructOfAction (shoulder_internal_rotation.name);
        Actions.Add (shoulder_internal_rotation);

		action shoulder_external_rotation = new action();
		shoulder_external_rotation.name = "shoulder external rotation";
		shoulder_external_rotation.one = GameObject.Find ("humerus_L").transform;
		shoulder_external_rotation.mc.Add(getMuscle("Deltoid__posterior_head","L"));
		shoulder_external_rotation.mc.Add(getMuscle("Teres_minor","L")); 
		shoulder_external_rotation.mc.Add(getMuscle("Infraspinatus","L"));
        shoulder_external_rotation.primaryMotion = "Glenohumeral Joint";
        shoulder_external_rotation.secondaryMuscles.Add(getMuscle("Deltoid__posterior_head", "L"));
        shoulder_external_rotation.secondaryMuscles.Add(getMuscle("Teres_minor", "L"));
        shoulder_external_rotation.secondaryMuscles.Add(getMuscle("Infraspinatus", "L"));
        shoulder_external_rotation.secondaryMotion = "Scapulothoracic Motion";
        shoulder_external_rotation.secondaryMotionType = "Retraction";
        shoulder_external_rotation.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        shoulder_external_rotation.secondaryMotionMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        shoulder_external_rotation.secondaryMotionMuscles.Add(getMuscle("Trapezius_middle", "L"));
        shoulder_external_rotation.xRotation = 4.5f;
		shoulder_external_rotation.yRotation = -17.5f;
		shoulder_external_rotation.distanceLelObject = 4f;
		shoulder_external_rotation.centerCameraPos = new Vector3 (-5.8f, 2.3f, -3.9f);
		Actions.Add (shoulder_external_rotation);

		Shoulder.children.Add (shoulder_flexion);
		Shoulder.children.Add (shoulder_extension);
		Shoulder.children.Add (shoulder_horizontal_abduction);
		Shoulder.children.Add (shoulder_horizontal_adduction);
		Shoulder.children.Add (shoulder_abduction);
		Shoulder.children.Add (shoulder_adduction);
		Shoulder.children.Add (shoulder_internal_rotation);
		Shoulder.children.Add (shoulder_external_rotation);
		////////////// END PARENT

		//////////////////////////////////// PARENT
		action Upper_limbs = new action();
		Upper_limbs.name = "Upper limbs";
		Actions.Add (Upper_limbs);

		action elbow_flexion = new action();
		elbow_flexion.name = "elbow flexion";
		elbow_flexion.one = GameObject.Find ("Forearm_L").transform;
		elbow_flexion.mc.Add(getMuscle("Brachialis","L"));
		elbow_flexion.mc.Add(getMuscle("Biceps_brachii__long_head","L"));
		elbow_flexion.mc.Add(getMuscle("Brachioradialis","L"));
		elbow_flexion.mc.Add(getMuscle("Pronator_teres","L"));
        elbow_flexion.primaryMotion = "Elbow and Forearm";
        elbow_flexion.primaryMuscles.Add(getMuscle("Brachialis", "L"));
		elbow_flexion.primaryMuscles.Add(getMuscle("Biceps_brachii__long_head", "L"));
        elbow_flexion.secondaryMuscles.Add(getMuscle("Brachioradialis", "L"));
        elbow_flexion.secondaryMuscles.Add(getMuscle("Pronator_teres", "L"));


        elbow_flexion.xRotation = 13.5f;
		elbow_flexion.yRotation = 128f;
		elbow_flexion.distanceLelObject = 7f;
		elbow_flexion.centerCameraPos = new Vector3 (-7f, 0.5f, -3.3f);
		Actions.Add (elbow_flexion);

		action elbow_extension = new action();
		elbow_extension.name = "elbow extension";
		elbow_extension.one = GameObject.Find ("Forearm_L").transform;
		elbow_extension.mc.Add(getMuscle("Triceps__lateral_head","L"));
		elbow_extension.mc.Add(getMuscle("Anconeus","L"));
		elbow_extension.mc.Add (getMuscle ("Extensor_carpi_radialis_brevis", "L"));
		elbow_extension.mc.Add (getMuscle ("Extensor_carpi_radialis_longus", "L"));
        elbow_extension.primaryMotion = "Elbow and Forearm";
        elbow_extension.primaryMuscles.Add(getMuscle("Triceps__lateral_head", "L"));
        elbow_extension.secondaryMuscles.Add(getMuscle("Anconeus", "L"));
        elbow_extension.secondaryMuscles.Add(getMuscle("Extensor_carpi_radialis_brevis", "L"));
        elbow_extension.secondaryMuscles.Add(getMuscle("Extensor_carpi_radialis_longus", "L"));
        elbow_extension.xRotation = 21f;
		elbow_extension.yRotation = 53.5f;
		elbow_extension.distanceLelObject = 5.8f;
		elbow_extension.centerCameraPos = new Vector3 (-6.74f, 0.69f, -3.6f);
		Actions.Add (elbow_extension);
            
		action elbow_pronation = new action();
		elbow_pronation.name = "elbow pronation";
		elbow_pronation.one = GameObject.Find ("Forearm_L").transform;
		elbow_pronation.mc.Add(getMuscle("Pronator_teres","L"));
		elbow_pronation.mc.Add(getMuscle("Pronator_quadratus","L"));
		elbow_pronation.mc.Add(getMuscle ("Brachioradialis", "L"));
        elbow_pronation.primaryMotion = "Elbow and Forearm";
        elbow_pronation.primaryMuscles.Add(getMuscle("Pronator_teres", "L"));
        elbow_pronation.primaryMuscles.Add(getMuscle("Pronator_quadratus", "L"));
        elbow_pronation.secondaryMuscles.Add(getMuscle("Brachioradialis", "L"));
        elbow_pronation.xRotation = 12f;
		elbow_pronation.yRotation = 105f;
		elbow_pronation.distanceLelObject = 5.2f;
		elbow_pronation.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (elbow_pronation);

		action elbow_supination = new action();
		elbow_supination.name = "elbow supination";
		elbow_supination.one = GameObject.Find ("Forearm_L").transform;
		elbow_supination.mc.Add(getMuscle("Supinator","L"));
		elbow_supination.mc.Add(getMuscle("Biceps_brachii__long_head","L"));
		elbow_supination.mc.Add (getMuscle ("Brachioradialis", "L"));
        elbow_supination.primaryMotion = "Elbow and Forearm";
        elbow_supination.primaryMuscles.Add(getMuscle("Supinator", "L"));
		elbow_supination.primaryMuscles.Add(getMuscle("Biceps_brachii__long_head", "L"));
        elbow_supination.secondaryMuscles.Add(getMuscle("Brachioradialis", "L"));
        elbow_supination.xRotation = 12f;
		elbow_supination.yRotation = 105f;
		elbow_supination.distanceLelObject = 5.2f;
		elbow_supination.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (elbow_supination);

		//TODO a7ot ba2i amaken l cameras hena
		action wrist_extension = new action();
		wrist_extension.name = "wrist extension"; //here
		wrist_extension.one = GameObject.Find ("Hand_Controller_L").transform;
		wrist_extension.mc.Add(getMuscle("Extensor_carpi_radialis_longus","L"));
		wrist_extension.mc.Add(getMuscle("Extensor_carpi_radialis_brevis","L"));
		wrist_extension.mc.Add(getMuscle("Extensor_carpi_ulnaris","L"));
        wrist_extension.primaryMotion = "Wrist";
        wrist_extension.primaryMuscles.Add(getMuscle("Extensor_carpi_radialis_longus", "L"));
        wrist_extension.primaryMuscles.Add(getMuscle("Extensor_carpi_radialis_brevis", "L"));
        wrist_extension.primaryMuscles.Add(getMuscle("Extensor_carpi_ulnaris", "L"));
        wrist_extension.secondaryMuscles.Add(getMuscle("Extensor_digitorum", "L"));
        wrist_extension.secondaryMuscles.Add(getMuscle("Extensor_digiti_minimi", "L"));
        wrist_extension.secondaryMuscles.Add(getMuscle("Extensor_indicis", "L"));
        wrist_extension.secondaryMuscles.Add(getMuscle("Extensor_pollicis_longus", "L"));
        wrist_extension.xRotation = 12f; 
		wrist_extension.yRotation = 105f;
		wrist_extension.distanceLelObject = 5.2f;
		wrist_extension.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (wrist_extension);

		action wrist_extensionSM = new action();
		wrist_extensionSM.name = "wrist extension SM";
		wrist_extensionSM.one = GameObject.Find ("Hand_Controller_L").transform;
		wrist_extensionSM.mc.Add(getMuscle("Extensor_digitorum","L"));
		wrist_extensionSM.mc.Add(getMuscle("Extensor_digiti_minimi","L"));
		wrist_extensionSM.mc.Add(getMuscle("Extensor_indicis","L"));
		wrist_extensionSM.mc.Add(getMuscle("Extensor_pollicis_longus","L"));
        wrist_extensionSM.primaryMotion = "Wrist";
        wrist_extensionSM.primaryMuscles.Add(getMuscle("Extensor_carpi_radialis_longus", "L"));
        wrist_extensionSM.primaryMuscles.Add(getMuscle("Extensor_carpi_radialis_brevis", "L"));
        wrist_extensionSM.primaryMuscles.Add(getMuscle("Extensor_carpi_ulnaris", "L"));
        wrist_extensionSM.secondaryMuscles.Add(getMuscle("Extensor_digitorum", "L"));
        wrist_extensionSM.secondaryMuscles.Add(getMuscle("Extensor_digiti_minimi", "L"));
        wrist_extensionSM.secondaryMuscles.Add(getMuscle("Extensor_indicis", "L"));
        wrist_extensionSM.secondaryMuscles.Add(getMuscle("Extensor_pollicis_longus", "L"));
        wrist_extensionSM.xRotation = 12f; 
		wrist_extensionSM.yRotation = 105f;
		wrist_extensionSM.distanceLelObject = 5.2f;
		wrist_extensionSM.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (wrist_extensionSM);

		action wrist_flexion = new action();
		wrist_flexion.name = "wrist flexion";
		wrist_flexion.one = GameObject.Find ("Hand_Controller_L").transform;
		wrist_flexion.mc.Add(getMuscle("Flexor_carpi_radialis","L"));
		wrist_flexion.mc.Add(getMuscle("Flexor_carpi_ulnaris","L"));
        wrist_flexion.primaryMotion = "Wrist";
        wrist_flexion.primaryMuscles.Add(getMuscle("Flexor_carpi_radialis", "L"));
        wrist_flexion.primaryMuscles.Add(getMuscle("Flexor_carpi_ulnaris", "L"));
        wrist_flexion.secondaryMuscles.Add(getMuscle("Palmaris_longus", "L"));
        wrist_flexion.secondaryMuscles.Add(getMuscle("Flexor_digitorum_profundus", "L"));
        wrist_flexion.secondaryMuscles.Add(getMuscle("Flexor_digitorum_superficialis", "L"));
        wrist_flexion.secondaryMuscles.Add(getMuscle("Flexor_pollicis_longus", "L"));
        wrist_flexion.secondaryMuscles.Add(getMuscle("Abductor_pollicis_longus", "L"));
        wrist_flexion.xRotation = 12f; 
		wrist_flexion.yRotation = 105f;
		wrist_flexion.distanceLelObject = 5.2f;
		wrist_flexion.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (wrist_flexion);

		action wrist_flexionSM = new action();
		wrist_flexionSM.name = "wrist flexion SM";
		wrist_flexionSM.one = GameObject.Find ("Hand_Controller_L").transform;
		wrist_flexionSM.mc.Add(getMuscle("Palmaris_longus","L"));
		wrist_flexionSM.mc.Add(getMuscle("Flexor_digitorum_profundus","L"));
		wrist_flexionSM.mc.Add(getMuscle("Flexor_digitorum_superficialis","L"));
		wrist_flexionSM.mc.Add(getMuscle("Flexor_pollicis_longus","L"));
		wrist_flexionSM.mc.Add(getMuscle("Abductor_pollicis_longus","L"));
        wrist_flexionSM.primaryMotion = "Wrist";
        wrist_flexionSM.primaryMuscles.Add(getMuscle("Flexor_carpi_radialis", "L"));
        wrist_flexionSM.primaryMuscles.Add(getMuscle("Flexor_carpi_ulnaris", "L"));
        wrist_flexionSM.secondaryMuscles.Add(getMuscle("Palmaris_longus", "L"));
        wrist_flexionSM.secondaryMuscles.Add(getMuscle("Flexor_digitorum_profundus", "L"));
        wrist_flexionSM.secondaryMuscles.Add(getMuscle("Flexor_digitorum_superficialis", "L"));
        wrist_flexionSM.secondaryMuscles.Add(getMuscle("Flexor_pollicis_longus", "L"));
        wrist_flexionSM.secondaryMuscles.Add(getMuscle("Abductor_pollicis_longus", "L"));
        wrist_flexionSM.xRotation = 12f; 
		wrist_flexionSM.yRotation = 105f;
		wrist_flexionSM.distanceLelObject = 5.2f;
		wrist_flexionSM.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (wrist_flexionSM);

		action ulnar_deviation = new action();
		ulnar_deviation.name = "ulnar deviation";
		ulnar_deviation.one = GameObject.Find ("Hand_Controller_L").transform;
		ulnar_deviation.mc.Add(getMuscle("Flexor_carpi_ulnaris","L"));
		ulnar_deviation.mc.Add(getMuscle("Extensor_carpi_ulnaris","L"));
        ulnar_deviation.primaryMotion = "Wrist";
        ulnar_deviation.primaryMuscles.Add(getMuscle("Flexor_carpi_ulnaris", "L"));
        ulnar_deviation.primaryMuscles.Add(getMuscle("Extensor_carpi_ulnaris", "L"));
        ulnar_deviation.xRotation = 12f; 
		ulnar_deviation.yRotation = 105f;
		ulnar_deviation.distanceLelObject = 5.2f;
		ulnar_deviation.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (ulnar_deviation);

		action radial_deviation = new action ();
		radial_deviation.name = "radial deviation";
		radial_deviation.one = GameObject.Find ("Hand_Controller_L").transform;
		radial_deviation.mc.Add(getMuscle("Flexor_carpi_radialis","L"));
		radial_deviation.mc.Add(getMuscle("Extensor_carpi_radialis_longus","L"));
        radial_deviation.primaryMotion = "Wrist";
        radial_deviation.primaryMuscles.Add(getMuscle("Flexor_carpi_radialis", "L"));
        radial_deviation.primaryMuscles.Add(getMuscle("Extensor_carpi_radialis_longus", "L"));
        radial_deviation.secondaryMuscles.Add(getMuscle("Extensor_carpi_radialis_brevis", "L"));
        radial_deviation.secondaryMuscles.Add(getMuscle("Abductor_pollicis_longus", "L"));
        radial_deviation.secondaryMuscles.Add(getMuscle("Extensor_pollicis_brevis", "L"));
        radial_deviation.secondaryMuscles.Add(getMuscle("Extensor_pollicis_longus", "L"));
        radial_deviation.xRotation = 12f; 
		radial_deviation.yRotation = 105f;
		radial_deviation.distanceLelObject = 5.2f;
		radial_deviation.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (radial_deviation);

		action radial_deviationSM = new action ();
		radial_deviationSM.name = "radial deviation SM";
		radial_deviationSM.one = GameObject.Find ("Hand_Controller_L").transform;
		radial_deviationSM.mc.Add(getMuscle("Extensor_carpi_radialis_brevis","L"));
		radial_deviationSM.mc.Add(getMuscle("Abductor_pollicis_longus","L"));
		radial_deviationSM.mc.Add(getMuscle("Extensor_pollicis_brevis","L"));
		radial_deviationSM.mc.Add(getMuscle("Extensor_pollicis_longus","L"));
        radial_deviationSM.primaryMotion = "Wrist";
        radial_deviationSM.primaryMuscles.Add(getMuscle("Flexor_carpi_radialis", "L"));
        radial_deviationSM.primaryMuscles.Add(getMuscle("Extensor_carpi_radialis_longus", "L"));
        radial_deviationSM.secondaryMuscles.Add(getMuscle("Extensor_carpi_radialis_brevis", "L"));
        radial_deviationSM.secondaryMuscles.Add(getMuscle("Abductor_pollicis_longus", "L"));
        radial_deviationSM.secondaryMuscles.Add(getMuscle("Extensor_pollicis_brevis", "L"));
        radial_deviationSM.secondaryMuscles.Add(getMuscle("Extensor_pollicis_longus", "L"));
        radial_deviationSM.xRotation = 12f; 
		radial_deviationSM.yRotation = 105f;
		radial_deviationSM.distanceLelObject = 5.2f;
		radial_deviationSM.centerCameraPos = new Vector3 (-6.74f, 0.3f, -3.6f);
		Actions.Add (radial_deviationSM);
		//hand digits extension
		//hand digits flexion
		//thumb extension
		//thumb flexion
		//hand digits opoositions

		Upper_limbs.children.Add (elbow_flexion);
		Upper_limbs.children.Add (elbow_extension);
		Upper_limbs.children.Add (elbow_pronation);
		Upper_limbs.children.Add (elbow_supination);
		Upper_limbs.children.Add (wrist_extension);
		Upper_limbs.children.Add (wrist_extensionSM);
		Upper_limbs.children.Add (wrist_flexion);
		Upper_limbs.children.Add (wrist_flexionSM);
		Upper_limbs.children.Add (ulnar_deviation);
		Upper_limbs.children.Add (radial_deviation);
		Upper_limbs.children.Add (radial_deviationSM);
		////////////// END PARENT


		//////////////////////////////////// PARENT
		action Abdomen = new action ();
		Abdomen.name = "Abdomen";
		Actions.Add (Abdomen);
		action ribs_elevation = new action ();
		ribs_elevation.name = "ribs elevation";
		Actions.Add (ribs_elevation);
		action ribs_depression = new action ();
		ribs_depression.name = "ribs depression";
		Actions.Add (ribs_depression);
		Abdomen.children.Add (ribs_elevation);
		Abdomen.children.Add (ribs_depression);
		////////////// END PARENT

		//////////////////////////////////// PARENT
		action Spine_and_back = new action ();
		Spine_and_back.name = "Shoulder blade";
		Actions.Add (Spine_and_back);

		//hna fi moshklet el two
		action scapula_elevation = new action ();
		scapula_elevation.name = "scapula elevation";
		scapula_elevation.one = GameObject.Find ("clavicle_L").transform;
		//scapula_elevation.two = GameObject.Find ("scapula_L LookAt").transform; //was scapula_L LookAt
		scapula_elevation.mc.Add(getMuscle("Trapezius_upper","L"));
		scapula_elevation.mc.Add(getMuscle("Rhomboideus_major","L"));
		scapula_elevation.mc.Add(getMuscle("Rhomboideus_minor","L")); 
		scapula_elevation.mc.Add(getMuscle("Levator_scapulae","L"));
        scapula_elevation.primaryMotion = "Scapulothoracic Motion";
        scapula_elevation.primaryMuscles.Add(getMuscle("Trapezius_upper", "L"));
        scapula_elevation.primaryMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        scapula_elevation.primaryMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        scapula_elevation.primaryMuscles.Add(getMuscle("Levator_scapulae", "L"));
        scapula_elevation.xRotation = -11.75f;
        scapula_elevation.yRotation = 16f;
        scapula_elevation.distanceLelObject = 4.7f;
		scapula_elevation.centerCameraPos = new Vector3 (-5.8f, 2.1f, -3.8f);
		Actions.Add (scapula_elevation);

		//hna fi moshklet el two
		action scapula_depression = new action ();
		scapula_depression.name = "scapula depression";
		scapula_depression.one = GameObject.Find ("clavicle_L").transform;
		//scapula_depression.two = GameObject.Find ("humerus_L0").transform; //was scapula_L LookAt
		scapula_depression.mc.Add(getMuscle("Pectoralis_minor","L"));
		scapula_depression.mc.Add(getMuscle("Trapezius_lower","L"));
		scapula_depression.mc.Add(getMuscle("Serratus_anterior","L"));
        scapula_depression.primaryMotion = "Scapulothoracic Motion";
        scapula_depression.primaryMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        scapula_depression.primaryMuscles.Add(getMuscle("Trapezius_lower", "L"));
        scapula_depression.primaryMuscles.Add(getMuscle("Serratus_anterior", "L"));
        scapula_depression.xRotation = -11.75f;
		scapula_depression.yRotation = 16f;
		scapula_depression.distanceLelObject = 4.7f;
		scapula_depression.centerCameraPos = new Vector3 (-5.8f, 2.1f, -3.8f);
		Actions.Add (scapula_depression);

		action scapula_upper_rotation = new action ();
		scapula_upper_rotation.name = "scapula upper rotation";
		scapula_upper_rotation.one = GameObject.Find ("clavicle_L").transform;
		scapula_upper_rotation.mc.Add(getMuscle("Serratus_anterior","L"));
		scapula_upper_rotation.mc.Add(getMuscle("Trapezius_upper","L"));
		scapula_upper_rotation.mc.Add(getMuscle("Trapezius_middle","L"));
		scapula_upper_rotation.mc.Add(getMuscle("Trapezius_lower","L"));
        scapula_upper_rotation.primaryMotion = "Scapulothoracic Motion";
        scapula_upper_rotation.primaryMuscles.Add(getMuscle("Serratus_anterior", "L"));
        scapula_upper_rotation.primaryMuscles.Add(getMuscle("Trapezius_upper", "L"));
        scapula_upper_rotation.primaryMuscles.Add(getMuscle("Trapezius_middle", "L"));
        scapula_upper_rotation.primaryMuscles.Add(getMuscle("Trapezius_lower", "L"));
        scapula_upper_rotation.xRotation = 8.75f;
		scapula_upper_rotation.yRotation = 31.25f;
		scapula_upper_rotation.distanceLelObject = 6.9f;
		scapula_upper_rotation.centerCameraPos = new Vector3 (-5.9f, 2.2f, -4.31f);
		Actions.Add (scapula_upper_rotation);

		action scapula_downward_rotation = new action ();
		scapula_downward_rotation.name = "scapula downward rotation";
		scapula_downward_rotation.one = GameObject.Find ("clavicle_L").transform;
		scapula_downward_rotation.mc.Add(getMuscle("Rhomboideus_major","L"));
		scapula_downward_rotation.mc.Add(getMuscle("Rhomboideus_minor","L")); 
		scapula_downward_rotation.mc.Add(getMuscle("Pectoralis_minor","L"));
		scapula_downward_rotation.mc.Add(getMuscle("Levator_scapulae","L"));
        scapula_downward_rotation.primaryMotion = "Scapulothoracic Motion";
        scapula_downward_rotation.primaryMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        scapula_downward_rotation.primaryMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        scapula_downward_rotation.primaryMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        scapula_downward_rotation.primaryMuscles.Add(getMuscle("Levator_scapulae", "L"));
        scapula_downward_rotation.xRotation = -1.25f;
		scapula_downward_rotation.yRotation = 2.75f;
		scapula_downward_rotation.distanceLelObject = 5.2f;
		scapula_downward_rotation.centerCameraPos = new Vector3 (-5.7f, 2.5f, -4.0f);
		Actions.Add (scapula_downward_rotation);

		action scapula_protraction = new action ();
		scapula_protraction.name = "scapula protraction";
		scapula_protraction.one = GameObject.Find ("clavicle_L").transform;
		scapula_protraction.mc.Add(getMuscle("Pectoralis_minor","L"));
		scapula_protraction.mc.Add(getMuscle("Serratus_anterior","L"));
        scapula_protraction.primaryMotion = "Scapulothoracic Motion";
        scapula_protraction.primaryMuscles.Add(getMuscle("Pectoralis_minor", "L"));
        scapula_protraction.primaryMuscles.Add(getMuscle("Serratus_anterior", "L"));
        scapula_protraction.xRotation = 15f;
		scapula_protraction.yRotation = 41f;
		scapula_protraction.distanceLelObject = 4.9f;
		scapula_protraction.centerCameraPos = new Vector3 (-6.1f, 1.3f, -3.9f);
		Actions.Add (scapula_protraction);

		action scapula_retraction = new action();
		scapula_retraction.name = "scapula retraction";
		scapula_retraction.one = GameObject.Find ("clavicle_L").transform;
		scapula_retraction.mc.Add(getMuscle("Rhomboideus_major","L"));
		scapula_retraction.mc.Add(getMuscle("Rhomboideus_minor","L")); 
		scapula_retraction.mc.Add(getMuscle("Trapezius_middle","L"));
        scapula_retraction.primaryMotion = "Scapulothoracic Motion";
        scapula_retraction.primaryMuscles.Add(getMuscle("Rhomboideus_major", "L"));
        scapula_retraction.primaryMuscles.Add(getMuscle("Rhomboideus_minor", "L"));
        scapula_retraction.primaryMuscles.Add(getMuscle("Trapezius_middle", "L"));
        scapula_retraction.xRotation = 8f;
		scapula_retraction.yRotation = 7.25f;
		scapula_retraction.distanceLelObject = 4.1f;
		scapula_retraction.centerCameraPos = new Vector3 (-5.7f, 2.3f, -3.8f);
		Actions.Add (scapula_retraction);

		//spine flexion
		//spine extension
		//spine lateral flexion
		//spine rotation

		Spine_and_back.children.Add (scapula_elevation);
		Spine_and_back.children.Add (scapula_depression);
		Spine_and_back.children.Add (scapula_upper_rotation);
		Spine_and_back.children.Add (scapula_downward_rotation);
		Spine_and_back.children.Add (scapula_protraction);
		Spine_and_back.children.Add (scapula_retraction);
		////////////// END PARENT


		// lesa 3aiz a3del dool , we lesa 3aiz a7otelhom l parents

		///////////////////////////////////////////////////////
		////HAND /////////////////////////////////////////////
//
//		action thumb_flexion = new action ();
//		thumb_flexion.name = "thumb flexion";
//		// missingExcel thumb_flexion.one = GameObject.Find ("").transform;
//		// missingObject thumb_flexion.mc.Add(getMuscle("Flexor__pollicis_brevis","L"));
//		// missingObject thumb_flexion.mc.Add(getMuscle("Opponens_pollicis","L"));
//		// missingObject thumb_flexion.mc.Add(getMuscle("Flexor__pollicis_longus","L"));
//		Actions.Add (thumb_flexion);
//
//		action thumb_extension = new action ();
//		thumb_extension.name = "thumb extension";
//		// missingExcel thumb_extension.one = GameObject.Find ("").transform;
//		thumb_extension.mc.Add(getMuscle("Extensor_pollicis_longus","L"));
//		// missingObject thumb_extension.mc.Add(getMuscle("Extensor_pollicis_brevis","L"));
//		thumb_extension.mc.Add(getMuscle("Abductor_pollicis_longus","L"));
//		Actions.Add (thumb_extension);
//
//		action thumb_abduction = new action ();
//		thumb_abduction.name = "thumb abduction";
//		// missingExcel thumb_abduction.one = GameObject.Find ("").transform;
//		thumb_abduction.mc.Add(getMuscle("Abductor_pollicis_longus","L"));
//		// missingObject thumb_abduction.mc.Add(getMuscle("Abductor_pollicis_brevis","L"));
//		Actions.Add (thumb_abduction);
//
//		action thumb_adduction = new action ();
//		thumb_adduction.name = "thumb adduction";
//		// missingExcel thumb_adduction.one = GameObject.Find ("").transform;
//		// missingObject thumb_adduction.mc.Add(getMuscle("Adductor_pollicis","L"));
//		Actions.Add (thumb_adduction);
//
//		action thumb_opposition = new action ();
//		thumb_opposition.name = "thumb opposition";
//		// missingExcel thumb_adduction.one = GameObject.Find ("").transform;
//		// missingObject thumb_adduction.mc.Add(getMuscle("Opponens pollicis","L"));
//		// missingObject thumb_adduction.mc.Add(getMuscle("Flexor_pollicis_brevis","L"));
//		Actions.Add (thumb_opposition);
//
//		action fingers_flexion = new action ();
//		fingers_flexion.name = "fingers flexion";
//		// missingExcel fingers_flexion.one = GameObject.Find ("").transform;
//		fingers_flexion.mc.Add(getMuscle("Flexor_digitorum_superficialis","L"));
//		fingers_flexion.mc.Add(getMuscle("Flexor_digitorum_profundus","L"));
//		// missingObject fingers_flexion.mc.Add(getMuscle("Flexor_pollicis_longus","L"));
//		// missingObject fingers_flexion.mc.Add(getMuscle("Flexor_digiti_minimi","L"));
//		Actions.Add (fingers_flexion);
//
//		action fingers_flexionSM = new action ();
//		fingers_flexionSM.name = "fingers flexion SM";
//		// missingExcel fingers_flexionSM.one = GameObject.Find ("").transform;
//		// missingObject fingers_flexionSM.mc.Add(getMuscle("Lumbrical","L"));
//		// missingObject fingers_flexionSM.mc.Add(getMuscle("Interossei","L"));
//		Actions.Add (fingers_flexionSM);
//
//		action fingers_extension = new action ();
//		fingers_extension.name = "fingers extension";
//		// missingExcel fingers_extension.one = GameObject.Find ("").transform;
//		fingers_extension.mc.Add(getMuscle("Extensor_digitorum","L"));
//		// missingObject fingers_extension.mc.Add(getMuscle("Extensor_indicis","L"));
//		fingers_extension.mc.Add(getMuscle("Extensor_digiti_minimi","L"));
//		fingers_extension.mc.Add(getMuscle("Extensor_pollicis_longus","L"));
//		// missingObject fingers_extension.mc.Add(getMuscle("Extensor_pollicis_brevis","L"));
//		Actions.Add (fingers_extension);
//
//		action hand_abduction = new action ();
//		hand_abduction.name = "hand abduction";
//		// missingExcel hand_abduction.one = GameObject.Find ("").transform;
//		// missingObject hand_abduction.mc.Add(getMuscle("Dorsal interossei","L"));
//		// missingObject hand_abduction.mc.Add(getMuscle("Abductor_digiti_minimi","L"));
//		// missingObject hand_abduction.mc.Add(getMuscle("Abductor_pollicis_brevis","L"));
//		Actions.Add (hand_abduction);
//
//		action hand_abductionSM = new action ();
//		hand_abductionSM.name = "hand abduction SM";
//		// missingExcel hand_abductionSM.one = GameObject.Find ("").transform;
//		hand_abductionSM.mc.Add(getMuscle("Extensor_digitorum","L"));
//		// missingObject hand_abductionSM.mc.Add(getMuscle("Extensor_indicis","L"));
//		hand_abductionSM.mc.Add(getMuscle("Extensor_digiti_minimi", "L"));
//		Actions.Add (hand_abductionSM);
//
//		action hand_adduction = new action ();
//		hand_adduction.name = "hand adduction";
//		// missingExcel hand_adduction.one = GameObject.Find ("").transform;
//		// missingObject hand_adduction.mc.Add(getMuscle("Palmar_interossei","L"));
//		// missingObject hand_adduction.mc.Add(getMuscle("Adductor_pollicis","L"));
//		Actions.Add (hand_adduction);
//
//		action hand_adductionSM= new action ();
//		hand_adductionSM.name = "hand adduction SM";
//		// missingExcel hand_adductionSM.one = GameObject.Find ("").transform;
//		hand_adductionSM.mc.Add(getMuscle("Flexor_digitorum_superficialis","L"));
//		hand_adductionSM.mc.Add(getMuscle("Flexor_digitorum_profundus","L"));
//		Actions.Add (hand_adductionSM);
	
	}
	[ObfuscateLiterals]
	private Transform getTransform(string name){
		int x = Rigg3D.transform.childCount;
		for (int i = 0; i < x; i++) {
			string objectName = Rigg3D.transform.GetChild (i).gameObject.name;
			int firstChar = objectName.IndexOf (name);
			if (firstChar!= -1) //lw 7etet el esm mawgoda fl bta3
				return Rigg3D.transform.GetChild (i).gameObject.transform;
		}
		Debug.Log ("error, didn't find the object!");
		return null;

	}
	[ObfuscateLiterals]
	private muscleControllersData_Managed getMuscle(string name, string LoR){
		int x = Rigg3D.transform.childCount;
		for (int i = 0; i < x; i++) {
			string objectName = Rigg3D.transform.GetChild (i).gameObject.name;
			int firstChar = objectName.IndexOf (name);
			if (firstChar!= -1) //lw 7etet el esm mawgoda fl bta3
			{//dawar 3la left aw L aw right aw R 3ashan a7adedha aktr
				if(Mathf.Abs(objectName.Length - name.Length) > 3) 
					continue;
				objectName = objectName.Substring(objectName.Length-1,1); //a5er 7arf
				firstChar = objectName.IndexOf(LoR);
				if (firstChar != -1) {
					// kda da hwa
					muscleControllersData_Managed dumm = Rigg3D.transform.GetChild(i).gameObject.GetComponent<muscleControllersData_Managed>();
					if (dumm == null)
						Debug.Log (name + " error, returned a wrong object!");
					return dumm;
				}
			}
		}
		Debug.Log (name + "error, didn't find the object!");
		return null;
	}
	[ObfuscateLiterals]
	private void clearTheList(){
		int c = this.transform.childCount;
		for (int i = 0; i < c; i++){
			GameObject.DestroyImmediate (this.transform.GetChild (0).gameObject);
		}
	}
	[ObfuscateLiterals]
	private void populateTheActionsMenu(){
		Queue <action> myQ = new Queue <action>();
		myQ.Enqueue (Actions [0]);

		foreach (action a in Actions) {
			GameObject dummy = Instantiate(prefab_actionObject);
            dummy.GetComponent<actionObjectScript>().action = a;
		 
			dummy.GetComponent<actionObjectScript> ().one = a.one;
			dummy.GetComponent<actionObjectScript> ().two = a.two;
			for (int i = 0; i < a.mc.Count; i++) {
				dummy.GetComponent<actionObjectScript> ().mc.Add (a.mc [i]);
			}
			dummy.transform.parent = this.transform;

			dummy.GetComponent<actionObjectScript> ().muscleMovers = a.muscleMovers;
            dummy.GetComponent<actionObjectScript>().actionName = a.name;

            GameObject cameraPos = new GameObject ("Camera Pos");
			cameraPos.transform.parent = dummy.transform;
			cameraPos.AddComponent<cameraPosition>();

			cameraPos.GetComponent<cameraPosition> ().centerCameraPos = a.centerCameraPos;
			cameraPos.GetComponent<cameraPosition> ().distanceLelObject = a.distanceLelObject;
			cameraPos.GetComponent<cameraPosition> ().xRotation = a.xRotation;
			cameraPos.GetComponent<cameraPosition> ().yRotation = a.yRotation;

			Text textObject = dummy.transform.GetChild (1).GetComponent<Text>();
			textObject.text = a.name;


			//7maya lel release mn el errors (el mynfa3sh ndoso a7mar)
			if ((a.one == null || a.distanceLelObject == 0.0f) && a.children.Count == 0) {
				textObject.color = Color.red;
			}

			for (int j =  0; j < a.children.Count ; j++) {
				dummy.GetComponent<actionObjectScript>().children.Add(a.children[j]);
			}
		}

		//di bgib fiha el menuObjects nafsohom ka children lel menuitems el tnyin l parent bto3hom
		//bdal el muscleHeir el mwgod fl children list , aw m3ah y3ni msh bdalo
		for (int i = 0; i < this.transform.childCount; i++) {
			if (this.transform.GetChild (i).GetComponent<actionObjectScript> ().children.Count != 0) {
				actionObjectScript v = this.transform.GetChild (i).GetComponent<actionObjectScript> ();
				for (int j = 0; j < v.children.Count; j++) {
					v.childrenMenuObjects.Add(getActionObjectOfName (v.children [j].name));
				}
			}
		}


		// 3aiz a5ali el actions el m3ndhash children tt7rak ymin 7etta
		//we l m3ndosh children bageblo l reference object bta3o
		Vector3 delta = new Vector3 (15.0f, 0f, 0f);

		for (int i = 0; i < this.transform.childCount; i++) {
			if (this.transform.GetChild (i).GetComponent<actionObjectScript> ().childrenMenuObjects.Count != 0) {
				for (int j = 0; j < this.transform.GetChild (i).GetComponent<actionObjectScript> ().childrenMenuObjects.Count; j++) {
					GameObject u = this.transform.GetChild (i).GetComponent<actionObjectScript> ().childrenMenuObjects [j];
					u.transform.GetChild (0).transform.position += delta;
					u.transform.GetChild (1).transform.position += delta;
					u.SetActive (true); //TODO da el mfrod yb2a false bs 3ashan moshklet en 
					//lw el actionObject m3molo set false , by5ali el action nafso 
					//msh shaghal
					//i can;t disable this menu, but i can make the items invisible
					u.GetComponent<RectTransform>().sizeDelta = new Vector2(208,0); //set height to 0
					//and hide the internals
					for (int k = 0; k < u.transform.childCount; k++) {
						u.transform.GetChild (0).gameObject.SetActive (false);
						u.transform.GetChild (1).gameObject.SetActive (false);
					}

					//fix for that the contentFitter is not dynamic (to refresh the menu)
					int sib = u.transform.GetSiblingIndex ();
					u.transform.SetAsLastSibling ();
					u.transform.SetSiblingIndex (sib);
				}
			} 
		}
			

	}
	[ObfuscateLiterals]
	private void getMusclesforeachAction(){
		// 3aiz a3adi 3lehom kolohom 
		// wel 3ndo count fl mc (muscle controllers, hagib l menuObject el 3ando nafs el referenceScript da)
		// wa3mlo instantiate hna tani wa7oto ta7t l bta3a wa5alih disabled
		//--done
		// b3d keda onclick , lw 3ndo mc count , a3adi 3lehom a5alihom enabled
		// bs kda
		int deltpos = 1;
		for (int i = 0; i < this.transform.childCount; i++) {
			i = i + deltpos - 1;
			deltpos = 1;
			if (i >= this.transform.childCount) // da by7sal fl a5er 5aleeeees , 7maya mn l error ha3ml break bs
				break;
			if (this.transform.GetChild (i).GetComponent<actionObjectScript> ().mc.Count != 0) {
				int c = this.transform.GetChild (i).GetComponent<actionObjectScript> ().mc.Count;

				for (int k = 0; k < c; k++) {
					GameObject dummy = getMenuObjectOfMuscle (this.transform.GetChild (i).GetComponent<actionObjectScript> ().mc [k]);
					//Debug.Log (dummy);
					if (dummy == null) continue;
					dummy = Instantiate(dummy);
					dummy.transform.parent = this.transform;
					dummy.transform.SetSiblingIndex (i + deltpos);
					deltpos++;
					dummy.SetActive (false);
					this.transform.GetChild (i).GetComponent<actionObjectScript> ().musclesofeachactionMenuObjects.Add (dummy);
				}
			}
		}
	}

	[ObfuscateLiterals]
	private GameObject getMenuObjectOfMuscle(muscleControllersData_Managed m){
		for (int i = 0; i < musclesMenuParent.transform.childCount; i++) {
			if (musclesMenuParent.transform.GetChild (i).GetComponent<menuObjectScript> ().referenceScript == m)
				return musclesMenuParent.transform.GetChild (i).gameObject;
		}
		Debug.Log ("errorrrr!!!!");
		return null;
	}

	[ObfuscateLiterals]
	private GameObject getActionObjectOfName(string t){
		for (int i = 0; i < this.transform.childCount; i++) {
			if (this.transform.GetChild (i).transform.GetChild (1).GetComponent<Text> ().text == t)
				return this.transform.GetChild (i).gameObject;
		}
		Debug.Log ("can't be!");
		return null;
	}

	private int getint(bool  x){
		if (x)
			return 1;
		else
			return 0;
	}
	[ObfuscateLiterals]
	//called by any click on a dysfunctional toggle
	public void calculateDisfunc(){
		for (int i = 0; i < this.transform.childCount; i++) {
			GameObject action = this.transform.GetChild (i).gameObject;
            string name = action.transform.GetChild(1).GetComponent<Text>().text;
            if (name == "shoulder flexion") { //hwa a name el msh bytghyar w mlosh parent da :/ estana hashof 3ndi ha2fel team viewer
                actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc [0].dysfunctional; 
				bool B = !x.mc [1].dysfunctional;
				bool C = !x.mc [2].dysfunctional; 
				bool D = !x.mc [3].dysfunctional; 

				string f1 = getint (!A && !B && !C && D).ToString ();
				string f2 = getint (A || B && C).ToString ();
				string f3 = getint (A || !B && C || B && !C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3;

			} else if (name == "scapula upper rotation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc [0].dysfunctional; 
				bool B = !x.mc [1].dysfunctional && !x.mc [2].dysfunctional && !x.mc [3].dysfunctional;
				string f1 = getint (B).ToString ();
				string f2 = getint (A).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "shoulder extension") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc [1].dysfunctional;
				bool C = !x.mc [2].dysfunctional; 
				bool D = !x.mc [3].dysfunctional; 
				bool E = !x.mc [4].dysfunctional; 

				string f1 = getint(!A && !B && !D && E).ToString ();
				string f2 = getint(A || B && C || C && !D && !E).ToString ();
				string f3 = getint(D || A || B && !C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1+f2+f3;
			}
			else if (name == "scapula downward rotation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc [0].dysfunctional && !x.mc [1].dysfunctional; 
				bool B = !x.mc [2].dysfunctional;
				bool C = !x.mc [3].dysfunctional;
				string f1 = getint(A || !B&C).ToString ();
				string f2 = getint(A).ToString ();
				string f3 = getint (B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3;
			}
			else if (name == "shoulder abduction") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc [1].dysfunctional;
				bool C = !x.mc [2].dysfunctional;
				bool D = !x.mc [3].dysfunctional;
				bool E = !x.mc [4].dysfunctional;

				string f1 = getint(D || C || A || !B && E).ToString ();
				string f2 = getint(D || A || B && !C).ToString ();
				string f3 = getint(C || A || B&&!D).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1+f2+f3;
			}
			else if (name == "shoulder adduction") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				bool D = !x.mc[3].dysfunctional;

				string f1 = getint(A && C || A && B).ToString ();
				string f2 = getint(C || B).ToString ();
				string f3 = getint(A).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1+f2+f3;
			}
			else if (name == "shoulder external rotation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(A || B && C).ToString ();
				string f2 = getint(A || B || C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1+f2;
			}
			else if (name == "scapula retraction") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional && !x.mc[1].dysfunctional;
				bool B = !x.mc[2].dysfunctional;
				string f1 = getint(A).ToString ();
				string f2 = getint(B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "shoulder internal rotation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				bool D = !x.mc[3].dysfunctional;
				bool E = !x.mc[4].dysfunctional;

				string f1 = getint(E || A || C&&D || B&&C).ToString ();
				string f2 = getint(D || A || !B&&C || C&&!E || B&&!C).ToString ();
				string f3 = getint(D || A || B&&!C || B&&!E || !B&&C&&E).ToString();
				action.GetComponent<actionObjectScript> ().function1 = f1+f2+f3;
			}
			else if (name == "scapula protraction") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				string f1 = getint(A).ToString ();
				string f2 = getint(B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "scapula elevation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional && !x.mc[2].dysfunctional;
				bool C = !x.mc[3].dysfunctional;
				string f1 = getint(A).ToString ();
				string f2 = getint(B || C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "scapula depression") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc [1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(A).ToString ();
				string f2 = getint(A && B).ToString ();
				string f3 = getint(B || !A&&C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3;
			}
			else if (name == "shoulder horizontal abduction") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(A || B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1;
			}
			else if (name == "shoulder horizontal adduction") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(A || !B&&C).ToString ();
				string f2 = getint(A || B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			////////////////////////////////////////////ELBOW AND FOREARM//////
			else if (name == "elbow flexion") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				bool D = !x.mc[3].dysfunctional;
				string f1 = getint(A || B || C&&!D).ToString ();
				string f2 = getint(A || B || D).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "elbow extension") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional && !x.mc[3].dysfunctional;
				string f1 = getint(A || C).ToString ();
				string f2 = getint(A || B&&!C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			} //heree
			else if (name == "elbow pronation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(A || B).ToString ();
				string f2 = getint(A || B || C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "elbow supination") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(A || B).ToString ();
				string f2 = getint(!A&&!B&&C || A&&!B&&!C).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "wrist flexion") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				string f1 = getint(A || B).ToString ();
				string f2 = getint(A || B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
				f1 = getint(!A&&!B).ToString ();
				action.GetComponent<actionObjectScript> ().function2 = f1; //E
			}
			else if (name == "wrist flexion SM") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional && !x.mc[2].dysfunctional;
				bool C = !x.mc[3].dysfunctional;
				bool D = !x.mc[4].dysfunctional;
				string f1 = getint(!A&&D || !A&&!B&&C || !A&&B&&!C).ToString ();
				string f2 = getint(!B&&!C&&D || A&&!C&&!D).ToString ();
				string f3 = getint(B || A&&!C&&!D).ToString ();
				string f4 = getint(C || B&&D || A&&!B&&!D).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3 + f4;
			}
			else if (name == "wrist extension") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				string f1 = getint(C).ToString ();
				string f2 = getint(A || B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
				f1 = getint(!A&&!B&&!C).ToString ();
				action.GetComponent<actionObjectScript> ().function2 = f1; //E
			}
			else if (name == "wrist extension SM") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				bool D = !x.mc[3].dysfunctional;
				string f1 = getint(A || B || C).ToString ();
				string f2 = getint(D).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "ulnar deviation") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				string f1 = getint(A).ToString ();
				string f2 = getint(B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
			else if (name == "radial deviation") {
				//soething is wrong in the excel sheet ! TODO
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				string f1 = getint(A || B).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1;
				f1 = getint(!A&&!B).ToString ();
				action.GetComponent<actionObjectScript> ().function2 = f1; //E
			}
			else if (name == "radial deviation SM") {
				actionObjectScript x = action.GetComponent<actionObjectScript> ();
				bool A = !x.mc[0].dysfunctional;
				bool B = !x.mc[1].dysfunctional;
				bool C = !x.mc[2].dysfunctional;
				bool D = !x.mc[3].dysfunctional;
				string f1 = getint(C || D).ToString ();
				string f2 = getint(B&&!C&&!D || A&&!C&&!D).ToString ();
				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
			}
//			else if (name == "thumb flexion") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				bool C = !x.mc[2].dysfunctional;
//				string f1 = getint(C || !A&&B).ToString ();
//				string f2 = getint(A || C).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
//			}
//			else if (name == "thumb extension") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				bool C = !x.mc[2].dysfunctional;
//				string f1 = getint(A || !B&&C).ToString ();
//				string f2 = getint(A || B).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
//			}
//			else if (name == "thumb abduction") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				string f1 = getint(A).ToString ();
//				string f2 = getint(A || B).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
//			}
//			else if (name == "thumb adduction") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				string f1 = getint(A).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1;
//			}
//			else if (name == "thumb opposition") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				string f1 = getint(A).ToString ();
//				string f2 = getint(A || B).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
//			}
//			else if (name == "fingers flexion") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				bool C = !x.mc[2].dysfunctional;
//				bool D = !x.mc[3].dysfunctional;
//				string f1 = getint(B || A || C&&!D).ToString ();
//				string f2 = getint(B || !A&&D || A&&C).ToString ();
//				string f3 = getint(C&&!D || B&&C || A&&C || !A&&!B&&!C&&D).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3;
//				f1 = getint(!A&&!B&&!C || !A&&!B&&D).ToString ();
//				action.GetComponent<actionObjectScript> ().function2 = f1; // E
//			}
//			else if (name == "fingers flexion SM") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				string f1 = getint(A || B).ToString ();
//				string f2 = getint(A).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
//			}
//			else if (name == "fingers extension") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				bool C = !x.mc[2].dysfunctional;
//				bool D = !x.mc[3].dysfunctional;
//				bool E = !x.mc[4].dysfunctional;
//				string f1 = getint(A&&!D&&!E || !A&&!B&&!C&&E || !A&&!B&&!C&&D).ToString ();
//				string f2 = getint(C&&E || C&&D || B&&E || B&&D || A&&E || A&&D).ToString ();
//				string f3 = getint(A&&E || A&&D || !A&&B&&C).ToString ();
//				string f4 = getint(A || !B&&C || B&&!D&&!E).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3 + f4;
//			}
//			else if (name == "hand abduction") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				bool C = !x.mc[2].dysfunctional;
//				string f1 = getint(A).ToString ();
//				string f2 = getint(A || B).ToString ();
//				string f3 = getint(A || C).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3;
//				f1 = getint(!C&&!B&&!A).ToString ();
//				action.GetComponent<actionObjectScript> ().function2 = f1;//E 
//			}
//			else if (name == "hand abduction SM") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				bool C = !x.mc[2].dysfunctional;
//				string f1 = getint(A).ToString ();
//				string f2 = getint(A || C).ToString ();
//				string f3 = getint(A || B).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2 + f3;
//			}
//			else if (name == "hand adduction") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				string f1 = getint(A).ToString ();
//				string f2 = getint(B).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1 + f2;
//				f1 = getint(!A&&!B).ToString ();
//				action.GetComponent<actionObjectScript> ().function2 = f1; // E
//			}
//			else if (name == "hand adduction SM") {
//				actionObjectScript x = action.GetComponent<actionObjectScript> ();
//				bool A = !x.mc[0].dysfunctional;
//				bool B = !x.mc[1].dysfunctional;
//				string f1 = getint(A || B).ToString ();
//				action.GetComponent<actionObjectScript> ().function1 = f1;
//			}

		}
	}
	[ObfuscateLiterals]
	public void calculateActionAngles(string specific = "none"){
		///5ali balak en el angles di msh mn hna bzabt , da mogarad 3ashan nozbot el slider
		/// value fi el musclesController_Managed di , we di el fiha el max angle bta3et kol muscle
		/// we da hwa el ta2sir el 7a2i2i , el angles el hna di bas mas2ola 3an enha t5afef ta2siro
		/// ka slider value bas! bs di msh angles 7a2i2ya
		/// y3ni hna el max angle mohema , 3ashan hya el mo2aser fl slider value di
		/// hya aham 7aga asln
		for (int i = 0; i < this.transform.childCount; i++) {
			actionObjectScript action = this.transform.GetChild (i).gameObject.GetComponent<actionObjectScript> ();
			if (action == null) //howa el list gaya null leh ?  sorry sorry 3ndi di , kml debug , 3adi awel wa7da null

				continue; //da 3ashan dlwa2ty b2a fi fl list di muscles m3 el actions . fa hyrga3li null 
			string name = action.transform.GetChild (1).GetComponent<Text> ().text;
			action.RotX1 = true;
			action.RotY1 = true;
			action.RotZ1 = true;
			action.RotX2 = true;
			action.RotY2 = true;
			action.RotZ2 = true;

			if (name == "shoulder flexion" ) {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula upper rotation");
				if (m != null)
					action.addAccompanyingAction (m, 1);
				if (action.function1 == "000" ) {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") {
					action.angle1x = -45;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "010") {
					action.angle1x = -90;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "011") {
					action.angle1x = -180;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "100") {
					action.angle1x = -90;//this
					action.angle1y = 0;
					action.angle1z = 0;
					m = getActionNamed ("elbow flexion");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				}
			} 
			else if (name == "scapula upper rotation") { //accompanier
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
					}
				else if (action.function1 == "01") {
					action.angle1x = -22;
					action.angle1y = 0;
					action.angle1z = -33;
					actionObjectScript m = getActionNamed ("scapula protraction");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "10") {
					action.angle1x = -22;
					action.angle1y = 0;
					action.angle1z = -33;
					actionObjectScript m = getActionNamed ("scapula retraction");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") {
					action.angle1x = -22;
					action.angle1y = 0;
					action.angle1z = -33;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			}
			else if (name == "shoulder extension") {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula downward rotation");
				if (m != null)
					action.addAccompanyingAction (m, 1);
				if (action.function1 == "000") { // 120
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") { // 120:0
					action.angle1x = 0;//120
					action.angle1z = 0;
				} else if (action.function1 == "010") {// 120:-40
					action.angle1x = 40;//160
					action.angle1z = 0;
				} else if (action.function1 == "011") {// 120:-60
					action.angle1x = 60;//180
					action.angle1z = 0;
				} else if (action.function1 == "100") { // 120:0
					action.angle1x = 10;//120
					action.angle1z = 0;
					m = getActionNamed ("elbow extension");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				}
			} 
			else if (name == "scapula downward rotation") {
				//accompanier
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "000") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") { //60:0
					action.angle1x = 50;
					action.angle1y = 5;
					action.angle1z = 15;
					actionObjectScript m = getActionNamed ("scapula depression");
					if (m != null)
						action.addAccompanyingAction (m, 0.5f);
					m = getActionNamed ("scapula protraction");
					if (m != null)
						action.addAccompanyingAction (m, 1.0f);
				} else if (action.function1 == "010") { //60:0
					action.angle1z = 60;
					action.angle1x = 5;
					action.angle1y = -3;
					actionObjectScript m = getActionNamed ("scapula depression");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "100") { //60:0
					action.angle1x = 55*0.4f;
					action.angle1y = -10*0.4f;
					action.angle1z = 38*0.4f;
					actionObjectScript m = getActionNamed ("scapula elevation");
					if (m != null)
						action.addAccompanyingAction (m, 0.4f);
				} else if (action.function1 == "110") { //60:0
					action.angle1x = 37;
					action.angle1y = 0;
					action.angle1z = 14;
					actionObjectScript m = getActionNamed ("scapula elevation");
					if (m != null)
						action.addAccompanyingAction (m, 0.1f);
					m = getActionNamed ("scapula retraction");
					if (m != null)
						action.addAccompanyingAction (m, 0.0f); //da mn l testing la2ena enna msh m7tagino
				} else if (action.function1 == "111") { //60:0
					action.angle1x = 19;
					action.angle1y = 0;
					action.angle1z = 23;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			
			} 
			else if (name == "shoulder abduction") {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula upper rotation");
				if (m != null)
					action.addAccompanyingAction (m, 1);
				if (action.function1 == "000") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") { 
					action.angle1z = -45;
				} else if (action.function1 == "011") { 
					action.angle1z = -90;
				} else if (action.function1 == "100") { 
					action.angle1z = -90;
					m = getActionNamed ("elbow flexion");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "101") { 
					action.angle1z = -47;
					m = getActionNamed ("shoulder flexion");//TODO check
					if (m != null)
						action.addAccompanyingAction (m, 0.5f);
				} else if (action.function1 == "110") { 
					action.angle1z = -30;
					m = getActionNamed ("shoulder extension");//TODO check
					if (m != null)
						action.addAccompanyingAction (m, 0.5f);
				} else if (action.function1 == "111") { 
					action.angle1z = -160;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}


			} 
			else if (name == "shoulder adduction") {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula downward rotation");
				if (m != null)
					action.addAccompanyingAction (m, 1);
				if (action.function1 == "000") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") { 
					action.angle1z = 120;
					m = getActionNamed ("shoulder flexion");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "010") { 
					action.angle1z = 120;
					m = getActionNamed ("shoulder extension");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "100") { 
					action.angle1z = 90;
					m = getActionNamed ("elbow flexion");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "101") { 
					action.angle1z = 90;
					m = getActionNamed ("shoulder flexion");//TODO
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "110") { 
					action.angle1z = 90;
					m = getActionNamed ("shoulder extension");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "111") { 
					action.angle1z = 160;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}

			} 
			else if (name == "shoulder external rotation") {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula retraction");
				if (m != null)
					action.addAccompanyingAction (m, 0.5f);
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1y = -30;
				} else if (action.function1 == "11") { 
					action.angle1y = -60;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "scapula retraction") {
				//accompanying
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m;
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1y = -10;
					m = getActionNamed ("scapula upper rotation");
					if (m != null)
						action.addAccompanyingAction (m, 0.4f);
				} else if (action.function1 == "10") { 
					action.angle1y = -10;
					m = getActionNamed ("scapula downward rotation");
					if (m != null)
						action.addAccompanyingAction (m, 0.2f);
				} else if (action.function1 == "11") { 
					action.angle1y = -12;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "shoulder internal rotation") {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula protraction");
				if (m != null)
					action.addAccompanyingAction (m, 0.2f);
				if (action.function1 == "000") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") { 
					action.angle1y = 60;
					m = getActionNamed ("scapula horizontal abduction");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "010") { 
					action.angle1y = 60;
					m = getActionNamed ("scapula horizontal adduction");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "011") { 
					action.angle1y = 60;
					m = getActionNamed ("shoulder extension");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "100") { 
					action.angle1y = 60;
					m = getActionNamed ("shoulder flexion");
					if (m != null)
						action.addAccompanyingAction (m, 0.58f);
				} else if (action.function1 == "111") { 
					action.angle1y = 60;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "scapula protraction") {
				//accompanying
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m;
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1x = -10;
					action.angle1y = 30;
					m = getActionNamed ("scapula upper rotation");
					if (m != null)
						action.addAccompanyingAction (m, 0.2f);
				} else if (action.function1 == "10") { 
					action.angle1x = 18;
					action.angle1y = 30;
					m = getActionNamed ("scapula downward rotation");
					if (m != null)
						action.addAccompanyingAction (m, 0.2f);
				} else if (action.function1 == "11") { 
					action.angle1x = 0;
					action.angle1y = 30;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "scapula elevation") {
				//accompanying  --- el bta3 da kolo 7rakto ghalat
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m; //translation in y
				//action.weightc1x = 1.15f; //3ashan matod5olsh fl ribs
				//action.weightc1y = 0.21f;
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
					action.angle2x = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1x = 35;
					action.angle1y = 0;
					action.angle1z = 10;
					//sheltaha we ha3mlha bl arkam el hena , 3shan el moshkla el kant hena en el scapula downward rotation aslan met2assara be el muscle l dysfunctional l da5altena fl case di
					//m = getActionNamed ("scapula downward rotation");
					//if (m != null)
					//	action.addAccompanyingAction (m, 0.6f);
				} else if (action.function1 == "10") { 
					action.angle1x = 30;
					action.angle1y = 0;
					action.angle1z = 0;
					m = getActionNamed ("scapula upper rotation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") { 
					action.angle1x = 30;
					action.angle1y = 0;
					action.angle1z = -30;
					//lma n3oz n7ot el uppward rotation m3aha fi 7aga nb2a n7otaha accompanying action hya kman
					//m = getActionNamed ("scapula upper rotation");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "scapula depression") {
				//accompanying  --- el bta3 da kolo 7rakto ghalat
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m; //translation in y
				//action.weightc1x = 1.15f; //3ashan matod5olsh fl ribs
				//action.weightc1y = 0.21f;
				if (action.function1 == "000") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "001") { 
					action.angle1x = -30;
					action.angle2x = 10;
					m = getActionNamed ("scapula upper rotation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
					m = getActionNamed ("shoulder adduction");
					if (m != null)
						action.addAccompanyingAction (m, 0.3f);
				} else if (action.function1 == "100") { 
					action.angle1x = -30;
					action.angle2x = 10;
					m = getActionNamed ("scapula downward rotation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
					m = getActionNamed ("shoulder adduction");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "101") { 
					action.angle1x = -30;
					action.angle2x = 10;
					m = getActionNamed ("scapula downward rotation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
					m = getActionNamed ("shoulder abduction");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "111") { 
					action.angle1x = -10; //keep this value
					action.angle1z = 3; //keep this value 
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "shoulder horizontal abduction") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "0") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "1") { 
					action.angle1y = -20;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			} 
			else if (name == "shoulder horizontal adduction") {
				//it's basic accompanying action
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m = getActionNamed ("scapula protraction");
				if (m != null)
					action.addAccompanyingAction (m, 1);
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "01") { 
					action.angle1x = 0;
					action.angle1y = 60;
					action.angle1z = 0;
				} else if (action.function1 == "10") { 
					action.angle1x = 0;
					action.angle1y = 30;
					action.angle1z = 0;
				} else if (action.function1 == "11") {
					action.angle1x = 0;
					action.angle1y = 90;
					action.angle1z = 0;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			}

			///////////////////// ELBOW AND FOREARM ///////////////
			//ba3d mashaghal el 7agat l msh mwgoda , lesa hazbot el angles wasa7a7ha
			else if (name == "elbow flexion") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1x = -90;
					actionObjectScript m = getActionNamed ("elbow pronation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "10") {
					action.angle1x = -110;
				} else if (action.function1 == "11") {
					action.angle1x = -130;
				} 
			}
			else if (name == "elbow extension") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00") { //140
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") {  //140:90
					action.angle1x = 60;
				} else if (action.function1 == "10") { //140:50
					action.angle1x = 90;
					//actionObjectScript m = getActionNamed ("wrist extension");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") {	//140:0
					action.angle1x = 140;
				}
			}
			else if (name == "elbow pronation") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1y = 45;
				} else if (action.function1 == "11") {
					action.angle1y = 90;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			}
			else if (name == "elbow supination") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				}
				else if (action.function1 == "01") { 
					action.angle1y = -45;
				} else if (action.function1 == "10") {
					action.angle1y = -90;
					actionObjectScript m;
					m = getActionNamed ("elbow flexion");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") {
					action.angle1y = -90;
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			}
			else if (name == "wrist flexion") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m;
				if (action.function1 == "00") {
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "01") { 
					action.angle1x = -70;
					action.angle1z = 15;
					m = getActionNamed ("ulnar deviation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "10") { 
					action.angle1x = -70;
					action.angle1z = 15;
					m = getActionNamed ("radial deviation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") { 
					action.angle1x = -70;
					action.angle1z = 15;
				} 
				if (action.function2 == "1") { // E
					m = getActionNamed ("wrist flexion SM");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			}
			else if (name == "wrist flexion SM") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m;
				if (action.function1 == "0000"){
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "0001") { 
					action.angle1x = -70;
					action.angle1z = 15;
					//m = getActionNamed ("thumb flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "0010") {
					action.angle1x = -70;
					action.angle1z = 15;
					//m = getActionNamed ("fingers flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "0011") {
					action.angle1x = -70;
					action.angle1z = 15;
					//m = getActionNamed ("thumb flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
					//m = getActionNamed ("fingers flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "0100") {
					action.angle1x = -70;
					action.angle1z = 15;
					//m = getActionNamed ("thumb abduction");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "0111") {
					action.angle1x = -70;
					action.angle1z = 15;

					//starting from here needs revising TODO !
				} else if (action.function1 == "1001") {
					action.angle1x = -30;
					//m = getActionNamed ("thumb flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "1010") {
					action.angle1x = -30;
					//m = getActionNamed ("fingers flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "1011") {
					action.angle1x = -30;
					//m = getActionNamed ("fingers flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
					//m = getActionNamed ("thumb flexion");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "1100") {
					action.angle1x = -30;
					//m = getActionNamed ("thumb abduction");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				}  if (action.newc == 'R') {
					action.angle1y = - action.angle1y;
					action.angle1z = - action.angle1z;
				}
			}
			else if (name == "wrist extension") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m;
				if (action.function1 == "00"){
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "01") { 
					action.angle1x = 80;
					m = getActionNamed ("radial deviation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "10") { 
					action.angle1x = 80;
					m = getActionNamed ("ulnar deviation");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") { 
					action.angle1x = 80;
				}
				if (action.function2 == "1") { 
					//action.angle1y = 80;
					m = getActionNamed ("wrist extension SM");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				}  
			}
			else if (name == "wrist extension SM") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				actionObjectScript m;
				if (action.function1 == "00"){
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "01") { 
					action.angle1x = 30;
					//m = getActionNamed ("thumb extension");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "10") { 
					action.angle1x = 30;
					//m = getActionNamed ("fingers extension");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") { 
					action.angle1x = 30;
					//m = getActionNamed ("thumb extension");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
					//m = getActionNamed ("fingers extension");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} 
			}
			else if (name == "ulnar deviation") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00"){
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "01") { 
					action.angle1z = 40;
					actionObjectScript m = getActionNamed ("wrist extension");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "10") { 
					action.angle1z = 40;
					actionObjectScript m = getActionNamed ("wrist flexion");
					if (m != null)
						action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") { 
					action.angle1z = 40;
				} 
			}
			else if (name == "radial deviation") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "0"){
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "1") { 
					action.angle1z = -30;
				} 
			}
			else if (name == "radial deviation SM") {
				action.accompayingActions.Clear ();
				action.accompayingWeights.Clear ();
				if (action.function1 == "00"){
					action.angle1x = 0;//this
					action.angle1y = 0;
					action.angle1z = 0;
				} else if (action.function1 == "01") { 
					action.angle1z = -15;
				} else if (action.function1 == "10") { 
					action.angle1z = -30;
					//m = getActionNamed ("thumb extension");
					//if (m != null)
					//	action.addAccompanyingAction (m, 1);
				} else if (action.function1 == "11") { 
					//missing in Excellsheet
				}

			}



		}
	}
	[ObfuscateLiterals]
	private actionObjectScript getActionNamed(string s){
		// ana el mfrod lesa ha3ml l scapula actions dol manual 3ashan homa msh mwgodin
		int count = this.transform.childCount;
		for (int i = 0; i < count; i++) {
			string name = this.transform.GetChild (i).GetChild (1).GetComponent<Text> ().text;
			if (name == s)
				return this.transform.GetChild (i).GetComponent<actionObjectScript> ();
		}
		Debug.Log ("Action not found!");
		return null;
	}
	[ObfuscateLiterals]
	private List <CurveStruct> getCurveStructOfAction(string name){
		for (int i = 0; i < curvesDataBase.transform.GetComponent<curvesDB> ().DB.Count ; i++) {
			if (curvesDataBase.transform.GetComponent<curvesDB> ().DB [i].actionName == name)
				return curvesDataBase.transform.GetComponent<curvesDB> ().DB [i].Curves;
		}
		Debug.Log ("error can't be!");
		return null;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//EventTrigger da by3ml block lel event 3an el parent bta3o
//3ashan hwa bya5od kol l functions el y3ozha we mask el ba2i
//	IPointerClickHandler fl script el tani shghal kois we msh by3ml block leh ?


public class menuItemHoverChangeColor  : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler{

	Image img = null;
	Color c;

	//on muscles in MUSCLES menu only till now
	public void OnPointerEnter(PointerEventData evd){
		if (img == null)
			img =  this.transform.GetComponent<Image>();
		c.r = img.color.r;
		c.g = img.color.g;
		c.b = img.color.b;
		c.a = img.color.a + 0.15f;
		img.color= c;
	}
	public void OnPointerExit(PointerEventData evd){
		c.a = img.color.a - 0.15f;
		if (c.a < 0)
			c.a = 0.0f;
		img.color = c;
	}


}


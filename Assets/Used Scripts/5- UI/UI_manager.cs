﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Beebyte.Obfuscator;
public class UI_manager : MonoBehaviour {

	public GameObject Rigg3D;

	public movementManager MovManager;
	public rotationManager RotManager;

	public GameObject musclesMenuContent; 
	public GameObject actionsMenuContent;
	public GameObject prefab_box;
	public GameObject prefab_box2;

	public GameObject boxesParent;
	public List <GameObject> boxes = new List <GameObject> ();

	public Sprite [] sprites = new Sprite [4];
	//list of selected (menuObjects)
	private List <GameObject> selected = new List <GameObject> ();

	Color darktxt = new Color (0.196f, 0.196f, 0.196f, 1.0f);
	Color brighttxt = new Color (0.9f, 0.9f, 0.9f, 1.0f);

	public Transform CameraTr;
	public Transform CameraCenterTr;

    public GameObject actionElementPrefab;

	public Material selectedMuscleMaterial;
	public Material nonSelectedMuscleMaterial;

	// el script da mas2ol 3an ai 7aga liha 3laka bl selection of muscles
	// wel options bta3ethom we keda

	// when something is selected (from the list or the 3d-view) the click will
	// be reported to this manager we hwa ywasal el click di le ba2i el 7gat
	// el fi el brnameg el httghyar be sbab el click di
	// we hwa el byghayar el alwan bta3et el 3dlat, wel material
	// we y3ml l box el hyb2a ta7t 3l shemal


	/// ///////////////////////////////////////////
	//the menus part
	GameObject currentOpenMenu;
	bool menusOpen = false;
	bool insideTheMenusMargins = false; //da 3ashan a3raf ana 3ala menu aw option mnha wla la2
	//3ashan el click el bte2fel l menus atanesh-ha lw ana gowa el menus
	bool globalLight = true;
	bool cameraLight = false;
	public Sprite lightbg;
	public Sprite darkbg;
	//called from outside - by mouse events in MenuClickPublisher on the menus images
	[ObfuscateLiterals]
	public void openMenu(GameObject go){
		//Debug.Log (go.transform.GetChild (1).GetComponent<Text> ().text.ToString ());
		if (!menusOpen) {
			currentOpenMenu = go;
			menusOpen = true;
			//Debug.Log (go.ToString ());
			int child = go.transform.childCount;
			for (int i = 2; i < child; i++)
				go.transform.GetChild (i).gameObject.SetActive (true);
			go.transform.GetChild (0).GetComponent<Image> ().sprite = darkbg;
		}
		else if (menusOpen){
			int child = currentOpenMenu.transform.childCount;
			menusOpen = false;
			for (int i = 2; i < child; i++)
				currentOpenMenu.transform.GetChild (i).gameObject.SetActive (false);
			currentOpenMenu.transform.GetChild (0).GetComponent<Image> ().sprite = lightbg;
			currentOpenMenu = null;
		}

	}
	public void setCurrentOpenMenu(GameObject go){
		//lw msh hya hya , hatfi el kont fiha wafta7 el gdida
		if(!menusOpen) return;
		if (go != currentOpenMenu) {
			int child = currentOpenMenu.transform.childCount;
			menusOpen = false;
			for (int i = 2; i < child; i++)
				currentOpenMenu.transform.GetChild (i).gameObject.SetActive (false);
			currentOpenMenu.transform.GetChild (0).GetComponent<Image> ().sprite = lightbg;
			currentOpenMenu = null;
			openMenu (go);
		}

	}
	[ObfuscateLiterals]
	public void closeOpenMenu(){
		if(!menusOpen) return;
		if (insideTheMenusMargins)
			return;
		int child = currentOpenMenu.transform.childCount;
		menusOpen = false;
		for (int i = 2; i < child; i++)
			currentOpenMenu.transform.GetChild (i).gameObject.SetActive (false);
		currentOpenMenu.transform.GetChild (0).GetComponent<Image> ().sprite = lightbg;
		currentOpenMenu = null;

	}
	public void ignoreTheNormalclosingClick(bool t){
		insideTheMenusMargins = t;
	}
	/// ///////////////////////////////////////////

	void Start () {
		

	}
	[ObfuscateLiterals]
	void FixedUpdate () {
		//for the menus (the only solution i had in mind)
		//TODO el 7l el tani eni a3adi 3ala kol l events el fl barnameg
		// el bta5od el mouse click , wa5aliha tb3at lel manager
		// t2olo e2fl l menus :/
		if (Input.GetMouseButtonDown (0)) {
			closeOpenMenu ();
		}
			
		//TODO nfakar fiha
		// lazm l 7ta el ht5tar el lon 
		// tb2a 3arfa hwa dysfunctional wla la2
		//			  hwa selected wla la2
		//		      wel slidervalue be kam dlwa2ty -> da el hy5ali l muscle di liha mkan fl list di 
	}

	// called from outside - from the box toggle function
	// or the lists toggle functions
	[ObfuscateLiterals]
	public void visibilityToggleChanged(GameObject go, bool val){
		//y3adi 3ala kol el visibilty toggles el bayna fl shasha bta3et el 7aga
		// el etghyaret (y3ni lw fi togle fl box we fl list , y5ali el 2 nafs el value
		// search fi el list of boxes we fi el menu objects list 3an el go da 3ashan adihom l value di
		GameObject menuObject = null;
		menuObject = getMenuObject (go);
		GameObject boxObject = null;
		boxObject = getBoxObject (menuObject);

		//menuObject.transform.GetChild (1).GetChild(1).GetComponent<Toggle> ().isOn = val;
		if (boxObject != null)
			boxObject.transform.GetChild (3).GetComponent<Toggle> ().isOn = val;
		//---

		//TODO - visibility toggle stuff 2
		//ynafez el 7aga 
		// for now , 3ashan el mwdo3 da 3an el materials we da lesa hytghayar ktir
		SkinnedMeshRenderer dumm = go.GetComponent<SkinnedMeshRenderer>();
		dumm.enabled = val;
		//Debug.Log(go.ToString() + " visibility is being set to " + val.ToString());
	}
	[ObfuscateLiterals]
	public void dysfunctionalToggleChanged(GameObject go, bool val){
		//y3adi 3ala kol el dysfunction toggles el bayna fl shasha bta3et el 7aga
		// el etghyaret (y3ni lw fi togle fl box we fl list , y5ali el 2 nafs el value
		GameObject menuObject = null;
		menuObject = getMenuObject (go);
		GameObject boxObject = null;
		boxObject = getBoxObject (menuObject);

		//menuObject.transform.GetChild (1).GetChild(0).GetComponent<Toggle> ().isOn = val;
		if (boxObject != null)
			boxObject.transform.GetChild (4).GetComponent<Toggle> ().isOn = val;
		//---

		//ynafez el 7aga 
		muscleControllersData_Managed dumm = go.GetComponent<muscleControllersData_Managed>();
		dumm.setDysfunctional (!val);

		// hna ha5ali lon l object gray lw hya dysfunctional
		// lw functional white
		//TODO lma a deselect 7aga lazm ashof hya dysfunctional wla functional 3ashan a5liha abyad
		//	   wala gray
		if (!val) { // -> dysfunctional = false 
			go.GetComponent<Renderer> ().material.color = new Color32 (92, 89, 89, 1);
		} else {
			go.GetComponent<Renderer> ().material.color = Color.white;
		}


		actionsMenuContent.GetComponent<populateActionsTab> ().calculateDisfunc ();
		actionsMenuContent.GetComponent<populateActionsTab> ().calculateActionAngles ();

		//Debug.Log(go.ToString() + " functionality is being set to " + val.ToString());
	}
	
	[ObfuscateLiterals]
    private void DestroyActionControllers()
    {
        GameObject hoverWindow = GameObject.FindGameObjectWithTag("hoverWindows") as GameObject;
        for (int i = 0; i < hoverWindow.transform.childCount; i++)
        {
            GameObject.Destroy(hoverWindow.transform.GetChild(0).gameObject);
        }
    }
	[ObfuscateLiterals]
	//called from outside - btetndeh mn l click 3ala el menu items fl list (bta3et el muscles)
	//TODO --  aw mn click fl 3d-view 3la muscle .. (another function 3ashan l parameters htb2a gameObject msh menuObject - 3ashan el 3dView mfihosh menuObjects)
	public void addSelection(GameObject go , bool ctrl){
		//el go da hwa el menuObject lesa msh el object nafso, we di 7aga koisa
		if (!ctrl){
			// lw mfish ctrl 
			// yb2a 3adi 3ala el list of selections e3del alwanhom
			foreach (GameObject selectedGo in selected)
				deSelectMenuObject (selectedGo);
			// we emsa7 el list kolha
			selected.Clear();
			// we 7ot da bas fl list
			if (go.GetComponent<menuObjectScript>() !=null) //if it was a muscle menu object
				selectMenuObject (go);
			else if (go.GetComponent<actionObjectScript>() != null) //if it was a action menu object
				selectActionObject(go);
			selected.Add(go);

            //TODO  -- wefta7 box da
            if (go.GetComponent<menuObjectScript>() != null) //if it was a muscle menu object
                createBox(go);
            else if (go.GetComponent<actionObjectScript>() != null)   //if it was a action menu object
            {
                Utils.OpenActionController(go, new Vector3(-250, 950, 0), ctrl);
                //createBox2(go);
            }
		}
        // hya de lee m4 if else
		if (ctrl){
			// lw fi ctrl yb2a dawar el awel da fl list wla la2
			int c = searchInSelectedList(go);
			if (c != -1) {
				// lw fl list yb2a e3del lono hwa bas we shilo mn list	
				// we 7ot da bas fl list
				if (go.GetComponent<menuObjectScript>() !=null) //if it was a muscle menu object
					deSelectMenuObject (go);
				else if (go.GetComponent<actionObjectScript>() != null) //if it was a action menu object
					deSelectActionObject(go);
				selected.RemoveAt (c);

				showSelectedActionMuscles ();
			}
			//TODO -- we2fel l box bta3o
            //fen else y baba?
			if (c == -1){
                // lw msh fl list yb2a e3del lono hwa we zawedo 3l list
                // we 7ot da bas fl list
                if (go.GetComponent<menuObjectScript>() != null) //if it was a muscle menu object
                    selectMenuObject(go);
                else if (go.GetComponent<actionObjectScript>() != null) //if it was a action menu object
                {
                    selectActionObject(go);
                    Utils.OpenActionController(go, new Vector3(-250, 950, 0),ctrl);
                }
				selected.Add (go);

				showSelectedActionMuscles ();
			}
			//TODO -- wefta7lo box gdid 
			// msh hafta7lo box gdid -> mfish l klam da dlwa2ty 

		}
			
		//lw gali null y3ni dost fi 7etta fadya (hab2a a3ml event yb3at el function di)
		// yb2a e3del alwan kol l fl list we shilhom klohom we 5ali l lsit fadya
		// -- we2fel kol l boxes
		// -- we3del alwan l selected fl 3d-view
	}
	[ObfuscateLiterals]
	public void showSelectedActionMuscles(){
		//el function di ma3mola be este3gal ma3lesh
		for (int u = 0; u < Rigg3D.transform.childCount; u++) {
			Rigg3D.transform.GetChild (u).gameObject.SetActive (false);
		}

		for (int hh = 0; hh < selected.Count; hh++) {
			GameObject go = selected[hh];
			//hide all objects except his muscles , and leave all bones of course
			int o = Rigg3D.transform.childCount;
			List <muscleControllersData_Managed> s = go.GetComponent<actionObjectScript> ().mc;
			for (int i = 0; i < o; i++) {
				bool t = false;
				if (Rigg3D.transform.GetChild (i).gameObject.tag == "bone" || Rigg3D.transform.GetChild (i).gameObject.tag == "range" ) {
					t = true;
					Rigg3D.transform.GetChild (i).gameObject.SetActive (t);
					continue;
				}
				else {
					for (int k = 0; k < go.GetComponent<actionObjectScript> ().mc.Count; k++) {
						if (go.GetComponent<actionObjectScript> ().mc [k].gameObject == Rigg3D.transform.GetChild (i).gameObject)
							t = true;
						if (t)
							break;
					}
					for (int k = 0; k < go.GetComponent<actionObjectScript> ().accompayingActions.Count; k++) {
						for (int h = 0; h < go.GetComponent<actionObjectScript> ().accompayingActions[k].mc.Count; h++){
							if (go.GetComponent<actionObjectScript> ().accompayingActions [k].mc[h].gameObject == Rigg3D.transform.GetChild (i).gameObject)
								t = true;
							if (t)
								break;
						}
					}
				}
				if (t)
					Rigg3D.transform.GetChild (i).gameObject.SetActive (t);
			}
		}

		for (int u = 0; u < Rigg3D.transform.childCount; u++) {
				if (Rigg3D.transform.GetChild (u).gameObject.tag == "bone" || Rigg3D.transform.GetChild (u).gameObject.tag == "range" ) {
					Rigg3D.transform.GetChild (u).gameObject.SetActive (true);
			}
		}

	}
	[ObfuscateLiterals]
	//called from outside - onClick in the Muscles tab panel fi ai 7ta fadia
	public void deselectAll(){
		foreach (GameObject selectedGo in selected)
			deSelectMenuObject (selectedGo);
		// we emsa7 el list kolha
		selected.Clear();
	}
	[ObfuscateLiterals]
	//called from outside - by the search box onValueChanged event
	public void muscleSearch(string text){
		//Debug.Log (text);
		text = text.ToLower();
		int children = musclesMenuContent.transform.childCount;
		for (int j = 0; j < children; j++) {
			GameObject g = musclesMenuContent.transform.GetChild (j).gameObject;
			string name = g.GetComponent<menuObjectScript> ().reference.name.ToLower();
			int firstCharacter = name.IndexOf(text);
			if (firstCharacter == -1)
				g.SetActive (false);
			else
				g.SetActive (true);
		}
	}
	[ObfuscateLiterals]
	//called from outside - by the search box onValueChanged event
	public void actionSearch(string text){
		//Debug.Log (text);
		text = text.ToLower();
		int children = actionsMenuContent.transform.childCount;
		for (int j = 0; j < children; j++) {
			GameObject g = actionsMenuContent.transform.GetChild (j).gameObject;
			string name = g.transform.GetChild(1).GetComponent<Text>().text.ToLower();
			int firstCharacter = name.IndexOf(text);
			if (firstCharacter == -1)
				g.SetActive (false);
			else
				g.SetActive (true);
		}
	}
	[ObfuscateLiterals]
	private GameObject getMenuObject(GameObject go){
		for (int i = 0; i < musclesMenuContent.transform.childCount; i++) {
			if (musclesMenuContent.transform.GetChild (i).GetComponent<menuObjectScript> ().reference == go)
				return musclesMenuContent.transform.GetChild (i).gameObject;
		}
		Debug.Log ("error!");
		return null;
	}
	[ObfuscateLiterals]
	private GameObject getBoxObject(GameObject menuObject){
		for (int i = 0; i < boxes.Count; i++) {
			if (boxes [i].GetComponent<boxReferenceObject> ().reference == menuObject)
				return boxes [i].gameObject;
		}
		Debug.Log ("don't have a box yet!");
		return null;
	}
	private void normalClickin3Dspace(){
		//TODO: da hyb2a el implementation , en lw el mouse down a5od mkano 
		// we lw b2a up fi range oryb awi mn mkano , a3tber di click fl 3dspace
		// sa3etha bs a3mlha Raycast , lw hit a3ml select
		// lw msh hit a3ml deselct all	
		// bema3na eni hashil l click wel raycast da mn l selection script 5ales

		// 3aiz a3mlha event 3ashan msh 3aiz a7otaha fi update
	}
	[ObfuscateLiterals]
	public void setCamera(Vector3 center, float xRot, float yRot, float zoomDistance){
		if (true) {
			CameraCenterTr.position = center;
			CameraCenterTr.GetComponent<newCameraRotation> ().setRot (xRot, yRot);
			CameraTr.GetComponent<newCameraScript> ().setDistance (zoomDistance);
		}
	}
	private void selectMenuObject(GameObject go){
		// nghyar lon l menuObject nfso
			//set the dark gray color of the background
			GameObject img = go.transform.GetChild(0).gameObject;
			Color bt = img.GetComponent<Image> ().color;
			bt.a = 1.0f;
			img.GetComponent<Image> ().color = bt;
        //set text color
            //GameObject txt = go.transform.GetChild (1).GetChild(2).gameObject;
            GameObject txt = go.transform.GetChild(1).gameObject;
            txt.GetComponent<Text> ().color = brighttxt;

		//set color of 3d-object (dlw2ty kda bas lghayet ma n3ml l materials)
			GameObject go3d = go.GetComponent<menuObjectScript>().reference;
        //go3d is null hna, el mfrod refrence yt7t b2ee?
			//go3d.GetComponent<Renderer>().material.color = Color.yellow;
			go3d.GetComponent<Renderer>().material = selectedMuscleMaterial;

			bool dys = go.GetComponent<menuObjectScript> ().referenceScript.dysfunctional;
			if (dys) 
				go3d.GetComponent<Renderer> ().material.color = new Color32  (92, 89, 89, 1);
        Utils.ShowAllMuscles();
	}
	[ObfuscateLiterals]
	public void showThisActionMuscles(actionObjectScript aos){
		//hide all
		for (int u = 0; u < Rigg3D.transform.childCount; u++) {
			if (Rigg3D.transform.GetChild (u).gameObject.tag != "bone" && Rigg3D.transform.GetChild (u).gameObject.tag != "range" ) 
				Rigg3D.transform.GetChild (u).gameObject.SetActive (false);
		}
		for (int i = 0; i < aos.mc.Count; i++) {
			Rigg3D.transform.Find (aos.mc [i].gameObject.name).gameObject.SetActive (true);
		}
		for (int i = 0; i < aos.accompayingActions.Count; i++) {
			for (int k = 0; k < aos.accompayingActions[i].mc.Count; k++)
				Rigg3D.transform.Find (aos.accompayingActions[i].mc[k].gameObject.name).gameObject.SetActive (true);
		}

	}
	[ObfuscateLiterals]
	private void selectActionObject(GameObject go){
		// tezher kol 7aga we b3den t3di te5fi kol l msh m3a el object da
		GameObject img = go.transform.GetChild(0).gameObject;
		Color bt = img.GetComponent<Image> ().color;
		bt.a = 1.0f;
		img.GetComponent<Image> ().color = bt;
		//set text color
		GameObject txt = go.transform.GetChild (1).gameObject;
		txt.GetComponent<Text> ().color = brighttxt;

		//hide all objects except his muscles , and leave all bones of course
		int o = Rigg3D.transform.childCount;
		for (int i = 0; i < o; i++) {
			bool t = false;
			if (Rigg3D.transform.GetChild (i).gameObject.tag == "bone" || Rigg3D.transform.GetChild (i).gameObject.tag == "range" ) {
				t = true;
				continue;
			}
			else {
				for (int k = 0; k < go.GetComponent<actionObjectScript> ().mc.Count; k++) {
					if (go.GetComponent<actionObjectScript> ().mc [k].gameObject == Rigg3D.transform.GetChild (i).gameObject)
						t = true;
					if (t)
						break;
				}
				for (int k = 0; k < go.GetComponent<actionObjectScript> ().accompayingActions.Count; k++) {
					for (int h = 0; h < go.GetComponent<actionObjectScript> ().accompayingActions[k].mc.Count; h++){
						if (go.GetComponent<actionObjectScript> ().accompayingActions [k].mc[h].gameObject == Rigg3D.transform.GetChild (i).gameObject)
						t = true;
					if (t)
						break;
					}
				}
			}
			Rigg3D.transform.GetChild (i).gameObject.SetActive (t);
		}
	}
	[ObfuscateLiterals]
	private void deSelectActionObject(GameObject go){
		// tezher kol l 7gat
		GameObject img = go.transform.GetChild (0).gameObject;
		Color bt = img.GetComponent<Image> ().color;
		bt.a = 0.0f;
		img.GetComponent<Image> ().color = bt;
		//set text color
		GameObject txt = go.transform.GetChild (1).gameObject;
		txt.GetComponent<Text> ().color = brighttxt;

		//loop 3la kolo e3mlo undhide
		for (int i = 0; i < Rigg3D.transform.childCount; i++) {
			Rigg3D.transform.GetChild (i).gameObject.SetActive (Rigg3D.transform.GetChild(i).transform.gameObject.name != "Sphere_001" && !Rigg3D.transform.GetChild(i).transform.gameObject.name.Contains("Range"));

		}
	}
	[ObfuscateLiterals]
	public void deSelectMenuObject(GameObject go){
		if (go.GetComponent<actionObjectScript> () != null)
			// y3ni da action
			deSelectActionObject (go);
		else {
			// nghyar lon l menuObject nfso
			//set the dark gray color of the background
			GameObject img = go.transform.GetChild (0).gameObject;
			Color bt = img.GetComponent<Image> ().color;
			bt.a = 0.0f;
			img.GetComponent<Image> ().color = bt;
			//set text color
			//GameObject txt = go.transform.GetChild (1).GetChild(2).gameObject;
            GameObject txt = go.transform.GetChild(1).gameObject;
            //txt.GetComponent<Text> ().color = darktxt;
			////set color of the first icon
			//GameObject tog1 = go.transform.GetChild(1).GetChild(0).gameObject;
			//GameObject tog1bg = tog1.transform.GetChild (0).gameObject;
			//GameObject tog1cm = tog1bg.transform.GetChild (0).gameObject;
			//tog1cm.GetComponent<Image> ().sprite = sprites [3];
			//tog1bg.GetComponent<Image> ().sprite = sprites [3];
			////set color of the second icon
			//GameObject tog2 = go.transform.GetChild(1).GetChild(0).gameObject;
			//GameObject tog2bg = tog2.transform.GetChild (0).gameObject;
			//GameObject tog2cm = tog2bg.transform.GetChild (0).gameObject;
			//tog2cm.GetComponent<Image> ().sprite = sprites [1];
			//tog2bg.GetComponent<Image> ().sprite = sprites [1];


			bool dys = go.GetComponent<menuObjectScript> ().referenceScript.dysfunctional;
			//set color of 3d-object (dlw2ty kda bas lghayet ma n3ml l materials)
			GameObject go3d = go.GetComponent<menuObjectScript> ().reference;
			//mo2akatn lghayet ma n3ml l materials
			go3d.GetComponent<Renderer> ().material = nonSelectedMuscleMaterial;
			if (!dys)
				go3d.GetComponent<Renderer> ().material.color = new Color32 (255, 214, 212, 1);
			else 
				go3d.GetComponent<Renderer> ().material.color = new Color32  (92, 89, 89, 1);
		}
	}
	[ObfuscateLiterals]
	private int searchInSelectedList(GameObject go){
		for (int i = 0; i < selected.Count; i++)
			if (selected [i] == go)
				return i;
		return -1;
	}
	[ObfuscateLiterals]
	//for the muscle menu objects
    //This is for opening Muscle Controllers, wrote it on a tight deadline, sorry xD 
    //El 5wl elly ktb el function de xD, ana 8alban on a tight deadline
	private void createBox(GameObject go){
		//dawar 3leh fl list of boxes
		for (int i = 0; i < boxes.Count; i++) {
			if (boxes [i].GetComponent<boxReferenceObject> ().reference == go)
				return;
			// lw mwgod 5las return 
		}

        // lw msh mwgod e3mlo we 7oto fi mkano 
        Utils.DestroyMuscleControllers();
		GameObject dummy = Instantiate(prefab_box);
        //Destroy(dummy.transform.GetChild(4).GetChild(0));
		dummy.GetComponent<boxReferenceObject> ().reference = go;
        dummy.transform.parent = GameObject.FindGameObjectWithTag("hoverWindows").transform;

        /// el function di depend on tartib el objects fl prefab , matghyaroosh!
        //dummy.transform.GetChild(2).GetComponent<Text>().text 
        //	= dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript>().reference.name;
        //dummy.transform.GetChild (3).GetComponent<Toggle> ().isOn = dummy.GetComponent<boxReferenceObject> ().reference.transform.GetChild (3).GetComponent<Toggle> ().isOn;
        //dummy.transform.GetChild (4).GetComponent<Toggle> ().isOn = dummy.GetComponent<boxReferenceObject> ().reference.transform.GetChild (2).GetComponent<Toggle> ().isOn;
        //dummy.transform.GetChild (10).GetComponent<Slider> ().value = dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript>().reference.GetComponent<muscleControllersData_Managed> ().sliderValue;
        //Set Muscle Name
        //boxes.Insert (0, dummy);
        //setBoxes ();
        const int INFO_PANEL_INDEX = 4;
        dummy.transform.GetChild(1).GetComponent<Text>().text = dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript>().reference.name;
        //Set Muscle origin, insertion and function
        dummy.transform.GetChild(INFO_PANEL_INDEX).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text += dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().referenceScript.origin;
		dummy.transform.GetChild (INFO_PANEL_INDEX).GetChild (0).GetChild (0).GetChild (0).GetComponent<OriginInsertionButton> ().setMuscle (dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript> ().referenceScript);
        dummy.transform.GetChild(INFO_PANEL_INDEX).GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>().text += dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().referenceScript.insertion;
		dummy.transform.GetChild (INFO_PANEL_INDEX).GetChild (0).GetChild (0).GetChild (1).GetComponent<OriginInsertionButton> ().setMuscle (dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript> ().referenceScript);
        dummy.transform.GetChild(INFO_PANEL_INDEX).GetChild(0).GetChild(0).GetChild(2).GetComponent<Text>().text += dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().referenceScript.action;
        dummy.transform.GetChild(INFO_PANEL_INDEX).GetChild(0).GetChild(0).GetChild(3).GetComponent<Text>().text += dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().referenceScript.nerve_supply;
		if (!dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript> ().referenceScript.nerve_root.Equals("")) {
			dummy.transform.GetChild (INFO_PANEL_INDEX).GetChild (0).GetChild (0).GetChild (4).GetComponent<Text> ().text = "Nerve Root: " + dummy.GetComponent<boxReferenceObject> ().reference.GetComponent<menuObjectScript> ().referenceScript.nerve_root;
		}
        //Settnig Visibilty and function toggles
        //dummy.transform.GetChild (7).GetChild(0).GetComponent<TriggerButton> ().enable = dummy.GetComponent<boxReferenceObject> ().reference.transform.GetChild (1).GetChild(1).GetComponent<Toggle> ().isOn;
        //dummy.transform.GetChild (7).GetChild(1).GetComponent<TriggerButton> ().enable = dummy.GetComponent<boxReferenceObject> ().reference.transform.GetChild (1).GetChild(0).GetComponent<Toggle> ().isOn;
        //Now putting the actions in the action view
        foreach (actionObjectScript action in dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().actions)
        {
            GameObject actionElement = Instantiate(actionElementPrefab);
            actionElement.transform.GetChild(0).GetComponent<Text>().text = action.actionName;
            actionElement.GetComponent<MuscleActionExecuter>().action = action;
            actionElement.GetComponent<MuscleActionExecuter>().muscle = dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().referenceScript;
            actionElement.transform.SetParent(dummy.transform.GetChild(5));
        }
        dummy.SetActive(true);
        
		//el satr da el kan bye5fi kol 7aga m3ada el bones wel muscle el e5tartha
		//for (int i = 0; i < Rigg3D.transform.childCount; i++){
        //    Rigg3D.transform.GetChild(i).gameObject.SetActive(Rigg3D.transform.GetChild(i).name == dummy.GetComponent<boxReferenceObject>().reference.GetComponent<menuObjectScript>().reference.name || Rigg3D.transform.GetChild(i).gameObject.tag == "bone");
        //}

        //Will there be a slider in this controller
	}
	[ObfuscateLiterals]
	public void IsolateToggleChanged(GameObject go, bool val){
		
		for (int i = 0; i < Rigg3D.transform.childCount; i++){
		    Rigg3D.transform.GetChild(i).gameObject.SetActive(Rigg3D.transform.GetChild(i).name == go.name || Rigg3D.transform.GetChild(i).gameObject.tag == "bone" || val);
		}
	}
	[ObfuscateLiterals]
	//for the actions menu objects
	private void createBox2(GameObject go){
		//dawar 3leh fl list of boxes
		for (int i = 0; i < boxes.Count; i++) {
			if (boxes [i].GetComponent<boxReferenceObject> ().reference == go)
				return;
			// lw mwgod 5las return 
		}
		// lw msh mwgod e3mlo we 7oto fi mkano 
		GameObject dummy = Instantiate(prefab_box2);
		dummy.GetComponent<boxReferenceObject> ().reference = go;
		dummy.GetComponent<boxReferenceObject> ().mc = go.GetComponent<actionObjectScript> ().mc;
		dummy.transform.parent = boxesParent.transform;
		dummy.transform.GetChild (2).GetComponent<Text> ().text = go.transform.GetChild(1).GetComponent<Text>().text;
		boxes.Insert (0, dummy);
		setBoxes ();

	}
	[ObfuscateLiterals]
	public void closeBox(GameObject go){
		for (int i = 0; i < boxes.Count; i++) {
			if (boxes [i].GetComponent<boxReferenceObject> ().reference == go) {
				Destroy(boxes[boxes.Count - 1]);
				boxes.RemoveAt(boxes.Count-1);
				setBoxes ();
			}
		}
	}
	[ObfuscateLiterals]
	public void closeAllBoxes(){
		while (boxes.Count > 0){
			Destroy(boxes[0]);
			boxes.RemoveAt(0);
		}
	}
	[ObfuscateLiterals]
	private void setBoxes(){
		int boxWidth = 163 + 20;
		int width = Screen.width - 420; //minus right part width

		int allboxes = boxes.Count * boxWidth;
		while (allboxes > width) {
			// lw mfish mkan wel shasha keda 5alsana
			// hashil a5er wa7ed
			Destroy(boxes[boxes.Count - 1]);
			boxes.RemoveAt(boxes.Count-1);
			allboxes = boxes.Count * boxWidth;
		}

		//set places
		for (int i = 0; i < boxes.Count; i++) {
			int x = 20 + 183 * i;
			boxes [i].transform.position = new Vector3 (x, 0, 0);	
		}
	}

	[ObfuscateLiterals]
	public GameObject getMuscleObject(muscleControllersData_Managed mcdm){
		int musclesCount = Rigg3D.transform.childCount;
		for (int i = 0; i < musclesCount; i++) {
			if (Rigg3D.transform.GetChild (i).transform.GetComponent<muscleControllersData_Managed> () == mcdm)
				return Rigg3D.transform.GetChild (i).gameObject;
		}
		Debug.Log ("not found");
		return null;
	}
}

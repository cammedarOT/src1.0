﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
public class menuItemClickPublisher: MonoBehaviour , IPointerClickHandler{
	bool first = true;
	public UI_manager Manager;

	bool ctrl = false;


	//for a click on an item in the MUSCLE tab menu - only muscles till now
	[ObfuscateLiterals]
	void Update(){
		if (first) {
			first = false;
			Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
			//Debug.Log (Manager.ToString ());
		}

		// makan el event handling bta3 el keys da msh 3agbni , akid fi tri2a tania
		if (Input.GetKeyDown (KeyCode.LeftControl) || Input.GetKeyDown (KeyCode.RightControl))
			ctrl = true;
		if (Input.GetKeyUp (KeyCode.LeftControl) || Input.GetKeyUp (KeyCode.RightControl))
			ctrl = false;

		// bokra
		// dlw2ty ana ha2fesh el control we lma ados 3ala 7aga hab3tha lel function el fl manager

	}
	[ObfuscateLiterals]
	public void OnPointerClick(PointerEventData evd){
		Debug.Log ("Clicked");
		//da el selection trigger el adim
		//GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<selectionScriptUpdated> ().Select (
		//	this.transform.parent.GetComponent<menuObjectScript> ().reference);
		//da el gdid - el ta7t da

		//dlwa2ty ana lma ados , hagi hna fl function
		//hashof hya 3ndha component menuObject script wla la2 (muscle wla action)
		//lw 3ndha , hagib 3dad el children , lw zero akamel wandah el manager 
		//lw msh zero , ha3tberni dost 3la el + toggle wa3ml enable/disable le kol l children
		if (this.transform.parent.gameObject.GetComponent<menuObjectScript> () != null) {
			//y3ni di muscle menu object
			if (this.transform.parent.gameObject.GetComponent<menuObjectScript> ().children.Count == 0) {
				Manager.addSelection (this.transform.parent.gameObject, ctrl);
				float xRot = this.transform.parent.gameObject.transform.GetChild(4).GetComponent<cameraPosition>().xRotation;
				float yRot = this.transform.parent.gameObject.transform.GetChild(4).GetComponent<cameraPosition>().yRotation;
				float zoom = this.transform.parent.gameObject.transform.GetChild(4).GetComponent<cameraPosition>().distanceLelObject;
				Vector3 centrPos = this.transform.parent.gameObject.transform.GetChild(4).GetComponent<cameraPosition>().centerCameraPos;
				Manager.setCamera (centrPos, xRot, yRot, zoom);
			} else { //3ndha children
				// kofta keda h3mlha kda -> lw awel child active , ha3ml disable , lw awel child msh active ha3ml enable
				//TODO hna lw menu fiha el parent bto3 children , el children l ta7t msh byet2eflo
				menuObjectScript c = this.transform.parent.gameObject.GetComponent<menuObjectScript> ();
				if (c.childrenMenuObjects [0].activeSelf == false) {
					for (int i = 0; i < c.childrenMenuObjects.Count; i++) {
						c.childrenMenuObjects [i].SetActive (true);
						// el 3 setoor dool by3mlo refresh lel layoutGroup fa my5rafsh we yb2a dynamic (hwa msh ma3mol eno yb2a dynamic)
						int sib = c.childrenMenuObjects [i].transform.GetSiblingIndex ();
						c.childrenMenuObjects [i].transform.GetComponent<RectTransform> ().SetAsLastSibling ();
						c.childrenMenuObjects [i].transform.GetComponent<RectTransform> ().SetSiblingIndex (sib);
					}
				} else {
					for (int i = 0; i < c.childrenMenuObjects.Count; i++) {
						c.childrenMenuObjects [i].SetActive (false);
					}
				}
			}
		} else { // di action
			Manager.addSelection (this.transform.parent.gameObject, ctrl);
			// msh di :/ mwgoda fi 7ata tania
		}



	}
}

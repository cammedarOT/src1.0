﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
public class actionsItemClickPublisher : MonoBehaviour  , IPointerClickHandler{
	public UI_manager Manager;
	public CameraManager camManager; //for the quick hack 
	public TriggerButton multiSelectEnableButton;
	// Use this for initialization
	[ObfuscateLiterals]
	void Start () {
		//for the quick hack of "shoulder flexion"
		camManager = GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<CameraManager>();
		//
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
		multiSelectEnableButton = GameObject.FindGameObjectWithTag ("multiSelectBtn").GetComponent<TriggerButton> ();

	}
	[ObfuscateLiterals]
	public void OnPointerClick(PointerEventData evd){

		//shoulderflexion camera quickhack
		setCamerasForFlexion_quickHack();

		//di lw child fl heirerchy
		if (this.transform.parent.gameObject.GetComponent<actionObjectScript> ().childrenMenuObjects.Count == 0) {
			//multiSelectEnableButton.enable ma2loba  , 5od el not
			float xRot = this.transform.parent.gameObject.transform.GetChild (4).GetComponent<cameraPosition> ().xRotation;
			float yRot = this.transform.parent.gameObject.transform.GetChild (4).GetComponent<cameraPosition> ().yRotation;
			float zoom = this.transform.parent.gameObject.transform.GetChild (4).GetComponent<cameraPosition> ().distanceLelObject;

			//quickFix -> lw el xRot yRot zoom msh mazbotin , ha3ml return k2eno matda-sh
			if (xRot == 0 && yRot == 0 && zoom == 0)
				return;
			Manager.addSelection (this.transform.parent.gameObject, !multiSelectEnableButton.enable);
			Vector3 centrPos = this.transform.parent.gameObject.transform.GetChild (4).GetComponent<cameraPosition> ().centerCameraPos;
			Manager.setCamera (centrPos, xRot, yRot, zoom);


			if (this.transform.parent.gameObject.GetComponent<actionObjectScript> ().musclesofeachactionMenuObjects.Count != 0) {
				actionObjectScript h = this.transform.parent.gameObject.GetComponent<actionObjectScript> ();

				// a el tri2a el ghbya el kont b3mlha di bta3et lw awel wa7ed true we lw awel wa7ed false -_- a el hbal da
				//eb2a dawar 3leha fi kol 7ta we3delha
				// HASSAN -> hna hattala3 el muscles fl action controller box (lw zorar el multiselect msh metdas)
				// multiSelectEnableButton.enable ma2loba  , 5od el not
				bool bol = !h.musclesofeachactionMenuObjects [0].activeSelf;
				for (int i = 0; i < h.musclesofeachactionMenuObjects.Count; i++)
					h.musclesofeachactionMenuObjects [i].SetActive (bol);
			}
		}

		//di lw parent
		else {
			actionObjectScript ii = this.transform.parent.gameObject.GetComponent<actionObjectScript> ();
			bool activation = !ii.childrenMenuObjects [0].transform.GetChild(0).gameObject.activeSelf;
			//agib state awel wa7ed wa3mlhom klhom 3aksaha
			for (int h = 0; h < ii.childrenMenuObjects.Count; h++) {
				setactiveActionObject (ii.childrenMenuObjects [h], activation);
				//to refresh the content holder (they say online that content holder is not dynamic by itself)
				int sib = ii.childrenMenuObjects [h].transform.GetSiblingIndex ();
				ii.childrenMenuObjects [h].transform.SetAsLastSibling ();
				ii.childrenMenuObjects [h].transform.SetSiblingIndex (sib);
			}
		}
	}
	[ObfuscateLiterals]
	//to set the actionMenuObject as active self or not (a solve for the problem that the actionMenuObject
	//should alwas be activeSelf = true as to have the actionObjectScript running
	private void setactiveActionObject(GameObject menuActionObject, bool tf){
		
		//i can;t disable this menu, but i can make the items invisible
		if (tf == false) menuActionObject.GetComponent<RectTransform>().sizeDelta = new Vector2(208,0); //set height to 0
		else if (tf == true) menuActionObject.GetComponent<RectTransform>().sizeDelta = new Vector2(208,20); 
		//and hide the internals
		for (int k = 0; k < menuActionObject.transform.childCount; k++) {
			menuActionObject.transform.GetChild (0).gameObject.SetActive (tf);
			menuActionObject.transform.GetChild (1).gameObject.SetActive (tf);
		}
	}

	[ObfuscateLiterals]
	private void setCamerasForFlexion_quickHack(){
		string name = this.transform.parent.gameObject.GetComponent<actionObjectScript> ().actionName;
		if (name == "shoulder flexion") {
			camManager.setCameraPosition_oldWay (1, 5.0f, 93.75f, 6.2f);
			camManager.setCameraPosition_oldWay (2, 16.5f, -182.25f, 4.0f, -5.88f, 2.344f, -2.96f);
			camManager.setCameraPosition_oldWay (3, 9.5f, 1.5f, 4.0f, -6.036f, 2.342f, -2.54f);
			camManager.setCameraPosition_oldWay (4, 66.0f, 4.0f, 6.05f, -5.59f, 1.66f, -2.618f);
		}
	}
}

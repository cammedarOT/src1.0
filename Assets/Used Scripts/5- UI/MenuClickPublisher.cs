﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
public class MenuClickPublisher : MonoBehaviour , IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

	bool first = true;
	public UI_manager Manager;
	[ObfuscateLiterals]
	void Update(){
		if (first) {
			first = false;
			Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
			//Debug.Log (Manager.ToString ());
		}

	}
	[ObfuscateLiterals]
	public void OnPointerClick(PointerEventData evd){
		Manager.openMenu (this.transform.parent.gameObject);
	}
	[ObfuscateLiterals]
	public void OnPointerEnter(PointerEventData evd){
		Manager.setCurrentOpenMenu (this.transform.parent.gameObject);
		Manager.ignoreTheNormalclosingClick (true);
	}
	[ObfuscateLiterals]
	public void OnPointerExit(PointerEventData evd){
		Manager.ignoreTheNormalclosingClick (false);
	}
}

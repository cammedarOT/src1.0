﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using Beebyte.Obfuscator;
public class boxReferenceObject : MonoBehaviour {

	public GameObject reference;
	public int width = 163;
	public UI_manager Manager;
    const int ACTION_PLAYER_INDEX = 7;
    const int SLIDER_INDEX = 1;
    //For Debugging;
    static int counter = 0;
	//for actions
	public List <muscleControllersData_Managed> mc = new List <muscleControllersData_Managed>();
	[ObfuscateLiterals]
	void Start(){
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
	}

	public void exitClicked (BaseEventData e){
		//Debug.Log ("click");
		Manager.closeBox(reference);
	}
	[ObfuscateLiterals]
	public void _contractionvalueChanged (){

        //Debug.Log("Entered COntraction VAlue changed");
		if (this.gameObject.tag != "optionsBox2") {
			float contractionValue = this.transform.GetChild (10).GetComponent<Slider> ().value;
			if (reference.GetComponent<menuObjectScript> () != null) { //da muscle 3adia
				//if el advancing enabled fl bone el hya mas2ola 3anha
				reference.GetComponent<menuObjectScript> ().reference.GetComponent<muscleControllersData_Managed> ().sliderValue = contractionValue;
				//else -> sliders htb2a = lastValidSliderValue
			}
		}

		else if (this.gameObject.tag == "optionsBox2") { // da lw action box
			float contractionValue = this.transform.GetChild (ACTION_PLAYER_INDEX).GetChild(SLIDER_INDEX).GetComponent<Slider> ().value;
			//first get the angle value based on the action function stored in the menuObject
			actionObjectScript action = this.reference.GetComponent<actionObjectScript>();
			string name = reference.transform.GetChild (1).GetComponent<Text> ().text;

			//hagib l bone limit rotation script
			bool inRange;
            if (this.reference.GetComponent<actionObjectScript>().one != null)
            {
                if (this.reference.GetComponent<actionObjectScript>().one.gameObject.GetComponent<limitRotation_sphere_Managed>() != null)
                    inRange = this.reference.GetComponent<actionObjectScript>().one.gameObject.GetComponent<limitRotation_sphere_Managed>().advancingEnabled;
                else
                    inRange = true;
            }
            else return;
			//see the accompanying actions advancingEnabled too
			int count = this.reference.GetComponent<actionObjectScript> ().accompayingActions.Count;
			for (int i = 0; i < count; i++) {
				if (this.reference.GetComponent<actionObjectScript> ().accompayingActions [i].one.gameObject.GetComponent<limitRotation_sphere_Managed> () != null)
					inRange = inRange && this.reference.GetComponent<actionObjectScript> ().accompayingActions [i].one.gameObject.GetComponent<limitRotation_sphere_Managed> ().advancingEnabled;
			}

			//if el advancing enabled fl bone el hya mas2ola 3anha
			if (inRange || contractionValue <= action.lastValidSliderVal ) {
				action.sliderValue = contractionValue;
				action.lastValidSliderVal = contractionValue;
				action.updateMusclesAccordingToCurves ();
			}
			//else -> sliders htb2a = lastValidSliderValue
			else {
				//mfrod hna a5ali l slider value gowa el actionobject we ka slider 3adi yb2o l lastvalidvalue
				action.sliderValue = action.lastValidSliderVal;
				action.updateMusclesAccordingToCurves ();
                //This commented line caused me lots of pain to debug, please don't uncomment it if you don't know what are you doing
                //BASSEM!!!!!!!
                //this.transform.GetChild (ACTION_PLAYER_INDEX).GetChild(SLIDER_INDEX).GetComponent<Slider> ().value = action.lastValidSliderVal;
                contractionValue = action.lastValidSliderVal;
			}


			for (int i = 0; i < action.accompayingActions.Count; i++) {
				action.accompayingActions [i].sliderValue = contractionValue * action.accompayingWeights [i];
				action.accompayingActions[i].updateMusclesAccordingToCurves ();
			}
		}
        //Debug.Log("Exited COntraction VAlue changed");
    }

}

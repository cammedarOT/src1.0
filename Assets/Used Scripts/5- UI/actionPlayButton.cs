﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class actionPlayButton :  MonoBehaviour{

	bool play;
	float sinceItReachedMax = 0;
	float maxValue;
	float currentValue;


	// function (t, b, c, d){
	// 		t /= d/2;
	//		if (t < 1) return c/2*t*t*t + b;
	//		t -= 2;
	//		return c/2*(t*t*t + 2) + b;
	// };

	void Start(){
		maxValue = this.transform.parent.GetChild (4).GetComponent<Slider> ().maxValue;
	}
	[ObfuscateLiterals]
	void Update(){
		play = this.gameObject.GetComponent<Toggle> ().isOn;
		if (play) {
			//TODO 3aiz ab2 asta5dem (wafham l awel) el cubic easing function
			//TODO aghyar el icon
			currentValue =	this.transform.parent.GetChild (4).GetComponent<Slider> ().value;
			if (currentValue >= maxValue) {
				sinceItReachedMax += 0.005f;
			} else
				currentValue += 0.005f;
			
			if (sinceItReachedMax > 0.2f) {
				currentValue = 0;
				sinceItReachedMax = 0f;
			}

			this.transform.parent.GetChild (4).GetComponent<Slider> ().value = currentValue;
		}
	}


	// CUBIC EASE IN OUT 
	//t = time , b = start value , c = delta , d = duration
	float values (float t, float b, float c, float d){
		t /= d / 2;
		if (t < 1)
			return c / 2 * t * t * t + b;
		t -= 2;
		return c / 2 * (t * t * t + 2) + b;
	}


}

﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
/// <summary>
/// first animation script - not important.
/// 18-3-2017
/// --
/// *used in Scene1 (the hand on the back) 
/// this script utilizes the sin function to move the gameobject holding the script in a closed loop
/// forth and backwards during runtime
/// public variables are not behaving as expected so i used to hard code the variable numbers into the code.
/// </summary>

public class bassemAnimationScript : MonoBehaviour {

	private Vector3 _startPosition;
	public float animationSpeed = 3.0f;  // lma ba7ot el variable byb2a asra3 msh 3ref leeh :/ akid 7aga liha 3laka bl float wel double
	public float range = 25;
	// Use this for initialization
	[ObfuscateLiterals]
	void Start () {
		_startPosition = transform.position;
	}

	// Update is called once per frame
	[ObfuscateLiterals]
	void Update () {
		//gameObject.transform.Translate (Vector3.left * Time.deltaTime );
		transform.position = _startPosition + new Vector3 (Mathf.Sin(Time.time * 3.0f)/range, 0.0f, 0.0f);
	}
}

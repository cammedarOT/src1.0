﻿
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
/// <summary>
/// this script was supposed to replace the camera movement JavaScript from the tutorial but it's not finished
/// yet, still I have to work on it
/// *Scene1
/// </summary>

public class newCameraScript : MonoBehaviour {

	// Use this for initialization
	/// ZOOM ///////////////////////////
	private float number;
	public float zoomSpeed = 55;
	//==
	public float xtrans;
	public float ytrans;

	private Vector3 refPos;
	public float distanceFromCenter;

	Rect boundingofRenderTexture = new Rect (0,0,Screen.width, Screen.height); 

	void Start () {
		refPos = this.transform.position;
	}
	[ObfuscateLiterals]
	//i will put a button to control enable/disable script
	void Update () {

		///////////////////////////////////////////////////////////
		// Zooming in and out
		if (!EventSystem.current.IsPointerOverGameObject ()) {
			Vector3 s = Input.mousePosition;
			if (boundingofRenderTexture.Contains (s)) {
				number = Input.GetAxis ("Mouse ScrollWheel");
				//this.transform.parent.transform.Translate(Vector3.forward * Time.deltaTime * number * zoomSpeed);
				gameObject.transform.Translate (Vector3.forward * Time.deltaTime * number * zoomSpeed);
				//move the spot light with it -- new
				this.transform.parent.GetChild(0).transform.Translate (Vector3.forward * Time.deltaTime * number * zoomSpeed);
			}
		}

		//3aiz a5ali l dragging da y7awel y2arab le ai muscle aw ai object mwgod keda
		//3ashan mayro7sh fl hawa
		// bs m3mltesh keda lsa
		if (Input.GetMouseButton(1) && !EventSystem.current.IsPointerOverGameObject()){  // right click to rotate
			/////////////////////////////////////////////////////////
			// based on https://www.youtube.com/watch?v=3JsuldsGuNw
			//mouse look orbitting

			//if (boundingofRenderTexture.Contains (Input.mousePosition))
			//	Debug.Log (this.transform.name);
			Vector3 s = Input.mousePosition;
			if (boundingofRenderTexture.Contains (s)) {
				xtrans = Input.GetAxis ("Mouse X");
				ytrans = Input.GetAxis ("Mouse Y");

				//transform.position +=  -transform.right * xtrans / 8; 
				//transform.position += -transform.up * ytrans / 8;

				transform.parent.transform.position += -transform.right * xtrans / 3; 
				transform.parent.transform.position += -transform.up * ytrans / 3;
			}

			///===================================
		}
	}
	[ObfuscateLiterals]
	public void setDistance(float d){
		float distance = Vector3.Distance (this.transform.position, this.transform.parent.transform.position);
		float delta = distance - d;
		gameObject.transform.Translate (Vector3.forward * delta);

	}
	[ObfuscateLiterals]
	public void setRenderTextureRect(Rect r){
		boundingofRenderTexture = r;
	}

}

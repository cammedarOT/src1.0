﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;


/// <summary>
/// this script was supposed to replace the camera movement JavaScript from the tutorial but it's not finished
/// yet, still I have to work on it
/// *Scene1
/// </summary>

public class newCameraRotation : MonoBehaviour {

	/// ORBITTING ///////////////////////////
	public float lookSensitivity = 5;
	public float yRotation;
	public float xRotation;
	public float currentYRotation;
	public float currentXRotation;
	private float yRotationV;
	private float xRotationV;
	public  float lookSmoothDamp = 0.1f;

	public float distance;  // di bs 3ashan ma2lebsh ashof script tani
	public bool resetter = false;

	//==
	Rect boundingofRenderTexture = new Rect (0,0,Screen.width, Screen.height); 
	private Camera myCam;

	private Vector3 refPos;

	void Start(){
		myCam = this.transform.GetChild(0).GetComponent<Camera> ();
		refPos = this.transform.position;
	}
	[ObfuscateLiterals]
	//i will put a button to control enable/disable script
	void Update () {
		
		if (resetter)
			reset ();
		// el event system da byes2al el mouse 3ala 7aga tba3 el UI wla la2
		if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject()){  
			/////////////////////////////////////////////////////////
			// based on https://www.youtube.com/watch?v=3JsuldsGuNw
			// Master Indie youtube channel
			//mouse look orbitting

			//b3d keda hashof leeh el renderTexture bt2oli null (ana ba3mlha casting fl manager btw)
			//b3d keda ha7ot 2 more camera we a3ml mode kaman 
			//b3d keda ashghal el single view (a5alih msh by7ot null lel render texture?)
			Vector3 s = Input.mousePosition;
			if (boundingofRenderTexture.Contains (s)) {
				yRotation += Input.GetAxis ("Mouse X") * lookSensitivity;
				xRotation -= Input.GetAxis ("Mouse Y") * lookSensitivity;
			}
		}
		xRotation = Mathf.Clamp (xRotation, -90, 90);

		currentXRotation = Mathf.SmoothDamp (currentXRotation, xRotation, ref xRotationV, lookSmoothDamp);
		currentYRotation = Mathf.SmoothDamp (currentYRotation, yRotation, ref yRotationV, lookSmoothDamp);

		transform.rotation = Quaternion.Euler (currentXRotation, currentYRotation, 0);
		distance = this.transform.GetChild (0).transform.localPosition.z;
	}
	[ObfuscateLiterals]
	public void reset(){
		yRotation = 0;
		xRotation = 0;
		currentXRotation = 0;
		currentYRotation = 0;
		resetter = false;
		this.transform.position = refPos; 
	}
	[ObfuscateLiterals]
	public void setRot(float x, float y){
		yRotation = y;
		xRotation = x;
	}
	[ObfuscateLiterals]
	public void setRenderTextureRect(Rect r){
		boundingofRenderTexture = r;
	}
	[ObfuscateLiterals]
	public void useRenderTexture(RenderTexture rt){
		myCam = this.transform.GetChild(0).GetComponent<Camera> ();
		myCam.targetTexture = rt;
		myCam.forceIntoRenderTexture = true;
	}
	[ObfuscateLiterals]
	public void freeRenderTexture(){
		myCam.forceIntoRenderTexture = false;
		myCam.targetTexture = null;
	}


}

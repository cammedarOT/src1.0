﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Beebyte.Obfuscator;
using System;

public enum TaskState
{
    SUCCESSS,
    FAULTED,
    RUNNING
};
public class TaskWrapper<T>
{
    private TaskState  state;
    public TaskState State { get { return state; } set { state = value; } }
    private List<string> exceptions;
    public List<string> Exceptions { get { return exceptions; } }
    public TaskWrapper()
    {
        exceptions = new List<string>();
        state = TaskState.RUNNING;
    }
    private T result;
    public T Result { get { return result; } set { result = value; } }
}
public class DatabaseController {
    FirebaseApp app;
    DatabaseReference reference;
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser databaseUser;
    User businessModelUser;
    // Use this for initialization
    public DatabaseController()
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

    }
    private TaskWrapper<User> SignIn(string userName, string password)
    {
        TaskWrapper<User> wrapper = new TaskWrapper<User>();
        auth.SignInWithEmailAndPasswordAsync(userName, password).ContinueWith(task =>
        {
            if (task.IsCanceled || task.IsFaulted)
            {
                Debug.Log(task.Exception);
                wrapper.State = TaskState.FAULTED;
                wrapper.Exceptions.Add(task.Exception.Message);
            }
            else if (task.IsCompleted)
            {
                databaseUser = task.Result;
                User currentUser = new User
                {
                    userId = databaseUser.UserId
                };
                wrapper.Result = currentUser;
                wrapper.State = TaskState.SUCCESSS;
            }

        });
        return wrapper;
    }

    [ObfuscateLiterals]
    private TaskWrapper<bool> GetSignInStateforUserByUid(User user)
    {
        TaskWrapper<bool> wrapper = new TaskWrapper<bool>();
        reference.Child("Users").Child(user.userId).GetValueAsync().ContinueWith(task =>
        {

            if (task.IsFaulted || task.IsCanceled)
            {
                wrapper.State = TaskState.FAULTED;
                wrapper.Exceptions.Add(task.Exception.Message);

            }
            else if (task.IsCompleted)
            {
                DataSnapshot dataSnapshot = task.Result;
                string userId = user.userId;
                user = JsonUtility.FromJson<User>(dataSnapshot.GetRawJsonValue());
                user.userId = userId;
                wrapper.Result = user.IsConfirmed();
                wrapper.State = TaskState.SUCCESSS;
            }
        });
        return wrapper;

    }
    [ObfuscateLiterals]
    public IEnumerator SignInWrapper(string userName, string password, Action signInSuccessAction, Action<string> warningAction)
    {
        var signInWrapper = SignIn(userName, password);
        yield return (new WaitUntil(() => !signInWrapper.State.Equals(TaskState.RUNNING)));
        if (signInWrapper.State.Equals(TaskState.FAULTED))
        {
            warningAction("Error while signing in");
            DebugLogException(signInWrapper.Exceptions);

        }
        else if (signInWrapper.State.Equals(TaskState.SUCCESSS))
        {
            warningAction("Sign in success");
            User currentUser = signInWrapper.Result;
            var getStateWrapper = GetSignInStateforUserByUid(currentUser);
            yield return (new WaitUntil(() => !getStateWrapper.State.Equals(TaskState.RUNNING)));
            if (getStateWrapper.State.Equals(TaskState.FAULTED))
            {
                warningAction("Error while getting data for the user");
                DebugLogException(getStateWrapper.Exceptions);
            }
            else if (getStateWrapper.State.Equals(TaskState.SUCCESSS))
            {
                Debug.Log("Getting Data Success");
                if (getStateWrapper.Result)
                    signInSuccessAction();
                else
                {
                    warningAction("This user is not confirmed yet, or the device id doesn't match");
                    SignOut();
                }
            }
        }
    }
    [ObfuscateLiterals]
    private TaskWrapper<bool> SignUp(User user, string email, string password)
    {
        Debug.Log("Signing up");
        TaskWrapper<bool> wrapper = new TaskWrapper<bool>();
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled || task.IsFaulted)
            {
                wrapper.State = TaskState.FAULTED;
            }
            if (task.IsCompleted)
            {
                var newUser = task.Result;
                user.userId = newUser.UserId;
                wrapper.State = TaskState.SUCCESSS;
            }
        });
        return wrapper;
    }
    [ObfuscateLiterals]
    private TaskWrapper<bool> AddDataToUserByID(User user)
    {
        TaskWrapper<bool> wrapper = new TaskWrapper<bool>();
        reference.Child("Users").Child(user.userId).SetRawJsonValueAsync(JsonUtility.ToJson(user)).ContinueWith(task =>
        {
            if (task.IsCanceled || task.IsFaulted)
            {
                wrapper.State = TaskState.FAULTED;
                wrapper.Exceptions.Add(task.Exception.Message);
            }
            else if (task.IsCompleted)
            {
                wrapper.State = TaskState.SUCCESSS;
                wrapper.Result = true;
            }
        });
        return wrapper;
    }
    [ObfuscateLiterals]
    public IEnumerator SignUpWrapper(User user, string email, string password, Action<User> signUpSuccessAction, Action<string> warningAction)
    {
        var signUpWrapper = SignUp(user, email, password);
        yield return new WaitUntil(() => !signUpWrapper.State.Equals(TaskState.RUNNING));
        if (signUpWrapper.State.Equals(TaskState.FAULTED))
        {
            warningAction("Error While signing up");
            DebugLogException(signUpWrapper.Exceptions);
        }
        else if (signUpWrapper.State.Equals(TaskState.SUCCESSS))
        {
            warningAction("Sign up Success");
            var addDataWrapper = AddDataToUserByID(user);
            yield return new WaitUntil(() => !addDataWrapper.State.Equals(TaskState.RUNNING));
            if (addDataWrapper.State.Equals(TaskState.FAULTED))
            {
                warningAction("Failed to add data to the current user");
                DebugLogException(addDataWrapper.Exceptions);
            }
            else if(addDataWrapper.State.Equals(TaskState.SUCCESSS))
            {
                warningAction("Add Data Sucess");
                signUpSuccessAction(user);
            }

        }
    }
    [ObfuscateLiterals]
    public void ResetPassword(string email,Action<string> warningAction)
    {
        auth.SendPasswordResetEmailAsync(email).ContinueWith(task =>
        {
            if (task.IsCanceled || task.IsFaulted)
            {
                warningAction("Failed to Reset password");
                Debug.Log(task.Exception.Message);
            }
            else if(task.IsCompleted)
            {
                warningAction("Reset password mail was sent");
            }
        });
    }
    [ObfuscateLiterals]
    public void GetMessage(Action<string> action)
    {
        reference.Child("message").GetValueAsync().ContinueWith(task=>{
            if(task.IsCanceled || task.IsFaulted)
            {
                Debug.Log("Failed to get message");
                Debug.Log(task.Exception.Message);
            }
            else if (task.IsCompleted)
            {
                action(task.Result.GetValue(true).ToString());
            }

        });

    }

    private void DebugLogException(List<string> exceptions)
    {
        foreach (string exception in exceptions)
        {
            Debug.Log(exception);
        }

    }
    public void SignOut()
    {
        auth.SignOut();
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class User
{
    public string userId, confirmed,department, deviceID, expectedData, personalEmail, phone, type, universityEmail;
    public bool IsConfirmed()
    {
        return confirmed.ToLower().Equals("true") && deviceID.Equals(SystemInfo.deviceUniqueIdentifier);
    }
}

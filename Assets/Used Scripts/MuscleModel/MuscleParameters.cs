﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface  IMuscleParameters
{

    /*
     * This interface contains the default parameters
     * for all muscles
     * 
     * It contains only the properties for the muscles
     * 
     * It needs to be  inherited to actually define the values of these parameters
     * 
     * This architechture gives lots of flexibility on how you wish to define the values
     * of this parameters
     * ex : if you wish to group a group of muscles within the same properties you simply implement this interface in a common 
     * abstract class then your muscle owuld inherit from this abstract class
     * if you wish for a muscle with unique parameters it can simply implement this interface itself
     */


    // Contractile Element (CE)
    /*************************/

    /*
     * Extensor --> Muscle
     * bellCurve --> normal distrubution
     * v_CE --> veclocity of contractile element
     * F_isom --> isometric force of the muscle
     * 
     * Isometric contraction --> The length of the muscle is constant while it is applying force
     * Concentric Contractions --> The length of the muscle is decreasing while it is applying force
     * Eccentric Contractions --> The length of the muscle is increasing while it is applying force
     */

    /// <summary>
    /// For debugging and identification purposes, gets redefined by every muscle
    /// </summary>
    int MuscleID { get; set; }

    /// <summary>
    /// F_max in [N] for Extensor (Kistemaker et al., 2006)
    /// F_max
    /// </summary>
    float MaxExtensorForce { get; set; }

    /// <summary>
    /// optimal length of CE in [m] for Extensor (Kistemaker et al., 2006)
    /// l_CEopt
    /// </summary>
    float ContractileElementOptimalLength { get; set; }

    /// <summary>
    /// width of normalized bell curve in descending branch (Moerl et al., 2012)
    /// DeltaW_limb_des
    /// </summary>
    float DeltaWidthDescendingLimb { get; set; }

    /// <summary>
    /// width of normalized bell curve in ascending branch (Moerl et al., 2012)
    /// DeltaW_limb_asc
    /// </summary>
    float DeltaWidthAscendingLimb { get; set; }

    /// <summary>
    /// exponent for descending branch (Moerl et al., 2012)
    /// v_CElimb_des
    /// </summary>
    float DescendingBranchExponent { get; set; }

    /// <summary>
    ///  exponent for ascending branch(Moerl et al., 2012)
    ///  v_CElimb_asc
    /// </summary>
    float AscendingBranchExponent { get; set; }

    /// <summary>
    /// parameter for contraction dynamics: maximum value of A_rel(Guenther, 1997, S. 82)
    /// A_rel0
    /// </summary>
    float MaxA_rel { get; set; }

    /// <summary>
    /// parameter for contraction dynamics: maximum value of B_rel(Guenther, 1997, S. 82)
    /// B_rel0
    /// </summary>
    float MaxB_rel { get; set; }

    /// <summary>
    /// relation between F(v) slopes at v_CE=0 (van Soest & Bobbert, 1993)
    /// S_eccentric
    /// </summary>
    float SEccentric { get; set; }

    /// <summary>
    ///  factor by which the force can exceed F_isom for large eccentric velocities(van Soest & Bobbert, 1993)
    ///  Also called the asymptotic force
    ///  F_eccentric
    /// </summary>
    float FEccentric { get; set; }

    //===========================================================================================================//

    // Parallel Elastic Element (PEE)
    /************************/
    /// <summary>
    /// rest length of PEE(Guenther et al., 2007)
    /// l_PEE0
    /// </summary> 
    float PEERestLength { get; set; }

    /// <summary>
    /// rest length of PEE normalized to optimal lenght of CE (Guenther et al., 2007)
    /// L_PEE0
    /// </summary>
    float PEERestLengthNormalized { get; set; }

    /// <summary>
    /// exponent of F_PEE (Moerl et al., 2012)
    /// v_PEE
    /// </summary>
    float PEEExponent { get; set; }

    /// <summary>
    /// force of PEE if l_CE is stretched to deltaWlimb_des (Moerl et al., 2012)
    /// F_PEE
    /// </summary>
    float PEEForce { get; set; }

    /// <summary>
    /// factor of non-linearity in F_PEE (Guenther et al., 2007)
    /// K_PEE
    /// </summary>
    float PEENonLinearityFactor { get; set; }

    //===========================================================================================================//

    // Serial Damping Element (SDE)
    /******************************/

    /// <summary>
    /// xxx dimensionless factor to scale d_SEmax (Moerl et al., 2012)
    /// D_SE
    /// </summary>
    float SerialDampingFactorScale { get; set; }

    /// <summary>
    /// minimum value of d_SE normalised to d_SEmax (Moerl et al., 2012)
    /// R_SE
    /// </summary>
    float MinDampingFactorNormalized { get; set; }

    /// <summary>
    /// maximum value in d_SE in [Ns/m] (Moerl et al., 2012)
    /// d_SEmax
    /// </summary>
    float MaxDampingFactor { get; set; }

    //===========================================================================================================//

    // Serial Elastic Element
    /************************/

    /// <summary>
    /// rest length of SEE in [m] (Kistemaker et al., 2006)
    /// l_SEE0
    /// </summary>
    float RestSEELength { get; set; }

    /// <summary>
    /// relativ stretch at non-linear linear transition (Moerl et al., 2012)
    /// DeltaU_SEEnll
    /// </summary>
    float RelativeStretchNonLinear { get; set; }

    /// <summary>
    /// relativ additional stretch in the linear part providing a force increase of deltaF_SEE0 (Moerl, 2012)
    /// DeltaU_SEEl
    /// </summary>
    float RelativeStretchLinear { get; set; }

    /// <summary>
    /// both force at the transition and force increase in the linear part in [N] (~ 40% of the maximal isometric muscle force)
    /// DeltaF_SEE0
    /// </summary>
    float SEEForce { get; set; }

    //Some derived values, will write their equations in the interface implementation
    float L_SEEnll { get; set; }
    float V_SEE { get; set; }
    float KSEEnl { get; set; }
    float KSEEl { get; set; }
    //===========================================================================================================//
}


public abstract class DefaultMuscleParameters: IMuscleParameters
{
    /*
     * Default Muscle Parameters Class
     * This is an example for a muscle parameters which will be needed by the muscle to calculate force
     * Some parameters can be setted in the interface, but in this model they can't be set, so an excpetion is thrown if you try to set them
     * I think none of these params need to be set in the first place, If so I will remove all setters
     */
    public abstract int MuscleID { get; set; }

    private float maxExtensorForce = 1420;
    public float MaxExtensorForce { get { return maxExtensorForce; } set { maxExtensorForce = value; } }

    private float contractileElementOptimalLength = 0.092f;
    public float ContractileElementOptimalLength { get { return contractileElementOptimalLength; } set { ContractileElementOptimalLength = value; } }

    private float deltaWidthDescendingLimb = 0.35f;
    public float DeltaWidthDescendingLimb { get { return deltaWidthDescendingLimb; } set { deltaWidthDescendingLimb = value; } }

    private float deltaWidthAscendingLimb = 0.35f;
    public float DeltaWidthAscendingLimb { get { return deltaWidthDescendingLimb; } set { deltaWidthDescendingLimb = value; } }

    private float descendingBranchExponent = 1.5f;
    public float DescendingBranchExponent { get { return descendingBranchExponent; } set { descendingBranchExponent = value; } }

    private float ascendingBranchExponent = 3.0f;
    public float AscendingBranchExponent { get { return ascendingBranchExponent; } set { ascendingBranchExponent = value; } }

    private float maxA_rel = 0.25f;
    public float MaxA_rel { get { return maxA_rel; } set { maxA_rel = value; } }

    private float maxB_rel = 2.25f;
    public float MaxB_rel { get { return maxB_rel; } set { maxB_rel = value; } }

    private float sEccentric = 2f;
    public float SEccentric { get { return sEccentric; } set { sEccentric = value; } }

    private float fEccentric = 1.5f;
    public float FEccentric { get { return fEccentric; } set { fEccentric = value; } }

    private float pEERestLength;
    public float PEERestLength {
        get
        {
            pEERestLength = pEERestLengthNormalized * contractileElementOptimalLength;
            return pEERestLength;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    private float pEERestLengthNormalized = 0.9f;
    public float PEERestLengthNormalized { get { return pEERestLengthNormalized; } set { pEERestLengthNormalized = value; } }

    private float pEEExponent = 2.5f;
    public float PEEExponent { get { return pEEExponent; } set { pEEExponent = value; } }

    private float pEEForce = 2.0f;
    public float PEEForce { get { return pEEForce; } set { pEEForce = value; } }

    private float pEENonLinearityFactor;
    public float PEENonLinearityFactor {
        get
        {
            pEENonLinearityFactor = pEEForce * (maxExtensorForce / Mathf.Pow((deltaWidthDescendingLimb + 1 - pEERestLengthNormalized), pEEExponent));
            return pEENonLinearityFactor;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    private float serialDampingFactorScale = 0.3f;
    public float SerialDampingFactorScale { get { return serialDampingFactorScale; } set { serialDampingFactorScale = value; } }

    private float minDampingFactorNormalized = 0.01f;
    public float MinDampingFactorNormalized { get { return minDampingFactorNormalized; } set { minDampingFactorNormalized = value; } }

    private float maxDampingFactor;
    public float MaxDampingFactor {
        get
        {
            maxDampingFactor = serialDampingFactorScale * (maxExtensorForce * maxA_rel) / (contractileElementOptimalLength * maxB_rel);
            return maxDampingFactor;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    private float restSEELength = 0.172f;
    public float RestSEELength { get { return restSEELength; } set { restSEELength = value; } }

    private float relativeStretchNonLinear = 0.0425f;
    public float RelativeStretchNonLinear { get { return relativeStretchNonLinear; } set { relativeStretchNonLinear = value; } }

    private float relativeStretchLinear = 0.017f;
    public float RelativeStretchLinear { get { return relativeStretchLinear; } set { relativeStretchLinear = value; } }

    private float sEEForce = 568;
    public float SEEForce { get { return sEEForce; } set { sEEForce = value; } }

    private float l_SEEnll;
    public float L_SEEnll {
        get
        {
            l_SEEnll = (1 + relativeStretchNonLinear) * restSEELength;
            return l_SEEnll;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    private float v_SEE;
    public float V_SEE
    {
        get
        {
            v_SEE = relativeStretchNonLinear / relativeStretchLinear;
            return v_SEE;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    private float kSEEnl;
    public float KSEEnl
    {
        get
        {
            kSEEnl = sEEForce / Mathf.Pow((relativeStretchNonLinear * restSEELength), v_SEE);
            return kSEEnl;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    private float kSEEl;
    public float KSEEl
    {
        get
        {
            kSEEl = sEEForce / relativeStretchLinear * restSEELength;
            return kSEEl;
        }
        set
        {
            throw new InvalidOperationException();
        }
    }

    public DefaultMuscleParameters()
    {
        //pEERestLength = pEERestLengthNormalized * contractileElementOptimalLength;
        //pEENonLinearityFactor = pEEForce *(maxExtensorForce / Mathf.Pow((deltaWidthDescendingLimb + 1 - pEERestLengthNormalized), pEEExponent));
        //maxDampingFactor = serialDampingFactorScale * (maxExtensorForce * maxA_rel) / (contractileElementOptimalLength * maxB_rel);
        //l_SEEnll = (1 + relativeStretchNonLinear) * restSEELength;
        v_SEE = relativeStretchNonLinear / relativeStretchLinear;
        //kSEEnl = sEEForce / Mathf.Pow((relativeStretchNonLinear * restSEELength), v_SEE);
        //KSEEl = sEEForce / relativeStretchLinear * restSEELength;
    }
}


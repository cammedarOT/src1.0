﻿using UnityEngine;
using System.Collections;

public class SHTI1 : MonoBehaviour
{

    public movementManager Manager;

    public Transform centerPt;
    public float radius;
    private Vector3 Ref;

    public float distance;

    Vector3 movement;
    Vector3 newPos;
    Vector3 offset;
    Vector3 vector;

    void Start()
    {
        //after agaza - mmkn ydrab el dnia
        if (radius == 0)
            radius = Mathf.Abs(Vector3.Distance(transform.position, centerPt.position));
        Ref = transform.position;
    }

    void FixedUpdate()
    {

    }



    // called from the manager to determine new pos after limiting distance
    public Vector3 limitThis(Vector3 posAfterCalcs)
    {
        //movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        newPos = posAfterCalcs;
        offset = newPos - centerPt.position;
        //transform.position = centerPt.position + Vector3.ClampMagnitude(offset, radius);
        Vector3 clampedPos = centerPt.position + Vector3.ClampMagnitude(offset, radius);

        distance = Mathf.Abs(Vector3.Distance(clampedPos, centerPt.position));
        if (distance < radius)
        {
            vector = clampedPos - centerPt.position;
            vector = vector.normalized;
            vector *= radius;

            clampedPos = centerPt.position + vector;
        }

        Vector3 deltaa = clampedPos - posAfterCalcs;
        return deltaa;
    }
}

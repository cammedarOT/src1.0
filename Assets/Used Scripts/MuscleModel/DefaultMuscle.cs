﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class DefaultMuscle : DefaultMuscleParameters, ICallable<float, float>
{
    private float initialMuscleLength = 0.092f + 0.172f;
    private float initialActivation = 0.5f;
    //The muscle ID must be set from the controller, put this value here for test purpose
    private int muscleID = 1;
    public override int MuscleID { get { return muscleID; } set { muscleID = value; } }
    private float muscleLength;
    public float MuscleLength { get { return muscleLength; } set { muscleLength = value; } }
    private float activation;
    public float Activation { get { return activation; } set { activation = value; } }
    private float contractionElementLength;
    public float ContractionElementLength { get { return contractionElementLength; } set { contractionElementLength = value; } }
    private float muscleSpeed;
    public float MuscleSpeed { get { return muscleSpeed ; } set { muscleSpeed = value; } }
    private float contractionElementSpeed;
    public float ContractionElementSpeed { get { return contractionElementSpeed; } set { contractionElementSpeed = value; } }


    public DefaultMuscle()
    {
        InitializeMuscleParameters();
    }

    public DefaultMuscle(float initialMuscleLength)
    {
        this.initialMuscleLength = initialMuscleLength;
        InitializeMuscleParameters();
    }
    public DefaultMuscle(float initialMuscleLength , float activation)
    {
        this.initialMuscleLength = initialMuscleLength;
        initialActivation = activation;
        InitializeMuscleParameters();
    }
    private void InitializeMuscleParameters()
    {
        muscleLength = initialMuscleLength;
        contractionElementLength = Mathmatics.CalculateRoot(this, 0, initialMuscleLength, 10e-9f);
        muscleSpeed = 0f;
        contractionElementSpeed = 0f;
    }

    public float Call(float value)
    {
        return InitializeMuscleForceEquilibrium(value, initialMuscleLength, initialActivation);
    }


    /*
     * Some Helper Function
     */

    private float CalculateIsometricForce(float contractionElementLength)
    {
        float isometricForce;
        if (contractionElementLength >= ContractileElementOptimalLength)
        {
            isometricForce = Mathf.Exp(-Mathf.Pow(Mathf.Abs((contractionElementLength / ContractileElementOptimalLength) - 1) / (DeltaWidthDescendingLimb), DescendingBranchExponent));
        }
        else
        {
            isometricForce = Mathf.Exp(-Mathf.Pow(Mathf.Abs((contractionElementLength / ContractileElementOptimalLength) - 1) / (DeltaWidthAscendingLimb), AscendingBranchExponent));
        }
        return isometricForce;
    }

    private float CalculatePEEForce(float contractionElementLength)
    {
        float pEEForce;
        if (contractionElementLength >= PEERestLength)
        {
            pEEForce = PEENonLinearityFactor * Mathf.Pow((contractionElementLength - PEERestLength), PEEExponent);
        }
        else
        {
            pEEForce = 0;
        }
        return pEEForce;
    }

    private float CalculateSEEForce(float contractionElementLength, float muscleLength)
    {
        float sEEForce, sEELength;
        sEELength = Mathf.Abs(muscleLength - contractionElementLength);
        if ((sEELength > RestSEELength) && (sEELength < L_SEEnll))
        {
            sEEForce = KSEEnl * Mathf.Pow((sEELength - RestSEELength), V_SEE);
        }
        else if (sEELength >= L_SEEnll)
        {
            sEEForce = SEEForce + KSEEl * (sEELength - L_SEEnll);
        }
        else
        {
            sEEForce = 0;
        }
        return sEEForce;

    }
    private Pair<float,float> ComputeHillParameters (float activation , float isometricForce, bool eccentric,float p_a_rel , float p_b_rel)
    {
        float a_rel, b_rel;
        a_rel = p_a_rel;
        b_rel = p_b_rel;
        if (!eccentric)
        {
            a_rel = a_rel * MaxA_rel * (1 / 4f) * (1 + 3 * activation);
            b_rel = MaxB_rel * (1 / 7f) * (3 + 4 * activation);
        }
        else if(eccentric)
        {
            b_rel = ((1 - FEccentric) * b_rel) / (SEccentric * (1 + (a_rel / activation * isometricForce)));
            a_rel = -FEccentric * isometricForce;
        }
        return new Pair<float, float>(a_rel, b_rel);
    }
    private List<float> SolveHillQuadraticEquation(float activation , float a_rel , float b_rel,float isometricForce, float pEEForce, float sEEForce)
    {
        float c0, c1, c2, d0;
        d0 = ContractileElementOptimalLength * b_rel * MaxDampingFactor * (MinDampingFactorNormalized * (1 - MinDampingFactorNormalized) * (activation * isometricForce / MaxExtensorForce));
        c2 = MaxDampingFactor * (MinDampingFactorNormalized - (a_rel - pEEForce / MaxExtensorForce) * (1 - MinDampingFactorNormalized));
        c1 = -c2 * muscleSpeed - d0 - sEEForce + pEEForce - MaxExtensorForce * a_rel;
        c0 = d0 * muscleSpeed + ContractileElementOptimalLength * b_rel * (sEEForce - pEEForce - MaxExtensorForce * activation * isometricForce);
        List<float> output = new List<float>(4){ c0, c1,  c2, d0};
        return output;
    }
    private void GetContractionElementSpeed(float activation, float a_rel, float b_rel, float isometricForce, float pEEForce, float sEEForce)
    {
        float c0, c1, c2, d0;
        List<float> quadraticEqautionParams = SolveHillQuadraticEquation(activation, a_rel, b_rel, isometricForce, pEEForce, pEEForce);
        c0 = quadraticEqautionParams[0];
        c1 = quadraticEqautionParams[1];
        c2 = quadraticEqautionParams[2];
        d0 = quadraticEqautionParams[3];

        if ((c1 * c1 - 4 * c2 * c0) < 0)
        {
            contractionElementSpeed = 0;
        }
        else
        {
            contractionElementSpeed = (-c1 - Mathf.Sqrt(c1 * c1 - 4 * c2 * c0)) / (2 * c2);
        }
    }

    /*
    * initial condition for internal degree of freedom(l_CE)
    *=========================================================
    * the initial condition MP(1).l_CE_init is found under the assumtion that 
    * all velocities are zero at the beginning of the simulation and that the
    * force-equilibrium is F_SEE - F_CE - F_PEE = 0.We still to find the value of 
    * l_CE which will make this function = 0
    * l_CE --> contraction element length
    */
    private float InitializeMuscleForceEquilibrium(float contractionElementLength, float muscleLength, float activation)
    {

        float contractionElementForce = MaxExtensorForce * activation * CalculateIsometricForce(contractionElementLength);
        return CalculateSEEForce(contractionElementLength, muscleLength) - contractionElementForce - CalculatePEEForce(contractionElementLength);
    }

    private float CalculateMuscleForce ()
    {
        float isometricForce = CalculateIsometricForce(contractionElementLength);
        float pEEForce = CalculatePEEForce(contractionElementLength);
        float sEEForce = CalculateSEEForce(contractionElementLength, muscleLength);

        float a_rel, b_rel = 0;
        if(contractionElementLength < ContractileElementOptimalLength)
        {
            a_rel = 1f;
        }
        else
        {
            a_rel = isometricForce;
        }
        Pair<float, float> hillParams = ComputeHillParameters(activation, isometricForce, false, a_rel, b_rel);
        a_rel = hillParams.First;
        b_rel = hillParams.Second;
        GetContractionElementSpeed(activation,a_rel,b_rel,isometricForce,pEEForce,sEEForce);

        if(contractionElementSpeed > 0)
        {
            hillParams = ComputeHillParameters(activation, isometricForce, true, a_rel, b_rel);
            a_rel = hillParams.First;
            b_rel = hillParams.Second;
            GetContractionElementSpeed(activation, a_rel, b_rel, isometricForce, pEEForce, sEEForce);
        }
        float cEForce = MaxExtensorForce * (((activation * isometricForce + a_rel) / (1 - contractionElementSpeed / (ContractileElementOptimalLength * b_rel))) - a_rel);
        float sDEForce = MaxDampingFactor * ((1 - MinDampingFactorNormalized) * ((cEForce + pEEForce) / MaxExtensorForce) + MinDampingFactorNormalized) * (muscleSpeed - contractionElementSpeed);
        float muscleForce = sEEForce + sDEForce;

        return muscleForce;
    }

    public float Step(float activation , float dt)
    {
        this.activation = activation;
        float muscleForce = CalculateMuscleForce();
        contractionElementLength += ContractionElementSpeed * dt;
        return muscleForce;
    }



}
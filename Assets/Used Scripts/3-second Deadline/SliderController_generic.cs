﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //required forr the slider

public class SliderController_generic : MonoBehaviour {

	/* 		SCENE BLENDSHAPES CODE
	public int Shapekey = 0;
	//create an object of same type of the script you are trying to ref.
	public ObjectConroller_generic target;

	// to make this the current slider, you have to drag-and-drop it
	// into the reference box in the GUI of unity, like i did to the
	// object reference
	public Slider mainSlider;

	void Start () {
		//debugging only
		//Debug.Log (mainSlider.value);
	}
	
	void Update () {
		//test only , el mfrod yb2a fi slider le kol object
		// aw yb2a slider wa7ed lel object el selected b2a 

		//mmkn a3ml object fady we msh rendered a7ot fih el global vairables
		if (!target.Blendshapes[Shapekey].Equals(null))
			target.Blendshapes [Shapekey] = mainSlider.value;
	}
	*/
	/* - will have to uncomment this function in the ObjectController script
	UpdateBlendShapes();

	and line 65 in selection script 
	mainSlider.value  = hitInfo.transform.gameObject.GetComponent<ObjectConroller_generic>().Blendshapes[0]; //temporary only

	*/

	// CURRENT CODE!

	//needs a myInsertionBone script in the selectable objects
	// and moveTowards script in the bone that will bi grabbed (animation bone)
//	public myInsertionBone muscleBonePointer;
	public moveTowards targetObject;
	public Slider mainSlider;

	void Update () {
	//	if (muscleBonePointer != null)
	//		targetObject = muscleBonePointer.target;

		if (targetObject != null)
			//slider between 1-10
			targetObject.DistanceDelta1 = mainSlider.value;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //required forr the slider

/// <summary>
/// needs:
/// - to assign a "object" tag to all selectable objects (for coloring)
/// - to assign a collider object to every selectable object
/// - to be assigned to an empty game object in the scene
/// 
/// </summary>


public class SelectionScript_generic : MonoBehaviour {

	//the selected game object
	public GameObject selectedObject;

	//the slider script
	public SliderController_generic targetSlider;
	//the dropdown script
	public DropdownController_generic targetDropdown;

	//the real slider
	public Slider mainSlider;

	// Use this for initialization
	void Start () {
	
	}
	
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			//Debug.Log("Mouse is down");

			RaycastHit hitInfo = new RaycastHit();
			bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
			if (hit) 
			{
				//for debugging
				Debug.Log("Hit " + hitInfo.transform.gameObject.name);
//				
				//find all gameobjects to un-color them. may be a better solution
				// would be to keep a trace of the last selected object
				GameObject[] bla = GameObject.FindGameObjectsWithTag("object");
				for (int i = 0; i < bla.Length; i++)
					bla [i].GetComponent<Renderer> ().material.color = new Color32(255, 87, 44, 1);
				hitInfo.transform.gameObject.GetComponent<Renderer>().material.color = Color.yellow;




				//--------after selection logic
				//get current value
			//	mainSlider.value = hitInfo.transform.gameObject.GetComponent<myInsertionBone> ()
			//		.target.GetComponent<moveTowards> ().DistanceDelta1;
				// the slider and dropdown configuration
				selectedObject = hitInfo.transform.gameObject;
			//	targetSlider.muscleBonePointer = selectedObject.GetComponent<myInsertionBone>();
				//targetDropdown.targetObj = selectedObject.GetComponent<ObjectConroller_generic>();

			} else {
				//Debug.Log("No hit");
			}
		} 
	}

}

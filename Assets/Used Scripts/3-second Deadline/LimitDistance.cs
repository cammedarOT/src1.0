﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
public class LimitDistance : MonoBehaviour {
	public Transform centerPt;
	public float radius;

	public float distance;
	public Vector3 Ref;

	Vector3 movement;
	Vector3 newPos;
	Vector3 offset;
	Vector3 vector;

	void Start(){
		//after agaza - mmkn ydrab el dnia
		if(radius == 0)
		radius = Mathf.Abs (Vector3.Distance (transform.position, centerPt.position));
	}
	[ObfuscateLiterals]
	void Update() {
		movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		newPos = transform.position + movement;
		offset = newPos - centerPt.position;
		transform.position = centerPt.position + Vector3.ClampMagnitude(offset, radius);

		distance = Mathf.Abs (Vector3.Distance (transform.position, centerPt.position));
		if (distance < radius) {
			vector = transform.position - centerPt.position;
			vector = vector.normalized;
			vector *= radius;

			transform.position = centerPt.position + vector;

		}


	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//tutorial for event handling 
// http://answers.unity3d.com/questions/852385/onpointerclick-function-not-triggering.html

public class TogglePublisher_1 : MonoBehaviour , IPointerClickHandler{

	public void OnPointerClick(PointerEventData evd){
		//Debug.Log ("clicked");
		this.transform.parent.GetComponent<menuObjectScript> ().sendData (this.GetComponent<Toggle>().isOn);
	}
}

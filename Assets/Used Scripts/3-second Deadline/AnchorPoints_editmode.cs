﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
//this works in edit mode, 3ashan ma3odsh a3ml simulate 3ashan agrab 

//The functions are not called constantly like they are in play mode.
// - Update is only called when something in the scene changed.
// - OnGUI is called when the Game View recieves an Event.
// - OnRenderObject and the other rendering callback functions are called on every repaint of the Scene View or Game View.

// - can't use delta time here

// anchor points code (LOOK AT) works as follows:
// ba7otelha ref le 2 objects , el awlani ya (this) ya object tani el na7ya el tania
// fa yb2a el object dah wa2ef fi el 5at el benhom
// wel ref el tani lel object el habos 3leeh
// el mask lesa msh shaghal 

//lw 3aiz 2 scripts to work together ba7ot parent object wadilo el script el tani
//we asln el look at nafso ba7oto fi parent gameobject 3ashan a3raf aghyar el front bta3o
// wel 3ashan ashghalo :
//		ba7ot el script, wa5alih y look at
//		ba5od el angles watfi el simulation
//		ba5ali el gameobject el parent bl rotation el mfrod y5ali el front bta3o (Z axis) na7yet front el object el ana 3aizo
//		ba5alih parent 3l object l 3adi bta3i


[ExecuteInEditMode]


public class AnchorPoints_editmode : MonoBehaviour {

	//dol el anchor points ( el bones fi unity homa point wa7da )
	//dlwa2ty ana b7othom hard coding 3ashan n5alas bsor3a bas

	//controller point 1 of rotation
	public Transform Bone1base;  //in unity the bone is only a base
	//controller point 2 of rotation
	public Transform Bone2base;  //in unity the bone is only a base

	public Transform testGameObject; 

	private Vector3 Bone1Start;
	private Vector3 Bone2Start;

	private Vector3 onNormal;
	public Quaternion targetRotation;

	public bool X_axis;
	public bool Y_axis;
	public bool Z_axis;

	private Vector3 bone1pos;
	private Vector3 bone2pos;

	public bool once = true;

	[ObfuscateLiterals]
	void Update () { 

		// first execution only
		if (once) {
			Bone1Start = Bone1base.position;
			Bone2Start = Bone2base.position;
			onNormal = Bone2Start - Bone1Start;

			once = false;
		}


		bone1pos = Bone1base.position;
		bone2pos = Bone2base.position;

		//5ali balak dol el mfrod local
		if (X_axis && !Y_axis && !Z_axis) {
			Vector3 projectedPos2;
			// el projection da bytla3 3nd el x = 0, bs ana 3aizha 3nd x = first object's x
			//projectedPos2 = Bone2base.position;
			Vector3 heading = Bone1base.position - Bone2base.position;
			//Vector3 force = Vector3.Project (heading, onNormal.normalized);
			Vector3 force = Vector3.Project (heading, Vector3.right) ;  //wrong !! global

			projectedPos2 = Bone2base.position + force;
			bone2pos = projectedPos2;
			testGameObject.position = projectedPos2;
		}
		else if (!X_axis && Y_axis && !Z_axis) {
			bone1pos.y = Bone1Start.y;
			bone2pos.y = Bone2Start.y;
		}
		else if (!X_axis && !Y_axis && Z_axis) {
			bone1pos.z = Bone1Start.z;
			bone2pos.z = Bone2Start.z;
		}

		targetRotation = Quaternion.LookRotation ( -(bone1pos - bone2pos), transform.up);
		//el tri2a di koisa bs msh 7ases far2 benha we ben eno yb2a el 3 axis 3adi 
		//we mthya2li hynfa3 3adi a5ali kaza 7aga t look at , bs msh nafs l parent b2a , hghyar el 
		// empty game object
		/*
		if (X_axis && !Y_axis && Z_axis) {
			Vector3 saveB1Pos = bone1pos;
			Vector3 saveB2Pos = bone2pos;

			//get rotation arround X
			bone1pos.x = Bone1Start.x;
			bone2pos.x = Bone2Start.x;
			targetRotation = Quaternion.LookRotation ( -(bone1pos - bone2pos), transform.up);
			// rotation for Y relative to my rotation
			Quaternion rotationXrelative = Quaternion.Inverse(transform.rotation) * targetRotation;


			//get rotation for Z
			bone1pos = saveB1Pos;
			bone2pos = saveB2Pos;
			bone1pos.z = Bone1Start.z;
			bone2pos.z = Bone2Start.z;
			targetRotation = Quaternion.LookRotation ( -(bone1pos - bone2pos), transform.up);
			// rotation for Z relative to my rotation 
			Quaternion rotationZrelative = Quaternion.Inverse (transform.rotation) * targetRotation;

			targetRotation = transform.rotation * rotationXrelative * rotationZrelative;

			//targetRotation =  transform.rotation * rotationZrelative;

		}
		*/

		//da by3ml transform.rotation = target.rotation - transform.rotation 3ashan kda msh byfdal ylef 3abit
		this.transform.rotation = targetRotation;
	
	}
}

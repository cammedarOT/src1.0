﻿using UnityEngine;
using System.Collections;

public class TransformLinks : MonoBehaviour {

	//mode translation
	public bool translation__;
	//mode distance
	public bool distance__;

	public Transform target;
	public Transform parentAxis;

	private Vector3 targetPosRef;
	private Quaternion targetRotRef;

	private Vector3 currentTranslation;
	private Vector3 lastTranslation;
	private Vector3 deltaTranslation;
	private Quaternion currentRotation;

	public bool Loc_;
	public bool Rot_;

	private float flag ; 
//	public float MinX_;
//	public float MaxX_;
//	public float MinY_;
//	public float MaxY_;
//	public float MinZ_;
//	public float MaxZ_;

	public bool mimicToLoc;
	public bool mimicToRot;

//	public float MinX;
//	public float MaxX;
//	public float MinY;
//	public float MaxY;
//	public float MinZ;
//	public float MaxZ;

	public float weight;

	public float currentDistance;
	float lastDistance;
	float deltaDistance;

	// Use this for initialization
	void Start () {
		targetPosRef = target.position;
		targetRotRef = target.rotation;

		currentTranslation = Vector3.zero;
		lastTranslation = Vector3.zero;

		///////////////////////

//		currentDistance = Vector3.Distance (this.transform.position, target.position);
//		lastDistance = currentDistance;

	
	}
	
	// Update is called once per frame
	void Update () {

		if (translation__) {
			////////////////////////////////
			/// da bya5od el pointer 3ala nafso , el target da 
			/// laken el mfrod yb2a mo3tamed 3ala el bottom controller
			/// fa lesa 3aiz azboto 3ala 7sab el distance b2a ben el bta3 da wel bottom controller
			currentTranslation = target.position - targetPosRef;

			//currentRotation = target.rotation * Quaternion.Inverse (targetRotRef);

			if (currentTranslation.magnitude > lastTranslation.magnitude)
				flag = 1;
			else
				flag = -1;

			deltaTranslation = currentTranslation - lastTranslation;
			lastTranslation = currentTranslation;

			//this.transform.localRotation = Quaternion.AngleAxis ( -weight * currentTranslation.magnitude, transform.position - transform.right);
			transform.RotateAround (transform.position, transform.right, flag * weight * deltaTranslation.magnitude);
			// di a5er 7aga wseltlha
		} else if (distance__) {
//			currentDistance = Vector3.Distance (this.transform.position, target.position);
//			deltaDistance = currentDistance - lastDistance;
//			lastDistance = currentDistance;
//
//			if (currentDistance > lastDistance)
//				flag = 1;
//			else
//				flag = -1;
//
//			transform.RotateAround (transform.position, transform.right, flag * weight * deltaDistance);

		
		}
	}
}

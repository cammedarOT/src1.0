﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System; //for Array.Sort
using Beebyte.Obfuscator;
[System.Serializable]
public class muscleHeir{
	public string name;
	public List <muscleHeir> children = new List <muscleHeir>();
	public bool muscle;

	public Vector3 centerCameraPos;

	public float distanceLelObject = 0; //da 3shan a7seb ldistance washof na2es ad a wa7rak l zoom
									// factor le odam l msafa el fadla (aw le wara :'(
	public float xRotation;
	public float yRotation;

	public muscleHeir (string n, bool m){
		this.name = n;
		this.muscle = m;
		//constructor lel transforms dol , ha5alihom l default location bta3 el camera?
//		centerCameraPos = new Vector3 (0f, 0f, 0f);
//		cameraRot = Quaternion.identity;
//		cameraPos = new Vector3 (0.2f, 0.2f, 0.2f);
	}
}

public class findMuscles : MonoBehaviour {
	public GameObject Rig3D;
	public GameObject [] array;
	public GameObject prefab_menuObject;
	public List <muscleHeir> Heirarchy = new List <muscleHeir>();
	[ObfuscateLiterals]
	void Start () {
		
		createHeir ();

		for (int i = 0; i < Heirarchy.Count; i++){
			bool first = true;
			Stack <muscleHeir> myQ = new Stack <muscleHeir>();
			myQ.Push (Heirarchy [i]);
			while (myQ.Count != 0) {
				muscleHeir x = myQ.Pop ();
				GameObject dummy = Instantiate(prefab_menuObject);
				dummy.transform.parent = this.transform;
				if (first) {
					first = false; 
				} else {
					dummy.SetActive (true); //Bassem : was false
				}
				GameObject cameraPos = new GameObject ("Camera Pos");
				cameraPos.transform.parent = dummy.transform;
				cameraPos.AddComponent<cameraPosition>();

				cameraPos.GetComponent<cameraPosition> ().centerCameraPos = x.centerCameraPos;
				cameraPos.GetComponent<cameraPosition> ().distanceLelObject = x.distanceLelObject;
				cameraPos.GetComponent<cameraPosition> ().xRotation = x.xRotation;
				cameraPos.GetComponent<cameraPosition> ().yRotation = x.yRotation;


                //Text textObject = dummy.transform.GetChild (1).GetChild(2).GetComponent<Text>();
                Text textObject = dummy.transform.GetChild(1).GetComponent<Text>();
                textObject.text = x.name;

                textObject.color = Color.white;
				textObject.enabled = true;
				//7maya tania lel object , lw m3ndosh camera position brdo fl release da ha5alih yb2a a7mar 3ashan
				// a5od bali // aw azra2 b2a hna
				if (x.distanceLelObject == 0.0f && x.children.Count == 0) {
					//textObject.color = Color.blue;  // instead , set the initial camera position
					cameraPos.GetComponent<cameraPosition> ().centerCameraPos = new Vector3 (-4.55f, 1.66f, -3.8f);
                    cameraPos.GetComponent<cameraPosition> ().distanceLelObject = 6.99f;
                    cameraPos.GetComponent<cameraPosition> ().xRotation = 0.0f;
                    cameraPos.GetComponent<cameraPosition> ().yRotation = 0.0f;
				}
				for (int j =  x.children.Count - 1; j > -1 ; j--) {
					myQ.Push (x.children [j]);
					dummy.GetComponent<menuObjectScript>().children.Add(x.children[j]);
				}
			}
		}

		//di bgib fiha el menuObjects nafsohom ka children lel menuitems el tnyin l parent bto3hom
		//bdal el muscleHeir el mwgod fl children list , aw m3ah y3ni msh bdalo
		for (int i = 0; i < this.transform.childCount; i++) {
			if (this.transform.GetChild (i).GetComponent<menuObjectScript> ().children.Count != 0) {
				menuObjectScript v = this.transform.GetChild (i).GetComponent<menuObjectScript> ();
				for (int j = 0; j < v.children.Count; j++) {
					v.childrenMenuObjects.Add(getMenuObjectOfName (v.children [j].name));
				}
			}
		}

		//hna mfrod a3adi 3la el layers kolha we kol layer ad5alo ymin shoia
		//we l m3ndosh children bageblo l reference object bta3o
		Queue <GameObject> Layer = new Queue <GameObject>();
		Queue <GameObject> nextLayer = new Queue <GameObject>();
		Vector3 delta = new Vector3 (0.0f, 0.0f, 0.0f);
		for (int i = 0; i < Heirarchy.Count; i++)
			Layer.Enqueue(getMenuObjectOfName (Heirarchy[i].name));
		while (Layer.Count != 0) {
			GameObject c = Layer.Dequeue ();
            // hgib l children bto3o fi next layer
            for (int i = 0; i < c.GetComponent<menuObjectScript>().children.Count; i++)
            {
                nextLayer.Enqueue(c.GetComponent<menuObjectScript>().childrenMenuObjects[i]);
                //c.transform.GetChild(1).GetChild(2).GetComponent<Text>().color = Color.green;
                //c.transform.GetChild(1).GetComponent<Text>().color = Color.green;
            }
            //lw mkansh 3ndo children hageblo el reference GameObject bta3o
            if (c.GetComponent<menuObjectScript> ().children.Count == 0) {
                //hna el mfrod agib l menuObject
                //string name = c.transform.GetChild(1).GetChild(2).GetComponent<Text>().text;
                string name = c.transform.GetChild(1).GetComponent<Text>().text;
				Transform r = Rig3D.transform.Find (name);
				muscleControllersData_Managed mcd = null;
				if (r) 
					mcd = r.gameObject.GetComponent<muscleControllersData_Managed> ();
				if (r != null && mcd != null) {
					
                //Transform r = Rig3D.transform.Find (name);
				//if (r != null) {
					//Debug.Log (name);
					GameObject t = r.gameObject;
					c.GetComponent<menuObjectScript> ().reference = t;
					c.GetComponent<menuObjectScript> ().referenceScript = t.GetComponent<muscleControllersData_Managed> ();
					c.GetComponent<menuObjectScript> ().referenceScript.name = name;
                    c.GetComponent<menuObjectScript>().actions = GetActionsByMuscle(c.GetComponent<menuObjectScript>().referenceScript);

				} else { //y3ni lw m3ndosh reference dlwa2ty , lesa m7atenahosh fl release di y3ni 
					//7maya lel release mn el errors (el mynfa3sh ndoso a7mar)
                    c.transform.GetChild(1).GetComponent<Text>().color = Color.red;
					c.transform.gameObject.SetActive (false);
                }
            }
            // ha3mlo += el delta

			//Bassem edit here: was not commented
            // Btshift el text m4 el object nfso??????????
			c.transform.GetChild(1).transform.position += delta;
			//check lw Layer 5eles -> hagib kol l fi nextLayer fih wazawed el delta
			if (Layer.Count == 0 && nextLayer.Count != 0){
				for (int i = 0; i < nextLayer.Count; i++){
					GameObject y = nextLayer.Dequeue ();
					Layer.Enqueue (y);
				}
				delta += new Vector3 (15.0f, 0f, 0f);
			}
		}

		//di el m3ndosh wla child active ha5alih not active
		for (int i = 0; i < this.transform.childCount; i++) {
			string name = this.transform.GetChild (i).GetChild (1).GetComponent<Text> ().text;
			bool state = findMyState (this.transform.GetChild (i).gameObject);
			this.transform.GetChild (i).gameObject.SetActive (state);
		}

	}
	[ObfuscateLiterals]
	//recursive search :O 
	private bool findMyState(GameObject menuObject){
		if (menuObject.GetComponent<menuObjectScript> ().childrenMenuObjects.Count == 0)
			return menuObject.activeSelf; //base case
		bool activated = false;
		for (int i = 0; i < menuObject.GetComponent<menuObjectScript> ().childrenMenuObjects.Count; i++) {
            // TO or two variables we use | not ||
			activated = activated || findMyState(menuObject.GetComponent<menuObjectScript> ().childrenMenuObjects [i]);
		}
		return activated;
	}
	[ObfuscateLiterals]
    private List<actionObjectScript> GetActionsByMuscle(muscleControllersData_Managed muscle)
    {
        List<actionObjectScript> actions = new List<actionObjectScript>();
        GameObject actionsTabContentPanel = GameObject.FindGameObjectWithTag("ActionsTabContentPanel");
        for (int i = 0; i < actionsTabContentPanel.transform.childCount; i++)
        {
            actionObjectScript actionScript = actionsTabContentPanel.transform.GetChild(i).GetComponent<actionObjectScript>();
            if (actionScript != null)
            {
                foreach (muscleControllersData_Managed actionMuscle in actionScript.mc)
                {
                    if (actionMuscle == muscle)
                    {
                        actions.Add(actionScript);
                        break;
                    }
                }
            }
        }
        return actions;
    }
	[ObfuscateLiterals]
	private GameObject getMenuObjectOfName(string t){
		for (int i = 0; i < this.transform.childCount; i++) {
            //if (this.transform.GetChild (i).transform.GetChild (1).GetChild(2).GetComponent<Text> ().text == t)
            //	return this.transform.GetChild (i).gameObject;
            if (this.transform.GetChild(i).transform.GetChild(1).GetComponent<Text>().text == t)
                return this.transform.GetChild(i).gameObject;
        }
        Debug.Log ("can't be!");
		return null;
	}
		
	//TODO 3
	//a7ot le kol bta3 fih true amaken el camera bta3to
	//camera xRotation
	//camera yRotation
	//camera zoom distance
	//center pos
	[ObfuscateLiterals]
	void createHeir(){
		muscleHeir m0 = new muscleHeir("Muscles of head and neck",false);
		Heirarchy.Add (m0);
		muscleHeir m1 = new muscleHeir("Muscles of back", false);
		Heirarchy.Add (m1);
		muscleHeir m2 = new muscleHeir ("Muscles of upper limb", false);
		Heirarchy.Add (m2);
		muscleHeir m3 = new muscleHeir("Muscles of thorax", false);
		Heirarchy.Add (m3);
		muscleHeir m4 = new muscleHeir("Muscles of abdomen", false);
		Heirarchy.Add (m4);
		muscleHeir m5 = new muscleHeir("Muscles of pelvis", false);
		Heirarchy.Add (m5);
		muscleHeir m6 = new muscleHeir("Muscles of lower limb", false);
		Heirarchy.Add (m6);


		Heirarchy [0].children.Add (new muscleHeir ("Carniofacial muscles",false));//0
		Heirarchy [0].children.Add (new muscleHeir ("Muscles of neck",false));//1
		Heirarchy [1].children.Add (new muscleHeir ("Right extrinsic muscles of back",false));//0
		Heirarchy [1].children.Add (new muscleHeir ("Left extrinsic muscles of back",false));//1
		Heirarchy [1].children.Add (new muscleHeir ("Right intrinsic muscles of back",false));//2
		Heirarchy [1].children.Add (new muscleHeir ("Left intrinsic muscles of back",false));//3
		Heirarchy [2].children.Add (new muscleHeir ("Muscles of right upper limb",false));//0
		Heirarchy [2].children.Add (new muscleHeir ("Muscles of left upper limb",false));//1
		Heirarchy [3].children.Add (new muscleHeir ("Diaphragm",true)); //0
		Heirarchy [3].children.Add (new muscleHeir ("Right muscles of thorax",false));//1
		Heirarchy [3].children.Add (new muscleHeir ("Left muscles of thorax",false));//2
		Heirarchy [4].children.Add (new muscleHeir ("Right muscles of abdomen",false));//0
		Heirarchy [4].children.Add (new muscleHeir ("Left muscles of abdomen",false));//1
		Heirarchy [5].children.Add (new muscleHeir ("Bulbospongiosus",true));//0
		Heirarchy [5].children.Add (new muscleHeir ("Right ischiocavernosus", true));//1
		Heirarchy [5].children.Add (new muscleHeir ("Right cremaster", true));//2
		Heirarchy [5].children.Add (new muscleHeir ("Left ischiocavernosus", true));//3
		Heirarchy [5].children.Add (new muscleHeir ("Left cremaster", true));//4
		Heirarchy [5].children.Add (new muscleHeir ("Right muscles of pelvis", false));//5
		Heirarchy [5].children.Add (new muscleHeir ("Left muscles of pelvis", false));//6
		Heirarchy [5].children.Add (new muscleHeir ("Levator ani", false));//7
		Heirarchy [5].children.Add (new muscleHeir ("Sphincter urethrae", true));//8
		Heirarchy [5].children.Add (new muscleHeir ("Conjoined longitudinal", true));//9
		Heirarchy [5].children.Add (new muscleHeir ("Anal_sphincter__L", true));//10
		Heirarchy [5].children.Add (new muscleHeir ("Anal_sphincter__R", true));//11 //was false
		Heirarchy [5].children.Add (new muscleHeir ("Corrugator cutis ani", true));//12
		Heirarchy [6].children.Add (new muscleHeir ("Muscles of right lower limb", false));//0
		Heirarchy [6].children.Add (new muscleHeir ("Muscles of left lower limb", false));//1


		Heirarchy [0].children [0].children.Add (new muscleHeir ("Orbicularis oris", true));//0
		Heirarchy [0].children [0].children.Add (new muscleHeir ("Epicranial apneurosis", true));//1
		Heirarchy [0].children [0].children.Add (new muscleHeir ("Right craniofacial muscles", false));//2
		Heirarchy [0].children [0].children.Add (new muscleHeir ("Left Craniofacial muscles", false));//3
		Heirarchy [0].children [1].children.Add (new muscleHeir ("Right muscles of neck", false));//0
		Heirarchy [0].children [1].children.Add (new muscleHeir ("Left muscles of neck", false));//1
		Heirarchy [1].children [0].children.Add (new muscleHeir ("Right serratus posterior inferior", true));//0
		Heirarchy [1].children [0].children.Add (new muscleHeir ("Right serratus posterior superior", true));//1
		Heirarchy [1].children [0].children.Add (new muscleHeir ("Right transversus nuchae", true));//2
		Heirarchy [1].children [0].children.Add (new muscleHeir ("Cervical_rotator__R", true));//3
		Heirarchy [1].children [0].children.Add (new muscleHeir ("Right lateral posterior cervical", true));//4
		Heirarchy [1].children [0].children.Add (new muscleHeir ("Right intertransversaii lumborum", false));//5
		Heirarchy [1].children [1].children.Add (new muscleHeir ("Left serratus posterior inferior", true));//0
		Heirarchy [1].children [1].children.Add (new muscleHeir ("Left serratus posterior superior", true));//1
		Heirarchy [1].children [1].children.Add (new muscleHeir ("Left transversus nuchae", true));//2
		Heirarchy [1].children [1].children.Add (new muscleHeir ("Cervical_rotator__L", true));//3
		Heirarchy [1].children [1].children.Add (new muscleHeir ("Left lateral posterior cervical intertransversarii", true));//4
		Heirarchy [1].children [1].children.Add (new muscleHeir ("Left intertransversaii lumborum", false));//5
		Heirarchy [1].children [2].children.Add (new muscleHeir ("Right erector spinae", false));//0
		Heirarchy [1].children [2].children.Add (new muscleHeir ("Right splenius", false));//1
		Heirarchy [1].children [2].children.Add (new muscleHeir ("Right transversospinales", false));//2
		Heirarchy [1].children [2].children.Add (new muscleHeir ("Right intertransversarii", false));//3
		Heirarchy [1].children [2].children.Add (new muscleHeir ("Interspinales_lumborum__R", true));//4
		Heirarchy [1].children [2].children.Add (new muscleHeir ("Interspinalis_thoracis__R", true));//5
		Heirarchy [1].children [3].children.Add (new muscleHeir ("Left erector spinae", false));//0
		Heirarchy [1].children [3].children.Add (new muscleHeir ("Left splenius", false));//1
		Heirarchy [1].children [3].children.Add (new muscleHeir ("Left transversospinales", false));//2
		Heirarchy [1].children [3].children.Add (new muscleHeir ("Left intertransversarii", false));//3
		Heirarchy [1].children [3].children.Add (new muscleHeir ("Interspinales_lumborum__L", true));//4
		Heirarchy [1].children [3].children.Add (new muscleHeir ("Interspinalis_thoracis__L", true));//4
		Heirarchy [2].children [0].children.Add (new muscleHeir ("Muscles of right pectoral gridle", false));//0
		Heirarchy [2].children [0].children.Add (new muscleHeir ("Muscles of right shoulder", false));//1
		Heirarchy [2].children [0].children.Add (new muscleHeir ("Muscles of right upper arm", false));//2
		Heirarchy [2].children [0].children.Add (new muscleHeir ("Muscles of right forearm", false));//3
		Heirarchy [2].children [0].children.Add (new muscleHeir ("Muscles of right hand", false));//4
		Heirarchy [2].children [1].children.Add (new muscleHeir ("Muscles of left pectoral gridle", false));//0
		Heirarchy [2].children [1].children.Add (new muscleHeir ("Muscles of left shoulder", false));//1
		Heirarchy [2].children [1].children.Add (new muscleHeir ("Muscles of left upper arm", false));//2
		Heirarchy [2].children [1].children.Add (new muscleHeir ("Muscles of left forearm", false));//3
		Heirarchy [2].children [1].children.Add (new muscleHeir ("Muscles of left hand", false));//4
		Heirarchy [3].children [1].children.Add (new muscleHeir ("Right intercostals", false));//0
		Heirarchy [3].children [1].children.Add (new muscleHeir ("Right transversus thoracis", true));//1
		Heirarchy [3].children [1].children.Add (new muscleHeir ("Right subcostales", true));//2
		Heirarchy [3].children [1].children.Add (new muscleHeir ("Right levatores costarum", true));//3
		Heirarchy [3].children [2].children.Add (new muscleHeir ("Left intercostals", false));//0
		Heirarchy [3].children [2].children.Add (new muscleHeir ("Left transversus thoracis", true));//1
		Heirarchy [3].children [2].children.Add (new muscleHeir ("Left subcostales", true));//2
		Heirarchy [3].children [2].children.Add (new muscleHeir ("Left levatores costarum", true));//3
		Heirarchy [4].children [0].children.Add (new muscleHeir ("Right pyramidalis", true));//0
		Heirarchy [4].children [0].children.Add (new muscleHeir ("External_obliques__R", true));//1
		Heirarchy [4].children [0].children.Add (new muscleHeir ("Right internal oblique", true));//2
		Heirarchy [4].children [0].children.Add (new muscleHeir ("Right inguinal falx", true));//3
		Heirarchy [4].children [0].children.Add (new muscleHeir ("Right transverse abdominis", true));//4
		Heirarchy [4].children [0].children.Add (new muscleHeir ("Rectus_abdominis__R", true));//5
		Heirarchy [4].children [0].children.Add (new muscleHeir ("Right quadratus lumborum", true));//6
		Heirarchy [4].children [1].children.Add (new muscleHeir ("Left pyramidalis", true));//0
		Heirarchy [4].children [1].children.Add (new muscleHeir ("External_obliques__L", true));//1
		Heirarchy [4].children [1].children [1].xRotation = 10f;
		Heirarchy [4].children [1].children [1].yRotation = 135f;
		Heirarchy [4].children [1].children [1].distanceLelObject = 7.6f;
		Heirarchy [4].children [1].children [1].centerCameraPos = new Vector3 (-6f,0.5f,-2.9f);
		Heirarchy [4].children [1].children.Add (new muscleHeir ("Left internal oblique", true));//2
		Heirarchy [4].children [1].children.Add (new muscleHeir ("Left inguinal falx", true));//3
		Heirarchy [4].children [1].children.Add (new muscleHeir ("Left transverse abdominis", true));//4
		Heirarchy [4].children [1].children.Add (new muscleHeir ("Rectus_abdominis__L", true));//5
		Heirarchy [4].children [1].children.Add (new muscleHeir ("Left quadratus lumborum", true));//6
		Heirarchy [5].children [5].children.Add (new muscleHeir ("Right iliopsoas", false));//0
		Heirarchy [5].children [5].children.Add (new muscleHeir ("Coccygeus__R", true));//1
		Heirarchy [5].children [5].children.Add (new muscleHeir ("Right superficial transverse muscle", true));//2
		Heirarchy [5].children [5].children.Add (new muscleHeir ("Deep_transverse_perineal__R", true));//3
		Heirarchy [5].children [6].children.Add (new muscleHeir ("Left iliopsoas", false));//0
		Heirarchy [5].children [6].children.Add (new muscleHeir ("Coccygeus__L", true));//1
		Heirarchy [5].children [6].children.Add (new muscleHeir ("Left superficial transverse muscle", true));//2
		Heirarchy [5].children [6].children.Add (new muscleHeir ("Deep_transverse_perineal__L", true));//3
		Heirarchy [5].children [7].children.Add (new muscleHeir ("Right levator ani", false));//0
		Heirarchy [5].children [7].children.Add (new muscleHeir ("Left levator ani", false));//1
		//Heirarchy [5].children [11].children.Add (new muscleHeir ("Subcutaneous part of external muscle", true));//0
		//Heirarchy [5].children [11].children.Add (new muscleHeir ("Superficial part of external muscle", true));//1
		//Heirarchy [5].children [11].children.Add (new muscleHeir ("Deep part of external muscle", true));//2
		Heirarchy [6].children [0].children.Add (new muscleHeir ("Right gluteal muscles", false));//0
		Heirarchy [6].children [0].children.Add (new muscleHeir ("Lateral rotator muscles of right hip", false));//1
		Heirarchy [6].children [0].children.Add (new muscleHeir ("Muscles of right thigh", false));//2
		Heirarchy [6].children [0].children.Add (new muscleHeir ("Muscles of right lower leg", false));//3
		Heirarchy [6].children [0].children.Add (new muscleHeir ("Muscles of right foot", false));//4
		Heirarchy [6].children [1].children.Add (new muscleHeir ("Left gluteal muscles", false));//0
		Heirarchy [6].children [1].children.Add (new muscleHeir ("Lateral rotator muscles of left hip", false));//1
		Heirarchy [6].children [1].children.Add (new muscleHeir ("Muscles of left thigh", false));//2
		Heirarchy [6].children [1].children.Add (new muscleHeir ("Muscles of left lower leg", false));//3
		Heirarchy [6].children [1].children.Add (new muscleHeir ("Muscles of left foot", false));//4


		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right frontalis", true)); //0
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right occiptalis", true)); //1
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right temporoparietalis", true)); //2
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right auricular muscles", false)); //3
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right nasalis", true)); //4
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right procerus", true)); //5
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right orbicularis oculi", true)); //6
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right depressor anguli oris", true)); //7
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right levator labii superioris", true)); //8
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right mentalis", true)); //9
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right risorius", true)); //10
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right zygomaticus minor", true)); //11
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right zygomaticus major", true)); //12
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right levator anguli oris", true)); //13
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right levator labii superioris alaeque", true)); //14
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right depressor labii", true)); //15
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right depressor supercilii", true)); //16
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right corrugator sepercilii", true)); //17
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right depressor septi nasi", true)); //18
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right buccinator", true)); //19
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right buccal fat pad", true)); //20
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right pterygomandibular raphe", true)); //21
		Heirarchy [0].children [0].children [2].children.Add (new muscleHeir ("Right masticatory muscles", false)); //22
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left frontalis", true)); //0
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left occiptalis", true)); //1
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left temporoparietalis", true)); //2
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left auicular muscles", false)); //3
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left nasalis", true)); //4
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left procerus", true)); //5
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left orbicularis oculi", true)); //6
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left depressor anguli oris", true)); //7
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left levator labii superioris", true)); //8
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left mentalis", true)); //9
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left risorius", true)); //10
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left zygomaticus minor", true)); //11
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left zygomaticus major", true)); //12
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left levator anguli oris", true)); //13
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left levator labii superioris alaeque", true)); //14
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left depressor labii", true)); //15
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left depressor supercilii", true)); //16
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left corrugator sepercilii", true)); //17
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left depressor septi nasi", true)); //18
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left buccinator", true)); //19
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left buccal fat pad", true)); //20
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left pterygomandibular raphe", true)); //21
		Heirarchy [0].children [0].children [3].children.Add (new muscleHeir ("Left masticatory muscles", false)); //22
		Heirarchy [0].children [1].children [0].children.Add (new muscleHeir ("Right suboccipital muscles", false)); //0
		Heirarchy [0].children [1].children [0].children.Add (new muscleHeir ("Right proper muscles of the neck", false)); //1
		Heirarchy [0].children [1].children [0].children.Add (new muscleHeir ("Right suprahyoid muscles", false)); //2
		Heirarchy [0].children [1].children [0].children.Add (new muscleHeir ("Right infrahyoid muscles", false)); //3
		Heirarchy [0].children [1].children [0].children.Add (new muscleHeir ("Right transverse arytenoid", true)); //4
		Heirarchy [0].children [1].children [0].children.Add (new muscleHeir ("Right muscles of throat", false)); //5
		Heirarchy [0].children [1].children [1].children.Add (new muscleHeir ("Left suboccipital muscles", false)); //0
		Heirarchy [0].children [1].children [1].children.Add (new muscleHeir ("Left proper muscles of the neck", false)); //1
		Heirarchy [0].children [1].children [1].children.Add (new muscleHeir ("Left suprahyoid muscles", false)); //2
		Heirarchy [0].children [1].children [1].children.Add (new muscleHeir ("Left infrahyoid muscles", false)); //3
		Heirarchy [0].children [1].children [1].children.Add (new muscleHeir ("Left transverse arytenoid", true)); //4
		Heirarchy [0].children [1].children [1].children.Add (new muscleHeir ("Left muscles of throat", false)); //5
		Heirarchy [1].children [0].children [5].children.Add (new muscleHeir ("Right intertransversarii laterales lumborum", true)); //0
		Heirarchy [1].children [0].children [5].children.Add (new muscleHeir ("Right intertransversarii mediales lumborum", true)); //1
		Heirarchy [1].children [1].children [5].children.Add (new muscleHeir ("Left intertransversarii laterales lumborum", true)); //0
		Heirarchy [1].children [1].children [5].children.Add (new muscleHeir ("Left intertransversarii mediales lumborum", true)); //1
		Heirarchy [1].children [2].children [0].children.Add (new muscleHeir ("Right iliocostalis", false)); //0
		Heirarchy [1].children [2].children [0].children.Add (new muscleHeir ("Right longissimus", false)); //1
		Heirarchy [1].children [2].children [0].children.Add (new muscleHeir ("Right spinalis", false)); //2
		Heirarchy [1].children [2].children [1].children.Add (new muscleHeir ("Splenius_capitis__R", true)); //0
		Heirarchy [1].children [2].children [1].children.Add (new muscleHeir ("Splenius_cervicis__R", true)); //1
		Heirarchy [1].children [2].children [2].children.Add (new muscleHeir ("Multifidus__R", true)); //0
		Heirarchy [1].children [2].children [2].children.Add (new muscleHeir ("Right semispinalis", false)); //1
		Heirarchy [1].children [2].children [2].children.Add (new muscleHeir ("Right rotatores", false)); //2
		Heirarchy [1].children [2].children [3].children.Add (new muscleHeir ("Right lumbar intertransversarii", true)); //0
		Heirarchy [1].children [2].children [3].children.Add (new muscleHeir ("Right thoracic intertransversarii", true)); //1
		Heirarchy [1].children [3].children [0].children.Add (new muscleHeir ("Left iliocostalis", false)); //0
		Heirarchy [1].children [3].children [0].children.Add (new muscleHeir ("Left longissimus", false)); //1
		Heirarchy [1].children [3].children [0].children.Add (new muscleHeir ("Left spinalis", false)); //2
		Heirarchy [1].children [3].children [1].children.Add (new muscleHeir ("Splenius_capitis__L", true)); //0
		Heirarchy [1].children [3].children [1].children.Add (new muscleHeir ("Splenius_cervicis__L", true)); //1
		Heirarchy [1].children [3].children [2].children.Add (new muscleHeir ("Multifidus__L", true)); //0
		Heirarchy [1].children [3].children [2].children.Add (new muscleHeir ("Left semispinalis", false)); //1
		Heirarchy [1].children [3].children [2].children.Add (new muscleHeir ("Left rotatotores", false)); //2
		Heirarchy [1].children [3].children [3].children.Add (new muscleHeir ("Left lumbar intertransversarii", true)); //0
		Heirarchy [1].children [3].children [3].children.Add (new muscleHeir ("Left thoracic intertransversarii", true)); //1
		Heirarchy [2].children [0].children [0].children.Add (new muscleHeir ("Right subclavius", true)); //0
		Heirarchy [2].children [0].children [0].children.Add (new muscleHeir ("Pectoralis_minor__R", true)); //1
		Heirarchy [2].children [0].children [0].children.Add (new muscleHeir ("Pectoralis_major__R", true)); //2
		Heirarchy [2].children [0].children [0].children.Add (new muscleHeir ("Pectoralis_major_clavicular_part__R", true)); //3
		Heirarchy [2].children [0].children [0].children.Add (new muscleHeir ("Pectoralis_major_Abdominal_part__R", true)); //4
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Trapezius_lower__R", true)); //0
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Trapezius_upper__R", true)); //1
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Trapezius_middle__R", true)); //2
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Latissimus_dorsi__R", true)); //3
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Levator_scapulae__R", true)); //4
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Rhomboideus_minor__R", true)); //5
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Rhomboideus_major__R", true)); //6
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Deltoid__anterior_head__R", true)); //7
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Deltoid__middle_head__R", true)); //8
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Deltoid__posterior_head__R", true)); //9
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Supraspinatus__R", true)); //10
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Infraspinatus__R", true)); //11
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Subscapularis__R", true)); //12
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Serratus_anterior__R", true)); //13
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Teres_minor__R", true)); //14
		Heirarchy [2].children [0].children [1].children.Add (new muscleHeir ("Teres_major__R", true)); //15
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Biceps_brachii__long_head__R", true)); //0
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Biceps_brachii__short_head__R", true)); //1
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Right bicipital aponeurosis", true)); //2
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Brachialis__R", true)); //3
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Coracobrachialis__R", true)); //4
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Triceps__medial_head__R", true)); //5
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Triceps__long_head__R", true)); //6
		Heirarchy [2].children [0].children [2].children.Add (new muscleHeir ("Triceps__lateral_head__R", true)); //7
		Heirarchy [2].children [0].children [3].children.Add (new muscleHeir ("Muscles of right forearm flexor", false)); //0
		Heirarchy [2].children [0].children [3].children.Add (new muscleHeir ("Muscles of right forearm extensor", false)); //1
		Heirarchy [2].children [0].children [4].children.Add (new muscleHeir ("Palmar muscles of right hand", false)); //0
		Heirarchy [2].children [0].children [4].children.Add (new muscleHeir ("Dorsal muscles of right hand", false)); //1
		Heirarchy [2].children [1].children [0].children.Add (new muscleHeir ("Left subclavius", true)); //0
		Heirarchy [2].children [1].children [0].children.Add (new muscleHeir ("Pectoralis_minor__L", true)); //1
		Heirarchy [2].children [1].children [0].children[1].xRotation = 18.5f;
		Heirarchy [2].children [1].children [0].children[1].yRotation = 169.25f;
		Heirarchy [2].children [1].children [0].children[1].distanceLelObject = 6.8f;
		Heirarchy [2].children [1].children [0].children[1].centerCameraPos = new Vector3 (-6.1f,1.9f,-2.8f);

		Heirarchy [2].children [1].children [0].children.Add (new muscleHeir ("Pectoralis_major__L", true)); //2
		Heirarchy [2].children [1].children [0].children[2].xRotation = 18.5f;
		Heirarchy [2].children [1].children [0].children[2].yRotation = 169.25f;
		Heirarchy [2].children [1].children [0].children[2].distanceLelObject = 6.8f;
		Heirarchy [2].children [1].children [0].children[2].centerCameraPos = new Vector3 (-6.1f,1.9f,-2.8f);
		Heirarchy [2].children [1].children [0].children.Add (new muscleHeir ("Pectoralis_major_clavicular_part__L", true)); //3
		Heirarchy [2].children [1].children [0].children.Add (new muscleHeir ("Pectoralis_major_Abdominal_part__L", true)); //4
		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Trapezius_lower__L", true)); //0
		Heirarchy [2].children [1].children [1].children[0].xRotation = -3.5f;
		Heirarchy [2].children [1].children [1].children[0].yRotation = -3f;
		Heirarchy [2].children [1].children [1].children [0].distanceLelObject = 4.4f;
		Heirarchy [2].children [1].children [1].children[0].centerCameraPos = new Vector3 (-5.6f,2f,-4.1f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Trapezius_upper__L", true)); //1
		Heirarchy [2].children [1].children [1].children[1].xRotation = -5.24f;
		Heirarchy [2].children [1].children [1].children[1].yRotation = -11.5f;
		Heirarchy [2].children [1].children [1].children[1].distanceLelObject = 7f;
		Heirarchy [2].children [1].children [1].children[1].centerCameraPos = new Vector3 (-5.4f,2.8f,-3.8f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Trapezius_middle__L", true)); //2
		Heirarchy [2].children [1].children [1].children[2].xRotation = 8.75f;
		Heirarchy [2].children [1].children [1].children[2].yRotation = 0.25f;
		Heirarchy [2].children [1].children [1].children[2].distanceLelObject = 7f;
		Heirarchy [2].children [1].children [1].children[2].centerCameraPos = new Vector3 (-5.5f,1.7f,-3.7f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Latissimus_dorsi__L", true)); //3
		Heirarchy [2].children [1].children [1].children[3].xRotation = -0.5f;
		Heirarchy [2].children [1].children [1].children[3].yRotation = 19.5f;
		Heirarchy [2].children [1].children [1].children[3].distanceLelObject = 7f;
		Heirarchy [2].children [1].children [1].children[3].centerCameraPos = new Vector3 (-5.8f,0.9f,-3.9f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Levator_scapulae__L", true)); //4
		Heirarchy [2].children [1].children [1].children[4].xRotation = 7.25f;
		Heirarchy [2].children [1].children [1].children[4].yRotation = 19.75f;
		Heirarchy [2].children [1].children [1].children[4].distanceLelObject = 4.8f;
		Heirarchy [2].children [1].children [1].children[4].centerCameraPos = new Vector3 (-5.6f,2.8f,-3.8f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Rhomboideus_minor__L", true)); //5
		Heirarchy [2].children [1].children [1].children[5].xRotation = 10.25f;
		Heirarchy [2].children [1].children [1].children[5].yRotation = 24f;
		Heirarchy [2].children [1].children [1].children[5].distanceLelObject = 4.8f;
		Heirarchy [2].children [1].children [1].children[5].centerCameraPos = new Vector3 (-5.7f,2.6f,-3.8f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Rhomboideus_major__L", true)); //6
		Heirarchy [2].children [1].children [1].children[6].xRotation = 4.75f;
		Heirarchy [2].children [1].children [1].children[6].yRotation = 18.25f;
		Heirarchy [2].children [1].children [1].children[6].distanceLelObject = 4.8f;
		Heirarchy [2].children [1].children [1].children[6].centerCameraPos = new Vector3 (-5.7f,2.34f,-3.9f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Deltoid__anterior_head__L", true)); //7
		Heirarchy [2].children [1].children [1].children[7].xRotation = 7.5f;
		Heirarchy [2].children [1].children [1].children[7].yRotation = 141.5f;
		Heirarchy [2].children [1].children [1].children[7].distanceLelObject = 7.5f;
		Heirarchy [2].children [1].children [1].children[7].centerCameraPos = new Vector3 (-6.6f,2.5f,-3.4f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Deltoid__middle_head__L", true)); //8
		Heirarchy [2].children [1].children [1].children[8].xRotation = 8.25f;
		Heirarchy [2].children [1].children [1].children[8].yRotation = 162.75f;
		Heirarchy [2].children [1].children [1].children[8].distanceLelObject = 7.5f;
		Heirarchy [2].children [1].children [1].children[8].centerCameraPos = new Vector3 (-6.7f,2.5f,-3.5f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Deltoid__posterior_head__L", true)); //9
		Heirarchy [2].children [1].children [1].children[9].xRotation = 6f;
		Heirarchy [2].children [1].children [1].children[9].yRotation = 16.25f;
		Heirarchy [2].children [1].children [1].children[9].distanceLelObject = 7.5f;
		Heirarchy [2].children [1].children [1].children[9].centerCameraPos = new Vector3 (-6.6f,2.4f,-3.9f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Supraspinatus__L", true)); //10
		Heirarchy [2].children [1].children [1].children[10].xRotation = 10.25f;
		Heirarchy [2].children [1].children [1].children[10].yRotation = 23f;
		Heirarchy [2].children [1].children [1].children[10].distanceLelObject = 7.5f;
		Heirarchy [2].children [1].children [1].children[10].centerCameraPos = new Vector3 (-6f,2.6f,-3.9f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Infraspinatus__L", true)); //11
		Heirarchy [2].children [1].children [1].children[11].xRotation = 9.75f;
		Heirarchy [2].children [1].children [1].children[11].yRotation = 20.25f;
		Heirarchy [2].children [1].children [1].children[11].distanceLelObject = 7.5f;
		Heirarchy [2].children [1].children [1].children[11].centerCameraPos = new Vector3 (-6f,2.2f,-4f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Subscapularis__L", true)); //12
		Heirarchy [2].children [1].children [1].children[12].xRotation = 10.75f;
		Heirarchy [2].children [1].children [1].children[12].yRotation = 18.25f;
		Heirarchy [2].children [1].children [1].children[12].distanceLelObject = 7.3f;
		Heirarchy [2].children [1].children [1].children[12].centerCameraPos = new Vector3 (-6.2f,1.6f,-3.3f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Serratus_anterior__L", true)); //13
		Heirarchy [2].children [1].children [1].children[13].xRotation = -11.25f;
		Heirarchy [2].children [1].children [1].children[13].yRotation = 26.25f;
		Heirarchy [2].children [1].children [1].children[13].distanceLelObject = 7.3f;
		Heirarchy [2].children [1].children [1].children[13].centerCameraPos = new Vector3 (-6.1f,1.5f,-3.7f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Teres_minor__L", true)); //14
		Heirarchy [2].children [1].children [1].children[14].xRotation = 4.75f;
		Heirarchy [2].children [1].children [1].children[14].yRotation = 19.75f;
		Heirarchy [2].children [1].children [1].children[14].distanceLelObject = 7.3f;
		Heirarchy [2].children [1].children [1].children[14].centerCameraPos = new Vector3 (-6.2f,2.1f,-3.8f);

		Heirarchy [2].children [1].children [1].children.Add (new muscleHeir ("Teres_major__L", true)); //15
		Heirarchy [2].children [1].children [1].children[15].xRotation = 4.75f;
		Heirarchy [2].children [1].children [1].children[15].yRotation = 19.75f;
		Heirarchy [2].children [1].children [1].children[15].distanceLelObject = 7.3f;
		Heirarchy [2].children [1].children [1].children[15].centerCameraPos = new Vector3 (-6.2f,2.1f,-3.8f);

		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Biceps_brachii__long_head__L", true)); //0
		Heirarchy [2].children [1].children [2].children[0].xRotation = 6.25f;
		Heirarchy [2].children [1].children [2].children[0].yRotation = 116.5f;
		Heirarchy [2].children [1].children [2].children[0].distanceLelObject = 7.3f;
		Heirarchy [2].children [1].children [2].children[0].centerCameraPos = new Vector3 (-6.5f,1.8f,-3.1f);

		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Biceps_brachii__short_head__L", true)); //1
		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Left bicipital aponeurosis", true)); //2
		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Brachialis__L", true)); //3
		Heirarchy [2].children [1].children [2].children[2].xRotation = 8.25f;
		Heirarchy [2].children [1].children [2].children[2].yRotation = 11.5f;
		Heirarchy [2].children [1].children [2].children[2].distanceLelObject = 8.2f;
		Heirarchy [2].children [1].children [2].children[2].centerCameraPos = new Vector3 (-6.3f,1.6f,-3.5f);
		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Coracobrachialis__L", true)); //4
		Heirarchy [2].children [1].children [2].children[3].xRotation = 8.25f;
		Heirarchy [2].children [1].children [2].children[3].yRotation = 11.5f;
		Heirarchy [2].children [1].children [2].children[3].distanceLelObject = 8.2f;
		Heirarchy [2].children [1].children [2].children[3].centerCameraPos = new Vector3 (-6.3f,1.6f,-3.5f);

		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Triceps__medial_head__L", true)); //5
		Heirarchy [2].children [1].children [2].children[4].xRotation = 12.5f;
		Heirarchy [2].children [1].children [2].children[4].yRotation = 82f;
		Heirarchy [2].children [1].children [2].children[4].distanceLelObject = 8.2f;
		Heirarchy [2].children [1].children [2].children[4].centerCameraPos = new Vector3 (-6.75f,1.3f,-4.2f);

		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Triceps__long_head__L", true)); //6
		Heirarchy [2].children [1].children [2].children.Add (new muscleHeir ("Triceps__lateral_head__L", true)); //7
		Heirarchy [2].children [1].children [3].children.Add (new muscleHeir ("Muscles of left forearm flexor", false)); //0
		Heirarchy [2].children [1].children [3].children.Add (new muscleHeir ("Muscles of left forearm extensor", false)); //1
		Heirarchy [2].children [1].children [4].children.Add (new muscleHeir ("Palmar muscles of left hand", false)); //0
		Heirarchy [2].children [1].children [4].children.Add (new muscleHeir ("Dorsal muscles of left hand", false)); //1
		Heirarchy [3].children [1].children [0].children.Add (new muscleHeir ("Right innermost intercostals", true)); //0
		Heirarchy [3].children [1].children [0].children.Add (new muscleHeir ("Internal_intercostal_2__R", true)); //1
		Heirarchy [3].children [1].children [0].children.Add (new muscleHeir ("Right ecternal intercostals", true)); //2
		Heirarchy [3].children [2].children [0].children.Add (new muscleHeir ("Left innermost intercostals", true)); //0
		Heirarchy [3].children [2].children [0].children.Add (new muscleHeir ("Internal_intercostal_2__L", true)); //1
		// still missing internal intercostal 2 :(
		Heirarchy [3].children [2].children [0].children[1].xRotation = 3.5f;
		Heirarchy [3].children [2].children [0].children[1].yRotation = 149f;
		Heirarchy [3].children [2].children [0].children[1].distanceLelObject = 4.1f;
		Heirarchy [3].children [2].children [0].children[1].centerCameraPos = new Vector3 (-4.95f,1.04f,-3.8f);
		Heirarchy [3].children [2].children [0].children.Add (new muscleHeir ("Left ecternal intercostals", true)); //2
		Heirarchy [5].children [5].children [0].children.Add (new muscleHeir ("Iliacus__R", true)); //0
		Heirarchy [5].children [5].children [0].children.Add (new muscleHeir ("Right psoas minor", true)); //1
		Heirarchy [5].children [5].children [0].children.Add (new muscleHeir ("Right psoas major", true)); //2
		Heirarchy [5].children [6].children [0].children.Add (new muscleHeir ("Iliacus__L", true)); //0
		Heirarchy [5].children [6].children [0].children.Add (new muscleHeir ("Left psoas minor", true)); //1
		Heirarchy [5].children [6].children [0].children.Add (new muscleHeir ("Left psoas major", true)); //2
		Heirarchy [5].children [7].children [0].children.Add (new muscleHeir ("Right iliococcygeus", true)); //0
		Heirarchy [5].children [7].children [0].children.Add (new muscleHeir ("Right pubococcygeus", true)); //1
		Heirarchy [5].children [7].children [0].children.Add (new muscleHeir ("Right puborectalis", true)); //2
		Heirarchy [5].children [7].children [0].children.Add (new muscleHeir ("Right tendinous arch", true)); //3
		Heirarchy [5].children [7].children [1].children.Add (new muscleHeir ("Left iliococcygeus", true)); //0
		Heirarchy [5].children [7].children [1].children.Add (new muscleHeir ("Left pubococcygeus", true)); //1
		Heirarchy [5].children [7].children [1].children.Add (new muscleHeir ("Left puborectalis", true)); //2
		Heirarchy [5].children [7].children [1].children.Add (new muscleHeir ("Left tendinous arch", true)); //3
		Heirarchy [6].children [0].children [0].children.Add (new muscleHeir ("Right tensor fasciae latae", true)); //0
		Heirarchy [6].children [0].children [0].children.Add (new muscleHeir ("Right iliotibial tract", true)); //1
		Heirarchy [6].children [0].children [0].children.Add (new muscleHeir ("Gluteus_maximus__R", true)); //2
		Heirarchy [6].children [0].children [0].children.Add (new muscleHeir ("Gluteus_medius__R", true)); //3
		Heirarchy [6].children [0].children [0].children.Add (new muscleHeir ("Gluteus_minimus__R", true)); //4
		Heirarchy [6].children [0].children [1].children.Add (new muscleHeir ("Right superior gemellus", true)); //0
		Heirarchy [6].children [0].children [1].children.Add (new muscleHeir ("Inferior_gemellus__R", true)); //1
		Heirarchy [6].children [0].children [1].children.Add (new muscleHeir ("Quadratus_femoris__R", true)); //2
		Heirarchy [6].children [0].children [1].children.Add (new muscleHeir ("Obturator_externus__R", true)); //3
		Heirarchy [6].children [0].children [1].children.Add (new muscleHeir ("Obturator_internus__R", true)); //4
		Heirarchy [6].children [0].children [1].children.Add (new muscleHeir ("Right piriformis", true)); //5
		Heirarchy [6].children [0].children [2].children.Add (new muscleHeir ("Anterior compartment muscles of right thigh", false)); //0
		Heirarchy [6].children [0].children [2].children.Add (new muscleHeir ("Posterior compartment muscles of right thigh", false)); //1
		Heirarchy [6].children [0].children [2].children.Add (new muscleHeir ("Medial compartment muscles of right thigh", false)); //2
		Heirarchy [6].children [0].children [3].children.Add (new muscleHeir ("Anterior compartment muscles of right lower leg", false)); //0
		Heirarchy [6].children [0].children [3].children.Add (new muscleHeir ("Posterior compartment muscles of right lower leg", false)); //1
		Heirarchy [6].children [0].children [3].children.Add (new muscleHeir ("Lateral compartment muscles of right lower leg", false)); //2
		Heirarchy [6].children [0].children [4].children.Add (new muscleHeir ("Plantar muscles of right foot", false)); //0
		Heirarchy [6].children [0].children [4].children.Add (new muscleHeir ("Dorsal muscles of right foot", false)); //1
		Heirarchy [6].children [1].children [0].children.Add (new muscleHeir ("Left tensor fasciae latae", true)); //0
		Heirarchy [6].children [1].children [0].children.Add (new muscleHeir ("Left iliotibial tract", true)); //1
		Heirarchy [6].children [1].children [0].children.Add (new muscleHeir ("Gluteus_maximus__L", true)); //2
		Heirarchy [6].children [1].children [0].children.Add (new muscleHeir ("Gluteus_medius__L", true)); //3
		Heirarchy [6].children [1].children [0].children.Add (new muscleHeir ("Gluteus_minimus__L", true)); //4
		Heirarchy [6].children [1].children [1].children.Add (new muscleHeir ("Left superior gemellus", true)); //0
		Heirarchy [6].children [1].children [1].children.Add (new muscleHeir ("Inferior_gemellus__L", true)); //1
		Heirarchy [6].children [1].children [1].children.Add (new muscleHeir ("Quadratus_femoris__L", true)); //2
		Heirarchy [6].children [1].children [1].children.Add (new muscleHeir ("Obturator_externus__L", true)); //3
		Heirarchy [6].children [1].children [1].children.Add (new muscleHeir ("Obturator_internus__L", true)); //4
		Heirarchy [6].children [1].children [1].children.Add (new muscleHeir ("Left piriformis", true)); //5
		Heirarchy [6].children [1].children [2].children.Add (new muscleHeir ("Anterior compartment muscles of left thigh", false)); //0
		Heirarchy [6].children [1].children [2].children.Add (new muscleHeir ("Posterior compartment muscles of left thigh", false)); //1
		Heirarchy [6].children [1].children [2].children.Add (new muscleHeir ("Medial compartment muscles of left thigh", false)); //2
		Heirarchy [6].children [1].children [3].children.Add (new muscleHeir ("Anterior compartment muscles of left lower leg", false)); //0
		Heirarchy [6].children [1].children [3].children.Add (new muscleHeir ("Posterior compartment muscles of left lower leg", false)); //1
		Heirarchy [6].children [1].children [3].children.Add (new muscleHeir ("Lateral compartment muscles of left lower leg", false)); //2
		Heirarchy [6].children [1].children [4].children.Add (new muscleHeir ("Plantar muscles of left foot", false)); //0
		Heirarchy [6].children [1].children [4].children.Add (new muscleHeir ("Dorsal muscles of left foot", false)); //1


		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right auricularis anterior",true));//0
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right auricularis superior",true));//1
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right auricularis posterior",true));//2
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right transverse muscle of auricle",true));//3
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right oblique muscle of auricle",true));//4
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right antitragus",true));//5
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right helicis major",true));//6
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right tragus",true));//7
		Heirarchy [0].children [0].children [2].children[3].children.Add(new muscleHeir("Right helicis minor",true));//8
		Heirarchy [0].children [0].children [2].children[22].children.Add(new muscleHeir("Right lateral pterygoid",false));//0
		Heirarchy [0].children [0].children [2].children[22].children.Add(new muscleHeir("Right medial pterygoid",false));//1
		Heirarchy [0].children [0].children [2].children[22].children.Add(new muscleHeir("Right temporalis",false));//2
		Heirarchy [0].children [0].children [2].children[22].children.Add(new muscleHeir("Right masseter",false));//3
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left auricularis anterior",true));//0
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left auricularis superior",true));//1
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left auricularis posterior",true));//2
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left transverse muscle of auricle",true));//3
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left oblique muscle of auricle",true));//4
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left antitragus",true));//5
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left helicis major",true));//6
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left tragus",true));//7
		Heirarchy [0].children [0].children [3].children[3].children.Add(new muscleHeir("Left helicis minor",true));//8
		Heirarchy [0].children [0].children [3].children[22].children.Add(new muscleHeir("Left lateral pterygoid",false));//0
		Heirarchy [0].children [0].children [3].children[22].children.Add(new muscleHeir("Left medial pterygoid",false));//1
		Heirarchy [0].children [0].children [3].children[22].children.Add(new muscleHeir("Left temporalis",false));//2
		Heirarchy [0].children [0].children [3].children[22].children.Add(new muscleHeir("Left masseter",false));//3
		Heirarchy [0].children [1].children [0].children[0].children.Add(new muscleHeir("Rectus_capitis_posterior_minor__R",true));//0
		Heirarchy [0].children [1].children [0].children[0].children.Add(new muscleHeir("Rectus_capitis_posterior_major__R",true));//1
		Heirarchy [0].children [1].children [0].children[0].children.Add(new muscleHeir("Rectus_capitis_anterior__R",true));//2
		Heirarchy [0].children [1].children [0].children[0].children.Add(new muscleHeir("Rectus_capitis_lateralis__R",true));//3
		Heirarchy [0].children [1].children [0].children[0].children.Add(new muscleHeir("Obliquus_capitis_superior__R",true));//4
		Heirarchy [0].children [1].children [0].children[0].children.Add(new muscleHeir("Obliquus_capitis_inferior__R",true));//4
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Platysma__R",true));//0
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Right longus colli",false));//1
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Right longus capitis",true));//2
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Scalene_anterior__R",true));//3
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Scalene_middle__R",true));//4
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Scalene_posterior__R",true));//5
		Heirarchy [0].children [1].children [0].children[1].children.Add(new muscleHeir("Sternocleidomastoid__R",true));//6
		Heirarchy [0].children [1].children [0].children[2].children.Add(new muscleHeir("Right stylohyoid",true));//0
		Heirarchy [0].children [1].children [0].children[2].children.Add(new muscleHeir("Right digastric",false));//1
		Heirarchy [0].children [1].children [0].children[2].children.Add(new muscleHeir("Right geniohyoid",true));//2
		Heirarchy [0].children [1].children [0].children[2].children.Add(new muscleHeir("Right mylohyoid",true));//3
		Heirarchy [0].children [1].children [0].children[3].children.Add(new muscleHeir("Right omohyoid",false));//0
		Heirarchy [0].children [1].children [0].children[3].children.Add(new muscleHeir("Right sternohyoid",true));//1
		Heirarchy [0].children [1].children [0].children[3].children.Add(new muscleHeir("Right sternothyroid",true));//2
		Heirarchy [0].children [1].children [0].children[3].children.Add(new muscleHeir("Right thyrohyoid",true));//3
		Heirarchy [0].children [1].children [0].children[5].children.Add(new muscleHeir("Right laryngeal muscles",false));//0
		Heirarchy [0].children [1].children [0].children[5].children.Add(new muscleHeir("Right muscles of soft palate and fauces",false));//1
		Heirarchy [0].children [1].children [0].children[5].children.Add(new muscleHeir("Right pharyngeal muscles",false));//2
		Heirarchy [0].children [1].children [1].children[0].children.Add(new muscleHeir("Rectus_capitis_posterior_minor__L",true));//0
		Heirarchy [0].children [1].children [1].children[0].children.Add(new muscleHeir("Rectus_capitis_posterior_major__L",true));//1
		Heirarchy [0].children [1].children [1].children[0].children.Add(new muscleHeir("Rectus_capitis_anterior__L",true));//2
		Heirarchy [0].children [1].children [1].children[0].children.Add(new muscleHeir("Rectus_capitis_lateralis__L",true));//3
		Heirarchy [0].children [1].children [1].children[0].children.Add(new muscleHeir("Obliquus_capitis_superior__L",true));//4
		Heirarchy [0].children [1].children [1].children[0].children.Add(new muscleHeir("Obliquus_capitis_inferior__L",true));//4
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Platysma__L",true));//0
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Left longus colli",false));//1
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Left longus capitis",true));//2
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Scalene_anterior__L",true));//3
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Scalene_middle__L",true));//4
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Scalene_posterior__L",true));//5
		Heirarchy [0].children [1].children [1].children[1].children.Add(new muscleHeir("Sternocleidomastoid__L",true));//6
		Heirarchy [0].children [1].children [1].children[2].children.Add(new muscleHeir("Left stylohyoid",true));//0
		Heirarchy [0].children [1].children [1].children[2].children.Add(new muscleHeir("Left digastric",false));//1
		Heirarchy [0].children [1].children [1].children[2].children.Add(new muscleHeir("Left geniohyoid",true));//2
		Heirarchy [0].children [1].children [1].children[2].children.Add(new muscleHeir("Left mylohyoid",true));//3
		Heirarchy [0].children [1].children [1].children[3].children.Add(new muscleHeir("Left omohyoid",false));//0
		Heirarchy [0].children [1].children [1].children[3].children.Add(new muscleHeir("Left sternohyoid",true));//1
		Heirarchy [0].children [1].children [1].children[3].children.Add(new muscleHeir("Left sternothyroid",true));//2
		Heirarchy [0].children [1].children [1].children[3].children.Add(new muscleHeir("Left thyrohyoid",true));//3
		Heirarchy [0].children [1].children [1].children[5].children.Add(new muscleHeir("Left laryngeal muscles",false));//0
		Heirarchy [0].children [1].children [1].children[5].children.Add(new muscleHeir("Left muscles of soft palate and fauces",false));//1
		Heirarchy [0].children [1].children [1].children[5].children.Add(new muscleHeir("Left pharyngeal muscles",false));//2
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Iliocostalis_cervicis__R",true));//0
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Iliocostalis_thoracis__R",true));//1
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Iliocostalis_lumborum__R",true));//2
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Longissimus_capitis__R",true));//0
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Right longissimus cervicis",true));//1
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Right longissimus thoracis",true));//2
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Spinalis_cervicis__R",true));//0
		Heirarchy [1].children [2].children [0].children[0].children.Add(new muscleHeir("Spinalis_thoracis__R",true));//1
		Heirarchy [1].children [2].children [2].children[1].children.Add(new muscleHeir("Semispinalis_capitis__R",true));//0
		Heirarchy [1].children [2].children [2].children[1].children.Add(new muscleHeir("Semispinalis_cervicis__R",true));//1
		Heirarchy [1].children [2].children [2].children[1].children.Add(new muscleHeir("Semispinalis_thoracis__R",true));//2
		Heirarchy [1].children [2].children [2].children[2].children.Add(new muscleHeir("Right rotatores cervicis",true));//0
		Heirarchy [1].children [2].children [2].children[2].children.Add(new muscleHeir("Thoracic_rotator__R",true));//1
		Heirarchy [1].children [2].children [2].children[2].children.Add(new muscleHeir("Lumbar_rotator__R",true));//2
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Iliocostalis_cervicis__L",true));//0
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Iliocostalis_thoracis__L",true));//1
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Iliocostalis_lumborum__L",true));//2
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Longissimus_capitis__L",true));//0
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Left longissimus cervicis",true));//1
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Left longissimus thoracis",true));//2
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Spinalis_cervicis__L",true));//0
		Heirarchy [1].children [3].children [0].children[0].children.Add(new muscleHeir("Spinalis_thoracis__L",true));//1
		Heirarchy [1].children [3].children [2].children[1].children.Add(new muscleHeir("Semispinalis_capitis__L",true));//0
		Heirarchy [1].children [3].children [2].children[1].children.Add(new muscleHeir("Semispinalis_cervicis__L",true));//1
		Heirarchy [1].children [3].children [2].children[1].children.Add(new muscleHeir("Semispinalis_thoracis__L",true));//2
		Heirarchy [1].children [3].children [2].children[2].children.Add(new muscleHeir("Left rotatores cervicis",true));//0
		Heirarchy [1].children [3].children [2].children[2].children.Add(new muscleHeir("Thoracic_rotator__L",true));//1
		Heirarchy [1].children [3].children [2].children[2].children.Add(new muscleHeir("Lumbar_rotator__L",true));//2
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Flexor_carpi_radialis__R",true));//0
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Right palmaris longus",true));//1
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Pronator_teres__R",true));//2
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Flexor_digitorum_superficialis__R",true));//3
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Flexor_pollicis_longus__R",true));//4
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Flexor_digitorum_profundus__R",true));//5
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Right pronator quadratus",true));//6
		Heirarchy [2].children [0].children [3].children[0].children.Add(new muscleHeir("Flexor_carpi_ulnaris__R",true));//7
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Brachioradialis__R",true));//0
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_carpi_radialis_longus__R",true));//1
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_carpi_radialis_brevis__R",true));//2
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_carpi_ulnaris__R",true));//3
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Anconeus__R",true));//4
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Abductor_pollicis_longus__R",true));//5
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_pollicis_longus__R",true));//6
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_pollicis_brevis__R",true));//7
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_indicis__R",true));//8
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Supinator__R",true));//9
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_digiti_minimi__R",true));//10
		Heirarchy [2].children [0].children [3].children[1].children.Add(new muscleHeir("Extensor_digitorum__R",true));//1
		Heirarchy [2].children [0].children [4].children[0].children.Add(new muscleHeir("Thenar muscles of right hand",false));//0
		Heirarchy [2].children [0].children [4].children[0].children.Add(new muscleHeir("Hypothenar muscles of right hand",false));//1
		Heirarchy [2].children [0].children [4].children[0].children.Add(new muscleHeir("Right palmaris brevis",true));//2
		Heirarchy [2].children [0].children [4].children[0].children.Add(new muscleHeir("Palmar_interossei‏__R",true));//3
		Heirarchy [2].children [0].children [4].children[0].children.Add(new muscleHeir("Lumbricals__Hand___R",true));//4
		Heirarchy [2].children [0].children [4].children[1].children.Add(new muscleHeir("Dorsal_interossei__R",true));//0
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Flexor_carpi_radialis__L",true));//0
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Palmaris_longus__L",true));//1
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Pronator_teres__L",true));//2
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Flexor_digitorum_superficialis__L",true));//3
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Flexor_pollicis_longus__L",true));//4
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Flexor_digitorum_profundus__L",true));//5
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Pronator_quadratus__L",true));//6
		Heirarchy [2].children [1].children [3].children[0].children.Add(new muscleHeir("Flexor_carpi_ulnaris__L",true));//7
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Brachioradialis__L",true));//0
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_carpi_radialis_longus__L",true));//1
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_carpi_radialis_brevis__L",true));//2
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_carpi_ulnaris__L",true));//3
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Anconeus__L",true));//4
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Abductor_pollicis_longus__L",true));//5
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_pollicis_longus__L",true));//6
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_pollicis_brevis__L",true));//7
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_indicis__L",true));//8
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Supinator__L",true));//9
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_digiti_minimi__L",true));//10
		Heirarchy [2].children [1].children [3].children[1].children.Add(new muscleHeir("Extensor_digitorum__L",true));//1
		Heirarchy [2].children [1].children [4].children[0].children.Add(new muscleHeir("Thenar muscles of left hand",false));//0
		Heirarchy [2].children [1].children [4].children[0].children.Add(new muscleHeir("Hypothenar muscles of left hand",false));//1
		Heirarchy [2].children [1].children [4].children[0].children.Add(new muscleHeir("Left palmaris brevis",true));//2
		Heirarchy [2].children [1].children [4].children[0].children.Add(new muscleHeir("Palmar_interossei‏__L",true));//3
		Heirarchy [2].children [1].children [4].children[0].children.Add(new muscleHeir("Lumbricals__Hand___L",true));//4
		Heirarchy [2].children [1].children [4].children[1].children.Add(new muscleHeir("Dorsal_interossei__L",true));//0
		Heirarchy [6].children [0].children [2].children[0].children.Add(new muscleHeir("Right sartorius",true));//0
		Heirarchy [6].children [0].children [2].children[0].children.Add(new muscleHeir("Right articularis genus",true));//1
		Heirarchy [6].children [0].children [2].children[0].children.Add(new muscleHeir("Right quadriceps femoris",false));//2
		Heirarchy [6].children [0].children [2].children[1].children.Add(new muscleHeir("Right semimembranosus",true));//0
		Heirarchy [6].children [0].children [2].children[1].children.Add(new muscleHeir("Right semitendinosus",true));//1
		Heirarchy [6].children [0].children [2].children[1].children.Add(new muscleHeir("Biceps_femoris_long_head___R",true));//2
		Heirarchy [6].children [0].children [2].children[1].children.Add(new muscleHeir("Biceps_femoris_short_head___R",true));//3
		Heirarchy [6].children [0].children [2].children[2].children.Add(new muscleHeir("Gracilis__R",true));//0
		Heirarchy [6].children [0].children [2].children[2].children.Add(new muscleHeir("Adductor_magnus__R",true));//1
		Heirarchy [6].children [0].children [2].children[2].children.Add(new muscleHeir("Adductor_longus__R",true));//2
		Heirarchy [6].children [0].children [2].children[2].children.Add(new muscleHeir("Adductor_brevis__R",true));//3
		Heirarchy [6].children [0].children [2].children[2].children.Add(new muscleHeir("Adductor_minimus__R",true));//4
		Heirarchy [6].children [0].children [2].children[2].children.Add(new muscleHeir("Pectineus__R",true));//5
		Heirarchy [6].children [0].children [3].children[0].children.Add(new muscleHeir("Tibialis_anterior__R",true));//0
		Heirarchy [6].children [0].children [3].children[0].children.Add(new muscleHeir("Extensor_hallucis_longus__R",true));//1
		Heirarchy [6].children [0].children [3].children[0].children.Add(new muscleHeir("Extensor_digitorum_longus__R",true));//2
		Heirarchy [6].children [0].children [3].children[0].children.Add(new muscleHeir("Fibularis_tertius__R",true));//3
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Right gastrocnemius",false));//0
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Plantaris__R",true));//1
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Soleus__R",true));//2
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Right clcaneal tendon",true));//3
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Popliteus__R",true));//4
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Right tibialis posterior",true));//5
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Flexor_hallucis_longus__R",true));//6
		Heirarchy [6].children [0].children [3].children[1].children.Add(new muscleHeir("Flexor_digitorum_longus__R",true));//7
		Heirarchy [6].children [0].children [3].children[2].children.Add(new muscleHeir("Fibularis_longus__R",true));//0
		Heirarchy [6].children [0].children [3].children[2].children.Add(new muscleHeir("Fibularis_brevis__R",true));//1
		Heirarchy [6].children [0].children [4].children[0].children.Add(new muscleHeir("First layer of plantar muscles of right foot",false));//0
		Heirarchy [6].children [0].children [4].children[0].children.Add(new muscleHeir("Second layer of plantar muscles of right foot",false));//1
		Heirarchy [6].children [0].children [4].children[0].children.Add(new muscleHeir("Third layer of plantar muscles of right foot",false));//2
		Heirarchy [6].children [0].children [4].children[1].children.Add(new muscleHeir("Superficial dorsal muscles of right foot",false));//0
		Heirarchy [6].children [0].children [4].children[1].children.Add(new muscleHeir("Deep dorsal muscles of right foot",false));//1
		Heirarchy [6].children [1].children [2].children[0].children.Add(new muscleHeir("Left sartorius",true));//0
		Heirarchy [6].children [1].children [2].children[0].children.Add(new muscleHeir("Left articularis genus",true));//1
		Heirarchy [6].children [1].children [2].children[0].children.Add(new muscleHeir("Left quadriceps femoris",false));//2
		Heirarchy [6].children [1].children [2].children[1].children.Add(new muscleHeir("Left semimembranosus",true));//0
		Heirarchy [6].children [1].children [2].children[1].children.Add(new muscleHeir("Left semitendinosus",true));//1
		Heirarchy [6].children [1].children [2].children[1].children.Add(new muscleHeir("Biceps_femoris_long_head___L",true));//2
		Heirarchy [6].children [1].children [2].children[1].children.Add(new muscleHeir("Biceps_femoris_short_head___L",true));//3
		Heirarchy [6].children [1].children [2].children[2].children.Add(new muscleHeir("Gracilis__L",true));//0
		Heirarchy [6].children [1].children [2].children[2].children.Add(new muscleHeir("Adductor_magnus__L",true));//1
		Heirarchy [6].children [1].children [2].children[2].children.Add(new muscleHeir("Adductor_longus__L",true));//2
		Heirarchy [6].children [1].children [2].children[2].children.Add(new muscleHeir("Adductor_brevis__L",true));//3
		Heirarchy [6].children [1].children [2].children[2].children.Add(new muscleHeir("Adductor_minimus__L",true));//4
		Heirarchy [6].children [1].children [2].children[2].children.Add(new muscleHeir("Pectineus__L",true));//5
		Heirarchy [6].children [1].children [3].children[0].children.Add(new muscleHeir("Tibialis_anterior__L",true));//0
		Heirarchy [6].children [1].children [3].children[0].children.Add(new muscleHeir("Extensor_hallucis_longus__L",true));//1
		Heirarchy [6].children [1].children [3].children[0].children.Add(new muscleHeir("Extensor_digitorum_longus__L",true));//2
		Heirarchy [6].children [1].children [3].children[0].children.Add(new muscleHeir("Fibularis_tertius__L",true));//3
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Left gastrocnemius",false));//0
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Plantaris__L",true));//1
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Soleus__L",true));//2
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Left clcaneal tendon",true));//3
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Popliteus__L",true));//4
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Left tibialis posterior",true));//5
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Flexor_hallucis_longus__L",true));//6
		Heirarchy [6].children [1].children [3].children[1].children.Add(new muscleHeir("Flexor_digitorum_longus__L",true));//7
		Heirarchy [6].children [1].children [3].children[2].children.Add(new muscleHeir("Fibularis_longus__L",true));//0
		Heirarchy [6].children [1].children [3].children[2].children.Add(new muscleHeir("Fibularis_brevis__L",true));//1
		Heirarchy [6].children [1].children [4].children[0].children.Add(new muscleHeir("First layer of plantar muscles of left foot",false));//0
		Heirarchy [6].children [1].children [4].children[0].children.Add(new muscleHeir("Second layer of plantar muscles of left foot",false));//1
		Heirarchy [6].children [1].children [4].children[0].children.Add(new muscleHeir("Third layer of plantar muscles of left foot",false));//2
		Heirarchy [6].children [1].children [4].children[1].children.Add(new muscleHeir("Superficial dorsal muscles of left foot",false));//0
		Heirarchy [6].children [1].children [4].children[1].children.Add(new muscleHeir("Deep dorsal muscles of left foot",false));//1

		Heirarchy [0].children [0].children [2].children[22].children[0].children.Add(new muscleHeir("Right inferior lateral pterygoid",true));//0
		Heirarchy [0].children [0].children [2].children[22].children[0].children.Add(new muscleHeir("Right superior lateral pterygoid",true));//1
		Heirarchy [0].children [0].children [2].children[22].children[1].children.Add(new muscleHeir("Right deep head medial pterygoid",true));//0
		Heirarchy [0].children [0].children [2].children[22].children[1].children.Add(new muscleHeir("Right superficial head medial pterygoid",true));//0
		Heirarchy [0].children [0].children [3].children[22].children[0].children.Add(new muscleHeir("Left inferior lateral pterygoid",true));//0
		Heirarchy [0].children [0].children [3].children[22].children[0].children.Add(new muscleHeir("Left superior lateral pterygoid",true));//1
		Heirarchy [0].children [0].children [3].children[22].children[1].children.Add(new muscleHeir("Left deep head medial pterygoid",true));//0
		Heirarchy [0].children [0].children [3].children[22].children[1].children.Add(new muscleHeir("Left superficial head medial pterygoid",true));//1
		Heirarchy [0].children [1].children [0].children[1].children[1].children.Add(new muscleHeir("Vertical intermediate part of right longus colli",true));//0
		Heirarchy [0].children [1].children [0].children[1].children[1].children.Add(new muscleHeir("Longus_colli_superior_oblique__R",true));//1
		Heirarchy [0].children [1].children [0].children[1].children[1].children.Add(new muscleHeir("inferior oblique part of right longus colli",true));//2
		Heirarchy [0].children [1].children [0].children[2].children[1].children.Add(new muscleHeir("Right posterior belly of digastric muscle",true));//0
		Heirarchy [0].children [1].children [0].children[2].children[1].children.Add(new muscleHeir("Right fibrous loop for intermediate digastric tendon",true));//1
		Heirarchy [0].children [1].children [0].children[2].children[1].children.Add(new muscleHeir("Right anterior belly of digastric muscle",true));//2
		Heirarchy [0].children [1].children [0].children[3].children[0].children.Add(new muscleHeir("Right superior belly of omohyoid",true));//0
		Heirarchy [0].children [1].children [0].children[3].children[0].children.Add(new muscleHeir("Right inferior belly of omohyoid",true));//1
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right cricothyroid",false));//0
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right oblique arytenoid",true));//1
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right aryepiglotticus",true));//2
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right posterior cricoarytenoid",true));//3
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right lateral cricoarytenoid",true));//4
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right vocalis",true));//5
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right thyroepiglotticus",true));//6
		Heirarchy [0].children [1].children [0].children[5].children[0].children.Add(new muscleHeir("Right thyroarytenoid",true));//7
		Heirarchy [0].children [1].children [0].children[5].children[1].children.Add(new muscleHeir("Right levator veli palatini muscle",true));//0
		Heirarchy [0].children [1].children [0].children[5].children[1].children.Add(new muscleHeir("Right tensor veli palatini muscle",true));//1
		Heirarchy [0].children [1].children [0].children[5].children[1].children.Add(new muscleHeir("Right palatopharyngeus",true));//2
		Heirarchy [0].children [1].children [0].children[5].children[1].children.Add(new muscleHeir("Right palatoglossus",true));//3
		Heirarchy [0].children [1].children [0].children[5].children[1].children.Add(new muscleHeir("Right musculus uvulae",true));//4
		Heirarchy [0].children [1].children [0].children[5].children[2].children.Add(new muscleHeir("Right superior pharyngeal constrictors",false));//0
		Heirarchy [0].children [1].children [0].children[5].children[2].children.Add(new muscleHeir("Right middle pharyngeal constrictor",true));//1
		Heirarchy [0].children [1].children [0].children[5].children[2].children.Add(new muscleHeir("Right inferior pharyngeal constrictors",true));//2
		Heirarchy [0].children [1].children [0].children[5].children[2].children.Add(new muscleHeir("Right lower part of cricopharyngeus",true));//3
		Heirarchy [0].children [1].children [0].children[5].children[2].children.Add(new muscleHeir("Right pharyngeal raphe",true));//4
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left cricothyroid",false));//0
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left oblique arytenoid",true));//1
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left aryepiglotticus",true));//2
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left posterior cricoarytenoid",true));//3
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left lateral cricoarytenoid",true));//4
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left vocalis",true));//5
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left thyroepiglotticus",true));//6
		Heirarchy [0].children [1].children [1].children[5].children[0].children.Add(new muscleHeir("Left thyroarytenoid",true));//7
		Heirarchy [0].children [1].children [1].children[5].children[1].children.Add(new muscleHeir("Left levator veli palatini muscle",true));//0
		Heirarchy [0].children [1].children [1].children[5].children[1].children.Add(new muscleHeir("Left tensor veli palatini muscle",true));//1
		Heirarchy [0].children [1].children [1].children[5].children[1].children.Add(new muscleHeir("Left palatopharyngeus",true));//2
		Heirarchy [0].children [1].children [1].children[5].children[1].children.Add(new muscleHeir("Left palatoglossus",true));//3
		Heirarchy [0].children [1].children [1].children[5].children[1].children.Add(new muscleHeir("Left musculus uvulae",true));//4
		Heirarchy [0].children [1].children [1].children[5].children[2].children.Add(new muscleHeir("Left superior pharyngeal constrictors",false));//0
		Heirarchy [0].children [1].children [1].children[5].children[2].children.Add(new muscleHeir("Left middle pharyngeal constrictor",true));//1
		Heirarchy [0].children [1].children [1].children[5].children[2].children.Add(new muscleHeir("Left inferior pharyngeal constrictors",true));//2
		Heirarchy [0].children [1].children [1].children[5].children[2].children.Add(new muscleHeir("Left lower part of cricopharyngeus",true));//3
		Heirarchy [0].children [1].children [1].children[5].children[2].children.Add(new muscleHeir("Left pharyngeal raphe",true));//4
		Heirarchy [2].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Abductor_pollicis_brevis__R",true));//0
		Heirarchy [2].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Flexor_pollicis_brevis__R",true));//1
		Heirarchy [2].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Adductor_pollicis__R",true));//2
		Heirarchy [2].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Right opponens pollicis",true));//3
		Heirarchy [2].children [0].children [4].children[0].children[1].children.Add(new muscleHeir("Flexor_digiti_minimi_brevis__R",true));//0
		Heirarchy [2].children [0].children [4].children[0].children[1].children.Add(new muscleHeir("Right opponens digiti minimi",true));//1
		Heirarchy [2].children [0].children [4].children[0].children[1].children.Add(new muscleHeir("Abductor_digiti_minimi__Hand___R",true));//2
		Heirarchy [2].children [0].children [4].children[0].children[4].children.Add(new muscleHeir("First lumbrical muscle of right hand",true));//0
		Heirarchy [2].children [0].children [4].children[0].children[4].children.Add(new muscleHeir("Second lumbrical muscle of right hand",true));//1
		Heirarchy [2].children [0].children [4].children[0].children[4].children.Add(new muscleHeir("Third lumbrical muscle of right hand",true));//2
		Heirarchy [2].children [0].children [4].children[0].children[4].children.Add(new muscleHeir("Fourth lumbrical muscle of right hand",true));//3
		Heirarchy [2].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Abductor_pollicis_brevis__L",true));//0
		Heirarchy [2].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Flexor_pollicis_brevis__L",true));//1
		Heirarchy [2].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Adductor_pollicis__L",true));//2
		Heirarchy [2].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Left opponens pollicis",true));//3
		Heirarchy [2].children [1].children [4].children[0].children[1].children.Add(new muscleHeir("Flexor_digiti_minimi_brevis__L",true));//0
		Heirarchy [2].children [1].children [4].children[0].children[1].children.Add(new muscleHeir("Left opponens digiti minimi",true));//1
		Heirarchy [2].children [1].children [4].children[0].children[1].children.Add(new muscleHeir("Abductor_digiti_minimi__Hand___L",true));//2
		Heirarchy [2].children [1].children [4].children[0].children[4].children.Add(new muscleHeir("First lumbrical muscle of left hand",true));//0
		Heirarchy [2].children [1].children [4].children[0].children[4].children.Add(new muscleHeir("Second lumbrical muscle of left hand",true));//1
		Heirarchy [2].children [1].children [4].children[0].children[4].children.Add(new muscleHeir("Third lumbrical muscle of left hand",true));//2
		Heirarchy [2].children [1].children [4].children[0].children[4].children.Add(new muscleHeir("Fourth lumbrical muscle of left hand",true));//3
		Heirarchy [6].children [0].children [2].children[0].children[2].children.Add(new muscleHeir("Right vastus medialis",true));//0
		Heirarchy [6].children [0].children [2].children[0].children[2].children.Add(new muscleHeir("Right vastus lateralis",true));//1
		Heirarchy [6].children [0].children [2].children[0].children[2].children.Add(new muscleHeir("Right vastus intermedius",true));//2
		Heirarchy [6].children [0].children [2].children[0].children[2].children.Add(new muscleHeir("Rectus_femoris__R",true));//3
		Heirarchy [6].children [0].children [2].children[0].children[2].children.Add(new muscleHeir("Right quadriceps femoris tendon",true));//4
		Heirarchy [6].children [0].children [3].children[1].children[0].children.Add(new muscleHeir("Gastrocnemius_lateral_head__R",true));//0
		Heirarchy [6].children [0].children [3].children[1].children[0].children.Add(new muscleHeir("Gastrocnemius_medial_head__R",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Abductor_hallucis__R",true));//0
		Heirarchy [6].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Flexor_digitorum_brevis__R",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[0].children.Add(new muscleHeir("Abductor_digiti_minimi__R",true));//2
		Heirarchy [6].children [0].children [4].children[0].children[1].children.Add(new muscleHeir("Right quadratus plantae",true));//0
		Heirarchy [6].children [0].children [4].children[0].children[1].children.Add(new muscleHeir("Lumbrical muscles of right foot",false));//1
		Heirarchy [6].children [0].children [4].children[0].children[2].children.Add(new muscleHeir("Right flexor hallucis brevis",false));//0
		Heirarchy [6].children [0].children [4].children[0].children[2].children.Add(new muscleHeir("Right adductor hallucis",false));//1
		Heirarchy [6].children [0].children [4].children[0].children[2].children.Add(new muscleHeir("Right interosseous muscles of right foot",false));//2
		Heirarchy [6].children [0].children [4].children[0].children[2].children.Add(new muscleHeir("Flexor_digiti_minimi_brevis__Hand___R",true));//3
		Heirarchy [6].children [0].children [4].children[1].children[0].children.Add(new muscleHeir("Extensor_hallucis_brevis__R",true));//0
		Heirarchy [6].children [0].children [4].children[1].children[0].children.Add(new muscleHeir("Right extensor digitorum brevis",true));//1
		Heirarchy [6].children [0].children [4].children[1].children[1].children.Add(new muscleHeir("Dorsi_interossei__Foot___R",true));//0
		Heirarchy [6].children [1].children [2].children[0].children[2].children.Add(new muscleHeir("Left vastus medialis",true));//0
		Heirarchy [6].children [1].children [2].children[0].children[2].children.Add(new muscleHeir("Left vastus lateralis",true));//1
		Heirarchy [6].children [1].children [2].children[0].children[2].children.Add(new muscleHeir("Left vastus intermedius",true));//2
		Heirarchy [6].children [1].children [2].children[0].children[2].children.Add(new muscleHeir("Rectus_femoris__L",true));//3
		Heirarchy [6].children [1].children [2].children[0].children[2].children.Add(new muscleHeir("Left quadriceps femoris tendon",true));//4
		Heirarchy [6].children [1].children [3].children[1].children[0].children.Add(new muscleHeir("Gastrocnemius_lateral_head__L",true));//0
		Heirarchy [6].children [1].children [3].children[1].children[0].children.Add(new muscleHeir("Gastrocnemius_medial_head__L",true));//1
		Heirarchy [6].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Abductor_hallucis__L",true));//0
		Heirarchy [6].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Flexor_digitorum_brevis__L",true));//1
		Heirarchy [6].children [1].children [4].children[0].children[0].children.Add(new muscleHeir("Abductor_digiti_minimi__L",true));//2
		Heirarchy [6].children [1].children [4].children[0].children[1].children.Add(new muscleHeir("Left quadratus plantae",true));//0
		Heirarchy [6].children [1].children [4].children[0].children[1].children.Add(new muscleHeir("Lumbrical muscles of left foot",false));//1
		Heirarchy [6].children [1].children [4].children[0].children[2].children.Add(new muscleHeir("Left flexor hallucis brevis",false));//0
		Heirarchy [6].children [1].children [4].children[0].children[2].children.Add(new muscleHeir("Left adductor hallucis",false));//1
		Heirarchy [6].children [1].children [4].children[0].children[2].children.Add(new muscleHeir("Left interosseous muscles of right foot",false));//2
		Heirarchy [6].children [1].children [4].children[0].children[2].children.Add(new muscleHeir("Flexor_digiti_minimi_brevis__Hand___L",true));//3
		Heirarchy [6].children [1].children [4].children[1].children[0].children.Add(new muscleHeir("Extensor_hallucis_brevis__L",true));//0
		Heirarchy [6].children [1].children [4].children[1].children[0].children.Add(new muscleHeir("Left extensor digitorum brevis",true));//1
		Heirarchy [6].children [1].children [4].children[1].children[1].children.Add(new muscleHeir("Dorsi_interossei__Foot___L",true));//0

		Heirarchy [0].children [1].children [0].children[5].children[0].children[0].children.Add(new muscleHeir("Right oblique part of cricothyroid",true));//0
		Heirarchy [0].children [1].children [0].children[5].children[0].children[0].children.Add(new muscleHeir("Right vertical part of cricothyroid",true));//1
		Heirarchy [0].children [1].children [0].children[5].children[2].children[0].children.Add(new muscleHeir("Right glossopharyngeal part of superior pharyngeal",true));//0
		Heirarchy [0].children [1].children [0].children[5].children[2].children[0].children.Add(new muscleHeir("Right superior pharyngeal constrictor muscle",true));//1
		Heirarchy [0].children [1].children [1].children[5].children[0].children[0].children.Add(new muscleHeir("Left oblique part of cricothyroid",true));//0
		Heirarchy [0].children [1].children [1].children[5].children[0].children[0].children.Add(new muscleHeir("Left vertical part of cricothyroid",true));//1
		Heirarchy [0].children [1].children [1].children[5].children[2].children[0].children.Add(new muscleHeir("Left glossopharyngeal part of superior pharyngeal",true));//0
		Heirarchy [0].children [1].children [1].children[5].children[2].children[0].children.Add(new muscleHeir("Left superior pharyngeal constrictor muscle",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("First lumbrical muscle of right foot",true));//0
		Heirarchy [6].children [0].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("Second lumbrical muscle of right foot",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("Third lumbrical muscle of right foot",true));//2
		Heirarchy [6].children [0].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("Fourth lumbrical muscle of right foot",true));//3
		Heirarchy [6].children [0].children [4].children[0].children[2].children[0].children.Add(new muscleHeir("Flexor_hallucis_brevis_medial_head__R",true));//0
		Heirarchy [6].children [0].children [4].children[0].children[2].children[0].children.Add(new muscleHeir("Flexor_hallucis_brevis_lateral_head__R",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[2].children[1].children.Add(new muscleHeir("Adductor_hallucis_oblique_head__R",true));//0
		Heirarchy [6].children [0].children [4].children[0].children[2].children[1].children.Add(new muscleHeir("Adductor_hallucis_transverse_head__R",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[2].children[2].children.Add(new muscleHeir("Interosseous_1st_plantar__R",true));//0
		Heirarchy [6].children [0].children [4].children[0].children[2].children[2].children.Add(new muscleHeir("Interosseous_2nd_plantar__R",true));//1
		Heirarchy [6].children [0].children [4].children[0].children[2].children[2].children.Add(new muscleHeir("Interosseous_third_plantar__R",true));//2
		Heirarchy [6].children [1].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("First lumbrical muscle of left foot",true));//0
		Heirarchy [6].children [1].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("Second lumbrical muscle of left foot",true));//1
		Heirarchy [6].children [1].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("Third lumbrical muscle of left foot",true));//2
		Heirarchy [6].children [1].children [4].children[0].children[1].children[1].children.Add(new muscleHeir("Fourth lumbrical muscle of left foot",true));//3
		Heirarchy [6].children [1].children [4].children[0].children[2].children[0].children.Add(new muscleHeir("Flexor_hallucis_brevis_medial_head__L",true));//0
		Heirarchy [6].children [1].children [4].children[0].children[2].children[0].children.Add(new muscleHeir("Flexor_hallucis_brevis_lateral_head__L",true));//1
		Heirarchy [6].children [1].children [4].children[0].children[2].children[1].children.Add(new muscleHeir("Adductor_hallucis_oblique_head__L",true));//0
		Heirarchy [6].children [1].children [4].children[0].children[2].children[1].children.Add(new muscleHeir("Adductor_hallucis_transverse_head__L",true));//1
		Heirarchy [6].children [1].children [4].children[0].children[2].children[2].children.Add(new muscleHeir("Interosseous_1st_plantar__L",true));//0
		Heirarchy [6].children [1].children [4].children[0].children[2].children[2].children.Add(new muscleHeir("Interosseous_2nd_plantar__L",true));//1
		Heirarchy [6].children [1].children [4].children[0].children[2].children[2].children.Add(new muscleHeir("Interosseous_third_plantar__L",true));//2

		//level kman : head and neck - done
		//			   lower limbs
	}

}

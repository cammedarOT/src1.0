﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*	this is object controller 1.0
 * 	it is supposed to control the animation, visibilty,
 * 	color ob an object
 */
	
	
public class ObjectConroller_generic : MonoBehaviour {

	SkinnedMeshRenderer skinnedMeshRenderer;
	Mesh skinnedMesh;
	//
	public bool Visible = true;
	public List<float> Blendshapes = new List<float>();
	public float Power;

	// lists in C# are like vectors for C++
	//
	private bool VisibleOptions;
	private int NoOfBlendshapes;


	void Awake ()
	{
		skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		skinnedMesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
		Power = 1.0f;
	}

	void Start () {
		//
		if (Visible)
			gameObject.GetComponent<Renderer>().enabled = true;
		//
		NoOfBlendshapes = skinnedMesh.blendShapeCount; 
		// Debug.Log (NoOfBlendshapes); //debugging only
		//
		for (int i = 0; i < NoOfBlendshapes; i++) {
			Blendshapes.Add (0.0f);
		}
	}


	void Update () {
		if (Visible) gameObject.GetComponent<Renderer>().enabled = true;
		else gameObject.GetComponent<Renderer>().enabled = false;
		//

		/* NOTE: BLENDSHAPES FILE CODE - WILL HAVE TO PUT IT BACK AGAIN
		UpdateBlendShapes();
		*/

	}



	/// <Custom functions, not called by the framework>
	/// /////////////////////////////////////////////
	void UpdateBlendShapes (){
		for (int i = 0; i < NoOfBlendshapes; i++) {
			if (Blendshapes [i] < 0)
				Blendshapes [i] = 0;
			if (Blendshapes [i] > 100)
				Blendshapes [i] = 100;
			skinnedMeshRenderer.SetBlendShapeWeight (i, Blendshapes [i] * Power);		
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //required forr the slider

public class DropdownController_generic : MonoBehaviour {

	public int Shapekey = 0;

	//create an object of same type of the script you are trying to ref.
	public ObjectConroller_generic targetObj;
	// to make this the current slider, you have to drag-and-drop it
	// into the reference box in the GUI of unity, like i did to the
	// object reference
	public Dropdown mainDropdown;

	void Start () {
		//Debug.Log (mainDropdown.itemText.ToString());
	}


	// search for how to interpolate the change between those valuse
	// to make the change smooth
	void Update () {
		if (targetObj != null) {
			if (mainDropdown.value == 0)
				targetObj.Power = 0.2f;
			if (mainDropdown.value == 1)
				targetObj.Power = 0.5f;
			if (mainDropdown.value == 2)
				targetObj.Power = 1.0f;
		}
	}
}

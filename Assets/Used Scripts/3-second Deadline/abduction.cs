﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class abduction : MonoBehaviour {

	public muscleControllersData_Managed PM_Deltoid;
	public muscleControllersData_Managed PM_Supra;
	public muscleControllersData_Managed SM_Biceps;

	public float sliderValue = 0;

	public Slider abductionSlider;
	public Toggle abductionEnable;

	public bool xx = false;

	void Start () {
		
	}
	
	void Update () {


		if (!abductionEnable.isOn)
			return;
		if (xx) {
			abductionSlider.value = 0.1f;
		}

		sliderValue = abductionSlider.value;

		if ((!PM_Deltoid.isActiveAndEnabled || !PM_Supra.isActiveAndEnabled) && SM_Biceps.isActiveAndEnabled)
			SM_Biceps.sliderValue = sliderValue;


		if (PM_Deltoid.isActiveAndEnabled)
			PM_Deltoid.sliderValue = sliderValue;
		if (PM_Supra.isActiveAndEnabled)
			PM_Supra.sliderValue = sliderValue;
				
	}
}

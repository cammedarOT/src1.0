﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Beebyte.Obfuscator;
public class menuObjectScript : MonoBehaviour {
	//the new way to create scripts, attached scripts
	// carry data only that will be accessed by a manager
	public GameObject reference;
    public muscleHeir muscleHeirerachyData;
	public muscleControllersData_Managed referenceScript; //3shan mafdalsh a3ml getcomponent we yb2a m3aia 3la tool
	public List <muscleHeir> children = new List <muscleHeir>(); // list of children (DB)
	public List <GameObject> childrenMenuObjects = new List <GameObject>();
    public List<actionObjectScript> actions;
	//di msh mosta5dama , mlhash lazma dlwa2ty
	[ObfuscateLiterals]
	public void sendData( bool state) //accessed from outside - observer pattern 
	{
		Debug.Log ("observed the change: " + state.ToString ());

		reference.SetActive(state);

	}
}

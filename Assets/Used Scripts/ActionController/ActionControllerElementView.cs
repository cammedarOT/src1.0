﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class ActionControllerElementView : MonoBehaviour, IPointerClickHandler
{
	
    public Text elementName;
    public Button playButton;
    private bool playButtonState = false;
    private int ID;     //The ID of this element in the action controller menu
    private MuscleType type;
    private bool MuscleState = true;
    private ActionController controller;
    public ActionController Controller { get { return controller; } set { controller = value; } }

    /*
    * @Name : Set Element Name
    * @Params : elementName (String)
    * @Behaviour : Sets The element with the argument string, The argument wouldn't be changed inside this function
    * Really would have declared it const but there no such thing in c#
    */
    public void SetElementName(string elementName)
    {
        this.elementName.text = elementName;
    }

    private void Start()
    {
        playButton.onClick.AddListener(ActionElementPlayButtonClickListener);
    }
    public void SetID(int ID)
    {
        this.ID = ID;
    }

    public void ActionElementPlayButtonClickListener()
    {
        playButtonState = !playButtonState;
        this.transform.parent.transform.parent.GetComponent<ActionView>().GetController().OnActionElementPlayButtonClicked(ID,type, playButtonState);
        this.transform.parent.transform.parent.GetComponent<ActionView>().playButton.GetComponent<TriggerButton>().IntermediateFunction();
        this.transform.parent.transform.parent.GetComponent<ActionView>().playButton.onClick.Invoke();
    }

    public void Destroy()
    {
        playButton.onClick.RemoveAllListeners();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        MuscleState = !MuscleState;
        if (MuscleState == true)   //Must Be enabled
        {
            elementName.color = ColorScheme.green;
            this.transform.parent.transform.parent.GetComponent<ActionView>().GetController().OnActionElementTextClicked(ID,type, MuscleState);
        }
        else
        {
            elementName.color = ColorScheme.red;
            this.transform.parent.transform.parent.GetComponent<ActionView>().GetController().OnActionElementTextClicked(ID,type, MuscleState);
        }
    }

    public void SetMuscleState(bool MuscleState)
    {
        this.MuscleState = !MuscleState;
        if (this.MuscleState == true)
        {
            elementName.color = ColorScheme.green;
        }
        else
        {
            elementName.color = ColorScheme.red;
        }
    }

    public void SetElementStatus(bool state)
    {
        MuscleState = state;
        if (MuscleState == true)
        {
            elementName.color = ColorScheme.green;
        }
        else
        {
            elementName.color = ColorScheme.red;
        }
    }
    public void SetType(MuscleType type)
    {
        this.type = type;
    }
    private void UpdateTextElement()
    {
        SetElementStatus(this.transform.parent.transform.parent.GetComponent<ActionView>().GetController().GetMuscleState(ID, type));
    }
    public void Update()
    {
        UpdateTextElement();
    }
}

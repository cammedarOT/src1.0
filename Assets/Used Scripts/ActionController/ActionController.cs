﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;

public enum MuscleType
{
    PRIMARY_MUSCLE = 0,
    SECONDARY_MUSCLE = 1,
    SECONDARY_MOTION_MUSCLE = 2
}
/*
 * A view class for dealing with the action controller Menu
 * This is a view/view controller class, it will not modify any data
 */
public class ActionController
{
    private const string ACTION_TAB_TAG = "ActionsTab";
    private const int ACTIONS_TAB_VIEW_PORT_INDEX = 0, ACTIONS_TAB_CONTENT_PANE_INDEX = 0;
    private GameObject actionController;
    private Canvas parentCanvas;
    private List<muscleControllersData_Managed> primaryMuscles;
    private List<muscleControllersData_Managed> secondaryMuscles;
    private List<muscleControllersData_Managed> secondaryMotionMuscles;
    private List<List<muscleControllersData_Managed>> muscles = new List<List<muscleControllersData_Managed>>(3);
    private const int ACTION_ELEMENT_TEXT = 0;
    private GameObject actionMenuObject;
    private UI_manager manager;
    public GameObject rigg3D;
    public GameObject actionsMenuContent;

    /*
     * @Name        Default Constructor
     * @Params      None
     * @Behaviour   Instintiates the Action Controller
     */
    public ActionController()
    {
        actionController = GameObject.Instantiate(Resources.Load("Action_Controller")) as GameObject;
        actionsMenuContent = GameObject.FindGameObjectWithTag(ACTION_TAB_TAG).GetComponent<ActionTabView>().actionTabContainer.transform.GetChild(ACTIONS_TAB_VIEW_PORT_INDEX).transform.GetChild(ACTIONS_TAB_CONTENT_PANE_INDEX).gameObject;
        rigg3D = GameObject.FindGameObjectWithTag("Rig") as GameObject;
        manager = GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<UI_manager>();
    }
    public void SetActionMenuObject(GameObject actionMenuObject)
    {
        this.actionMenuObject = actionMenuObject;
        actionController.GetComponent<boxReferenceObject>().reference = actionMenuObject;
    }

    public void SetActionName(string actionName)
    {
        actionController.GetComponent<ActionView>().SetActionName(actionName);
    }

	[ObfuscateLiterals]
    /*
     * @Name Show 
     * @params None
     * @Behaviour   Shows the ActionController Menu when called
     *              You muse set the controller position prior to calling or it will use a random position
     *              you must set the primary Muscles List form outside
     */
    public void Show()
    {
        //actionController.GetComponent<ActionView>().SetActionPrimaryMuscles(primaryMusclesElements);
        parentCanvas = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<Canvas>();
        actionController.GetComponent<ActionView>().SetController(this);
        //actionController.transform.SetParent(parentCanvas.transform);
        //BassemEdit
        actionController.transform.SetParent(GameObject.FindGameObjectWithTag("hoverWindows").transform);
        actionController.SetActive(true);
        actionController.transform.transform.localPosition = new Vector3(450, -300);

    }
    public void SetPrimaryMotion(string PrimaryMotion)
    {
        actionController.GetComponent<ActionView>().SetPrimaryMotionType(PrimaryMotion);
    }
    public void SetSecondaryMotion(string SecondaryMotion)
    {
        actionController.GetComponent<ActionView>().SetSecondaryMotion(SecondaryMotion);
    }
	[ObfuscateLiterals]
    /*
     * @Name: SetPrimaryMuscles
     * @Params : A list of MuscleControllerData_Managed containing the primary Muscles
     * @Behaviour : Sets the internal primaryMusclesList with a refrence from the external one, Creates the View elements
     *              Doesn't change the Muscles List by any way
     * 
     */
    public void SetPrimaryMuscels(List<muscleControllersData_Managed> primaryMuscles)
    {
        muscles.Insert((int)(MuscleType.PRIMARY_MUSCLE), this.primaryMuscles);
        if (primaryMuscles == null || primaryMuscles.Count ==0)
            return;
        this.primaryMuscles = primaryMuscles;
        actionController.GetComponent<boxReferenceObject>().mc.AddRange(primaryMuscles);
        actionController.GetComponent<ActionView>().SetActionPrimaryMuscles(ConvertMuscleListToActionElements(this.primaryMuscles, "ActionControllerElement", MuscleType.PRIMARY_MUSCLE));
        muscles[(int)(MuscleType.PRIMARY_MUSCLE)]= this.primaryMuscles;

    }
	[ObfuscateLiterals]
    /*
     * @Name: SetSecondaryMuscles
     * @Params : A list of MuscleControllerData_Managed containing the secondary Muscles
     * @Behaviour : Sets the internal Seoncdary Muscles List with a refrence from the external one, Creates the View elements
     *              Doesn't change the Muscles List by any way
     * 
     */
    public void SetSecondaryMuscles(List<muscleControllersData_Managed> secondaryMuscles)
    {
        muscles.Insert((int)(MuscleType.SECONDARY_MUSCLE), this.secondaryMuscles);
        if (secondaryMuscles == null || secondaryMuscles.Count == 0)
            return;
        this.secondaryMuscles = secondaryMuscles;
        actionController.GetComponent<boxReferenceObject>().mc.AddRange(this.secondaryMuscles);
        actionController.GetComponent<ActionView>().SetActionSecondaryMuscles(ConvertMuscleListToActionElements(this.secondaryMuscles, "ActionControllerElement", MuscleType.SECONDARY_MUSCLE));
        muscles[(int)(MuscleType.SECONDARY_MUSCLE)] = this.secondaryMuscles;
    }
	[ObfuscateLiterals]
    public void SetSecondaryMotionMuscles(List<muscleControllersData_Managed> secondaryMotionMuscles,string secondaryMotionType)
    {
        muscles.Insert((int)(MuscleType.SECONDARY_MOTION_MUSCLE), this.secondaryMotionMuscles); 
        if (secondaryMotionMuscles == null || secondaryMotionMuscles.Count == 0)
            return;
        this.secondaryMotionMuscles = secondaryMotionMuscles;
        actionController.GetComponent<ActionView>().SetActionSecondaryMotionMuscles(ConvertMuscleListToActionElements(this.secondaryMotionMuscles, "ActionControllerElement", MuscleType.SECONDARY_MOTION_MUSCLE), secondaryMotionType);
        muscles[(int)(MuscleType.SECONDARY_MOTION_MUSCLE)] =  this.secondaryMotionMuscles;
    }
	[ObfuscateLiterals]
    //4- hena ha7ot el function bta3et click el L wel R
    public void OnActionElementPlayButtonClicked(int elementID,MuscleType type, bool playButtonState)
    {
        SetAllMuscles(false);
        manager.dysfunctionalToggleChanged(manager.getMuscleObject(muscles[(int)(type)][elementID]), playButtonState);
    }

    public void OnActionElementTextClicked(int elementID,MuscleType type, bool playButtonState)
    {
        manager.dysfunctionalToggleChanged(manager.getMuscleObject(muscles[(int)(type)][elementID]), playButtonState);
    }

    public bool SetSliderValue(float sliderValue)
    {
        //TODO: create Setter
        return actionController.GetComponent<ActionView>().SetSliderValue(sliderValue);
    }

    public void setPositionOnCreate(Vector3 mousePos)
    {
        //TODO set position
        actionController.transform.Translate(mousePos);

    }


    /*
     *  @Name : Simulate Play Button CLick 
     *  @Params : NONE 
     *  @Behaviour : simulates the play button click by
     *                  a) Calling the action view button clicked
     *                  b) calling the Trigger Button intermediate function
     *  //Notes : Really need to remove this intermediate function
     */
    public void SimulatePlayButtonClick()
    {
        actionController.GetComponent<ActionView>().ActionControllerMenuPlayButtonClickSimulator();
    }
	[ObfuscateLiterals]
    //mwgoda tani fi setAllFunctionalityFunc.cs
    // true -> all functional 
    // false -> all dysfunctional
    public void SetAllMuscles(bool functionality)
    {
        int musclesNumber = rigg3D.transform.childCount;
        for (int i = 0; i < musclesNumber; i++)
        {
            GameObject muscle = rigg3D.transform.GetChild(i).gameObject;

            //if it is a muscle and not a parent in the heirarchy
            if (muscle.GetComponent<muscleControllersData_Managed>())
            {
                //the boolean value means (disfunctional) inside the object so we take its not
                muscle.GetComponent<muscleControllersData_Managed>().setDysfunctional(!functionality);

                //this code should not be here , somewhere else where we can now if the muscle is selected or not
                //to properly choose the current color of the muscle object (yellow, red, gray, .. ) and opacity
                if (!functionality)
                { // -> dysfunctional = false 
                    muscle.GetComponent<Renderer>().material.color = new Color32(92, 89, 89, 1);
                }
                else
                {
                    muscle.GetComponent<Renderer>().material.color = Color.white;
                }
            }
        }

        //this code to recalculate the action behaviour values based on the new functionlity of all the muscles
        actionsMenuContent.GetComponent<populateActionsTab>().calculateDisfunc(); //should only be called when the actions tab is populated
        actionsMenuContent.GetComponent<populateActionsTab>().calculateActionAngles();
    }

	[ObfuscateLiterals]
	/*
     *  @Name : left or right Clicked
     *  @Params : the action object script of the action controller on which left or right was clicked 
     *  @Behaviour : 1- uses the flipActionLandR() function to flip the given action
     * 				 2- uses the same function again to flip all its accompanying actions
     * 				 3- sets the new primary muscles of the action controller
     * 				 4- shows the new muscles in the Rigg3d
	*/
	public void leftOrrightClicked(actionObjectScript aos, char oldchar, char newchar){
		
		flipActionLandR (aos, oldchar, newchar);
		for (int i = 0; i < aos.accompayingActions.Count; i++)
			flipActionLandR (aos.accompayingActions [i], oldchar, newchar);
		SetPrimaryMuscels (aos.mc); // di hte3ml sa7 lma n merge
		aos.oldc = oldchar; aos.newc = newchar;
		List <muscleControllersData_Managed> mmmm = actionMenuObject.GetComponent<actionObjectScript>().mc;
		manager.showSelectedActionMuscles ();
		manager.showThisActionMuscles (aos);
		// we wna ba3ml l actionController a3rf bs hwa kan ma2fol 3la L wla R

		actionsMenuContent.GetComponent<populateActionsTab>().calculateDisfunc(); //should only be called when the actions tab is populated
		actionsMenuContent.GetComponent<populateActionsTab>().calculateActionAngles();


	}
	[ObfuscateLiterals]
	/*
     *  @Name : flip Action L and R
     *  @Params : the action script to be flipped 
     *  @Behaviour : - uses the names of the muscles and armature in the action 
     * 				   to find the new muscles
	*/
	private void flipActionLandR(actionObjectScript aos, char oldchar, char newchar){
		aos.newc = newchar; aos.oldc = oldchar;
		if (aos.one) {
			string oneName = aos.one.name;
			oneName = oneName.Replace(oldchar,newchar);//ana 3amlha kda 3ashan lw fi moshkla t3mli error hna, 3shan mfrod my7slsh moshkla
			aos.one = GameObject.Find(oneName).transform;
		} 
		if (aos.two) {
			string twoName = aos.two.name;
			twoName = twoName.Replace(oldchar,newchar);//ana 3amlha kda 3ashan lw fi moshkla t3mli error hna, 3shan mfrod my7slsh moshkla
			aos.two = GameObject.Find(twoName).transform;
		}
		for (int i = 0; i < aos.mc.Count; i++) {
			string mcName = aos.mc [i].name;
			mcName = mcName.Substring (1);
			mcName = mcName.Replace(oldchar,newchar);
			mcName = aos.mc [i].name [0] + mcName;
			GameObject cc = rigg3D.transform.Find (mcName).gameObject;
			aos.mc [i] = cc.GetComponent<muscleControllersData_Managed>();
		}
	}

    /*
     * @Name : SetAngle
     * @Params : The angle as a float value
     * @Behaviour : Shows the angle in the action controller
     */ 
    public void SetAngle(float angleValue)
    {
        actionController.GetComponent<ActionView>().SetAngle(angleValue.ToString());
    }


    private List<GameObject> ConvertMuscleListToActionElements(List<muscleControllersData_Managed> muscles,string actionElement,MuscleType type)
    {
        List<GameObject> actionElements = new List<GameObject>();
        foreach (muscleControllersData_Managed muscle in muscles)
        {
            //TODO: See why this muscle comes as null
            //I did this in this release cuz mohamed said we are not concentrating on actions
            //So let there be null checks
            //I know this is against the princible of not putting reduntant checks, so don't judge me
            if (muscle == null)
            {
                Debug.Log("This muscle comes to the action controller as NULL");
                continue;
            }
            GameObject currentMuscleElement = GameObject.Instantiate(Resources.Load(actionElement)) as GameObject;
            currentMuscleElement.GetComponent<ActionControllerElementView>().SetElementName(muscle.name);
            currentMuscleElement.GetComponent<ActionControllerElementView>().SetMuscleState(muscle.dysfunctional);
            currentMuscleElement.GetComponent<ActionControllerElementView>().SetID(muscles.IndexOf(muscle));
            currentMuscleElement.GetComponent<ActionControllerElementView>().SetElementStatus(!muscle.dysfunctional);
            currentMuscleElement.GetComponent<ActionControllerElementView>().SetType(type);
            actionElements.Add(currentMuscleElement);
        }
        return actionElements;
    }
    public void ClearMuscles()
    {
        actionController.GetComponent<ActionView>().DestoryMuscleElements();
    }

    /*
     * @Name : Set Muscles
     * @Params : Primary Motion Type , Primary Muscles List , Secondary Muscles List , Secondary Motion, Secondary Motion Type , Secondary Motion Muscles List
     * @Behaviour : Clears the Muscles Panel in the action controller, the puts each muscle into its place
     * @Notes : This is for bassem to use when he wants to switch between left and right
     */
    public void SetMuscles(string primaryMotion , List<muscleControllersData_Managed> primaryMuscles , List<muscleControllersData_Managed> secondaryMuscles , 
        string secondaryMotion , string secondaryMotionType , List<muscleControllersData_Managed> secondaryMotionMuscles)
    {
        ClearMuscles();
        SetPrimaryMotion(primaryMotion);
        SetPrimaryMuscels(primaryMuscles);
        SetSecondaryMuscles(secondaryMuscles);
        SetSecondaryMotion(secondaryMotion);
        SetSecondaryMotionMuscles(secondaryMotionMuscles, secondaryMotionType);

    }
    public bool GetMuscleState(int elementID, MuscleType type)
    {
        return !muscles[(int)type][elementID].dysfunctional;
    }
}
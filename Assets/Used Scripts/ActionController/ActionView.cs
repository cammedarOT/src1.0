﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class ActionView : MonoBehaviour
{
	
    public Text actionName;
    public GameObject musclesPanel;
    public Button leftButton;
    public Button rightButton;
    public Button leftRightButton;
    public Button playButton;
    public Slider actionControllerSlider;
    public Text angle;
    public GameObject titlePrefab, secondaryTitlePrefab;
    private ActionController controller;    //The view Must Have refrence to its controller to report events
    private ActionSliderManager Manager;
    private bool multiSelect;
    private const string ACTION_TAB_TAG = "ActionsTab";
	private bool left = true;
	private bool right = false;

    private void Start()
    {
        playButton.onClick.AddListener(ActionControllerMenuPlayButtonListener);
        actionControllerSlider.onValueChanged.AddListener(ActionControllerSliderValueChangedListener);
        Manager = new ActionSliderManager(actionControllerSlider, playButton);
		leftButton.onClick.AddListener (ActionControllerLeftButtonListener);
		rightButton.onClick.AddListener (ActionControllerRightButtonListener);
    }

    private void Update()
    {
        Manager.Manage();
	    calculateAngle ();
    }
 
    public void ActionControllerSliderValueChangedListener(float value)
    {
        //work around  as unity destroys action tab before action controller, causes null refrence exception when exit
        GameObject actionTab = GameObject.FindGameObjectWithTag(ACTION_TAB_TAG);
        if (actionTab != null)
        {
            actionTab.GetComponent<ActionTabView>().GetController().SetSliderValue(value);
        }

    }


    /*
     * @Name : Set Slider Value 
     * @Params : Value (FLoat)
     * @Behaviour : Sets the slider value with the argument
     * @Returns : True if the value was set correctly, false other wise
     */
    public bool SetSliderValue(float value)
    {
        return Utils.SetSliderValue(actionControllerSlider, value);
    }
    //Sets the action controller objcet for future refrencing
    public void SetController(ActionController controller)
    {
        this.controller = controller;
    }

    public ActionController GetController()
    {
        return controller;
    }
    /*
     * @Name : Set Action Name
     * @Params : actionName (String)
     * @Behaviour : Sets The action menu name with the argument string, The argument wouldn't be changed inside this function
     * Really would have declared it const but there no such thing in c#
     */
    public void SetActionName(string actionName)
    {
        this.actionName.text = actionName;
    }
    private void CreateTextPrefab(GameObject prefab , string text)
    {
        GameObject primaryMotionTypeText = GameObject.Instantiate(prefab);
        primaryMotionTypeText.GetComponent<Text>().text = text;
        primaryMotionTypeText.transform.SetParent(musclesPanel.transform);
    }
    public void SetPrimaryMotionType(string primaryMotionType)
    {
        CreateTextPrefab(titlePrefab, primaryMotionType);
    }
	[ObfuscateLiterals]
    /*
     * @Name : SetActionPrimaryMuscles
     * @Pararms : A list of ActionControllerElement Objects, each containg the name of a primary muscle
     * @Behaviour : Sets the parent of the elements as the musclePanel
     * 
     * 
     */
    public void SetActionPrimaryMuscles(List<GameObject> primaryMuscleElements)
    {
        CreateTextPrefab(secondaryTitlePrefab, "Primary Muscles");
        if (primaryMuscleElements == null)
            return;
        foreach (GameObject primaryMuscleElement in primaryMuscleElements)
        {
            primaryMuscleElement.transform.SetParent(musclesPanel.transform);
        }
    }
	[ObfuscateLiterals]
    public void SetActionSecondaryMuscles(List<GameObject> secondaryMuscleElements)
    {
        CreateTextPrefab(secondaryTitlePrefab, "Secondary Muscles");
        if (secondaryMuscleElements == null)
            return;
        foreach (GameObject primaryMuscleElement in secondaryMuscleElements)
        {
            primaryMuscleElement.transform.SetParent(musclesPanel.transform);
        }
    }
    public void SetSecondaryMotion(string SecondaryMotion)
    {
        CreateTextPrefab(titlePrefab, SecondaryMotion);
    }
    public void SetActionSecondaryMotionMuscles(List<GameObject> secondaryMotionMuscleElements , string seconadaryMotionType)
    {
        CreateTextPrefab(secondaryTitlePrefab, seconadaryMotionType);
        if (secondaryMotionMuscleElements == null)
            return;
        foreach (GameObject primaryMuscleElement in secondaryMotionMuscleElements)
        {
            primaryMuscleElement.transform.SetParent(musclesPanel.transform);
        }
    }
    //hagib l action el ana ba refer lih mn l boxReferenceObject script
    //ha3del l one wel two lw mwgooda , wel mc kolha , wazbot alwanhom a5dar wa7mar , wandah function calculate dysfunctions we calculateActionAngles w ablha a5ali el bool fl action be R aw be L 
    public void ActionControllerMenuPlayButtonListener()
    {
        Manager.ButtonClicked();
        GameObject.FindGameObjectWithTag(ACTION_TAB_TAG).GetComponent<ActionTabView>().GetController().SimulatePlayButtonClick();
    }

	public void ActionControllerLeftButtonListener(){
		if (right) {
			actionObjectScript action = this.gameObject.GetComponent<boxReferenceObject> ().reference.GetComponent<actionObjectScript> ();
			left = true;
			right = false;
			controller.leftOrrightClicked (action, 'R','L');
			rightButton.GetComponent<TriggerButton> ().IntermediateFunction ();
		} else {
			leftButton.GetComponent<TriggerButton> ().IntermediateFunction ();
		}

	}
	public void ActionControllerRightButtonListener(){
		if (left) {
			actionObjectScript action = this.gameObject.GetComponent<boxReferenceObject> ().reference.GetComponent<actionObjectScript> ();
			left = false;
			right = true;
			controller.leftOrrightClicked (action, 'L','R');
			leftButton.GetComponent<TriggerButton> ().IntermediateFunction ();
		} else {
			rightButton.GetComponent<TriggerButton> ().IntermediateFunction ();
		}
	}

    public void ActionControllerMenuPlayButtonClickSimulator()
    {
        Manager.ButtonClicked();
        playButton.GetComponent<TriggerButton>().IntermediateFunction();
    }
    public void SetMultiSelect(bool multiSelect)
    {
        this.multiSelect = multiSelect;
    }
    public void SetAngle(string angleValue)
    {
        angle.text = angleValue;
    }
    public void OnDestroy()
    {

        playButton.onClick.RemoveAllListeners();
        if (!multiSelect)
            SetSliderValue(0.0f);
    }
    public void DestoryMuscleElements()
    {
        for (int i = 0; i < musclesPanel.transform.childCount; i++)
        {
            GameObject.Destroy(musclesPanel.transform.GetChild(i).gameObject);
        }
    }


	//quick hack  - Bassem
	/*
     * @Name : calculate Angle
     * @Pararms : None
     * @Behaviour : calculates the angle of the action and calls set angle to set this angle
     * 				overrides the use of setAngle in the actionController class - untill we delete it
     * 
     * 
     */
	[ObfuscateLiterals]
	public void calculateAngle(){
		actionObjectScript actionObjectscriptRef = this.gameObject.GetComponent<boxReferenceObject> ().reference.GetComponent<actionObjectScript> ();
		Vector3 angles = actionObjectscriptRef.refRot.eulerAngles - actionObjectscriptRef.one.localRotation.eulerAngles;
		float angle = 0.0f;
		//anna asef ya Hassan , bs msh b 2idi :'( kol wa7da liha angle , we lsh msh keda haro7 le 5ara el Quaternion
		if (actionName.text == "shoulder flexion") 
			angle = angles.x + 281;	
		else if (actionName.text == "shoulder extension") 
			angle = - (angles.x + 281);
		else if (actionName.text == "shoulder horizontal abduction") 
			angle = angles.x;
		else if (actionName.text == "shoulder horizontal adduction") 
			angle = angles.x;
		else if (actionName.text == "shoulder abduction") 
			angle = angles.x;
		else if (actionName.text == "shoulder adduction") 
			angle = angles.x;
		else if (actionName.text == "shoulder internal rotation") 
			angle = angles.x;
		else if (actionName.text == "shoulder external rotation") 
			angle = angles.x;
		

		SetAngle (angle.ToString ());
	}
}

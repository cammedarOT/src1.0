﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TestConstants  {

    private  static Color disabledMuscleColor = new Color32(92, 89, 89, 1);
    public static Color DisabledMuscleColor { get { return disabledMuscleColor; } }

    private static string buttonToolBarTag = "ButtonToolBar";
    public static string ButtonToolBarTag { get { return buttonToolBarTag; } }
    private static  string gUISceneName = "GUI";
    public static string GUISceneName { get { return gUISceneName; } }
    private static string rigTag = "Rig";
    public static string RigTag { get { return rigTag; } }

}

﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace Cammedar.Tests
{
    public class ColorComparer : IComparer
    {
        private float epsilon = 0.1f;
        public ColorComparer(float epsilon)
        {
            this.epsilon = epsilon;
        }
        public ColorComparer()
        {

        }
        public int Compare(object x, object y)
        {
            if (!(x is Color) || !(y is Color))
                return -1;
            Color firstColor = (Color)x;
            Color secondColor = (Color)y;
            return Mathf.Abs(firstColor.r - secondColor.r) > epsilon || Mathf.Abs(firstColor.g - secondColor.g) > epsilon || Mathf.Abs(firstColor.b - secondColor.b) > epsilon || Mathf.Abs(firstColor.a - secondColor.a) > epsilon ? 1 : 0;
        }
    }

    public class DisableButtonTest {

        // A UnityTest behaves like a coroutine in PlayMode
        // and allows you to yield null to skip a frame in EditMode
        [UnityTest]
        public IEnumerator DisableButtonPressChangesTheMuscleColor() {
            GetDisableButton().onClick.Invoke();
            yield return null;
            GameObject rig3d = GetRig3d();
            for (int i = 0; i < rig3d.transform.childCount; i++)
            {
                if (rig3d.transform.GetChild(i).GetComponent<muscleControllersData_Managed>() != null)
                {
                    Assert.That(rig3d.transform.GetChild(i).GetComponent<SkinnedMeshRenderer>().material.color, Is.EqualTo(TestConstants.DisabledMuscleColor).Using(new ColorComparer(0.1f)), "Assertion Failed on " + rig3d.transform.GetChild(i).name);
                }
            }
        }
        [Test]
        public void TestGUISceneIsLoadedCorrectly()
        {
            Assert.That(SceneManager.GetActiveScene().name, Is.EqualTo(TestConstants.GUISceneName));
        }
        [Test]
        public void GetButtonsToolBarTest()
        {
            const string BUTTONS_TOOL_BAR_NAME = "Toolbar";
            Assert.That(GetButtonsToolBar().name, Is.EqualTo(BUTTONS_TOOL_BAR_NAME));
        }
        [Test]
        public void GetDisableButtonTest()
        {
            const string DISABLE_BUTTON_NAME = "Toolbar_Functional";
            Assert.That(GetDisableButton().name, Is.EqualTo(DISABLE_BUTTON_NAME));
        }
        [Test]
        public void GetRigTest()
        {
            const string RIG_NAME = "FullBody-NewRig_v2.5";
            Assert.That(GetRig3d().name, Is.EqualTo(RIG_NAME));
        }
        [OneTimeSetUp]
        public void SetupSceneForTest()
        {
            SceneManager.LoadScene(TestConstants.GUISceneName, LoadSceneMode.Single);
        }
        //These privates are dependant on unity's system. but I will create a tester for them so if theyy fail we know

        private Button GetDisableButton()
        {
            return GetButtonsToolBar().transform.GetChild(DISABLE_BUTTON_INDEX).GetComponent<Button>();
        }
        public GameObject GetButtonsToolBar()
        {
            return GameObject.FindGameObjectWithTag(TestConstants.ButtonToolBarTag);
        }
        public GameObject GetRig3d()
        {
            return GameObject.FindGameObjectWithTag(TestConstants.RigTag);
        }
        private const int DISABLE_BUTTON_INDEX = 2;
        
    }
}
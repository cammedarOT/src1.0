﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MuscleStartupListCreator : MonoBehaviour {
    public Refrences refrences;
   
    private List<Muscle> muscles;

	void Start () {
        
        muscles = TraverseRig();
        StringBuilder builder = new StringBuilder();
        foreach(Muscle muscle in muscles)
        {
            builder.Append(muscle.Name);
            builder.AppendLine();
        }
        System.IO.File.WriteAllText("Muscles.txt", builder.ToString());

    }
    /*
     * This function is used in the rig traversal, it's able to identify if a game object is a muscle or not
     * currently it just checks if it contains muscleControllersData_Managed, but I reckon the check will be much more
     * substantial
     * 
     */
    private bool ValidMuscle(GameObject muscle)
    {
        return muscle.GetComponent<muscleControllersData_Managed>() != null;
    }

    /*
     * This function is used to traverse the 3d rig and extract all objects which are
     * "Valid Muscles" from it
     * it uses DFS approach to traverse, with my own stack to avoid recursion
     * 
     */
    private List<Muscle> TraverseRig()
    {
        List<Muscle> muscles = new List<Muscle>();
        GameObject rig3d = refrences.rig3d;
        Stack<GameObject> muscleGameObjectsStack = new Stack<GameObject>();
        muscleGameObjectsStack.Push(rig3d);
        while(muscleGameObjectsStack.Count != 0)
        {
            GameObject currentMuscleGameObject = muscleGameObjectsStack.Pop();
            for(int i =0; i < currentMuscleGameObject.transform.childCount; i++)
            {
                muscleGameObjectsStack.Push(currentMuscleGameObject.transform.GetChild(i).gameObject);
            }
            if(ValidMuscle(currentMuscleGameObject))
            {
                Muscle currentMuscle = new Muscle
                {
                    MuscleGameObject = currentMuscleGameObject,
                    Enabled = true,
                    Name = Utils.FixMuscleName(currentMuscleGameObject.name)
                };
                muscles.Add(currentMuscle);
            }
        }
        return muscles;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muscle {

    private string name;
    public string Name { get { return name; } set { name = value; } }

    private bool enabled;
    public bool Enabled { get { return enabled; } set { enabled = value; } }


    private GameObject musclegameObject;
    public GameObject MuscleGameObject { get { return musclegameObject; } set { musclegameObject = value; } }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LayerChooseButton : MonoBehaviour {

    LayersController controller;
    private const string SCRIPT_OBJECT_TAG = "ScriptManager";
	// Use this for initialization
	void Start () {
       controller =  GameObject.FindGameObjectWithTag(SCRIPT_OBJECT_TAG).GetComponent<LayersController>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnLayerIncreaseButtonClick()
    {
		int layer = controller.GetLayer () + 1;
		if (layer >= controller.getNumLayers ())
			this.GetComponent<Button> ().interactable = false;
			
        controller.SetLayer(layer);
    }
    public void OnLayerDecreaseButtonClick()
	{
		int layer = controller.GetLayer () - 1;
		if (layer <= 0)
			this.GetComponent<Button> ().interactable = false;
		controller.SetLayer(layer);
    }
}

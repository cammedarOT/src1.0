﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;
public class LayersController : MonoBehaviour {
    private List<List<GameObject>> muscleLayerGameObjects = new List<List<GameObject>>();
    private List<List<string>> muscleLayersNames = new List<List<string>>();
    private const int NUM_LAYERS = 14;
    public int Layer = 14;
    // Use this for initialization
    void Start () {
        fillMuscleLayers();
        findMusclesOfLayers();
		SetLayer (this.GetLayer());
	}
	
	// Update is called once per frame
	void Update () {
	}
	public int getNumLayers()
	{
		return NUM_LAYERS;
	}
    public bool SetLayer(int Layer)
    {
        if (Layer > NUM_LAYERS || Layer < 0)
            return false;
        DeactivateAllMuscles();
        ActivateMusclesUpToLayer(Layer);
        this.Layer = Layer;
        return true;
    }
    public int GetLayer()
    {
        return Layer;
    }
	[ObfuscateLiterals]
    private void DeactivateAllMuscles()
    {
        foreach (List<GameObject> muscleList in muscleLayerGameObjects)
        {
            foreach (GameObject muscle in muscleList)
            {
                muscle.SetActive(false);
                if (muscle == null)
                    Debug.Log("Muscle Null");
            }
        }
    }
	[ObfuscateLiterals]
    private void ActivateMusclesUpToLayer(int Layer)
    {
        for (int i = 0; i < Layer; i++)
        {
            List<GameObject> muscleList = muscleLayerGameObjects[i];
            {
                foreach (GameObject muscle in muscleList)
                {
                    muscle.SetActive(true);
                }
            }
        }
    }
	[ObfuscateLiterals]
    private void fillMuscleLayers()
    {
		int counter = NUM_LAYERS-1;
        for (int i = 0; i < NUM_LAYERS; i++)
        {
            muscleLayersNames.Add(new List<string>());
        }
        string [] arr = new string [] 
        {
			"Skin"
        };
        muscleLayersNames[counter--].AddRange(arr);
		arr = new string [] {
			"Levator_palpebrae_superioris__L",
			"Levator_palpebrae_superioris__R",
			"Inferior_oblique__L",
			"Inferior_oblique__R",
			"Abductor_digiti_minimi__Hand___L",
			"Abductor_digiti_minimi__Hand___R",
			"Extensor_digitorum__L",
			"Extensor_digitorum__R",
			"Latissimus_dorsi__L",
			"Latissimus_dorsi__R",
			"Gluteus_maximus__L",
			"Gluteus_maximus__R",
			"Rectus_femoris__L",
			"Rectus_femoris__R",
			"Sartorius__L",
			"Sartorius__R",
			"Tensor_fascia_lata__L",
			"Tensor_fascia_lata__R",
			"Vastus_lateralis__L",
			"Vastus_lateralis__R",
			"Deltoid__anterior_head__L",
			"Deltoid__anterior_head__R",
			"Deltoid__middle_head__L",
			"Deltoid__middle_head__R",
			"Deltoid__posterior_head__L",
			"Deltoid__posterior_head__R",
			"Pectoralis_major__L",
			"Pectoralis_major__R",
			"External_obliques__L",
			"External_obliques__R",
			"Gastrocnemius_lateral_head__L",
			"Gastrocnemius_lateral_head__R",
			"Gastrocnemius_medial_head__L",
			"Gastrocnemius_medial_head__R",
			"Abductor_pollicis_brevis__L",
			"Abductor_pollicis_brevis__R",
			"Platysma__L",
			"Platysma__R"
		};
		muscleLayersNames[counter--].AddRange(arr);
        arr = new string [] {
			"Internal_obliques__L",
			"Internal_obliques__R",
			"Superior_rectus__L",
			"Superior_rectus__R",
			"Lateral_rectus__L",
			"Lateral_rectus__R",
			"Flexor_digiti_minimi_brevis__Hand___L",
			"Flexor_digiti_minimi_brevis__Hand___R",
			"Palmaris_longus__L",
			"Palmaris_longus__R",
			"Triceps__lateral_head__L",
			"Triceps__lateral_head__R",
			"Sternocleidomastoid__L",
			"Sternocleidomastoid__R",
			"Trapezius_lower__L",
			"Trapezius_lower__R",
			"Trapezius_upper__L",
			"Trapezius_upper__R",
			"Anal_sphincter__L",
			"Anal_sphincter__R",
			"Coccygeus__L",
			"Coccygeus__R",
			"Gluteus_medius__L",
			"Gluteus_medius__R",
			"Tibialis_anterior__L",
			"Tibialis_anterior__R",
			"Vastus_intermdius__L",
			"Vastus_intermdius__R",
			"Vastus_medialis__L",
			"Vastus_medialis__R",
			"Trapezius_middle__L",
			"Trapezius_middle__R",
			"Biceps_femoris_long_head___L",
			"Biceps_femoris_long_head___R",
			"Soleus__L",
			"Soleus__R",
			"Inguinal_ligament__L",
			"Inguinal_ligament__R",
			"Pectoralis_major_clavicular_part__L",
			"Pectoralis_major_clavicular_part__R",
			"Pectoralis_major_Abdominal_part__L",
			"Pectoralis_major_Abdominal_part__R",
			"Opponens_pollicis__L",
			"Opponens_pollicis__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Superior_oblique__L",
			"Superior_oblique__R",
			"Medial_rectus__L",
			"Medial_rectus__R",
			"Inferior_rectus__L",
			"Inferior_rectus__R",
			"Gastrocnemius__L",
			"Gastrocnemius__R",
			"Biceps_brachii__short_head__L",
			"Biceps_brachii__short_head__R",
			"Brachioradialis__L",
			"Brachioradialis__R",
			"Flexor_carpi_radialis__L",
			"Flexor_carpi_radialis__R",
			"Mylohyoid__L",
			"Mylohyoid__R",
			"Extensor_digitorum_longus__L",
			"Extensor_digitorum_longus__R",
			"Fibularis_longus__L",
			"Fibularis_longus__R",
			"Flexor_digitorum_longus__L",
			"Flexor_digitorum_longus__R",
			"Gluteus_minimus__L",
			"Gluteus_minimus__R",
			"Gracilis__L",
			"Gracilis__R",
			"Iliacus__L",
			"Iliacus__R",
			"Inferior_gemellus__L",
			"Inferior_gemellus__R",
			"Levator_ani__L",
			"Levator_ani__R",
			"Semitendinosus__L",
			"Semitendinosus__R",
			"Infraspinatus__L",
			"Infraspinatus__R",
			"Levator_scapulae__L",
			"Levator_scapulae__R",
			"Pectoralis_minor__L",
			"Pectoralis_minor__R",
			"Rhomboideus_major__L",
			"Rhomboideus_major__R",
			"Rhomboideus_minor__L",
			"Rhomboideus_minor__R",
			"Serratus_posterior_inferior__L",
			"Serratus_posterior_inferior__R",
			"Subclavius__L",
			"Subclavius__R",
			"Supraspinatus__L",
			"Supraspinatus__R",
			"Teres_major__L",
			"Teres_major__R",
			"Biceps_femoris_short_head___L",
			"Biceps_femoris_short_head___R",
			"Rectus_abdominis__L",
			"Rectus_abdominis__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Pronator_teres__L",
			"Pronator_teres__R",
			"Genioglossus__L",
			"Genioglossus__R",
			"Biceps_brachii__long_head__L",
			"Biceps_brachii__long_head__R",
			"Extensor_carpi_radialis_longus__L",
			"Extensor_carpi_radialis_longus__R",
			"Flexor_digitorum_superficialis__L",
			"Flexor_digitorum_superficialis__R",
			"Triceps__medial_head__L",
			"Triceps__medial_head__R",
			"Omohyoid__L",
			"Omohyoid__R",
			"Splenius_capitis__L",
			"Splenius_capitis__R",
			"Adductor_longus__L",
			"Adductor_longus__R",
			"Deep_transverse_perineal__L",
			"Deep_transverse_perineal__R",
			"Extensor_hallucis_longus__L",
			"Extensor_hallucis_longus__R",
			"Fibularis_brevis__L",
			"Fibularis_brevis__R",
			"Pectineus__L",
			"Pectineus__R",
			"Piriformis__L",
			"Piriformis__R",
			"Plantaris__L",
			"Plantaris__R",
			"Semimembranosus__L",
			"Semimembranosus__R",
			"Coracobrachialis__L",
			"Coracobrachialis__R",
			"Iliocostalis_lumborum__L",
			"Iliocostalis_lumborum__R",
			"Teres_minor__L",
			"Teres_minor__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Brachialis__L",
			"Brachialis__R",
			"Flexor_digitorum_profundus__L",
			"Flexor_digitorum_profundus__R",
			"Triceps__long_head__L",
			"Triceps__long_head__R",
			"Geniohyoid__L",
			"Geniohyoid__R",
			"Sternohyoid__L",
			"Sternohyoid__R",
			"Stylohyoid__L",
			"Stylohyoid__R",
			"Adductor_magnus__L",
			"Adductor_magnus__R",
			"Flexor_hallucis_longus__L",
			"Flexor_hallucis_longus__R",
			"Popliteus__L",
			"Popliteus__R",
			"Superior_gemellus__L",
			"Superior_gemellus__R",
			"Serratus_anterior__L",
			"Serratus_anterior__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Anconeus__L",
			"Anconeus__R",
			"Flexor_pollicis_brevis__L",
			"Flexor_pollicis_brevis__R",
			"Opponens_digiti_minimi__Hand___L",
			"Opponens_digiti_minimi__Hand___R",
			"Digastric__L",
			"Digastric__R",
			"Serratus_posterior_superior__L",
			"Serratus_posterior_superior__R",
			"Thyrohyoid__L",
			"Thyrohyoid__R",
			"Adductor_brevis__L",
			"Adductor_brevis__R",
			"Extensor_hallucis_brevis__L",
			"Extensor_hallucis_brevis__R",
			"Fibularis_tertius__L",
			"Fibularis_tertius__R",
			"Iliococcygeus__L",
			"Iliococcygeus__R",
			"Quadratus_femoris__L",
			"Quadratus_femoris__R",
			"Tibialis_posterior__L",
			"Tibialis_posterior__R",
			"Flexor_digitorum_brevis__L",
			"Flexor_digitorum_brevis__R",
			"Lumbricals__Hand___L",
			"Lumbricals__Hand___R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Extensor_carpi_radialis_brevis__L",
			"Extensor_carpi_radialis_brevis__R",
			"Extensor_carpi_ulnaris__L",
			"Extensor_carpi_ulnaris__R",
			"Flexor_pollicis_longus__L",
			"Flexor_pollicis_longus__R",
			"Supinator__L",
			"Supinator__R",
			"Splenius_cervicis__L",
			"Splenius_cervicis__R",
			"Sternothyroid__L",
			"Sternothyroid__R",
			"Adductor_minimus__L",
			"Adductor_minimus__R",
			"Iliocostalis_thoracis__L",
			"Iliocostalis_thoracis__R",
			"Abductor_digiti_minimi__L",
			"Abductor_digiti_minimi__R",
			"Abductor_hallucis__L",
			"Abductor_hallucis__R",
			"Flexor_hallucis_brevis_lateral_head__L",
			"Flexor_hallucis_brevis_lateral_head__R",
			"Obturator_internus__L",
			"Obturator_internus__R",
			"Longus_capitis__L",
			"Longus_capitis__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Extensor_digiti_minimi__L",
			"Extensor_digiti_minimi__R",
			"Pronator_quadratus__L",
			"Pronator_quadratus__R",
			"Iliocostalis_cervicis__L",
			"Iliocostalis_cervicis__R",
			"Obturator_externus__L",
			"Obturator_externus__R",
			"Longissimus_thoracis__L",
			"Longissimus_thoracis__R",
			"Subscapularis__L",
			"Subscapularis__R",
			"Flexor_accessorius__L",
			"Flexor_accessorius__R",
			"Flexor_digiti_minimi_brevis__L",
			"Flexor_digiti_minimi_brevis__R",
			"Flexor_hallucis_brevis_medial_head__L",
			"Flexor_hallucis_brevis_medial_head__R",
			"Dorsal_interossei__L",
			"Dorsal_interossei__R",
			"Longus_colli_inferior_oblique__L",
			"Longus_colli_inferior_oblique__R",
			"Lumbricals__Foot___L",
			"Lumbricals__Foot___R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Flexor_carpi_ulnaris__L",
			"Flexor_carpi_ulnaris__R",
			"External_intercostal__L",
			"External_intercostal__R",
			"Psoas__L",
			"Psoas__R",
			"Spinalis_cervicis__L",
			"Spinalis_cervicis__R",
			"Spinalis_thoracis__L",
			"Spinalis_thoracis__R",
			"Adductor_hallucis_oblique_head__L",
			"Adductor_hallucis_oblique_head__R",
			"Adductor_hallucis_transverse_head__L",
			"Adductor_hallucis_transverse_head__R",
			"Opponens_digiti_minimi__L",
			"Opponens_digiti_minimi__R",
			"Interspinalis_thoracis__L",
			"Interspinalis_thoracis__R",
			"Adductor_pollicis__L",
			"Adductor_pollicis__R",
			"Longus_colli_superior_oblique__L",
			"Longus_colli_superior_oblique__R",
			"Longus_colli_vertical_intermediate__L",
			"Longus_colli_vertical_intermediate__R",
			"Levatores_costarum__L",
			"Levatores_costarum__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
        arr = new string[]
        {
			"Abductor_pollicis_longus__L",
			"Abductor_pollicis_longus__R",
			"Internal_intercostal__L",
			"Internal_intercostal__R",
			"Longissimus_cervicis__L",
			"Longissimus_cervicis__R",
			"Semispinalis_capitis__L",
			"Semispinalis_capitis__R",
			"Semispinalis_cervicis__L",
			"Semispinalis_cervicis__R",
			"Interspinales_lumborum__L",
			"Interspinales_lumborum__R",
			"Semispinalis_thoracis__L",
			"Semispinalis_thoracis__R",
			"Interosseous_1st_plantar__L",
			"Interosseous_1st_plantar__R",
			"Interosseous_2nd_plantar__L",
			"Interosseous_2nd_plantar__R",
			"Interosseous_third_plantar__L",
			"Interosseous_third_plantar__R",
			"Lumbar_rotator__L",
			"Lumbar_rotator__R",
			"Palmar_interossei‏__L",
			"Palmar_interossei‏__R",
			"Interspinales_lumborum__L",
			"Interspinales_lumborum__R"
        };
        muscleLayersNames[counter--].AddRange(arr);
		arr = new string[]
		{
			"Extensor_pollicis_longus__L",
			"Extensor_pollicis_longus__R",
			"Innermost_intercostal__L",
			"Innermost_intercostal__R",
			"Thoracic_rotator__L",
			"Thoracic_rotator__R",
			"Scalene_anterior__L",
			"Scalene_anterior__R",
			"Obliquus_capitis_inferior__L",
			"Obliquus_capitis_inferior__R",
			"Obliquus_capitis_superior__L",
			"Obliquus_capitis_superior__R",
			"Rectus_capitis_posterior_major__L",
			"Rectus_capitis_posterior_major__R",
			"Dorsi_interossei__Foot___L",
			"Dorsi_interossei__Foot___R"
		};
		muscleLayersNames[counter--].AddRange(arr);
		arr = new string[]
		{
			"Extensor_pollicis_brevis__L",
			"Extensor_pollicis_brevis__R",
			"Longissimus_capitis__L",
			"Longissimus_capitis__R",
			"Scalene_middle__L",
			"Scalene_middle__R",
			"Cervical_rotator__L",
			"Cervical_rotator__R",
			"Transversus_thoracis__L",
			"Transversus_thoracis__R",
			"Transversus_abdominis__L",
			"Transversus_abdominis__R"
		};
		muscleLayersNames[counter--].AddRange(arr);
		arr = new string[]
		{
			"Extensor_indicis__L",
			"Extensor_indicis__R",
			"Scalene_posterior__L",
			"Scalene_posterior__R",
			"Rectus_capitis_anterior__L",
			"Rectus_capitis_anterior__R",
			"Rectus_capitis_lateralis__L",
			"Rectus_capitis_lateralis__R",
			"Rectus_capitis_posterior_minor__L",
			"Rectus_capitis_posterior_minor__R",
			"Multifidus__L",
			"Multifidus__R",
			"Linea_alba"
		};
		muscleLayersNames[counter--].AddRange(arr);
    }
	[ObfuscateLiterals]
    private void findMusclesOfLayers()
    {

        for (int i = 0; i < NUM_LAYERS; i++)
        {
            muscleLayerGameObjects.Add(new List<GameObject>());
        }
        foreach (List<string> nameList in muscleLayersNames)
        {
            foreach (string name in nameList)
            {
                GameObject muscle = GameObject.Find(name);
                if (muscle != null)
                {
                    muscleLayerGameObjects[muscleLayersNames.IndexOf(nameList)].Add(GameObject.Find(name));
                }
                else
                {
                    Debug.Log("Error in finding " + name);
                }
            }
            
        }
    }
}

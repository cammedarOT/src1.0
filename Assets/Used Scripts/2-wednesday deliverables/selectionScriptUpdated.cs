﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //required forr the slider
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
using Cammedar.Analytics;
public class selectionScriptUpdated : MonoBehaviour {

	private UI_manager Manager;
	private GameObject musclesTabContent;
    private const string SCRIPTS_OBJECT_TAG = "ScriptManager";
    private GameObject mouseDownObject;
	private GameObject mouseUpObject;
	private bool once = false;
	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<UI_manager> ();
		musclesTabContent = GameObject.FindGameObjectWithTag ("MusclesTabContentPanel");
	}
	[ObfuscateLiterals]
	void Update()
	{
		//ana sheltha dlw2ty 3ashan b2a fi collider sphere we el mouse lma bydos
		// 3lehom l dnia btfar2a3

		if (Input.GetMouseButtonDown (0)  && !EventSystem.current.IsPointerOverGameObject()) { // da shaghal , hwa bas ana shayel l colliders mn l objects
			RaycastHit hitInfo = new RaycastHit ();
			bool hit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
			if (hit) {
				mouseDownObject = hitInfo.transform.gameObject;
				Debug.Log (mouseDownObject);
			}
		} 

		if (Input.GetMouseButtonUp (0) && !EventSystem.current.IsPointerOverGameObject()) { // da shaghal , hwa bas ana shayel l colliders mn l objects
			RaycastHit hitInfo = new RaycastHit ();
			bool hit = Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo);
			if (hit) {
				mouseUpObject = hitInfo.transform.gameObject;
				once = true;
				Debug.Log (mouseUpObject);
			}
		} 

		if (once && mouseUpObject == mouseDownObject) {
			if (mouseUpObject == null)
				return;
			selectthisObject ();
			once = false;
		}
	}

	private void selectthisObject(){
		for (int i = 0; i < musclesTabContent.transform.childCount; i++) { 
			menuObjectScript m = musclesTabContent.transform.GetChild (i).GetComponent<menuObjectScript> ();
            if (m != null)
                if (m.reference != null)
                    if (m.reference.gameObject == mouseDownObject)
                    {
                        MuscleAnalytics(musclesTabContent.transform.GetChild(i).GetChild(1).GetComponent<Text>().text);
                        Manager.addSelection(musclesTabContent.transform.GetChild(i).gameObject, false);
                        break;
                    }
		}
	}
    //This function is wrong, and I shouldn't do this
    public void deselectthisObject()
    {
        for (int i = 0; i < musclesTabContent.transform.childCount; i++)
        {
            menuObjectScript m = musclesTabContent.transform.GetChild(i).GetComponent<menuObjectScript>();
            if (m != null)
                if (m.reference != null)
                    if (m.reference.gameObject == mouseDownObject)
                    {
                        
                        Manager.addSelection(musclesTabContent.transform.GetChild(i).gameObject, false);
                        break;
                    }
        }
    }

    //Don't take this as an example, choose any other analytics class
    //The things we do wlahy 
    private void MuscleAnalytics(string muscleName)
    {
        EventData data = new EventData();
        data.FrameCount = Time.frameCount;
        data.DataBaseMessage = "<<Frame Count: " + data.FrameCount + ">" + "<ID: ClickableMuscle:" + muscleName + ">>";
        Debug.Log("Registering Clickable Muscle:" + muscleName);
        Networking_Manager networkManager = GameObject.FindGameObjectWithTag(SCRIPTS_OBJECT_TAG).GetComponent<Networking_Manager>();
        if (networkManager != null)
        {
            networkManager.addEvent(data.DataBaseMessage);
        }
    }
}

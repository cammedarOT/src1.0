﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //required forr the slider
using System.Collections.Generic;
using Beebyte.Obfuscator;

// dlwa2ty el script da 3shan el est3gal bs hyt3amel m3 etghat el 7raka 3la enha
// vector3.up we right we forward bas , mfish etgahat tania we mfish mo3adlat 
// bs lazm a3ml el etgahat el tania wel mo3adlat b3d keda tb3n

// el script da lma b7ot 7aga fih fi 7aga tania btt7kem fiha bybawzha , 3ashan bya5od reference fl awel
// le mkanha we kol mara yraga3ha fih tani + el update bta3o hwa lwa7do
public class muscleControllersData : MonoBehaviour {

	public Transform controller1;
	public Transform controller2;
	public Transform controller3;

	public bool RotX1;
	public bool RotY1;
	public bool RotZ1;

	Vector3 cont1ref;
	Vector3 cont2ref;
	Vector3 cont3ref;

	// -- output to controller manager script
	public Vector3 controller1Delta;
	public Vector3 controller2Delta;
	public Vector3 controller3Delta;


	public float sliderValue = 0;
	public float weightc1x;
	public float weightc1y;
	public float weightc1z;
	public float weightc2x;
	public float weightc2y;
	public float weightc2z;
	public float weightc3x;
	public float weightc3y;
	public float weightc3z;

	public Slider mainslider;

	public float BSweight = 1f;

	SkinnedMeshRenderer skinnedMeshRenderer;
	Mesh skinnedMesh;
	public List<float> Blendshapes = new List<float>();
	private int NoOfBlendshapes;


	//must choose only one direction for one controller
	public bool c1X;
	public bool c1Y;
	public bool c1Z;

	public bool c2X;
	public bool c2Y;
	public bool c2Z;

	public bool c3X;
	public bool c3Y;
	public bool c3Z;

	Vector3 dirx,diry,dirz;



	/// ////////////
	float currenSlider1;
	float lastSlider1;
	float deltaSlider1;
	//
	float currenSlider2;
	float lastSlider2;
	float deltaSlider2;
	//
	float currenSlider3;
	float lastSlider3;
	float deltaSlider3;

	public float RotX1weight;
	//public float RotX2weight;
	public float RotY1weight;
	//public float RotY2weight;
	public float RotZ1weight;

	void Awake ()
	{
		skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		skinnedMesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
	}

	void Start () {
		if(controller1!=null)
			cont1ref = controller1.position;
		if(controller2!=null)
			cont2ref = controller2.position;
		if(controller3!=null)
			cont3ref = controller3.position;

		NoOfBlendshapes = skinnedMesh.blendShapeCount; 
		// Debug.Log (NoOfBlendshapes); //debugging only
		//
		for (int i = 0; i < NoOfBlendshapes; i++) {
			Blendshapes.Add (0.0f);
		}



		/////////////
		currenSlider1 = sliderValue;
		lastSlider1 = currenSlider1;
		currenSlider2 = sliderValue;
		lastSlider2 = currenSlider2;
		currenSlider3 = sliderValue;
		lastSlider3 = currenSlider3;
		
	}
	[ObfuscateLiterals]
	void Update () {

		if (controller1 != null) {
			if (c1X)
				dirx = Vector3.right;
			if (c1Y)
				diry = Vector3.up;
			if (c1Z)
				dirz = Vector3.forward;

			//was controller1.position = cont1ref + ....
			controller1Delta = sliderValue *  (dirx * weightc1x+ diry * weightc1y+ dirz * weightc1z);
		
			if (RotX1) {
				currenSlider1 = sliderValue;
				deltaSlider1= currenSlider1- lastSlider1;
				lastSlider1= currenSlider1;
				Vector3 save = controller1.position;
				controller1.transform.RotateAround (transform.position, transform.right,RotX1weight * deltaSlider1 * 5f);
				controller1.position = save;
			}   
			if (RotY1) {
				currenSlider2 = sliderValue;
				deltaSlider2= currenSlider2- lastSlider2;
				lastSlider2= currenSlider2;
				Vector3 save = controller1.position;
				controller1.transform.RotateAround (transform.position, transform.up,RotY1weight * deltaSlider2 * 5f);
				controller1.position = save;
			}   
			if (RotZ1) {
				currenSlider3 = sliderValue;
				deltaSlider3= currenSlider3- lastSlider3;
				lastSlider3= currenSlider3;
				Vector3 save = controller1.position;
				controller1.transform.RotateAround (transform.position, transform.forward,RotZ1weight * deltaSlider3 * 5f);
				controller1.position = save;
			}   
		
		}

		if (controller2 != null) {
			if (c2X)
				dirx = Vector3.right;
			if (c2Y)
				diry = Vector3.up;
			if (c2Z)
				dirz = Vector3.forward;

			controller2Delta = sliderValue *  (dirx * weightc2x+ diry * weightc2y+ dirz * weightc2z);
		}

		if (controller3 != null) {
			if (c3X)
				dirx = Vector3.right;
			if (c3Y)
				diry = Vector3.up;
			if (c3Z)
				dirz = Vector3.forward;

			controller3Delta = sliderValue *  (dirx * weightc3x+ diry * weightc3y+ dirz * weightc3z);
		}



		UpdateBlendShapes();

	}

	//missing updating the weigt value with the slider value

	/// <Custom functions, not called by the framework>
	/// /////////////////////////////////////////////
	void UpdateBlendShapes (){
		for (int i = 0; i < NoOfBlendshapes; i++) {
			if (Blendshapes [i] < 0)
				Blendshapes [i] = 0;
			if (Blendshapes [i] > 100)
				Blendshapes [i] = 100;
			skinnedMeshRenderer.SetBlendShapeWeight (i, Blendshapes [i] * BSweight * sliderValue);		
		}
	}
}

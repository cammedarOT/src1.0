﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
public class drawGizmos : MonoBehaviour
{[ObfuscateLiterals]
	void OnDrawGizmos()
	{
		Color color;
		color = Color.green;
		// local up
		DrawHelperAtCenter(this.transform.up, color, 2f);

		color = Color.blue;
		// local forward
		DrawHelperAtCenter(this.transform.forward, color, 2f);
		         
		color = Color.red;
		// local right
		DrawHelperAtCenter(this.transform.right, color, 2f);


		color = Color.cyan;
		//the updated foreward direction
		//Vector3 updatedFor;
		//updatedFor = GetComponent<newForeward> ().rotatedForeward;
		//DrawHelperAtCenter(updatedFor, color, 2f);

	}
	[ObfuscateLiterals]
	private void DrawHelperAtCenter(
		Vector3 direction, Color color, float scale)
	{
		Gizmos.color = color;
		Vector3 destination = transform.position + direction * scale;
		Gizmos.DrawLine(transform.position, destination);

	}

}

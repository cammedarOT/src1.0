﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
public class moveTowards : MonoBehaviour {

	// my position 
	Vector3 myrefpos;

	//target position
	public Transform point2;
	public Transform point3;

	public float DistanceDelta1;
	public float DistanceDelta2;

	float Maxdistance1;
	float Maxdistance2;
	[ObfuscateLiterals]
	void Start () {
		myrefpos = this.transform.position;
		Maxdistance1 = Vector3.Distance (this.transform.position, point2.position);
		Maxdistance2 = Vector3.Distance (this.transform.position, point3.position);
	}
	[ObfuscateLiterals]
	void Update () {
		
		//mixing all scripts
		//el mwdo3 complex awi :/ , lama ba testo y3ni 3ashan el code 3abit
		// lama bagib maximum motion (mfish overshoot fl function di) we b3d keda a7arak na7ya 3akso
		// bab2a msh ba3raf agib el full range , el mwdo3 m7tag maths b2a we weights we parameters ktir 
		// 7asab el ne3ozo , lazm n7ot conventions we ntefe2 3la 7gat b2a ana we ahmed 
		Vector3 delta = Vector3.MoveTowards (myrefpos, point2.position, DistanceDelta1*Maxdistance1/10)- myrefpos;
		delta += Vector3.MoveTowards (myrefpos, point3.position, DistanceDelta2*Maxdistance2/10) - myrefpos;
		Vector3 thenewplace = delta + myrefpos;
		transform.position = thenewplace;

	}
}

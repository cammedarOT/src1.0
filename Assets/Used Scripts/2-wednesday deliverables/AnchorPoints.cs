﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;


// anchor points code (LOOK AT) works as follows:
// ba7otelha ref le 2 objects , el awlani ya (this) ya object tani el na7ya el tania
// fa yb2a el object dah wa2ef fi el 5at el benhom
// wel ref el tani lel object el habos 3leeh
// el mask lesa msh shaghal 

//lw 3aiz 2 scripts to work together ba7ot parent object wadilo el script el tani
//we asln el look at nafso ba7oto fi parent gameobject 3ashan a3raf aghyar el front bta3o
// wel 3ashan ashghalo :
//		ba7ot el script, wa5alih y look at
//		ba5od el angles watfi el simulation
//		ba5ali el gameobject el parent bl rotation el mfrod y5ali el front bta3o (Z axis) na7yet front el object el ana 3aizo
//		ba5alih parent 3l object l 3adi bta3i



public class AnchorPoints : MonoBehaviour {

	//dol el anchor points ( el bones fi unity homa point wa7da )

	public Transform Bone1base;  //in unity the bone is only a base
	public Transform Bone2base;  

	Vector3 Bone1Start;
	Vector3 Bone2Start;

	private Vector3 onNormal;
	Quaternion targetRotation;

	public bool X_axis;
	public bool Y_axis;
	public bool Z_axis;

	Vector3 bone1pos;
	Vector3 bone2pos;

	void Start () {
		Bone1Start = Bone1base.position;
		Bone2Start = Bone2base.position;
		onNormal = Bone2Start - Bone1Start;
	}
	[ObfuscateLiterals]
	void FixedUpdate () { //yadi moshklet el fixed update di 
		
		bone1pos = Bone1base.position;
		bone2pos = Bone2base.position;

		//5ali balak dol el mfrod local
		if (X_axis && !Y_axis && !Z_axis) {
			Vector3 projectedPos2;
			Vector3 heading = Bone1base.position - Bone2base.position;
			Vector3 force = Vector3.Project (heading, this.transform.right) ;  //wrong !! global

			projectedPos2 = Bone2base.position + force;
			bone2pos = projectedPos2;

		}
		else if (!X_axis && Y_axis && !Z_axis) {
			bone1pos.y = Bone1Start.y;
			bone2pos.y = Bone2Start.y;
		}
		else if (!X_axis && !Y_axis && Z_axis) {
			bone1pos.z = Bone1Start.z;
			bone2pos.z = Bone2Start.z;
		}


		// NOOOOOOTEEEEEEEEEEEEEEEEE!!!!!!!!!!
		//////////////////////////----------------------------
		/// dlwa2ty el code da byshtghal almost 3la 6 7gat , 3ashan ana mmkn a7oto 3 marat fl object, mara 3la 
		/// kol axis , we kol mara mnhom bya5od 2 objects , ya2ema nafso we 7aga ybos 3leha, ya2ema
		/// wa7ed na7ia we wa7ed na7ia tania we hwa haya5od el 5at el benhom 
		/// --
		/// kol wa7ed mn el wa5ed el scripts lazm yb2a parent 3la el ta7to we fl a5er yb2a parent 3l bone
		/// - ghaleban keda lw 3aiz 7aga t follow be kaza axis e3mlha fi 2 scripts parent 3la ba3d , not sure
	

		//keda el rotated foreward mazboot - da lel viewing in drawGizmos
		targetRotation = Quaternion.LookRotation ( -(bone1pos - bone2pos).normalized, transform.up);

		//NOTE NOTE NOTE NOTE:
		//el code da byshtghal lma yb2a el front hwa el front bta3 el object el 7a2i2i
		//laken lma yb2a el front el gdid bta3i wadtar aghayar el mo3adla el fo2 bl 'ellafa'
		//fi component mn el rotation byroo7


		//el tri2a di koisa bs msh 7ases far2 benha we ben eno yb2a el 3 axis 3adi 
		//we mthya2li hynfa3 3adi a5ali kaza 7aga t look at , bs msh nafs l parent b2a , hghyar el 
		// empty game object
//		if (X_axis && !Y_axis && Z_axis) {
//			Vector3 saveB1Pos = bone1pos;
//			Vector3 saveB2Pos = bone2pos;
//
//			//get rotation arround X
//			bone1pos.x = Bone1Start.x;
//			bone2pos.x = Bone2Start.x;
//			targetRotation = Quaternion.LookRotation ( -(bone1pos - bone2pos), transform.up);
//			// rotation for Y relative to my rotation
//			Quaternion rotationXrelative = Quaternion.Inverse(transform.rotation) * targetRotation;
//
//
//			//get rotation for Z
//			bone1pos = saveB1Pos;
//			bone2pos = saveB2Pos;
//			bone1pos.z = Bone1Start.z;
//			bone2pos.z = Bone2Start.z;
//			targetRotation = Quaternion.LookRotation ( -(bone1pos - bone2pos), transform.up);
//			// rotation for Z relative to my rotation 
//			Quaternion rotationZrelative = Quaternion.Inverse (transform.rotation) * targetRotation;
//
//			targetRotation = transform.rotation * rotationXrelative * rotationZrelative;
//
//			//targetRotation =  transform.rotation * rotationZrelative;
//
//		}


		//da by3ml transform.rotation = target.rotation - transform.rotation 3ashan kda msh byfdal ylef 3abit
		//transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, 30.0f * Time.deltaTime);
		transform.rotation = targetRotation;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class controllersManager : MonoBehaviour {
	public CopyTranslation test;
	// array of all muscleController data scripts
	// array of all transformers of controllers/movabales by the scripts

	// loop on all the scripts and get the data calculated for motion (deltas)
	// save them in an array for every controller
	// sum them up and assign them to the transfrom.positions

	
	public List <muscleControllersData> scripts = new List <muscleControllersData> ();
	public List <Transform> transforms = new List <Transform> ();
	public List <Vector3> vectors = new List <Vector3> ();
	public List <Vector3> refs = new List <Vector3> ();
	public List <Vector3> realDeltas = new List <Vector3> ();


	public muscleControllersData zcript1;
	public muscleControllersData zcript2;
	public muscleControllersData zcript3;
	public muscleControllersData zcript4;
	public muscleControllersData zcript5;
	public muscleControllersData zcript6;
	public muscleControllersData zcript7;
	public muscleControllersData zcript8;
	public muscleControllersData zcript9;
	public muscleControllersData zcript10;

	public Transform krans1;
	public Transform krans2;
	public Transform krans3;
	public Transform krans4;
	public Transform krans5;
	public Transform krans6;
	public Transform krans7;


	void Start () {
		// add the scripts here!
		if (zcript1 != null)	scripts.Add (zcript1);
		if (zcript2 != null) 	scripts.Add (zcript2);
		if (zcript3 != null)	scripts.Add (zcript3);
		if (zcript4 != null)	scripts.Add (zcript4);
		if (zcript5 != null)	scripts.Add (zcript5);
		if (zcript6 != null)	scripts.Add (zcript6);
		if (zcript7 != null)	scripts.Add (zcript7);
		if (zcript8 != null)	scripts.Add (zcript8);
		if (zcript9 != null)	scripts.Add (zcript9);
		if (zcript10 != null)	scripts.Add (zcript10);

		//--
		if (krans1 != null)		transforms.Add (krans1);
		if (krans2 != null)		transforms.Add (krans2);
		if (krans3 != null)		transforms.Add (krans3);
		if (krans4 != null)		transforms.Add (krans4);
		if (krans5 != null)		transforms.Add (krans5);
		if (krans6 != null)		transforms.Add (krans6);
		if (krans7 != null)		transforms.Add (krans7);

		//same of controlled transforms/movable objects -> 7 now
		vectors.Add(new Vector3(0f,0f,0f));
		vectors.Add(new Vector3(0f,0f,0f));
		vectors.Add(new Vector3(0f,0f,0f));
		vectors.Add(new Vector3(0f,0f,0f));
		vectors.Add(new Vector3(0f,0f,0f));
		vectors.Add(new Vector3(0f,0f,0f));
		vectors.Add(new Vector3(0f,0f,0f));
		//same
		realDeltas.Add (new Vector3 (0f, 0f, 0f));
		realDeltas.Add (new Vector3 (0f, 0f, 0f));
		realDeltas.Add (new Vector3 (0f, 0f, 0f));
		realDeltas.Add (new Vector3 (0f, 0f, 0f));
		realDeltas.Add (new Vector3 (0f, 0f, 0f));
		realDeltas.Add (new Vector3 (0f, 0f, 0f));
		realDeltas.Add (new Vector3 (0f, 0f, 0f));

		//keep the ref position of every transform 
		for (int i = 0; i < transforms.Count; i ++)
			refs.Add(new Vector3 (transforms[i].position.x, transforms[i].position.y, transforms[i].position.z));
	}
	
	void Update () {

		//controller1
		//bensaffarhom
		for (int i = 0; i < 7; i++)
			vectors [i] = new Vector3 (0f, 0f, 0f);
		//we ngma3hom mn l awel
		for (int i = 0; i < scripts.Count; i++)
			if (scripts [i].controller1 != null)
				for (int j = 0; j < transforms.Count; j++)
					if (scripts [i].controller1 == transforms [j])
						vectors [j] += scripts [i].controller1Delta;
	
		for (int i = 0; i < realDeltas.Count; i++)
			realDeltas [i] = new Vector3 (vectors [i].x, vectors [i].y, vectors [i].z);

		//controller2
		for (int i = 0; i < 7; i++)
			vectors [i] = new Vector3 (0f, 0f, 0f);
		
		for (int i = 0; i < scripts.Count; i++)
			if (scripts [i].controller2 != null)
				for (int j = 0; j < transforms.Count; j++)
					if (scripts [i].controller2 == transforms [j])
						vectors [j] += scripts [i].controller2Delta;

		for (int i = 0; i < realDeltas.Count; i++)
			realDeltas [i] += new Vector3 (vectors [i].x, vectors [i].y, vectors [i].z);

		//controller3
		for (int i = 0; i < 7; i++)
			vectors [i] = new Vector3 (0f, 0f, 0f);

		for (int i = 0; i < scripts.Count; i++)
			if (scripts [i].controller3 != null)
				for (int j = 0; j < transforms.Count; j++)
					if (scripts [i].controller3 == transforms [j])
						vectors [j] += scripts [i].controller3Delta;

		for (int i = 0; i < realDeltas.Count; i++)
			realDeltas [i] += new Vector3 (vectors [i].x, vectors [i].y, vectors [i].z);

		//------------------------

		


		// bassem -> edits , test maskedDelta di tetshal , di test bas
		// el klam el mohem fl wra2a el fiha el graph
		for (int i = 0; i < transforms.Count; i++)
			if (transforms [i] != null)  // ana ba test bas, keda kolooo hyt7rak lma a7rak el copyTranslation da
				transforms[i].position = realDeltas [i] + refs [i]; //+ test.maskedDelta1 + test.maskedDelta2;
	}
}

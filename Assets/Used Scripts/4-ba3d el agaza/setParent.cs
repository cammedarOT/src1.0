﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;
public class setParent : MonoBehaviour {

	public string ParentName;
	public int siblingNo;
	[ObfuscateLiterals]
	void Start () {
		GameObject p = GameObject.Find (ParentName);
		if (p) {
			this.transform.parent = p.transform;
			this.transform.SetSiblingIndex (siblingNo);
		} else
			Debug.Log ("errorr! couldn't find parent of " + this.transform.gameObject.name);
	}
	
}
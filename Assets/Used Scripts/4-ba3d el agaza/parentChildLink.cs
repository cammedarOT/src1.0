﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parentChildLink : MonoBehaviour {

	public string childName;

	void Start () {
		int me = this.transform.GetSiblingIndex ();
		int childNumber = me + 1;
		for (int i = me; i < this.transform.parent.childCount; i++) {
			if (this.transform.parent.GetChild (i).name == childName)
				this.transform.parent.GetChild (i).transform.parent = this.transform;
		}
	}
}

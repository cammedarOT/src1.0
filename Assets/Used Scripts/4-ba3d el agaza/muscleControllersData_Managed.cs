﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //required forr the slider
using System.Collections.Generic;
using Beebyte.Obfuscator;

// dlwa2ty el script da 3shan el est3gal bs hyt3amel m3 etghat el 7raka 3la enha
// vector3.up we right we forward bas , mfish etgahat tania we mfish mo3adlat 
// bs lazm a3ml el etgahat el tania wel mo3adlat b3d keda tb3n

public class muscleControllersData_Managed : MonoBehaviour {
	movementManager Manager;
	rotationManager rotManager;
	lookat_Managed lookatCont1;
	lookat_Managed lookatCont2;

	public Transform controller1;
	public Transform controller2;
	public Transform controller3;

	public bool RotX1;
	public bool RotY1;
	public bool RotZ1;

	public bool RotX2;
	public bool RotY2;
	public bool RotZ2;

	Vector3 cont1ref;
	Vector3 cont2ref;
	Vector3 cont3ref;
	public bool dysfunctional = false;
	// -- output to controller manager script
	Vector3 controller1Delta;
	Vector3 controller2Delta;
	Vector3 controller3Delta;

	public float sliderValuefromCurvesDataBase = 0; //ha3ml l slider values ttgama3 fi el DB we on fixedUpdate
	// hagama3 el values bta3et kol muscle 3ashan yb2a m3aia value wa7da , we b3d keda on Fixed update ha3adi
	// 3ala l muscle bta3etha wadiha el value el wa7da di fi l value From Curves DB we b3d keda gowa el code
	// ha3ml slider value 3adia plus di wagm3hom fl logic , we b keda yfdal m3aia value el slider we value l DB 
	public float sliderValue = 0;
	public float lastValidSliderVal = 0; //for the limit rotation ranges -> solution of limit rotation bug - TODO

	public float weightc1x;
	public float weightc1y;
	public float weightc1z;
	public float weightc2x;
	public float weightc2y;
	public float weightc2z;
	public float weightc3x;
	public float weightc3y;
	public float weightc3z;

	Slider mainslider;

	public float BSweight = 1f;

	SkinnedMeshRenderer skinnedMeshRenderer;
	Mesh skinnedMesh;
	public List<float> Blendshapes = new List<float>();
	private int NoOfBlendshapes;


	//must choose only one direction for one controller
	public bool c1X;
	public bool c1Y;
	public bool c1Z;

	public bool c2X;
	public bool c2Y;
	public bool c2Z;

	public bool c3X;
	public bool c3Y;
	public bool c3Z;

	Vector3 dirx,diry,dirz;



	/// ////////////
	float currenSlider1;
	float lastSlider1;
	float deltaSlider1;
	//
	float currenSlider2;
	float lastSlider2;
	float deltaSlider2;
	//
	float currenSlider3;
	float lastSlider3;
	float deltaSlider3;

	public float RotX1finalAngle;
	public float RotY1finalAngle;
	public float RotZ1finalAngle;
	public float RotX2finalAngle;
	public float RotY2finalAngle;
	public float RotZ2finalAngle;

	public float c1xreal;
	public float c1yreal;
	public float c1zreal;

	public float c1xlimit;
	public bool c1xHigher;
	public bool c1xLower;
	public float c1ylimit;
	public bool c1yHigher;
	public float c1zlimit;
	public bool c1zHigher;


    public string origin;
    public string insertion;
    public string action;
    public string nerve_supply;
    public string nerve_root;
    void Awake ()
	{
		skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        if (skinnedMeshRenderer != null)
        {
            skinnedMesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
        }
        else
        {
            Debug.Log("Can't find skinned mush Renderer");
        }
	}
	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<movementManager> ();
		rotManager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<rotationManager> ();

		if (controller1 != null) {
			cont1ref = controller1.position;
			Manager.addGameObject (controller1, cont1ref);
		}
		if (controller2 != null) {
			cont2ref = controller2.position;
			Manager.addGameObject (controller2, cont2ref);
		}
		if (controller3 != null) {
			cont3ref = controller3.position;
			Manager.addGameObject (controller3, cont3ref);
		}
        // TB lee m7et4 check hna, lee y 7bby
        //TODO : Ask bassem whether el check for null da s7 wla 8lt
        if (controller1 != null && controller2 != null)
        {
            if (RotX1 || RotY1 || RotZ1) 
            {
                rotManager.addData(controller1, this);
                lookatCont1 = controller1.GetComponent<lookat_Managed>();
            }
            if (RotX2 || RotY2 || RotZ2) 
            {
                rotManager.addData(controller2, this);
                lookatCont2 = controller2.GetComponent<lookat_Managed>();
            }
        }
		//el satr da shelto mo2aatan , rga3o tani
		//NoOfBlendshapes = skinnedMesh.blendShapeCount; 
		// Debug.Log (NoOfBlendshapes); //debugging only
		//
		for (int i = 0; i < NoOfBlendshapes; i++) {
			Blendshapes.Add (0.0f);
		}
	}
	[ObfuscateLiterals]
	void FixedUpdate () {

		// ka rotations , msh hab2a aslnnn ba2ol le wa7ed mn el next rotations yrotate
		// ana t3amli m3 el first rotation , fa da msh moshkla 
		/// laken mynfa3sh a7seb angle el rotation abl ma kol el translations t5las :/
		/// a3ml a ? 

		if (controller1 != null) {
			if (c1X)
				dirx = Vector3.right;
			if (c1Y)
				diry = Vector3.up;
			if (c1Z)
				dirz = Vector3.forward;

			//was controller1.position = cont1ref + ....
			controller1Delta = Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1) *  (dirx * weightc1x+ diry * weightc1y+ dirz * weightc1z);
			if (!dysfunctional)
				Manager.addDelta (controller1, controller1Delta);

		}

		if (controller2 != null) {
			if (c2X)
				dirx = Vector3.right;
			if (c2Y)
				diry = Vector3.up;
			if (c2Z)
				dirz = Vector3.forward;

			controller2Delta =  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1) *  (dirx * weightc2x+ diry * weightc2y+ dirz * weightc2z);
			if (!dysfunctional)
				Manager.addDelta (controller2, controller2Delta);
		}

		if (controller3 != null) {
			if (c3X)
				dirx = Vector3.right;
			if (c3Y)
				diry = Vector3.up;
			if (c3Z)
				dirz = Vector3.forward;

			controller3Delta =  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1) *  (dirx * weightc3x+ diry * weightc3y+ dirz * weightc3z);
			if (!dysfunctional)
				Manager.addDelta (controller3, controller3Delta);
		}


		//hna aghyar loon l muscle 7asab el slider value
		//this da el gameobject el 3d

		//sa7i7 fi moshkla , da hyb2a malosh da3wa bl action contractions , y3ni el muscle msh 
		// httlwen kda lma el action l mas2ola 3anha t contract
//		int R1, G1, B1, R2, G2, B2, newR, newG, newB;
//		R1 = 255; G1 = 214; B1 = 212; R2 = 255; G2 = 97; B2 = 88;
//		newR = (int)Mathf.Lerp (R1, R2, sliderValue);
//		newG = (int)Mathf.Lerp (G1, G2, sliderValue);
//		newB = (int)Mathf.Lerp (B1, B2, sliderValue);
//		this.transform.gameObject.GetComponent<SkinnedMeshRenderer>().materials[0].color = new Color32 ((byte)newR, (byte)newG, (byte)newB, 1);
//		UpdateBlendShapes();

	}

	//missing updating the weigt value with the slider value
	[ObfuscateLiterals]
	/// <Custom functions, not called by the framework>
	/// /////////////////////////////////////////////
	void UpdateBlendShapes (){
		for (int i = 0; i < NoOfBlendshapes; i++) {
			if (Blendshapes [i] < 0)
				Blendshapes [i] = 0;
			if (Blendshapes [i] > 100)
				Blendshapes [i] = 100;
			skinnedMeshRenderer.SetBlendShapeWeight (i, Blendshapes [i] * BSweight *  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1));		
		}
	}


	[ObfuscateLiterals]
	// di mfrod el rotation 
	public void rotate(){
		if (RotX1 && RotX1finalAngle != 0) {
			// hwa el transform .right da bta3 el muscle object :O ? a el hbal da
			Quaternion q = Quaternion.AngleAxis(RotX1finalAngle *  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1), transform.right);
			if (!dysfunctional)
				rotManager.addDelta (controller1.transform, q);
			//Debug.Log ("here");

		}   
		if (RotY1 && RotY1finalAngle != 0) {
			// hwa el transform .right da bta3 el muscle object :O ? a el hbal da
			Quaternion q = Quaternion.AngleAxis(RotY1finalAngle *  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1), transform.up);
			if (!dysfunctional)
				rotManager.addDelta (controller1.transform, q);
		}   

		if (RotZ1 && RotZ1finalAngle != 0) {
			Quaternion q = Quaternion.AngleAxis(RotZ1finalAngle *  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1) , transform.forward);
			if (!dysfunctional)
				rotManager.addDelta (controller1.transform, q);
		}   
		///////////////////////////////
		if (RotX2) {
			// hwa el transform .right da bta3 el muscle object :O ? a el hbal da
			Quaternion q = Quaternion.AngleAxis(RotX2finalAngle *  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1), transform.right);
			if (!dysfunctional)
				rotManager.addDelta (controller2.transform, q);
		}   
		if (RotY2) {
			// hwa el transform .right da bta3 el muscle object :O ? a el hbal da
			Quaternion q = Quaternion.AngleAxis(RotY2finalAngle*  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1), transform.up);
			if (!dysfunctional)
				rotManager.addDelta (controller2.transform, q);
		}   

		if (RotZ2) {
			Quaternion q = Quaternion.AngleAxis(RotZ2finalAngle *  Mathf.Clamp(sliderValue + sliderValuefromCurvesDataBase,0,1) , transform.forward);
			if (!dysfunctional)
				rotManager.addDelta (controller2.transform, q);
		}   

	}

	public void setDysfunctional(bool val){
		dysfunctional = val;
		if (!dysfunctional)
			this.gameObject.GetComponent<Renderer> ().material.color = Color.yellow;
		else 
			this.gameObject.GetComponent<Renderer>().material.color =  new Color32 (92, 89, 89, 1);
	}

	public void setLastFrameAngles(float x, float y, float z){
		c1xreal = x;
		c1yreal = y;
		c1zreal = z;
	}
}

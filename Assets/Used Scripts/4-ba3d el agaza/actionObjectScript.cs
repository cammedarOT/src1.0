﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Beebyte.Obfuscator;

public class actionObjectScript : MonoBehaviour {

	//ana dlwa2ty 3aiz le kol action script muscleControllersData tani lwa7do keda
	// yb2a hwa mwgod fi ai object m3afen keda marmi bs byet7akem fl bone l mo3iana el 3aiznhha di
	// bs keda
	// we el slider value hadihalo 3adi be nafs el tri2a
	//TODO here !!!
	// hashof l mwdo3 da bokra :(
	public List <muscleControllersData_Managed> mc = new List <muscleControllersData_Managed>();
	public Transform one;
	public Transform two;
    public string actionName;
	public float sliderValue = 0;
	public float lastValidSliderVal = 0; //for the limit rotation ranges -> solution of limit rotation bug
    public action action;
	public List <action> children = new List <action>(); // list of children (DB)
	public List <GameObject> childrenMenuObjects = new List <GameObject>();
	public List <GameObject> musclesofeachactionMenuObjects = new List <GameObject> ();
	//dlwa2ty l mfrod l action ylef el bone bas kda
	public string function1;
	public string function2;
	public float angle1x;
	public float angle1y;
	public float angle1z;
	public float angle2x;
	public float angle2y;
	public float angle2z;

	public float weightc1x;
	public float weightc1y;
	public float weightc1z;
	public float weightc2x;
	public float weightc2y;
	public float weightc2z;

	public bool RotX1;
	public bool RotY1;
	public bool RotZ1;
	public bool RotX2;
	public bool RotY2;
	public bool RotZ2;

	//for the left and right handling
	public char oldc = 'L';
	public char newc = 'L'; //start with Left 

	public Quaternion refRot; //for calculating the angle of the action (to view it only, not used in calculations)

	//this is set int the populate actions tab to be refereing to the DB part that have the muscles of this action
	public List <CurveStruct> muscleMovers;

    public List<actionObjectScript> accompayingActions;
	public List <float> accompayingWeights = new List <float> ();
	movementManager movManager;
	rotationManager Manager;
	[ObfuscateLiterals]
	void Start(){
        accompayingActions = new List<actionObjectScript>();
        refRot = this.transform.localRotation;
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<rotationManager> ();
		movManager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<movementManager> ();
		if (one) {//rotation
			Manager.addData2 (one, this);
			//el 7eta di 3ashan mwdo3 el L wel R , b3ml assign le nafs el script mrten fl manager . mra bl L we mra bl R , m3mltsh trace lel mwdo3 awi fa msh 3aref fih mshakel wla la2
			string oneOldname = one.name;
			string oneName = one.name;
			oneName = oneName.Replace('L','R');//ana 3amlha kda 3ashan lw fi moshkla t3mli error hna, 3shan mfrod my7slsh moshkla
			one = GameObject.Find(oneName).transform;
			Manager.addData2 (one, this);
			one = GameObject.Find(oneOldname).transform;
		}
		if (two) {//movement
			Manager.addData2 (two, this);
			movManager.addGameObject (two, two.position);
			//el 7eta di 3ashan mwdo3 el L wel R , b3ml assign le nafs el script mrten fl manager . mra bl L we mra bl R , m3mltsh trace lel mwdo3 awi fa msh 3aref fih mshakel wla la2
			string twoOldname = two.name;
			string twoName = two.name;
			twoName = twoName.Replace('L','R');//ana 3amlha kda 3ashan lw fi moshkla t3mli error hna, 3shan mfrod my7slsh moshkla
			two = GameObject.Find(twoName).transform;
			Manager.addData2 (two, this);
			two = GameObject.Find(twoOldname).transform;
		}
	}
	[ObfuscateLiterals]
	public void FixedUpdate(){
		if (two) {
			//was controller1.position = cont1ref + ....
			Vector3 Delta= sliderValue *  (Vector3.right * weightc1x+ Vector3.up * weightc1y+ Vector3.forward * weightc1z);
			movManager.addDelta (two, Delta);
		}
	}
	[ObfuscateLiterals]

	// lw one hwa hwa two fl action  nfso yb2a el bta3 da hybooz - bs a el hy5li one hwa hwa two
	public void rotate(Transform t){
		if (RotX1 && one == t) {
			Quaternion q = Quaternion.AngleAxis(angle1x * sliderValue, Vector3.right);
			Manager.addDelta (one, q);
		}   
		if (RotY1 && one == t) {
			Quaternion q = Quaternion.AngleAxis(angle1y * sliderValue, Vector3.up);
			Manager.addDelta (one, q);
		}   
		if (RotZ1 && one == t) {
			Quaternion q = Quaternion.AngleAxis(angle1z * sliderValue , Vector3.forward);
			Manager.addDelta (one, q);
		} 

		//////////////////////////////////////
		if (RotX2 && two == t) {
			Quaternion q = Quaternion.AngleAxis(angle2x * sliderValue, Vector3.right);
			Manager.addDelta (two, q);
		}   
		if (RotY2 && two == t) {
			Quaternion q = Quaternion.AngleAxis(angle2y * sliderValue, Vector3.up);
			Manager.addDelta (two, q);
		}   
		if (RotZ2 && two == t) {
			Quaternion q = Quaternion.AngleAxis(angle2z * sliderValue , Vector3.forward);
			Manager.addDelta (two, q);
		} 
	}
	[ObfuscateLiterals]
	public void addAccompanyingAction(actionObjectScript aos, float i){
		//el moshkla hna en el action di byt3mlha add ka accompan.action kaza mra 3nd kaza action
		//we fi mnhom 3aizha L we mnhom 3aizha R , fa bta5od state a5er wa7ed 7taha 3ndo

		//Debugging
		//if (aos.actionName == "scapula upper rotation")
		//	Debug.Log ("once"); //di btigi hna kaza mara , di el moshkla, m3rfsh hwa shaghal ezay dlwa2ty 

		//first flip the muscles of the action before adding it (to make sure the right muscles and bones are selected)
		//aos.newc = newc; aos.oldc = oldc;
		if (aos.one) {
			string oneName = aos.one.name;
			oneName = oneName.Replace(oldc,newc);//ana 3amlha kda 3ashan lw fi moshkla t3mli error hna, 3shan mfrod my7slsh moshkla
			aos.one = GameObject.Find(oneName).transform;
		} 
		if (aos.two) {
			string twoName = aos.two.name;
			twoName = twoName.Replace(oldc,newc);//ana 3amlha kda 3ashan lw fi moshkla t3mli error hna, 3shan mfrod my7slsh moshkla
			aos.two = GameObject.Find(twoName).transform;
		}
		for (int j = 0; j < aos.mc.Count; j++) {
			string mcName = aos.mc [j].name;
			mcName = mcName.Substring (1);
			mcName = mcName.Replace(oldc,newc);
			mcName = aos.mc [j].name [0] + mcName;
			GameObject cc = GameObject.FindGameObjectWithTag("Rig").transform.Find (mcName).gameObject;
			aos.mc [j] = cc.GetComponent<muscleControllersData_Managed>();
		}


		accompayingActions.Add (aos);
		accompayingWeights.Add (i);
	}
	[ObfuscateLiterals]
	public void updateMusclesAccordingToCurves(){
		if (muscleMovers == null) {
			//Debug.Log ("don't have muscleCurves");
			return;
		}
		for (int i = 0; i < muscleMovers.Count; i++) {
			//fi hna moshkla enha lw kaza wa7da 3ndhom curve le nafs el muscle , hay overwrite 3leh! TODO
			//we shof 7war el shoulder flexion leeh el pecs major msh bt2el tani (3ashan msh btwsal le a5erha , 3ashan l range sphere soghira mn odam )
			float itsSliderValue;
			itsSliderValue = this.muscleMovers[i].curve.Evaluate (sliderValue);

			//old way -> had the overwrite bug
			//muscleMovers [i].musclePointer.sliderValue = itsSliderValue;

			//new way ->
			this.muscleMovers[i].setValue(itsSliderValue);
		}
	}
}

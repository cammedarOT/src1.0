﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Beebyte.Obfuscator;

[System.Serializable]  // el kelma di bas , 5alet el list tetla3 fl inspector :O <3
public class gameObjectinRotList
{
	public Transform transfrom;
	public lookat_Managed lookAt;
	public limitRotation_sphere_Managed limitRot;
	public blindTrace_Managed blindTrace;
	// d list 3shan kol objet mmkn yb2a fi kaza bta3 3aizin y7arakooh 3adi
	public List <muscleControllersData_Managed> data = new List <muscleControllersData_Managed> ();
	public List <actionObjectScript> data2 = new List <actionObjectScript> ();

	public List <Preceding> precedings = new List <Preceding>();
	public List <Transform> precedinngDebug = new List <Transform> ();

	public Quaternion delta;
	public Quaternion refRot;

	public int priority;  

	public bool done;


	public gameObjectinRotList(Transform t, lookat_Managed l, Quaternion r)
	{
		transfrom = t;
		lookAt = l;
		refRot = r;
		delta = Quaternion.identity;

	}
}


public class rotationManager : MonoBehaviour {

	public List<gameObjectinRotList> gameObjects = new List<gameObjectinRotList>();
	public List<gameObjectinRotList> debugg = new List<gameObjectinRotList> ();

	private bool firstTime = true;

	movementManager movManager;
	[ObfuscateLiterals]
	void Start () {
		movManager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<movementManager> ();

	}
	
	void FixedUpdate () {
		
	}
	[ObfuscateLiterals]
	public void customUpdate()
	{
		if (firstTime) {
			arrangePriorities ();
			firstTime = false;
		}
		updateGameObjects ();

		debugg.Clear ();
		for (int kk = 0; kk < gameObjects.Count; kk++) {
			debugg.Add(new gameObjectinRotList (gameObjects[kk].transfrom,gameObjects[kk].lookAt,gameObjects[kk].refRot));
			debugg [kk].delta = gameObjects [kk].delta;
			debugg [kk].data = gameObjects [kk].data;
		}
			
		foreach(gameObjectinRotList go in gameObjects)
			go.delta = Quaternion.identity;
	}
	[ObfuscateLiterals]
	public void addGameObject(Transform t, lookat_Managed l, Quaternion r){
		foreach (gameObjectinRotList go in gameObjects) {
			//if it is found in the list
			if (go.transfrom == t) {
				//Debug.Log (t.ToString () + " already exists");
				go.lookAt = l;
				//go.copyTrans = cT;
				return;
			}
		}
		//if the loop ends and it is not found in the list - creat a new one
		gameObjects.Add(new gameObjectinRotList (t,l,r));
		//Debug.Log ("added : " + t.ToString ());
	}
	[ObfuscateLiterals]
	public void addDelta(Transform t, Quaternion delta){
		foreach (gameObjectinRotList go in gameObjects)
			if (go.transfrom == t) {
				go.delta = go.delta * delta;
				//go.deltafloatx = go.deltafloatx + dx;
				//go.deltafloaty = go.deltafloaty + dy;
				//go.deltafloatz = go.deltafloatz + dz;
				return;
			}
		//Debug.Log (t.ToString () + " object was not found in gameLoop!");
	}
	[ObfuscateLiterals]
	public void setRef(Transform t , Quaternion r){
		foreach (gameObjectinRotList go in gameObjects)
			if (go.transfrom == t) {
				go.refRot = r;
				return;
			}
		Debug.LogError (t.ToString () + " object was not found in gameLoop!");

	}
	[ObfuscateLiterals]
	public void addData(Transform t, muscleControllersData_Managed m)
	{
		foreach (gameObjectinRotList go in gameObjects) {
			//if it is found in the list
			if (go.transfrom == t) {
				//Debug.Log (t.ToString () + " already exists");
				go.data.Add(m);
				return;
			}
		}
		//if the loop ends and it is not found in the list - creat a new one
		gameObjects.Add(new gameObjectinRotList (t,null,t.rotation));
		gameObjects [gameObjects.Count - 1].data.Add(m);
		//Debug.Log ("added : " + t.ToString ());
	}
	[ObfuscateLiterals]
	public void addData2(Transform t, actionObjectScript m)
	{
		foreach (gameObjectinRotList go in gameObjects) {
			//if it is found in the list
			if (go.transfrom == t) {
				//Debug.Log (t.ToString () + " already exists");
				go.data2.Add(m);
				return;
			}
		}
		//if the loop ends and it is not found in the list - creat a new one
		gameObjects.Add(new gameObjectinRotList (t,null,t.rotation));
		gameObjects [gameObjects.Count - 1].data2.Add(m);
		//Debug.Log ("added : " + t.ToString ());
	}
	[ObfuscateLiterals]
	public void addLimitRot(Transform t, limitRotation_sphere_Managed limit){
		foreach (gameObjectinRotList go in gameObjects) {
			//if it is found in the list
			if (go.transfrom == t) {
				//Debug.Log (t.ToString () + " already exists");
				go.limitRot = limit;
				return;
			}
		}
		//if the loop ends and it is not found in the list - creat a new one
		gameObjects.Add(new gameObjectinRotList (t,null,t.rotation));
		gameObjects [gameObjects.Count - 1].limitRot = limit;
		Debug.Log (t.ToString () + "error, can't be");
	}
	[ObfuscateLiterals]
	public void addblindTrace(Transform t, blindTrace_Managed bt){
		foreach (gameObjectinRotList go in gameObjects) {
			//if it is found in the list
			if (go.transfrom == t) {
				//Debug.Log (t.ToString () + " already exists");
				go.blindTrace = bt;
				return;
			}
		}
		//if the loop ends and it is not found in the list - creat a new one
		gameObjects.Add(new gameObjectinRotList (t,null,t.rotation));
		gameObjects [gameObjects.Count - 1].blindTrace = bt;
		Debug.Log (t.ToString () + "error, can't be ever!");
	}
	[ObfuscateLiterals]
	public void resetRotations(){
		for (int i = 0; i < gameObjects.Count; i++) {
			gameObjects [i].transfrom.rotation = gameObjects [i].refRot;
		}
	}
	[ObfuscateLiterals]
	public void updateGameObjects(){//TODO today2: b3d keda wna b3ml update hdawar fi KOL l ba3do 3la ai direct child 3ndi
									// lw la2et haghyar el reference/aw adihom l far2 da 3la eno delta
									// bs keda
		for (int i = 0; i < gameObjects.Count; i++) {
			if (gameObjects [i].lookAt != null)
				gameObjects [i].lookAt.rotate ();
			if (gameObjects [i].data != null) {
				for (int v = 0; v < gameObjects[i].data.Count ; v++)
					gameObjects [i].data[v].rotate ();
			}
			if (gameObjects [i].data2 != null) {
				for (int v = 0; v < gameObjects[i].data2.Count ; v++)
					gameObjects [i].data2[v].rotate (gameObjects[i].transfrom);
			}
			if (gameObjects [i].limitRot != null) {
				if (gameObjects [i].blindTrace != null)
					gameObjects [i].blindTrace.getTheSphereNearMe (); 
				gameObjects [i].delta = gameObjects [i].limitRot.limitThis (gameObjects [i].delta, gameObjects[i].transfrom, gameObjects [i].refRot);
			}
				
			gameObjects[i].transfrom.rotation = gameObjects[i].delta * gameObjects[i].refRot ;



//			//set the angle of the object as a LastFrameAngle in muscleControllersData and ActionObjectScript
//			if (gameObjects [i].data != null) {
//				for (int v = 0; v < gameObjects[i].data.Count ; v++)
//					gameObjects [i].data[v].setLastFrameAngles (gameObjects[i].transfrom.rotation.eulerAngles.x,gameObjects[i].transfrom.rotation.eulerAngles.y, gameObjects[i].transfrom.rotation.eulerAngles.z);
//			}
//			if (gameObjects [i].data2 != null) {
//				for (int v = 0; v < gameObjects[i].data2.Count ; v++)
//					gameObjects [i].data2[v].setLastFrameAngles (gameObjects[i].transfrom.rotation.eulerAngles.x,gameObjects[i].transfrom.rotation.eulerAngles.y, gameObjects[i].transfrom.rotation.eulerAngles.z);
//			}
//			/////


			// ta3del: ha3adi 3la kol l children ( el homa asln 3ashan mtratebin fa lesa dorhom
			// magash, wa3ml set lel refRot el gdid bta3hom bl laffa l gdida di
			if (gameObjects [i].lookAt == null)
			for (int p = 0; p < gameObjects [i].transfrom.childCount; p++) {
				Transform dumm = gameObjects [i].transfrom.GetChild (p).transform;
				//hna el mfrod el refRot bta3 el child brdo
				int j = searchForTransform(dumm);
				if (j!=-1){
					//abo j da el child el ana lesa mdawar 3leh bl search, el hwa hwa dumm da
					Quaternion delta = dumm.rotation * Quaternion.Inverse(gameObjects[j].refRot);
					if (gameObjects[j].lookAt == null)
						addDelta (gameObjects [i].transfrom.GetChild (p).transform, delta);
				}
			}
		}
	
	}
	[ObfuscateLiterals]
	void arrangePrioritiesOld(){ 
		int counter = 1;
		bool oneisNotDoneYet = true;
		while (oneisNotDoneYet) {
			// hna el mohem bs en el 3ndo nextRotation yt3ml abl l next Rotation bta3to
			// w lw 7ad parent 3la 7ad kman
			for (int g = 0; g < gameObjects.Count; g++) {
				if (gameObjects [g].done == true) {
					continue;
				}

				if (gameObjects [g].lookAt == null) {
					gameObjects [g].priority = counter;
					gameObjects [g].done = true;
					counter++;
					continue;
				}

				if (gameObjects [g].lookAt.nextRotation != null) {
					gameObjects [g].priority = counter;
					gameObjects [g].done = true;
					counter++;

					int c = searchForLookAt (gameObjects [g].lookAt.nextRotation);
					if (c == -1)
						Debug.LogError ("error , can't happen");
					gameObjects [c].priority = counter;
					gameObjects [c].done = true;
					counter++;
				}

				if (gameObjects [g].lookAt.precedingRotation != null) {
					continue;
				}

				if (gameObjects [g].lookAt.precedingRotation == null && gameObjects [g].lookAt.nextRotation == null) {
					gameObjects [g].priority = counter;
					gameObjects [g].done = true;
					counter++;
				}
			}

			// check if there exist a missing one not done yet
			for (int ll = 0; ll < gameObjects.Count; ll++) {
				if (gameObjects [ll].done == false) {
					oneisNotDoneYet = true;
					break;
				}
				else {
					oneisNotDoneYet = false;
				}
			}
		}

		gameObjects.Sort(SortingFn);
	}

	[ObfuscateLiterals]
	void arrangePriorities (){
		for ( int i = 0; i < gameObjects.Count; i++) {
			// le kol wa7ed hagib l parent washofo fl list wla la2 
			Transform searchforthis = gameObjects[i].transfrom.parent;
			while (searchforthis != null) {
				int x = searchForTransform (searchforthis);
				// lw la2 agib el parent el fo2o .. so on lghayet ma y5laso aw lghayet ma ala2ih
				// fl list, a3mlo add fl precedings wa3ml add fl done be false
				if (x == -1) {
					// lw msh fl list -> hat el parent bta3o 
					searchforthis = searchforthis.parent;
				} else {
					// lw rege3 rakam , y3ni hwa fl list
					gameObjects[i].precedings.Add ( new Preceding(x,false));
					gameObjects [i].precedinngDebug.Add (gameObjects [x].transfrom);
					searchforthis = searchforthis.parent;
				}
			}
				
			//hna htb2a 3ando previuos rotation wla la2
			// we 3ndo look at wla la2a
			if (gameObjects[i].lookAt != null){
				Transform searchforthis2 = gameObjects[i].lookAt.target1;
				while (searchforthis2 != null) {
					int x = searchForTransform (searchforthis2);
					// lw la2 agib el parent el fo2o .. so on lghayet ma y5laso aw lghayet ma ala2ih
					// fl list, a3mlo add fl precedings wa3ml add fl done be false
					if (x == -1) {
						// lw msh fl list -> hat el parent bta3o 
						searchforthis2 = searchforthis2.parent;
					} else {
						// lw rege3 rakam , y3ni hwa fl list
						gameObjects[i].precedings.Add ( new Preceding(x,false));
						gameObjects [i].precedinngDebug.Add (gameObjects [x].transfrom);
						searchforthis2 = searchforthis2.parent;
					}
				}

				lookat_Managed ct = gameObjects [i].lookAt.precedingRotation;
				if (ct != null) {
					int xx = searchForTransform (ct.target1.transform);
					if (xx != -1) {
						gameObjects [i].precedings.Add (new Preceding (xx, false));
						gameObjects [i].precedinngDebug.Add (gameObjects [xx].transfrom);
					}
				}
			}
		}

		int prio = 1;
		bool oneisNotDoneYet = true; // di tb2a false lw 3adet mara kamla we mfish wla wa7ed preceding false
		bool allprecedingsaredone = true; //di 3an el precedings nafsohom
		// b3d keda, ha3adi 3lehom eli el preceding bto3o be null hadih rakam wa3mlo ++

		int coco = 500;
		while (oneisNotDoneYet) {
			coco--;
			if (coco == 0)
				break;

			///////////////////////////// hereeee :((((
			/// // el moshkla fl 7eta di , mi done we min msh done fl precedings list bas
			// do the ready ones (assign them numbers and set done to true)

			for(int ll = 0; ll < gameObjects.Count; ll++) {
				allprecedingsaredone = true;
				if (gameObjects [ll].done == true)
					continue;
				else if (gameObjects [ll].precedings.Count == 0 && gameObjects [ll].done == false) {
					// kda yb2a da tmam 
					gameObjects [ll].priority = prio;
					prio++;
					gameObjects [ll].done = true;
					continue;
				}
				//-- then -> 
				// loop on all objects and set the bools of precedings according to done or not
				else {
					for (int v = 0; v < gameObjects [ll].precedings.Count; v++) {
						int placeOfThisPreceding = gameObjects [ll].precedings [v].trans;
						gameObjects [ll].precedings [v].done = gameObjects [placeOfThisPreceding].done;
						if (gameObjects [ll].precedings [v].done == true) {
							continue;
						} else {
							allprecedingsaredone = false;
						}
					}

					if (allprecedingsaredone) {
						gameObjects [ll].priority = prio;
						prio++;
						gameObjects [ll].done = true;
						continue;
					}
				}		
			}

			// check if there exist a missing one not done yet
			for (int ll = 0; ll < gameObjects.Count; ll++) {
				if (gameObjects [ll].done == false) {
					oneisNotDoneYet = true;
					break;
				}
				else {
					oneisNotDoneYet = false;
				}
			}
		}
		////////////////////////////
		/// now rearrange the list according to the priorities 
		gameObjects.Sort(SortingFn);

		// for debugging , re calculate the precedings numbers
		for (int bre = 0; bre < gameObjects.Count; bre++) {
			for (int k = 0; k < gameObjects [bre].precedings.Count; k++) {
				int h = searchForTransform(gameObjects[bre].precedinngDebug[k]);
				if (h == -1)
					Debug.LogError ("can't be not in the list here!");
				else
					gameObjects [bre].precedings [k].trans = h;
			}
		}


	}

	[ObfuscateLiterals]
	int searchForLookAt(lookat_Managed la){
		for (int i = 0; i < gameObjects.Count; i++)
			if (gameObjects [i].lookAt == la)
				return i;
		return -1;
	}

	int searchForTransform(Transform tr){
		for (int i = 0; i < gameObjects.Count; i++)
			if (gameObjects [i].transfrom == tr)
				return i;
		return -1;
	}

	static int SortingFn(gameObjectinRotList go1, gameObjectinRotList go2)
	{
		return go1.priority.CompareTo(go2.priority);
	}


}

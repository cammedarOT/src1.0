﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
/// <summary>
/// 9-April-2017
/// this script copies the translation of another object to the object/holder of the script
/// -it have a refernce object to the Transform of the target object
/// -contains a smoothing factor to smoothen the follow movement (should be unnoticable) so higher is better
/// -can mask one/two/three of the axis 
/// 
/// </summary>

public class copyTranslation_Managed : MonoBehaviour {

	public movementManager Manager;

	//first followed object variables
	public Transform target1;            // The position that I will be following.
	Vector3 offset1;                     // The initial offset from the target.
	Vector3 target1RefPos;				 // initial target position - added to implement weight
	Vector3 delta1;

	//second followed object variables
	public Transform target2;
	Vector3 offset2;
	Vector3 target2RefPos;
	Vector3 delta2;

	Vector3 myTargetPos;
	public Vector3 maskedDelta1;
	public Vector3 maskedDelta2;

	public bool X_axis;
	public bool Y_axis;
	public bool Z_axis;

	public bool X_axis2;
	public bool Y_axis2;
	public bool Z_axis2;

	public float weight = 1.0f;
	public float weight2 = 1.0f;
	public float smoothing = 15f;        // The speed with which I will be following (better to be not noticable).

	Vector3 movement;
	[ObfuscateLiterals]
	void Start () {
		if (target1 == null && target2 == null)
			return;
		// Calculate the initial offset.
		//i will use only one of those offsets and target position to calculate my relative position
		if (target1 != null)
			offset1 = transform.position - target1.position;
		if (target2 != null)
			offset2 = transform.position - target2.position;

		//store refpos for the target object
		if (target1 != null) {
			target1RefPos = target1.position;
		}
		if (target2 != null) {
			target2RefPos = target2.position;
		}

		Manager.addGameObject (transform, transform.position);
		Manager.addcTfunction (transform, this);	
	}

	void FixedUpdate ()
	{


		// here


		//////kol el klam da mlosh lazma dlwa2ty

//		//myTargetPos = target.position + offset;
//		if (target1 != null && target2 != null)
//			myTargetPos = target1RefPos + offset1 + maskedDelta1 + maskedDelta2;
//		else if (target1 != null)
//			myTargetPos = target1RefPos + offset1 + maskedDelta1 + maskedDelta2;
//		else if (target2 != null)
//			myTargetPos = target2RefPos + offset2 + maskedDelta1 + maskedDelta2;
//		// Assign my transformation to the new vector variables
//		float x, y, z;
//		x = transform.position.x;
//		y = transform.position.y;
//		z = transform.position.z;
//
//		x = Mathf.Lerp (transform.position.x, myTargetPos.x, smoothing * Time.deltaTime);
//		y = Mathf.Lerp (transform.position.y, myTargetPos.y, smoothing * Time.deltaTime);
//		z = Mathf.Lerp (transform.position.z, myTargetPos.z, smoothing * Time.deltaTime);
//
//
//		// da el mfrod myb2ash mwgod 5las b2a , el mfrod el Manager hwa el y7ot el 7ta
//		// di msh howa , hwa hytla3 el value bas
//		//transform.position = new Vector3 (x, y, z);
	}
	[ObfuscateLiterals]
	// called from outside
	public void copy(){

		if (target1 == null && target2 == null)
			return ;

		// Create a postion I am aiming for based on the offset from the target.
		if (target1 != null)
			delta1 = Vector3.MoveTowards(target1RefPos, target1.position, weight*Vector3.Distance(target1RefPos, target1.position)) - target1RefPos;
		if (target2 != null)
			delta2 = Vector3.MoveTowards(target2RefPos, target2.position, weight2*Vector3.Distance(target2RefPos, target2.position)) - target2RefPos;

		// masking the required axiis
		maskedDelta1 = new Vector3 (0f, 0f, 0f);
		maskedDelta2 = new Vector3 (0f, 0f, 0f);
		if (X_axis && target1!=null)
			maskedDelta1.x = delta1.x;
		if (Y_axis && target1!=null)
			maskedDelta1.y = delta1.y;
		if (Z_axis && target1!=null)
			maskedDelta1.z = delta1.z;
		if (X_axis2 && target2!=null)
			maskedDelta2.x = delta2.x;
		if (Y_axis2 && target2!=null)
			maskedDelta2.y = delta2.y;
		if (Z_axis2 && target2!=null)
			maskedDelta2.z = delta2.z;

		Manager.addDelta (transform, maskedDelta1 + maskedDelta2);
	}

}

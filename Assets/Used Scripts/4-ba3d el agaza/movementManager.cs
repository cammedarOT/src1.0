﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Beebyte.Obfuscator;

[System.Serializable]  // el kelma di bas , 5alet el list tetla3 fl inspector :O <3
public class Preceding{
	public int trans;
	public bool done;

	public Preceding (int i , bool b)
	{
		trans = i;
		done = b;
	}
} 


[System.Serializable]  // el kelma di bas , 5alet el list tetla3 fl inspector :O <3
public class gameObjectinList
{
	
	public Transform transfrom;
	public Vector3 delta;
	public Vector3 nextFrameDelta;
	public Vector3 Ref;
	public limitDistance_Managed limitD;
	public copyTranslation_Managed copyTrans; 
	public int priority;  

	//public Transform preceding;  // -> ha3ml el tri2a di 
	//public bool done;

	public List <Preceding> precedings = new List <Preceding>();
	public List <Transform> precedinngDebug = new List <Transform> ();
	public bool done;

	public gameObjectinList(Transform t, Vector3 d , Vector3 r)
	{
		transfrom = t;
		delta = d;
		Ref = r;
		limitD = null;
		copyTrans = null;
	}
}
[System.Serializable]  // el kelma di bas , 5alet el list tetla3 fl inspector :O <3
public class movementManager : MonoBehaviour {

	public rotationManager rotManager;
	public List<gameObjectinList> gameObjects = new List<gameObjectinList>();
	public List<gameObjectinList> debugg = new List<gameObjectinList> ();

	private bool firstTime = true;
	void Start () {
		Application.targetFrameRate = 60;
		//  we 3amalt enable lel GPU skinning , fi el project settings -> player
	}

	//da lma kan update bas , we fi ai object tani Fixed update , kan by5araf 3ashan far2 el time
	// 5ali balak mn fixedupdate wla update!
	void FixedUpdate () {
		if (firstTime) {
			arrangePriorities ();
			firstTime = false;
		}

		//a7ot el next frame deltas bto3 el frame el fat
		foreach (gameObjectinList go in gameObjects) {
			//hya keda keda hna be zero asln el delta
			go.delta += go.nextFrameDelta;
			go.nextFrameDelta = new Vector3 (0f, 0f, 0f);
		}

		updateGameObjects ();
		//updateGameObjects (); // hahaha :V da 3adal l ra3sha TODO

		//for debugging , 3ashan el list el 7a2i2ya lazm asaffar el deltas kolha abl ma2fel l loop
		debugg.Clear ();
		for (int kk = 0; kk < gameObjects.Count; kk++) {
			debugg.Add(new gameObjectinList (gameObjects[kk].transfrom,new Vector3 (0f, 0f, 0f),gameObjects[kk].Ref));
			debugg [kk].transfrom = gameObjects [kk].transfrom;
			debugg [kk].delta = gameObjects [kk].delta;
			debugg [kk].Ref = gameObjects [kk].Ref;
			debugg [kk].limitD = gameObjects [kk].limitD;
			debugg [kk].precedings = gameObjects [kk].precedings;
			debugg [kk].priority = gameObjects [kk].priority;
			debugg [kk].nextFrameDelta = gameObjects [kk].nextFrameDelta;
			foreach (Preceding p in gameObjects[kk].precedings)
				debugg [kk].precedinngDebug.Add (gameObjects [p.trans].transfrom);
			debugg[kk].done = gameObjects [kk].done;


		}


		//bensfarhom tani
		foreach(gameObjectinList go in gameObjects)
			go.delta = new Vector3 (0f, 0f, 0f);
	}



	//used to add a game object to the controlled list by the manager
	//and add their ref position - on Start at the other scripts
	// here i will check first if I (the manager) have a reference on this
	// gameObject before or not
	public void addGameObject(Transform t, Vector3 r){
		foreach (gameObjectinList go in gameObjects) {
			//if it is found in the list
			if (go.transfrom == t) {
				//if (go.Ref != r) Debug.Log ("error in ref of" + t.ToString());
				go.Ref = r;
				//Debug.Log (t.ToString () + " already exists");
				//go.copyTrans = cT;
				return;
			}
		}
		//if the loop ends and it is not found in the list - creat a new one
		gameObjects.Add(new gameObjectinList (t,new Vector3 (0f, 0f, 0f),r));
		//gameObjects [gameObjects.Count - 1].copyTrans = cT;
		//Debug.Log ("added : " + t.ToString ());
	}

	[ObfuscateLiterals]
	//called from outside
	public void addLimitFunction(Transform t, limitDistance_Managed LD, Vector3 r){
		foreach (gameObjectinList go in gameObjects)
			if (go.transfrom == t) {
				go.limitD = LD;
				return;
			}
		// we lw msh mwgod fi el list
		// keda 3mlt add mn gher priority
		gameObjects.Add(new gameObjectinList (t,new Vector3 (0f, 0f, 0f),r));
		gameObjects [gameObjects.Count - 1].limitD = LD;
		//Debug.Log ("added : " + t.ToString ());
	}
	[ObfuscateLiterals]
	//called from outside
	public void addcTfunction(Transform t, copyTranslation_Managed cT){
		foreach (gameObjectinList go in gameObjects)
			if (go.transfrom == t) {
				go.copyTrans = cT;
				return;
			}
	}
	[ObfuscateLiterals]
	//called from outside
	// di aw7ash function fihom , kol (frame fih ta3dil) loop kbira 3ashan ala2i l ana 3aizo :(
	public void addDelta(Transform t, Vector3 d){
		foreach (gameObjectinList go in gameObjects)
			if (go.transfrom == t) {
				go.delta += d;
				return;
			}
		Debug.Log (t.ToString () + " object was not found in gameObjects list!");
	}

	// dlwa2ty el 7al eni a3ml watch lel list wa3raf el delta el btetnattat min we keda
	// we a3ml l tartib da ashof a el hy7sal
	// washof kman leh el example bta3i mdrabsh , we leeh ghato 3ala ba3d keda
	[ObfuscateLiterals]
	public void addNextFrameDelta(Transform t, Vector3 d){
		foreach (gameObjectinList go in gameObjects)
		if (go.transfrom == t) {
				go.nextFrameDelta += d;
			return;
		}
		//Debug.Log (t.ToString () + " object was not found in gameObjects list!");
	}
	[ObfuscateLiterals]
	public Vector3 getRefPosPlusDeltas(Transform t){
		foreach (gameObjectinList go in gameObjects) {
			if (go.transfrom == t)
				return go.Ref + go.delta;
		}
		Debug.Log ("error ! don'thave ref position");
		return Vector3.zero;
	}
	[ObfuscateLiterals]
	void arrangePriorities (){
		for ( int i = 0; i < gameObjects.Count; i++) {
			// le kol wa7ed hagib l parent washofo fl list wla la2 
			Transform searchforthis = gameObjects[i].transfrom.parent;
			while (searchforthis != null) {
				int x = searchForTransform (searchforthis);
				// lw la2 agib el parent el fo2o .. so on lghayet ma y5laso aw lghayet ma ala2ih
				// fl list, a3mlo add fl precedings wa3ml add fl done be false
				if (x == -1) {
					// lw msh fl list -> hat el parent bta3o 
					searchforthis = searchforthis.parent;
				} else {
					// lw rege3 rakam , y3ni hwa fl list
					gameObjects[i].precedings.Add ( new Preceding(x,false));
					gameObjects [i].precedinngDebug.Add (gameObjects [x].transfrom);
					searchforthis = searchforthis.parent;
				}
			}
			// we swa2 weselt aw la2 , hashof 3ndo brdo fi copy translation wla la2
			// lw fi , ha5od el object , a7oto fl list wa7ot bool be false
			copyTranslation_Managed ct = gameObjects[i].transfrom.GetComponent<copyTranslation_Managed>();
			if (ct != null) {
				int xx = searchForTransform (ct.target1.transform);
				if (xx != -1) {
					gameObjects [i].precedings.Add (new Preceding (xx, false));
					gameObjects [i].precedinngDebug.Add (gameObjects [xx].transfrom);
				}
				if (ct.target2 != null) {
					xx = searchForTransform (ct.target2.transform);
					if (xx != -1) {
						gameObjects [i].precedings.Add (new Preceding (xx, false));
						gameObjects [i].precedinngDebug.Add (gameObjects [xx].transfrom);
					}
				}
			}
			limitDistance_Managed ld = gameObjects[i].transfrom.GetComponent<limitDistance_Managed>();
			if (ld != null) {
				int uu = searchForTransform (ld.centerPt);
				if (uu != -1) {
					gameObjects [i].precedings.Add (new Preceding (uu, false));
					gameObjects [i].precedinngDebug.Add (gameObjects [uu].transfrom);
				}
			}
		}
			
		int prio = 1;
		bool oneisNotDoneYet = true; // di tb2a false lw 3adet mara kamla we mfish wla wa7ed preceding false
		bool allprecedingsaredone = true; //di 3an el precedings nafsohom
		// b3d keda, ha3adi 3lehom eli el preceding bto3o be null hadih rakam wa3mlo ++

		int coco = 500;
		while (oneisNotDoneYet) {
			coco--;
			if (coco == 0)
				break;
			
			///////////////////////////// hereeee :((((
			/// // el moshkla fl 7eta di , mi done we min msh done fl precedings list bas
			// do the ready ones (assign them numbers and set done to true)

			for(int ll = 0; ll < gameObjects.Count; ll++) {
				allprecedingsaredone = true;
				if (gameObjects [ll].done == true)
					continue;
				else if (gameObjects [ll].precedings.Count == 0 && gameObjects [ll].done == false) {
					// kda yb2a da tmam 
					gameObjects [ll].priority = prio;
					prio++;
					gameObjects [ll].done = true;
					continue;
				}
				//-- then -> 
				// loop on all objects and set the bools of precedings according to done or not
				else {
					for (int v = 0; v < gameObjects [ll].precedings.Count; v++) { //hna msh m7tagin l done el gwa el preceding object nfso , mmkn a5do mn l gameObject nafso mn l list
						//lma nefda nzabatha
						int placeOfThisPreceding = gameObjects [ll].precedings [v].trans;
						gameObjects [ll].precedings [v].done = gameObjects [placeOfThisPreceding].done;
						if (gameObjects [ll].precedings [v].done == true) {
							continue;
						} else {
							allprecedingsaredone = false;
						}
					}

					if (allprecedingsaredone) {
						gameObjects [ll].priority = prio;
						prio++;
						gameObjects [ll].done = true;
						continue;
					}
				}		
			}
		
			// check if there exist a missing one not done yet
			for (int ll = 0; ll < gameObjects.Count; ll++) {
				if (gameObjects [ll].done == false) {
					oneisNotDoneYet = true;
					break;
				}
				else {
					oneisNotDoneYet = false;
				}
			}
		}
		////////////////////////////
		/// now rearrange the list according to the priorities 
		gameObjects.Sort(SortingFn);

		// for debugging , re calculate the precedings numbers
		for (int bre = 0; bre < gameObjects.Count; bre++) {
			for (int k = 0; k < gameObjects [bre].precedings.Count; k++) {
				int h = searchForTransform(gameObjects[bre].precedinngDebug[k]);
				if (h == -1)
					Debug.LogError ("can't be not in the list here!");
				else
					gameObjects [bre].precedings [k].trans = h;
			}
		}


	}
	[ObfuscateLiterals]
	static int SortingFn(gameObjectinList go1, gameObjectinList go2)
	{
		return go1.priority.CompareTo(go2.priority);
	}

	public int searchForTransform(Transform tr){
		for (int i = 0; i < gameObjects.Count; i++)
			if (gameObjects [i].transfrom == tr)
				return i;
		return -1;
	}

	void resetMovements(){
		for (int i = 0; i < gameObjects.Count; i++) {
			gameObjects [i].transfrom.position = gameObjects [i].Ref;
		}
	}
	[ObfuscateLiterals]

	// used to assign the new pos data to all the game objects in the list
	void updateGameObjects(){

		//reset the rotations here
		rotManager.resetRotations();
		resetMovements ();

		for (int i = 0; i < gameObjects.Count; i++) {
			if (gameObjects[i].copyTrans != null)
				gameObjects[i].copyTrans.copy();
			if (gameObjects[i].limitD != null)
				gameObjects[i].delta += gameObjects[i].limitD.limitThis (gameObjects[i].Ref + gameObjects[i].delta);

			gameObjects[i].transfrom.position = gameObjects[i].Ref + gameObjects[i].delta;

			//ha3adi 3la kol l children , el homa metratebin asln abl kda fa lesa
			//dorhom magash , w adihom 7tet el delta el et7rakoha di 
			// ( ghaleban lw w7ed 3ndo copyTrans le wa7ed tani child fi nafs el level msh h3ml kda ) lsa hashof
			for (int p = 0; p < gameObjects [i].transfrom.childCount; p++) {
				Transform dumm = gameObjects [i].transfrom.GetChild (p).transform;
				//hna el mfrod el refRot bta3 el child brdo
				int j = searchForTransform(dumm);
				if (j!=-1){
					//abo j da el child el ana lesa mdawar 3leh bl search, el hwa hwa dumm da
					Vector3 delta = gameObjects[j].transfrom.position - gameObjects[j].Ref;
					//if (gameObjects[j].lookAt == null) //akid fi condition
						addDelta (gameObjects [i].transfrom.GetChild (p).transform, delta);
				}
			}
		}

		rotManager.customUpdate ();

		//el 7eta di bn3mlha 3ashan lw fi rotation by7sal bysabeb translation , wel translation da
		//mfrod yt3adel be limit distance (aw yta5ed fi copy translation -- bs di lsa msh mhandelha hna)
		//fa bnndah el limit distance tani hna 3ashan t3del el 7raka el et7rakha
		for (int i = 0; i < gameObjects.Count; i++) {
			if (gameObjects[i].limitD != null)
				//hna mynfa3sh ab3tlha (Ref+delta) zy ma kont b3ml foo2 , 3ashan di dlwa2ty 7asal rotations
				//asaret 3leha we el rotations di adet le taghyir el position akid , fa lw est5demt el ref + delta
				//halghi ta2sir ai rotation sabbeb translation 7asal hnak fi el rotation manager
				gameObjects[i].delta += gameObjects[i].limitD.limitThis (gameObjects[i].transfrom.position);

			gameObjects[i].transfrom.position = gameObjects[i].transfrom.position + gameObjects[i].delta;
		}


	}


}



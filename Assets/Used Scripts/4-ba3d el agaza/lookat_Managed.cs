﻿using UnityEngine;
using System.Collections;
using Beebyte.Obfuscator;
public class lookat_Managed : MonoBehaviour {

	public rotationManager Manager;
	public lookat_Managed nextRotation;
	public lookat_Managed precedingRotation;
	public Transform target1;  

	public Vector3 refPos;
	public Quaternion refRot;
	public Quaternion saveRot;
	public Vector3 refUp;

	public Quaternion delta1;

	public Quaternion targetRotation;
	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<rotationManager> ();

		refPos = transform.position;
		refRot = transform.rotation;
		saveRot = transform.rotation;
		refUp = transform.up;

		Manager.addGameObject (this.transform, this, refRot);
	}
	
	void FixedUpdate () {
	}
	[ObfuscateLiterals]
	public void rotate(){

		//int t = Time.frameCount;
		//if (transform.name == "two (1)" && (t == 1 || t == 3 || t == 5))
		//	return;
		//if (transform.name == "one" && (t == 2 || t == 4 || t == 6))
		//	return;


		// to keep track of object position
		refPos = transform.position;

		// 3ashan el parent yrga3 le mkano el awlani fa y3oz ylef tani , myfdalsh sabet 
		if (nextRotation != null)
			nextRotation.transform.rotation = nextRotation.saveRot;

		delta1 = Quaternion.LookRotation ( -(refPos - target1.position).normalized, refUp); 
		delta1 = delta1 * Quaternion.Inverse (refRot);
		targetRotation = delta1; 

		//transform.rotation = targetRotation * refRot;
		//call add delta here instead
		Manager.addDelta(this.transform, targetRotation);

		if (nextRotation != null) {
			nextRotation.transform.rotation = targetRotation * nextRotation.saveRot;
			// save da bdal el ref 3ashan ana bghayar el ref kol mara, fa save da vdal l ref
			nextRotation.refRot = nextRotation.transform.rotation;
			nextRotation.Manager.setRef (nextRotation.transform, nextRotation.refRot);
			nextRotation.refUp = nextRotation.transform.up; // etfooooooooooo

			//el ana 3amlo da mynfa3sh , 3ashan el nextrotation da el mfrod ybda2
			//mn mkan el delta di , msh tb2a 7ettet delta zyada m3ah , lazm ylef 3la tool
			// y3ni abl ma hwa nndah el rotation bta3to hwa 
			//y3ni el mfrod el delta di , tt7at fl refRot wel real position b3at el next
			//rotation , msh ka delta fl manager
			// bs ana lazm ala2i 7al a5ali el manager shayef el 7asal da 3ashan may5rafsh 

			// kont ha3ml el foo2 da , bs mynfa3sh a3ml ai set rotation mn hna :/
			//nextRotation.Manager.addDelta(nextRotation.transform, targetRotation);
		}

		if (precedingRotation != null) {

			//el 7amdolaah battalt el konafa el kanet bt7sal we da 3eb msh bayen
			// bs akid hayet3ebni ba3den fa 3aiz a7lo dlwa2ty
			//weshof el contraction lesa bya5od 2 frames leeh	

			//mthya2li da el mfrod yt7at fl delta , 3ashan
			// fl manager ana bandah el awel el rotate di we b3d keda bandah rotate bta3et
			// el data  . fa rotate bta3et data bte7seb el rotation el gdida based on 
			// the deltas only , we el 7ta di bettri 5ales
			//precedingRotation.Manager.addDelta(precedingRotation.transform, targetRotation);

			// wel satr da ka cover 3ashan lw mfish rotate() tania fa yfdal da keda keda
			//DA MOHEM!
			// da el byshaghal el test case , aw el byshghal el objects el 3andha 
			// next we preceding we m3ndhash rotate tania (data)
			// ana msh fahem 7aga :/ bs hwa dlwa2ty el test msh shghal wel bta3 nafso
			// shghal :/

			precedingRotation.transform.rotation = targetRotation * precedingRotation.transform.rotation;
			// eshtghalet lma 5adt bali mn trtib el rotations :'(

			////////3aaaaaaaaaaaaa msh shghala leeeh

			////////////////// 5ali balak dol 2 different objects el btklm 3lehom kda
			/// dlwa2ty el moshkla fi a , eni kont ba3ml 7aga gwa el lookat
			/// hna y3ni , we di malhash asar fl rotation manager
			/// is not tracked y3ni , instantanoious keda
			/// b3mlha fi function rotate , le object tani gher el ana fih ..
			/// ana bab2a 3adeto 5las y3ni fa 5las mlosh update tbwazo 
			/// 3ashan keda kanet shghala we m7asetsh be mshakel fl test
			/// fl bta3 b2a nafso a el 7sal
			/// 
		
		}

		//Debug.Log (this.transform.name.ToString () + Time.frameCount.ToString());

	}
}

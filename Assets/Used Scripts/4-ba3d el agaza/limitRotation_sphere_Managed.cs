﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class limitRotation_sphere_Managed : MonoBehaviour {
	public Collider coll;
	public Collider coll2;

	Transform trans;
	Quaternion lastValidDelta;
	rotationManager Manager;

	//da solution mwdo3 el limit rotation lma kan 3amel error bsbab l range we l sliders
	public bool advancingEnabled = true;


	public Transform newFwd_transform;
	public Quaternion fwdDelta; //da el far2 el ana 3aizo ben el forward el 3adi wel forward l gdida el h3mlha
	public float x;
	public float y;
	public float z;
	[ObfuscateLiterals]
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("ScriptManager").GetComponent<rotationManager> ();
		Manager.addLimitRot (this.transform, this);
		newFwd_transform = Instantiate(newFwd_transform); //da prefab of an empty Gameobject 3adi 5ales
		newFwd_transform.transform.parent = this.transform;
		lastValidDelta = Quaternion.identity;
		if (fwdDelta.x == 0 && fwdDelta.y == 0 && fwdDelta.z == 0 && fwdDelta.w == 0)
			fwdDelta = Quaternion.identity;
	}

	public void disableAdvancingSlider(){
		advancingEnabled = false;
	}
	public void enableAdvancingSlider(){
		advancingEnabled = true;
	}
	[ObfuscateLiterals]
	public Quaternion limitThis (Quaternion delta, Transform t, Quaternion refRot) {
		fwdDelta = Quaternion.Euler (x, y, z);
		//5li balak el delta el bat3amel m3aha barra di bta3et el newforward
		// di fl global axiis ! GLOBAL ROTATION AXIIS
		trans = t;
		trans.rotation = delta * refRot;
		//Quaternion * Vector3  : rule , el 3aks hydrab
		newFwd_transform.position = t.position;
		// M3LOMA GDIDA MOHEMA : tartib el rotations byefre2 , tefre2 alefo el awel fl orientation
		// e gdid (fwdDelta) b3d keda alefo bl delta wla alefo el awel bl delta b3d keda lel orientation l gdid
		newFwd_transform.rotation = delta * fwdDelta * refRot;

		//Ray ray = new Ray (trans.position, trans.forward);
		Ray ray = new Ray (trans.position, newFwd_transform.forward);
		//casts a ray that ignore all the colliders except this one
		//true when intersects

		//3ashan ana bghyar b3d keda 7aga fl movement manager bt5ali mkan da msh hwa mkan l object
		//fa ana barsem el ray fl lateUpdate ta7t
		//Debug.DrawRay(transform.position, newFwd_transform.forward * 3, Color.green);
		RaycastHit hit;
		///ana 3aiz m5alish da y save ai rotation asln , 3aiz el 7al myb2ash mn gwa!
		///3aiz 7al tani gher el saved rotation di 
		/// 
		/// el action script yb2a 3ndo current slider w last slider (last valid)
		/// ha3ml l slider nfso hwa el bysave fi kol frame last valid value bta3to
		/// we a3ml fih function esmaha disableAdvancing more than this value y3ni 
		/// we mmkn andaha a2olha disable andavcing aw enable advancing
		/// we bttndeh mn gwa hna msh mn l manager? 3ashan hyb2a fi hna reference 3l slider msln ? 
		/// we byt7at - yt assign fl 7eta el bt3ml el box 
		/// tb efred afal l box w fta7o , hyb2a el rakam tar? tb efred kan 5arag bra el range abl ma y2fel
		if (coll.Raycast (ray, out hit, 3.0f)) {
			//inRange
			lastValidDelta = trans.rotation * Quaternion.Inverse (refRot);
			//el tri2a el gdida
			this.enableAdvancingSlider (); //bta3 el script el ana gowah da
			return lastValidDelta; 
		} else if (coll2){
			if (coll2.Raycast (ray, out hit, 3.0f)) {
				//inRange
				lastValidDelta = trans.rotation * Quaternion.Inverse (refRot);
				//el tri2a el gdida
				this.enableAdvancingSlider (); //bta3 el script el ana gowah da
				return lastValidDelta;
			}
		}

		//outofRange
		//hna mfrod maghyarsh lastValidDelta bs d test lel tri2a el gdida 3ashan mghyarsh code ktir
		lastValidDelta = trans.rotation * Quaternion.Inverse(refRot);
		//plus b2a eni hast5dem l tri2a el gdida
		this.disableAdvancingSlider(); //bta3 el script el ana gowah da
		return lastValidDelta;  
	}

	void LateUpdate(){
        //TODO : know why this gives null exception
		//Ray ray = new Ray (trans.position, newFwd_transform.forward);
		//Debug.DrawRay(transform.position, newFwd_transform.forward * 3, Color.green);
	}


}

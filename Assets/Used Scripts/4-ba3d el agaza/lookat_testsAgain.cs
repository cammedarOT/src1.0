﻿using UnityEngine;
using System.Collections;

public class lookat_testsAgain : MonoBehaviour {

	public Transform target1;  

	public Transform Ref;
	public Transform tester;

	public Quaternion delta1;
	public Quaternion delta2;
	Quaternion Delta;

	public Quaternion targetRotation;

	void Start () {
		Ref = this.transform;


		// a7ot el script da fl manager 3ashan yndaho washilo mn update hena
		// a5alih ynfa3 yb2a m3mollo mask - di h3mlha bl angles wel trig. msh zy da
		// 3ashan aftker el bayez -> run we pause we dos check el hbal we dos next frame
		// el rotation bye7sal ba3dhom

		// ana et2akedt , el rotation hwa el wa7id el fadel el by5ali el gesm
		// y3ml 7rakat met2ata3a fi frames fl barnameg keda
		// we wna ba7elo h7awel ashof moshklet el rotation el ray7a gher rag3a di 
		// el bt5ali el scapula tetir

		// el bta3 da byshtghal we y3ml add lel deltas we tmam lw hwa mwgod fi 
		// object we bylef object tani we msh 3aref leeh y3ni 
		// fa ba3ml object tani adilo da wa5alih fi nafs position el awlani
		// wadilo copytranslation 3leh 3ashan ymshi m3ah bzabt , w hwa ylefo

		// msh mohem awi dlwa2ty ad5alo fl manager , 3ashan el rotations el mo3tameda
		// 3ala ba3d msh ktir le draget enha tban , homa 2 frames bas
		// l mohem dlwa2ty ala2i el bybawaz el regoo3

		// ana mla2etsh el moshkla bs et7allet
		// lma 5alet fi reference gheri , y3ni 3mlt mwdo3 en object tani hwa el by7rak
		// we ylef el object el 3aiz allefo we ba3tber el tani da el reference
		// hwa kda eshtghal ! we mfish ra3sh wla error

		//close to finish
		// tartib el bta3 da byefre2 gedaaaaaaaaaaan
		// lama ghyart el parents (tla3t one barra mra we tla3t two bara mara)
		// fra2et ktir awiii

		// el 7al bta3 el look at bta3 el scapula el kan bayz da 
		// eni 3adalt el trtib bas :O bs keda ! :O
	
	}
	
	void FixedUpdate () {
		delta1 = Quaternion.LookRotation ( -(Ref.position - target1.position).normalized, Ref.up);
		delta1 = delta1 * Quaternion.Inverse(Ref.rotation);
		targetRotation = delta1; //* delta2; //Quaternion.Inverse (delta2);
		tester.rotation = targetRotation * Ref.rotation;


		//mmkn a7awel el rotation le euler washof ana 3aiz mnha a warag3ha tani quat
		//aw a7awelha euler wa3ml mask wa-rotate bl euler , msh 3aref b2a , 
		// bs el 7ta di 3ashan a-mask el rotations wa7t 2 rotations on one object
		//Vector3 eu = targetRotation.eulerAngles;

	}

	//called from outside
	void rotate(){
		delta1 = Quaternion.LookRotation ( -(Ref.position - target1.position).normalized, Ref.up);
		delta1 = delta1 * Quaternion.Inverse(Ref.rotation);
		targetRotation = delta1; //* delta2; //Quaternion.Inverse (delta2);
		tester.rotation = targetRotation * Ref.rotation;
	}
}

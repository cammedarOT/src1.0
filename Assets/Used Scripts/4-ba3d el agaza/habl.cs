﻿using UnityEngine;
using System.Collections;

public class habl : MonoBehaviour {

	Vector3 Ref;
	public bool ttest;

	void Start () {
		ttest = false;
		Ref = this.transform.position;
	}
	
	void FixedUpdate () {
		if (ttest)
			this.transform.position = Ref + 3* Vector3.up + 3 * Vector3.left;
		else
			this.transform.position = Ref;
	}
}

﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Security;
using System.Collections;
using System.Text;

namespace Security {
		
	public class FingerPrint : MonoBehaviour {
			
		void Start(){
			Debug.Log (Value ());
		}

		private static string fingerPrint = string.Empty;
			
		public static string Value() {
			if (string.IsNullOrEmpty(fingerPrint)) {
				//fingerPrint = GetHash("CPU >> " + SystemInfo. + "\nBIOS >> " + 
				//	biosId() + "\nBASE >> " + baseId() +
				//	videoId() +"\nMAC >> "+ macId() );
				fingerPrint = SystemInfo.deviceUniqueIdentifier;
			}

			return fingerPrint;
		}

		private static string GetHash(string s) {
			MD5 sec = new MD5CryptoServiceProvider();
			ASCIIEncoding enc = new ASCIIEncoding();
			byte[] bt = enc.GetBytes(s);
			return GetHexString(sec.ComputeHash(bt));
		}
			
		private static string GetHexString(byte[] bt) {
			string s = string.Empty;
			for (int i = 0; i < bt.Length; i++) {
				byte b = bt[i];
				int n, n1, n2;
				n = (int)b;
				n1 = n & 15;
				n2 = (n >> 4) & 15;
				if (n2 > 9)
					s += ((char)(n2 - 10 + (int)'A')).ToString();
				else
					s += n2.ToString();
				if (n1 > 9)
					s += ((char)(n1 - 10 + (int)'A')).ToString();
				else
					s += n1.ToString();
				if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
			}
			return s;
		}
			

	}
}
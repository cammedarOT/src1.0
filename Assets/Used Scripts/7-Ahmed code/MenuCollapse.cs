﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuCollapse : MonoBehaviour , IPointerClickHandler {

	bool open = false;
	float targetX = Screen.width;
	float X = Screen.width;
	Image thisImage;
	RectTransform thisRect;
	public RectTransform menu;
	public Image arrowImage;
	public Sprite expandBarImg;
	public Sprite expandArrowImg;
	public Sprite collapseBarImg;
	public Sprite collapseArrowImg;
	void Start() {
		thisImage = this.gameObject.GetComponent<Image> ();
		thisRect = this.gameObject.GetComponent<RectTransform> ();
	}
	void Update () {
		/*if (!open) {
			this.gameObject.GetComponent<RectTransform> ().position = new Vector3 (Screen.width + 185, this.transform.position.y, this.transform.position.z);
		} else {
			this.gameObject.GetComponent<RectTransform> ().position = new Vector3 (Screen.width, this.transform.position.y, this.transform.position.z);
		}
		*/
		if (!open) targetX = Screen.width;
		if (open) targetX = Screen.width + 250;
		X = Mathf.Lerp (X, targetX, 0.15f);
		menu.position = new Vector3 (X, this.transform.position.y, this.transform.position.z);

	}

	public void OnPointerClick(PointerEventData evd){
		open = !open;
		if (!open) {
			Debug.Log ("Expand");
			targetX = Screen.width;
			thisImage.sprite = collapseBarImg;
			arrowImage.sprite = collapseArrowImg;
			//thisRect.position.Set (0, this.transform.position.y, this.transform.position.z);//  = new Vector3 (0, this.transform.position.y, this.transform.position.z);
			thisRect.position = new Vector3 (Screen.width, this.transform.position.y, this.transform.position.z);
		} else {
			Debug.Log ("Collapse");
			targetX = Screen.width + 250;
			thisImage.sprite = expandBarImg;
			arrowImage.sprite = expandArrowImg;
			//thisRect.position.Set (this.transform.position.x-30, this.transform.position.y, this.transform.position.z);
			thisRect.position = new Vector3 (Screen.width-275, this.transform.position.y, this.transform.position.z);
		}

		//OppositeExpCollpse.SetActive (true);
		//this.gameObject.SetActive (false);
	}

	public bool isOpen()
	{
		return open;
	}
}

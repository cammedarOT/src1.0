﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionTabView : MonoBehaviour {

    private const string HOVER_WINDOWS_TAG = "hoverWindows";
    private const int ACTION_CONTROLLER_INDEX = 0;
    public GameObject actionTabContainer;
    public Slider actionTabSlider;
    public Button actionTabPlayButton;
    private ActionSliderManager Manager;
    private ActionTabController controller;

    // Use this for initialization
    void Start () {
        actionTabSlider.onValueChanged.AddListener(ActionTabSliderValueChangedListener);
        actionTabPlayButton.onClick.AddListener(ActionTabPlayButtonClickListerner);
        Manager = new ActionSliderManager(actionTabSlider, actionTabPlayButton);
        controller = new ActionTabController(this);
	}
	
	// Update is called once per frame
	void Update () {
        //quick fix 34an m3ndy4 w2t 2day, Am sry to whoever hy4of el code da
        //TODO: Find why manager becomes null?
        if (Manager == null)
        {
            Manager = new ActionSliderManager(actionTabSlider, actionTabPlayButton);
        }
        else
        {
            Manager.Manage();
        }
	}
    public bool SetSliderValue (float value)
    {
        return Utils.SetSliderValue(actionTabSlider, value);
    }
    private void ActionTabSliderValueChangedListener(float value)
    {
        //TODO : Create Hover Windows Controller to Manage its children
        if (GameObject.FindGameObjectWithTag(HOVER_WINDOWS_TAG).transform.childCount > 0)
            GameObject.FindGameObjectWithTag(HOVER_WINDOWS_TAG).transform.GetChild(ACTION_CONTROLLER_INDEX).GetComponent<ActionView>().GetController().SetSliderValue(value);
        else
        {
            actionTabSlider.value = actionTabSlider.minValue;
        }
    }
    private void ActionTabPlayButtonClickListerner()
    {
        GameObject.FindGameObjectWithTag(HOVER_WINDOWS_TAG).transform.GetChild(ACTION_CONTROLLER_INDEX).GetComponent<ActionView>().GetController().SimulatePlayButtonClick();
        Manager.ButtonClicked();
    }
    public void ActionTabPlayButtonClickSimulator()
    {
        Manager.ButtonClicked();
        actionTabPlayButton.GetComponent<TriggerButton>().IntermediateFunction();
    }

    public void SetController(ActionTabController controller)
    {
        this.controller = controller;
    }
    public ActionTabController GetController ()
    {
        return controller;
    }


}

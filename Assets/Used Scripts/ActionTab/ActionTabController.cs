﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionTabController{

    private const string ACTION_TAB_TAG = "ActionsTab";
    private ActionTabView actionTabView;
    public ActionTabController (ActionTabView actionTabView)
    {
        this.actionTabView = actionTabView;

    }

    public void  SimulatePlayButtonClick()
    {
        actionTabView.ActionTabPlayButtonClickSimulator();
    }
    public bool SetSliderValue(float value)
    {
        return actionTabView.SetSliderValue(value);
    }
}

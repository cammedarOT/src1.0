﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public interface ICallable <T,U> 
{
    T Call(U value);
}

public class Mathmatics
{

    /*
     * Takes a function (Callable), rwo limiting values, and computes the root of this function
     * The root is guranteed to be within lower and upper limits
     * This is a mathmatical function, I am not gonna write a documentation for it
     */

    public static float CalculateRoot(ICallable<float, float> callable, float lowerLimit, float upperLimit, float accuracy)
    {
        const int maxIterations = 100;
        float epsilon = 10e-10f;
        float a = lowerLimit, b = upperLimit, c = upperLimit;
        float d = 0, e = 0;
        float fa = callable.Call(a), fb = callable.Call(b), fc;
        float p, q, r, s, tol, xm;
        if ((fa > 0f && fb > 0f) || (fa < 0f && fb < 0f))
            throw new ArgumentException("Root must be bracketed within upper and lower bounds");
        fc = fb;
        for (int iterations = 0; iterations < maxIterations; iterations++)
        {
            if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0))
            {
                c = a;
                fc = fa;
                e = d = b - a;
            }
            if (Mathf.Abs(fc) < Mathf.Abs(fb))
            {
                a = b;
                b = c;
                c = a;
                fa = fb;
                fb = fc;
                fc = fa;
            }
            tol = 2f * epsilon * Mathf.Abs(b) + 0.5f * accuracy;
            xm = 0.5f * (c - b);
            if (Mathf.Abs(xm) <= tol || fb == 0.0) return b;
            if (Mathf.Abs(e) >= tol && (Mathf.Abs(fa) > Mathf.Abs(fb)))
            {
                s = fb / fa;
                if (a == c)
                {
                    p = 2f * xm * s;
                    q = 1f - s;
                }
                else
                {
                    q = fa / fc;
                    r = fb / fc;
                    p = s * (2f * xm * q * (q - r) - (b - a) * (r - 1f));
                    q = (q - 1f) * (r - 1f) * (s - 1f);
                }
                if (p > 0f)
                {
                    q = -q;
                }
                p = Math.Abs(p);
                float min1 = 3f * xm * q - Mathf.Abs(tol * q);
                float min2 = Mathf.Abs(e * q);
                if (2f * p < (Mathf.Min(min1, min2)))
                {
                    e = d;
                    d = p / q;
                }
                else
                {
                    d = xm;
                    e = d;
                }
            }
            a = b;
            fa = fb;
            if (Math.Abs(d) > tol)
            {
                b += d;
            }
            else
            {
                b += SIGN(tol, xm);
            }
            fb = callable.Call(b);
        }
        throw new Exception("Maximum number of iterations exceeded");
    }


    private static float SIGN(float a, float b)
    {
        return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);
    }

}
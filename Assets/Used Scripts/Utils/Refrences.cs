﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * This file is used to collect all the commonly used refrences in the program
 * The simple rule is : once you need a specific refrence in more than two files then it goes in here
 * then you can refrence this file from any place inside the program to get whatever refrence you like
 * Refrencing this file should happen in the inspector, don't use this find by method
 * 
 * Pros
 *      Changing some game object is now easy, just replace the refrence in this file only, and all thing should oberate normally
 *      No need to use the find costly operation
 * Cons
 *      in every file you have access to all refrences
 *      =========WARNING======================
 *          Always declare all your refrences in the beginning of the class/ function you wish to use them in
 *          i don't want to see SomeObject.GetCompnonet<Something>().anything.anywho.anywhere  --> m4 hy7slk kwys sd2ny
 *      ======================================
 * 
*/
public class Refrences : MonoBehaviour {

    public GameObject rig3d;

}

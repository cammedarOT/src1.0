﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionSliderManager  {


    private const float SLIDER_VALUE_INCREMENT = 0.005f;
    private Slider slider;
    private Button button;
    private bool buttonEnabled = false;
    private bool sliderStoppedOnce = false;


    /*
     * @Name : Constructor for action slider manager
     * @Params : slider and its attached play button
     *           for now the attached play buton must have a trigger button script attached to it
     *           otherwise it throws null argument exception when calling Manage
     * 
     * 
     */
    public ActionSliderManager(Slider slider , Button button)
    {
        this.slider = slider;
        this.button = button;
    }


    /*
     * Name : Manage Action Slider
     * @params : NONE
     * Behaviour : Manages the action controller menu Big play Button
     *              When the button is pressed it starts playing the slider
     *              When the slider reaches its end it will stop the button, when the button is clicked again 
     *              it starts playing form the beginning
     */
    public void Manage()
    {
        if (!buttonEnabled)
        {
            return;
        }
        else
        {
            if (sliderStoppedOnce)
            {
                /*if (slider.value == slider.maxValue)
                {
                    //Set The slider to min value unless it was moved by the user
                    slider.value = slider.minValue;
                }*/
                slider.value = slider.minValue;
                sliderStoppedOnce = false;
            }
            if (!MoveSlider(slider, buttonEnabled, SLIDER_VALUE_INCREMENT))
            {
                Debug.Log("Can't Move Slider");
                buttonEnabled = false;
                button.transform.GetComponent<TriggerButton>().IntermediateFunction();
                sliderStoppedOnce = true;
            }
        }
        return;
    }

    private bool MoveSlider(Slider slider, bool canMove, float movementValue)
    {
        float oldSliderValue = slider.value;
        if (canMove)
        {
            slider.value += movementValue;
        }
        else return false;
        return slider.value != oldSliderValue;
    }


    /* 
     * @Name : Button Clicked
     * @Params : None
     * @ Behaviour : just a publisher function to indicate that a button has been clicked
     *  //TODO : Event Publishing system, may take a week or so, using unity's right now
     */
    public void ButtonClicked()
    {
        buttonEnabled = !buttonEnabled;
    } 
}

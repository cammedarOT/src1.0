﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;
using Beebyte.Obfuscator;
public class Utils
{
	


    public static bool SetSliderValue(Slider slider, float value)
    {
        if (slider.value == value)
            return false;
        slider.value = value;
        return slider.value == value;
    }
	[ObfuscateLiterals]
	public static void setMusclesVisibility(Boolean visiblity)
	{
		const string SCRIPT_OBJECT_TAG = "ScriptManager";
		LayersController layerController =  GameObject.FindGameObjectWithTag(SCRIPT_OBJECT_TAG).GetComponent<LayersController>();
		if(visiblity)
			layerController.SetLayer(layerController.getNumLayers ()-1); //-1 is to ignore skin layer
		else
			layerController.SetLayer(0);
	}
	[ObfuscateLiterals]
    public static void Reset()
    {
        //remember to add the onclick in the GUI
        UI_manager Manager = GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<UI_manager>();
        Manager.deselectAll();
        //Manager.closeAllBoxes ();
        GameObject[] array;
        array = GameObject.FindGameObjectsWithTag("object");
        foreach (GameObject go in array)
        {
            muscleControllersData_Managed mc = go.GetComponent<muscleControllersData_Managed>();
            if (mc != null)
                mc.sliderValue = 0;
        }

		//reset camera
		//find the gameObject tagged MainCamera, then get it's parent which contain the script which have the reset function
		GameObject.FindGameObjectWithTag("MainCamera").transform.parent.GetComponent<newCameraRotation>().reset();

        //di msh shghala lesa - close options menu l fo2
        //Manager.closeOpenMenu ();
        ResetMotion();

        //hna mfrod atfi el open boxes/windows
        GameObject openWindows = GameObject.FindGameObjectWithTag("hoverWindows");
        int windowsCount = openWindows.transform.childCount;
        while (windowsCount > 0)
        {
            windowsCount--;
            GameObject.Destroy(openWindows.transform.GetChild(0).gameObject);
        }
        GameObject Rigg3D = GameObject.FindGameObjectWithTag("Rig");
        for (int i = 0; i < Rigg3D.transform.childCount; i++)
        {
            Rigg3D.transform.GetChild(i).gameObject.SetActive(Rigg3D.transform.GetChild(i).transform.gameObject.name != "Sphere_001" && !Rigg3D.transform.GetChild(i).transform.gameObject.name.Contains("Range"));

        }

		//Reset layers count
		setMusclesVisibility (true);
    }
    public static void ShowAllMuscles()
    {
        GameObject Rigg3D = GameObject.FindGameObjectWithTag("Rig");
        for (int i = 0; i < Rigg3D.transform.childCount; i++)
        {
            Rigg3D.transform.GetChild(i).gameObject.SetActive(Rigg3D.transform.GetChild(i).transform.gameObject.name != "Sphere_001" && !Rigg3D.transform.GetChild(i).transform.gameObject.name.Contains("Range"));

        }
    }
	[ObfuscateLiterals]
    public static void ResetMotion()
    {
        GameObject sideBar = GameObject.FindGameObjectWithTag("SideBar");
        GameObject actionsContent = sideBar.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).gameObject;
        if (actionsContent == null)
            return;
        for (int i = 0; i < actionsContent.transform.childCount; i++)
        {
            actionsContent.transform.GetChild(i).GetComponent<actionObjectScript>().sliderValue = 0;
            actionsContent.transform.GetChild(i).GetComponent<actionObjectScript>().lastValidSliderVal = 0;
        }
    }
	[ObfuscateLiterals]
    public static void OpenActionController(GameObject actionObject , Vector3 position, bool multiSelect)
    {
		Utils.DestroyMuscleControllers(); //Bassem 
        DestroyActionControllers(multiSelect);
        ActionController controller = new ActionController();
        controller.SetActionMenuObject(actionObject);
        controller.SetSliderValue(actionObject.GetComponent<actionObjectScript>().sliderValue);
        controller.SetActionName(actionObject.transform.GetChild(1).GetComponent<Text>().text);
        //Now That's a function Call xD
        controller.SetMuscles(
            actionObject.GetComponent<actionObjectScript>().action.primaryMotion, actionObject.GetComponent<actionObjectScript>().action.primaryMuscles,
            actionObject.GetComponent<actionObjectScript>().action.secondaryMuscles, actionObject.GetComponent<actionObjectScript>().action.secondaryMotion,
            actionObject.GetComponent<actionObjectScript>().action.secondaryMotionType, actionObject.GetComponent<actionObjectScript>().action.secondaryMotionMuscles
            );
        controller.Show();
        controller.setPositionOnCreate(position);
    }
	[ObfuscateLiterals]
    public static void DestroyActionControllers(bool multiSelect)
    {
        if (!multiSelect)
            ResetMotion();
        GameObject hoverWindow = GameObject.FindGameObjectWithTag("hoverWindows") as GameObject;
        if (hoverWindow.transform.childCount > 0 && hoverWindow.transform.GetChild(0).GetComponent<ActionView>() != null)
        {
            for (int i = 0; i < hoverWindow.transform.childCount; i++)
            {
                hoverWindow.transform.GetChild(0).GetComponent<ActionView>().SetMultiSelect(multiSelect);
                hoverWindow.transform.GetChild(0).GetComponent<ControllerAnalyticsRemover>().Remove();
                GameObject.Destroy(hoverWindow.transform.GetChild(0).gameObject);
            }
        }
    }
	[ObfuscateLiterals]
    public static void DestroyMuscleControllers()
    {
        GameObject hoverWindow = GameObject.FindGameObjectWithTag("hoverWindows") as GameObject;
        if (hoverWindow.transform.childCount > 0 && hoverWindow.transform.GetChild(0).GetComponent<boxReferenceObject>() != null)
        {
            for (int i = 0; i < hoverWindow.transform.childCount; i++)
            {
                hoverWindow.transform.GetChild(0).GetComponent<ControllerAnalyticsRemover>().Remove();
                GameObject.Destroy(hoverWindow.transform.GetChild(0).gameObject);
            }
        }
    }



	[ObfuscateLiterals]
    /*
     * Just a few quick notes for any one editing this code other than me
     * This function doesn't belong here, I know that, don't think too bad of me
     * If I refactored this code in any point of time I promise I will put it in appropriate location
     * 
     * Now What does this do
     * The action normally takes a value from 0--> 1 and moves the bones accordingly
     * It returns true if the value I provided is ni its range, else it returns false
     * 
     * 
     * It Takes the action and contraction value
     */
    public static bool ExecuteAction(actionObjectScript action , float contractionValue)
    {
        //hagib l bone limit rotation script
        bool inRange, returnValue;
        if (action.one.gameObject.GetComponent<limitRotation_sphere_Managed>() != null)
        {
            inRange = action.one.gameObject.GetComponent<limitRotation_sphere_Managed>().advancingEnabled;
        }
        else
        {
            inRange = true;
        }
        //if el advancing enabled fl bone el hya mas2ola 3anha
        if (inRange || contractionValue <= action.lastValidSliderVal)
        {
            action.sliderValue = contractionValue;
            action.lastValidSliderVal = contractionValue;
            action.updateMusclesAccordingToCurves();
            returnValue = true;
        }
        //else -> sliders htb2a = lastValidSliderValue
        else
        {
            //mfrod hna a5ali l slider value gowa el actionobject we ka slider 3adi yb2o l lastvalidvalue
            action.sliderValue = action.lastValidSliderVal;
            action.updateMusclesAccordingToCurves();
            //this.transform.GetChild(ACTION_PLAYER_INDEX).GetChild(SLIDER_INDEX).GetComponent<Slider>().value = action.lastValidSliderVal;
            contractionValue = action.lastValidSliderVal;
            returnValue = false;
        }


        for (int i = 0; i < action.accompayingActions.Count; i++)
        {
            action.accompayingActions[i].sliderValue = contractionValue * action.accompayingWeights[i];
            action.accompayingActions[i].updateMusclesAccordingToCurves();
        }
        //do you feel it <3  29 August 


        return returnValue;
    }
    //mwgoda tani fi setAllFunctionalityFunc.cs
    // true -> all functional 
    // false -> all dysfunctional
	[ObfuscateLiterals]
    public static void SetAllMuscles(bool functionality)
    {
        GameObject rigg3D = GameObject.FindGameObjectWithTag("Rig");
        int musclesNumber = rigg3D.transform.childCount;
        for (int i = 0; i < musclesNumber; i++)
        {
            GameObject muscle = rigg3D.transform.GetChild(i).gameObject;

            //if it is a muscle and not a parent in the heirarchy
            if (muscle.GetComponent<muscleControllersData_Managed>())
            {
                //the boolean value means (disfunctional) inside the object so we take its not
                muscle.GetComponent<muscleControllersData_Managed>().setDysfunctional(!functionality);

                //this code should not be here , somewhere else where we can now if the muscle is selected or not
                //to properly choose the current color of the muscle object (yellow, red, gray, .. ) and opacity
                if (!functionality)
                { // -> dysfunctional = false 
                    muscle.GetComponent<Renderer>().material.color = new Color32(92, 89, 89, 1);
                }
                else
                {
                    muscle.GetComponent<Renderer>().material.color = Color.white;
                }
            }
        }
    }
    /*
     * This function takes the bad name generated in unity and converts it to a name that
     * the user can understand
     * it's the little things that matter
     * it removes the _ and changed L and R to left and right   
     * 
     */ 
    [ObfuscateLiterals]
    public static string FixMuscleName (String badMuscleName)
    {
        char[] seperators = new char[] { '_' };
        string[] splittedName = badMuscleName.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
        StringBuilder stringBuilder = new StringBuilder();
        foreach (string part in splittedName)
        {
            if (part.ToLower().Equals("l"))
                stringBuilder.Append("left");
            else if (part.ToLower().Equals("r"))
                stringBuilder.Append("right");
            else stringBuilder.Append(part);
            stringBuilder.Append(" ");
        }
        return stringBuilder.ToString().TrimEnd();

    }

}

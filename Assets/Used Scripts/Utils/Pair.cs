﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
public class Pair <T,U>
{
    T first;
    U second;
    public T First { get { return first; } set { first = value; } }
    public U Second { get { return second; } set { second = value; } }
    public Pair(T first, U second)
    {
        this.first = first;
        this.second = second;
    }
}

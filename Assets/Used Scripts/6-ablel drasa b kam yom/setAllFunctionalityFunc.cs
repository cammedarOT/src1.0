﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beebyte.Obfuscator;
public class setAllFunctionalityFunc : MonoBehaviour {


	public bool doIt = false;
	public bool functional = false;
    public GameObject Rigg3D;
    public GameObject actionsMenuContent;
    public void Update(){
		if (doIt) {
			
			setAllMuscles (functional);
			doIt = false;
		}
	}

	//mwgoda tani fi ActionController.cs
	// true -> all functional 
	// false -> all dysfunctional
	public void setAllMuscles(bool functionality){
		int musclesNumber = Rigg3D.transform.childCount;
		for (int i = 0; i < musclesNumber; i++) {
			GameObject muscle = Rigg3D.transform.GetChild (i).gameObject;

			//if it is a muscle and not a parent in the heirarchy
			if (muscle.GetComponent<muscleControllersData_Managed> ()) {
				//the boolean value means (disfunctional) inside the object so we take its not
				muscle.GetComponent<muscleControllersData_Managed> ().setDysfunctional (!functionality); 

				//this code should not be here , somewhere else where we can now if the muscle is selected or not
				//to properly choose the current color of the muscle object (yellow, red, gray, .. ) and opacity
				if (!functionality) { // -> dysfunctional = false 
					muscle.GetComponent<SkinnedMeshRenderer> ().material.color = new Color32 (92, 89, 89, 1);
				} else {
					muscle.GetComponent<SkinnedMeshRenderer> ().material.color = Color.white;
				}
			}
		}

		//this code to recalculate the action behaviour values based on the new functionlity of all the muscles
		actionsMenuContent.GetComponent<populateActionsTab> ().calculateDisfunc (); //should only be called when the actions tab is populated
		actionsMenuContent.GetComponent<populateActionsTab> ().calculateActionAngles ();

	}
}




// hna ha5ali lon l object gray lw hya dysfunctional


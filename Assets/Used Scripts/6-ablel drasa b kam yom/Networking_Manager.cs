﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.IO;
using UnityEngine.UI;
using System;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Beebyte.Obfuscator;

public class Networking_Manager : MonoBehaviour {

	public Text textObject; //not static , for the fisrt scene only
	public Network_based_actions netAction; //not static ,don't need to
    public GameObject statusBox;
    private const int STATUS_TEXT_FIELD_IDX = 0;
    private DatabaseController controller;

	/*
	 * This is the session data of the user, once logged in, all these data are filled and
	 * used through the session for the online communication through this class only
	 * - API key 		: Web API Key of the firebase project, can be get from settings->project settings
	 * - ID token		: A Firebase Auth ID token for the authenticated user.
	 * - Project ID		: This is a unique identifier for your Firebase project which can be found in this project's URL
	 * - refresh token	: A Firebase Auth refresh token for the authenticated user, used to refresh the session
	 * - userName
	 * - password
	 * - expirationTime	: @@TODO should be auto-decremented while the program is running to refresh the session on time
	 * - Signed in		: a boolean that is set when sucessfully signed in to redirect the program behaviour to the online
	 * 					  database behaviour (to get the data from the firebase database, not offline.
	 */
	private static string 	API_KEY;
	private static string 	ID_token;
	private static string 	PROJECT_ID;
	private static string 	refreshToken;
	private static string	userName; 
	private static string 	password;
	private static int 		expirationTime;
    private static string   localID;
    private static string   sessionID;
    private static bool 	SIGNED_IN = false;
    private static int      eventCounter = 0;
	//////////////////////////////


	// variables used as ASYNC Functions declerators that I can wait for using 
	// yield return new WaitUntil(() => get_SIGNED_IN() == true); inside this class only
	private bool addData_done;

	/*
	 * here structs for the request body and the responses body
	 * filled on using the request of their type to convert the JSON data to an object
	 * that can be accessed in the program 
	 */
	struct signInRequest {
		public string 	email;
		public string 	password;
		public bool 	returnSecureToken;
	};
	struct signInResponse {
		public string 	kind;
		public string 	idToken;
		public string 	email;
		public string 	refreshToken;
		public string 	expiresIn;
		public string 	localId;
		public bool	 	registered;
	};
	struct refreshTokenRequest {
		public string 	grant_type;
		public string 	refresh_token;
	};
	struct refreshTokenResponse {
		public string 	expires_in;
		public string 	token_type;
		public string 	refresh_token;
		public string 	id_token;
		public string 	user_id;
		public string 	project_id;
	};
	struct addDataRequest_UserData {
		public string universityEmail;
		public string personalEmail;
		public string department;
		public string expectedDate;
		public string type;
		public string confirmed;
		public string phone;
		public string sessions;
		public string deviceID;
	};
	struct signUpRequest {
		public string 	email;
		public string 	password;
		public bool 	returnSecureToken;
	};
	struct signUpResponse {
		public string 	kind;
		public string 	idToken;
		public string 	email;
		public string 	refreshToken;
		public string 	expiresIn;
		public string 	localId;
	};
    struct session
    {
        public string sessionID;
        public dataBaseEvent dummyEvent;
    };
    struct dataBaseEvent
    {
        public string message;
    };
	/// //////////////////////////////////////////////



	//setters
	private void 	set_API_KEY(string s)		{API_KEY = s; }
	private void 	set_ID_token(string s)		{ID_token = s; }
	private void 	set_refreshToken(string s)	{refreshToken = s; }
	private void 	set_userName(string s)		{userName = s; }
	private void 	set_password(string s)		{password = s; }
	private void 	set_expirationTime(int i)	{expirationTime = i; }
	private void 	set_PROJECT_ID(string s)	{PROJECT_ID = s; }
	private void 	set_SIGNED_IN(bool b)		{SIGNED_IN = b; }
    private void    set_session_ID(string s)    { sessionID = s; }
    private void    set_local_ID(string s)      { localID = s; }
    //getters
    private string 	get_API_KEY()		    {return API_KEY;  }
	private string 	get_ID_token()		    {return ID_token; }
	private string 	get_refreshToken()	    {return refreshToken; }
	private string 	get_userName()		    {return userName; }
	private string 	get_password()		    {return password; }
	private int 	get_expirationTime()    {return expirationTime; }
	private string 	get_PROJECT_ID()	    {return PROJECT_ID; }
	private bool 	get_SIGNED_IN()		    {return SIGNED_IN; }
    private string  get_local_ID()          { return localID; }
    private string  get_session_ID()        { return sessionID; }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    /*
	 * on start function: set the project ID and the API web key to be used on sign in
	 */
    [ObfuscateLiterals]
	void Start () {
		set_PROJECT_ID ("cammedar-website-816de");
		set_API_KEY ("AIzaSyAsZi_pinqiarSFDsI9Gu6f5kzmI09UTOY");
        set_session_ID(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        controller = new DatabaseController();
	}


	/*
	* name: Warn User
	* params: string s 
	* used to give the user a message in the sign in page
	*/
    public void WarnUser(string s) {
        statusBox.SetActive(true);
        statusBox.transform.GetChild(STATUS_TEXT_FIELD_IDX).GetComponent<Text>().text = s;
    }

	/*
	* name: warning
	* params: string s 
	* used to give the developer a warning in the console
	*/
	private void warning (string s){
		Debug.LogError (s);
	}


	////////////////////////////////////////////////////////////
	// network functions here should be created as an IEnumerator (non blocking calls) but those are the functions called by
	// the program classes to use the Network capabilities
	/*
	* name: sign in caller
	* params: string userName , string password
	* calls the IEnumerator by the function startCoroutine to initiate the sign In function declared below
	*/
	public void signIn_caller(string _userName, string _password){
		StartCoroutine (controller.SignInWrapper(_userName,_password,()=>netAction.succesfully_signed_in(),warning => WarnUser(warning)));
	}

    /*
	* name: sign up caller
	* params: string userName , string password , ... TODO
	* calls the IEnumerator by the function startCoroutine to initiate the sign up function declared below
	*/
    [ObfuscateLiterals]
    public void signUp_caller(string _userName, string _password, string _personalEmail, string _Dep, string _cellPhone, string _expectedDate = "") {
        User user = new User
        {
            personalEmail = _personalEmail,
            department = _Dep,
            phone = _cellPhone,
            expectedData = _expectedDate,
            deviceID = SystemInfo.deviceUniqueIdentifier,
            confirmed = "false",
            universityEmail = _userName,
            type = _expectedDate.Equals("") ? "Professional" : "Student"
        };
        StartCoroutine(controller.SignUpWrapper(user, _userName, _password, p_user =>
        {
            sendEmail(_userName, p_user.userId,p_user.type , _Dep);
            sendconfirmationEmail(p_user.personalEmail);
            netAction.succesfully_signed_up();
        }, message => WarnUser(message)));

    }

	/*
	* name: refresh token caller
	* params: none
	* calls the IEnumerator by the function startCoroutine to initiate the refresh token function declared below
	*/
	public void refreshToken_caller(){
		StartCoroutine (refreshTokenAction ());	
	}

	/*
	* name: get data caller
	* params: none
	* calls the IEnumerator by the function startCoroutine to initiate the get data function declared below
	* NEEDS TO BE REDONE!!! TODO
	*/
	public void getData_caller(string _heirarchyLink, string objectType){
		//StartCoroutine (getData (_heirarchyLink, objectType));	
	}

	/*
	* name: add data caller
	* params: string HeirarchyLink
	* calls the IEnumerator by the function startCoroutine to initiate the refresh token function declared below
	* heirarchyLink ex: /Users/Sessions/ID/MouseX
	* heirarchyLink ex: /Muscles/ID/origin
	*/
	public void addData_caller(string _heirarchyLink,string _data ,string objectType){
		StartCoroutine (addData (_heirarchyLink, _data, objectType));	
	}
	[ObfuscateLiterals]
    public void addEvent(string eventData)
    {
        string jsonObject = "{\"event" + (eventCounter++) + "\":\"" + eventData + "\"}"; 
        StartCoroutine(addData("/Users/" + this.get_local_ID()+ "/sessions/" + this.get_session_ID(), jsonObject, null));
    }

    public void ResetPassword(string email)
    {
        controller.ResetPassword(email,message=>WarnUser(message));
    }
	/*
	* name: create Custom Unity Web Request
	* params: string url , string method, string JSONdatastring
	* global function for any HTTP request
	* used to create the request body that will be sent to the Url link provided to it using the method provided
	* request object is returned to the calling function
	* handled methods: GET, POST, PATCH, PUT, DELETE
	*/
	[ObfuscateLiterals]
	private UnityWebRequest createCustomUnityWebRequest(string url, string method, string dataStringJSON = ""){
		bool patch = false;
		if (method == "PATCH") {
			patch = true;
			method = "POST";
		}
		UnityWebRequest wr = new UnityWebRequest (url, method); 
		wr.SetRequestHeader ("Content-Type", "application/json");
		if (patch)	wr.SetRequestHeader ("X-HTTP-Method-Override", "PATCH");
		UploadHandlerRaw upHandler;
		if (method != "GET") {
			byte[] data = System.Text.Encoding.UTF8.GetBytes (dataStringJSON);
			upHandler = new UploadHandlerRaw (data);
			upHandler.contentType = "application/json";
			wr.uploadHandler = upHandler;
		} 
		DownloadHandlerBuffer downHandler = new DownloadHandlerBuffer ();
		wr.downloadHandler = downHandler;

		wr.useHttpContinue = false;
		wr.chunkedTransfer = false;
		wr.redirectLimit = 0;
		wr.timeout = 100;

		return wr;
	}



	/* FIREBASE REST API LINKS:
	 * - signIn : https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=[API_KEY]
	 * request body / response body -> https://firebase.google.com/docs/reference/rest/auth/#section-sign-in-email-password
	 * done
 	 *
	 * - signUp: https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=[API_KEY]
	 * request body / response body -> https://firebase.google.com/docs/reference/rest/auth/#section-create-email-password
	 * not done
 	 *
	 * - refreshToken : https://securetoken.googleapis.com/v1/token?key=[API_KEY]
	 * request body / response body -> https://firebase.google.com/docs/reference/rest/auth/#section-refresh-token
	 * done
 	 *
	 * - getData : https://[PROJECT_ID].firebaseio/[users/jack/name].json
	 * response body?
	 * not done
 	 *
	 * - addData : https://[PROJECT_ID].firebaseio/[users/jack/name].json
	 * request body -> el data
	 * done
 	 *
	 * to authenticate dataBase transactions with the token add ?auth=<ID_TOKEN> to the end of the request
	 * ex: https://<DATABASE_NAME>.firebaseio.com/users/ada/name.json?auth=<ID_TOKEN>"
	 * done
	*/



	// custom functions for dealing with firebase

	/*
	* name: sign in
	* params: string userName , string password
	* *this function's sequence diagram is in "/src1.0/sequenceDiagrams for network/successfull sign in sequence"
	* this function is IEnumerator type to allow it to be non-blocking and called by start coroutine in @signIn_Caller
	* - creates the signInRequest object
	* - creates unityWebRequest object with the url created by the API KEY and data provided by user
	* - sends the request
	* - if it sucessfully signs in:
	* 		- uses the sign in response to fill the network session data
	* 		- checks the /ConfirmedUsers/UserUID.json by getting data from the DB to check if the user is confirmed
	* 				- if the user is confirmed, it initiates the @succesfully_signed_in() function
	*/
	[SkipRename]
	[ObfuscateLiterals]
	IEnumerator signIn(string _userName, string _password){
		//Debug.Log ("SignIn started!");
		set_userName (_userName);
		set_password (_password);

		signInRequest x;
		x.email = _userName;
		x.password = _password;
		x.returnSecureToken = true;
		string dataJSON = JsonUtility.ToJson(x);
        WarnUser("Connecting to server");
		UnityWebRequest wr = createCustomUnityWebRequest ("https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + get_API_KEY(), "POST", dataJSON);
		yield return wr.Send ();

		if (wr.isNetworkError) {
			//y3ni da eno mradesh asln 3la el request
			warning(wr.error);
			warning ("couldn't sign in");
		} else {
			if (wr.downloadHandler.text.IndexOf ("error") == 4) {
				//da test keda ahbal bs ba3raf mno y3ni , lw 7ad emailo maktob fih kelmet error fi nafs el mkan bl sodfa hydrab
				warning ("couldn't sign in");
                WarnUser("Error In signing In");
			}
			//shit , da byerga3 el error obbject bs hwa msh bye3tbro error 
			// laken da y3ni eno raga3 response bs keda , gwah error wla 3adi msh mohem , hyigi hna
			else {
				signInResponse y = JsonUtility.FromJson<signInResponse> (wr.downloadHandler.text);
				set_ID_token (y.idToken);
				set_refreshToken (y.refreshToken);
				set_expirationTime (int.Parse(y.expiresIn));
				set_SIGNED_IN (true);
                set_local_ID(y.localId);
                //addSession();

				bool confirmed = false; 

				//check if user is confirmed in online DB
				string url = "https://" + get_PROJECT_ID() + ".firebaseio.com/" + "Users/" + y.localId + "/confirmed" +".json" + "?auth=" + get_ID_token();
				UnityWebRequest wr2 = createCustomUnityWebRequest (url, "GET");
				yield return wr2.Send ();
				if (wr2.isNetworkError) {
					warning (wr2.error);
					WarnUser("error connecting to server.");
				} else {
					//check if user is confirmed
					Debug.Log(wr2.downloadHandler.text);
					if (wr2.downloadHandler.text.Contains ("true"))
						confirmed = true;
					else {
						WarnUser("this user is not confirmed yet.");
						yield break;
					}
				}
				/////////////////////////////////////////

				//check if same device ID
				url = "https://" + get_PROJECT_ID() + ".firebaseio.com/" + "Users/" + y.localId + "/deviceID" +".json" + "?auth=" + get_ID_token();
				UnityWebRequest wr3 = createCustomUnityWebRequest (url, "GET");
				yield return wr3.Send ();
				if (wr3.isNetworkError) {
					warning (wr3.error);
					WarnUser("error connecting to server.");
				} else {
					//check if user is confirmed
					Debug.Log(wr3.downloadHandler.text);
					if (wr3.downloadHandler.text.Contains(SystemInfo.deviceUniqueIdentifier) && confirmed)
						netAction.succesfully_signed_in ();
					else {
						WarnUser("this is not the device associated to this account.");
					}
				}
				/////////////////////////////////////////

			}
		}
	}


    /*
	* name: sign up
	* params: string userName(UniversityEmail) , string password, string personalEmail, string department, string cellPhone, string expectedYearOfGrad = ""
	* *this function's sequence diagram is in "/src1.0/sequenceDiagrams for network/sign up sequence"
	* this function is IEnumerator type to allow it to be non-blocking and called by start coroutine in @signUp_Caller
	* - creates the signUpRequest object
	* - creates unityWebRequest object with the url created by the API KEY and data provided by user
	* - sends the request
	* - if it sucessfully signs up:
	* 		- send Email to mohamed with UID 
	* 		- add data to database
	*/
    public Text debugText;
	[ObfuscateLiterals]
	[SkipRename]
	IEnumerator signUp(string _userName, string _password, string _personalEmail, string _Dep, string _cellPhone, string _expectedDate = ""){
		set_userName (_userName);
		set_password (_password);

		signUpRequest x;
		x.email = _userName;
		x.password = _password;
		x.returnSecureToken = true;
		string dataJSON = JsonUtility.ToJson(x);
		WarnUser("Connecting to server");
		UnityWebRequest wr = createCustomUnityWebRequest ("https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=" + get_API_KEY(), "POST", dataJSON);
		yield return wr.Send ();

		if (wr.isNetworkError) {
			//y3ni da eno mradesh asln 3la el request
			warning(wr.error);
			warning ("couldn't sign up");
			WarnUser("couldn't sign up");
            debugText.text += "Network Error\n";
		} else {
			if (wr.downloadHandler.text.IndexOf ("error") == 4) {
				//da test keda ahbal bs ba3raf mno y3ni , lw 7ad emailo maktob fih kelmet error fi nafs el mkan bl sodfa hydrab
				warning ("couldn't sign up");
				WarnUser("Error In signing Up");
                debugText.text += "Donwload Handler error\n";
                debugText.text += wr.downloadHandler.text;
			}
			//shit , da byerga3 el error obbject bs hwa msh bye3tbro error 
			// laken da y3ni eno raga3 response bs keda , gwah error wla 3adi msh mohem , hyigi hna
			else {
                debugText.text += "No errors";
                debugText.text += wr.downloadHandler.text;
				signInResponse y = JsonUtility.FromJson<signInResponse> (wr.downloadHandler.text);
				set_ID_token (y.idToken);
				set_refreshToken (y.refreshToken);
				set_expirationTime (int.Parse(y.expiresIn));
				set_SIGNED_IN (true);

				addDataRequest_UserData UserDataObject;
				UserDataObject.personalEmail = _personalEmail;
				UserDataObject.universityEmail = _userName;
				UserDataObject.department = _Dep;
				UserDataObject.expectedDate = _expectedDate;
				UserDataObject.confirmed = "false";
				UserDataObject.phone = _cellPhone;
				UserDataObject.sessions = "null";
				if (_expectedDate!="")
					UserDataObject.type = "student";
				else UserDataObject.type = "professional";
				UserDataObject.deviceID = SystemInfo.deviceUniqueIdentifier;

                //send email
                debugText.text += "Sending Email";
                sendEmail(_userName, y.localId, UserDataObject.type, _Dep);
				sendconfirmationEmail (_personalEmail);
				addData_done = false;
                debugText.text += "Adding Data";
                addData_caller("/Users/" + y.localId, JsonUtility.ToJson(UserDataObject), "a");
				set_SIGNED_IN (false);
				yield return new WaitUntil(() => addData_done == true);
				netAction.succesfully_signed_up ();
			}
		}
	}


	/*
	* name: refresh token
	* params: none
	* sends a request to fireBase API to refresh the access token and uodates the session's access token saved
	* in the program run with the new access token got in the response
	*/
	[SkipRename]
	[ObfuscateLiterals]
	IEnumerator refreshTokenAction(){
		//Debug.Log ("refreshToken started!");
		set_SIGNED_IN (false);
		if (get_ID_token () == "" || get_ID_token () == null) {
			//still haven;t signed in (or the response is not back yet :/ ha3ml a sa3etha?
			//warning("You're not signed in yet");
			yield return new WaitUntil(() => get_SIGNED_IN() == true);
		}
		refreshTokenRequest x;
		x.grant_type = "refresh_token";
		x.refresh_token = get_refreshToken ();
		string dataJSON = JsonUtility.ToJson(x);

		string url = "https://securetoken.googleapis.com/v1/token?key=" + get_API_KEY();
		UnityWebRequest wr = createCustomUnityWebRequest (url, "POST", dataJSON);
		yield return wr.Send ();

		if (wr.isNetworkError) {
			warning(wr.error);
			warning ("can't refresh token");
		} else {
			if (wr.downloadHandler.text.IndexOf ("error") == 4) {
				//da test keda ahbal bs ba3raf mno y3ni , lw 7ad emailo maktob fih kelmet error fi nafs el mkan bl sodfa hydrab
				warning ("can't refresh token");
			}
			// Show results as text
			else{
				Debug.Log(wr.downloadHandler.text);
				refreshTokenResponse y = JsonUtility.FromJson<refreshTokenResponse> (wr.downloadHandler.text);
				set_ID_token (y.id_token);
				set_refreshToken (y.refresh_token);
				set_expirationTime (int.Parse(y.expires_in));
				set_SIGNED_IN (true);
				//Debug.Log ("refreshToken finished!");
			}
		}
	}


	//di bayza 5ales lesa
	//3ashan mfrod el function tb2a non blocking, bs hya lw non blocking yb2a el ndahha 3adda el 7eta el kan mstani fiha el data 5las
	//fa lazm lw hya async/non-blocking yb2a fi 7aga tania ta5od mnha el data t7otha fl mkan bta3ha , aw hya el t7ot el data fl mkan
	//msh te return l data lel flow bta3 l brnameg el 3adi
	//y3ni mmkn msln a3ml thread gdida , we andah fiha el function we yrg3lha el data , ha w b3d keda? msh 3aref 3aiz as2al 7ad ..
	[SkipRename]
	[ObfuscateLiterals]
	IEnumerator getData(string _heirarchyLink, string objectType){
		// _heirarchyLink ex: "/door/color"
		//Debug.Log ("getData started!");
		if (get_ID_token () == "" || get_ID_token () == null) {
			//still haven;t signed in (or the response is not back yet :/ ha3ml a sa3etha?
			//warning("You're not signed in yet");
			yield return new WaitUntil(() => get_SIGNED_IN() == true);
		}
		string url = "https://" + get_PROJECT_ID() + ".firebaseio.com" + _heirarchyLink + ".json" + "?auth=" + get_ID_token();
		UnityWebRequest wr = createCustomUnityWebRequest (url, "GET");
		yield return wr.Send ();

		if (wr.isNetworkError) {
			warning(wr.error);
			warning ("can't get data");
		} else {
			// Show results as text
			Debug.Log(wr.downloadHandler.text);
			// TODO hna mfrod yb2a fi l switch cases bta3et l response object type
			//Debug.Log ("getData finished!");
		}
	}

	/*
	* name: add Data
	* params: string _heirarchyLink, string _data
	* sends a request to fireBase API to add data to the @_heirarchyLink in the parameters
	*/
	[SkipRename] 
	[ObfuscateLiterals]
	IEnumerator addData(string _heirarchyLink, string _data, string objectType){
		// _heirarchyLink ex: "/door/color"
		if (get_ID_token () == "" || get_ID_token () == null) {
			//still haven;t signed in (or the response is not back yet :/ ha3ml a sa3etha?
			//warning("You're not signed in yet");
			yield return new WaitUntil(() => get_SIGNED_IN() == true);
		}

		//these are done outside the function, so the incoming data to the function is JSON format
//		addDataRequest x;
//		x.x = 4;
//		x.y = 2;
//		string dataJSON = JsonUtility.ToJson(x);

		addData_done = false;

		string url = "https://" + get_PROJECT_ID() + ".firebaseio.com" + _heirarchyLink + ".json" + "?auth=" + get_ID_token();
		UnityWebRequest wr = createCustomUnityWebRequest (url, "PATCH", _data);
		yield return wr.Send ();

		if (wr.isNetworkError) {
			warning(wr.error);
			warning ("can't add data");
		} else {
			// Show results as text
			Debug.Log(wr.downloadHandler.text);
			addData_done = true;
			//Debug.Log ("addData finished!");
		}
	}

	[ObfuscateLiterals]
	private void sendEmail(string userName, string UID, string type, string dep){
		
		MailMessage mail = new MailMessage ();
		mail.From = new MailAddress ("cammedarinfo@gmail.com");
        //mail.To.Add ("bassems.design@gmail.com");
        mail.To.Add("team@cammedar.com");
		mail.Subject = "A new user has just signed up!";
		mail.Body = "A new user has just signed up in our program! now we have 'x' users.\n" +
			"You can confirm the new user's account using his data:\n" +
			"E-mail: " + userName + "\n" +
			"UID: " + UID + "\n" + 
			"He/She is a " + type + " at " + dep + ".\nHave a nice day.\n";
		

		SmtpClient smtpServer = new SmtpClient ("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential ("cammedarinfo@gmail.com", "Cammedar1234") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			return true;
		};
		smtpServer.Send (mail);
		Debug.Log ("success");
	}

	[ObfuscateLiterals]
	//this function should be merged with send email function
	public void sendconfirmationEmail(string to){
		MailMessage mail = new MailMessage ();
		mail.From = new MailAddress ("cammedarinfo@gmail.com");
		//mail.To.Add ("bassems.design@gmail.com");
		mail.To.Add(to);
		mail.Subject = "Welcome to Cammedar!";
		mail.Body = "Welcome to Cammedar\nOur team is now working on checking your registration info. to confirm your new account, " +
			"please check your account again in 24 hours.";


		SmtpClient smtpServer = new SmtpClient ("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential ("cammedarinfo@gmail.com", "Cammedar1234") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			return true;
		};
		smtpServer.Send (mail);
		Debug.Log ("success");
	}

	[ObfuscateLiterals]
	public void sendFeedback(string text){

		MailMessage mail = new MailMessage ();
		mail.From = new MailAddress ("cammedarinfo@gmail.com");
		mail.To.Add ("team@cammedar.com");
		mail.Subject = "Feedback from " + this.get_userName();
		mail.Body = text + "\n";


		SmtpClient smtpServer = new SmtpClient ("smtp.gmail.com");
		smtpServer.Port = 587;
		smtpServer.Credentials = new System.Net.NetworkCredential ("cammedarinfo@gmail.com", "Cammedar1234") as ICredentialsByHost;
		smtpServer.EnableSsl = true;
		ServicePointManager.ServerCertificateValidationCallback = 
			delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			return true;
		};
		smtpServer.SendAsync (mail, null);
		Debug.Log ("success");
	}

	[ObfuscateLiterals]
	//this function is temporary untill we create the async getData function
	public IEnumerator getData_Sync(string _heirarchyLink, GameObject panel){
		//ma3reftesh a3mlha sync fa 3mltha customized
		string url = "https://" + get_PROJECT_ID() + ".firebaseio.com" + _heirarchyLink + ".json" + "?auth=" + get_ID_token();
		UnityWebRequest wr = createCustomUnityWebRequest (url, "GET");
		yield return wr.Send ();

		if (wr.isNetworkError) {
			warning (wr.error);
			warning ("can't get data");
		} else {
			// Show results as text
			Debug.Log (wr.downloadHandler.text);
			if (!wr.downloadHandler.text.Contains("error")) { //case to not show the DB message
				panel.SetActive (true);
				//change text in the message box (child[2])
				panel.transform.GetChild (2).GetComponent<Text> ().text = wr.downloadHandler.text;
				panel.GetComponent<RectTransform> ().sizeDelta = new Vector2 (panel.GetComponent<RectTransform> ().rect.width,panel.transform.GetChild (2).GetComponent<Text> ().rectTransform.rect.height + 53	);
			}
		}
	}
    public void GetData(string link, GameObject panel)
    {
        controller.GetMessage(message =>
        {
            panel.SetActive(true);
            //change text in the message box (child[2])
            panel.transform.GetChild(2).GetComponent<Text>().text = message;
            panel.GetComponent<RectTransform>().sizeDelta = new Vector2(panel.GetComponent<RectTransform>().rect.width, panel.transform.GetChild(2).GetComponent<Text>().rectTransform.rect.height + 53);
        });
    }
    public void OnDestroy()
    {
        //controller.SignOut();
    }

}

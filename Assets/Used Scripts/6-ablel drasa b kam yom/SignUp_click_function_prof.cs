﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Globalization;
using System;
public class SignUp_click_function_prof : MonoBehaviour{

	public Text emailInput;
	public Text passwordInput;
	public Text personalEmail;
	public Text Department;
	public Text cellPhone;
	public InputField passwordField;
	public Networking_Manager net;
	[ObfuscateLiterals]
	public void clickingFn(){
		if (emailInput.text == "" || passwordField.text == "" || personalEmail.text == "" || Department.text == "" || cellPhone.text == "") {
			net.WarnUser ("please fill all the required data.");
			return;
		}

		if (!(IsValidEmail (emailInput.text) && IsValidEmail (personalEmail.text))) {
			net.WarnUser ("invalid email adress.");
			return;
		}

		if (emailInput.text == personalEmail.text) {
			net.WarnUser ("both email adresses are the same.");
			return;
		}

		if (passwordField.text.Length < 6) {
			net.WarnUser ("password too short.");
			return;
		}

		string pass = passwordField.text;
		net.signUp_caller(emailInput.text, pass, personalEmail.text, Department.text,  cellPhone.text);
	}

	bool invalid = false;
	[ObfuscateLiterals]
	public bool IsValidEmail(string strIn)
	{
		invalid = false;
		if (String.IsNullOrEmpty(strIn))
			return false;

		// Use IdnMapping class to convert Unicode domain names.
		strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper);
		if (invalid) 
			return false;

		// Return true if strIn is in valid e-mail format.
		return Regex.IsMatch(strIn, 
			@"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + 
			@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$", 
			RegexOptions.IgnoreCase);
	}

	private string DomainMapper(Match match)
	{
		// IdnMapping class with default property values.
		IdnMapping idn = new IdnMapping();

		string domainName = match.Groups[2].Value;
		try {
			domainName = idn.GetAscii(domainName);
		}
		catch (ArgumentException) {
			invalid = true;      
		}      
		return match.Groups[1].Value + domainName;
	}
}

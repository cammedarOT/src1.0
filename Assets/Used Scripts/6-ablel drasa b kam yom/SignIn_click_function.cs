﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class SignIn_click_function : MonoBehaviour , IPointerClickHandler{

	public Text emailInput;
	public Text passwordInput;
	public InputField passwordField;
	public Networking_Manager net;

	public void OnPointerClick(PointerEventData evd){
		string pass = passwordField.text;
        net.signIn_caller(emailInput.text, pass);
	}
}

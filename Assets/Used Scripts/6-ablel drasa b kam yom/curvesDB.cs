﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CurveStruct{
	public AnimationCurve curve;
	public muscleControllersData_Managed musclePointer;
	public float Value;
	public curvesDB DBobject;

	public void setValue(float v){
		this.Value = v;
		DBobject.doUpdate ();
	}

}

[System.Serializable]
public class noRepeatCurve{
	public muscleControllersData_Managed musclePointer;
	public float Value;
}

[System.Serializable]
public class actionCurveStruct {
	public string actionName;
	public List <CurveStruct> Curves = new List <CurveStruct>();

}

public class curvesDB : MonoBehaviour {

	public List <actionCurveStruct> DB = new List <actionCurveStruct>();
	public List <noRepeatCurve> noRep = new List <noRepeatCurve> ();

	void Start(){
		// find noRep curves from the DB
		for (int i = 0; i < DB.Count; i++) {
			for (int j = 0; j < DB [i].Curves.Count; j++) {
				if (!findInNoRep (DB [i].Curves [j].musclePointer)) {
					noRepeatCurve noo = new noRepeatCurve ();
					noo.musclePointer = DB [i].Curves [j].musclePointer;
					noRep.Add (noo);
				}
			}
		}
	}

	void FixedUpdate(){
		//TODO third step to add this value to the musclecontrollers data
		//i think it don't have to be in fixedupdate, do update is enough , think about it
	}

	public void doUpdate () {
		// calculate sum of all numbers per muscle controller (uniques are collected in roRep)
		//initialize them to 0
		for (int i = 0; i < noRep.Count; i++) {
			noRep [i].Value = 0;
		}
		//get sums .. so slow approach , needs revising
		for (int i = 0; i < noRep.Count; i++) {
			for (int j = 0; j < DB.Count; j++) {
				for (int k = 0; k < DB [j].Curves.Count; k++) {
					if (noRep [i].musclePointer == DB [j].Curves [k].musclePointer)
						noRep [i].Value += DB [j].Curves [k].Value;
				}
			}
		}

		for (int i = 0; i < noRep.Count; i++) {
			noRep [i].musclePointer.sliderValuefromCurvesDataBase = noRep [i].Value;
		}
	}

	bool findInNoRep( muscleControllersData_Managed m ){
		for (int i = 0; i < noRep.Count; i++) {
			if (noRep [i].musclePointer == m)
				return true;
		}
		return false;
	}


}



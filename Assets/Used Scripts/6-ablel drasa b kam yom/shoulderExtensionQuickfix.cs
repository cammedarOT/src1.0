﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class shoulderExtensionQuickfix : MonoBehaviour {

	private GameObject actionTabContent;
	private actionObjectScript shoulderFlexionAction = null;
	private actionObjectScript shoulderExtensionAction = null;
	private bool first = true;
	private const int TEXT_IN_ACTION_INDEX = 1;
	[ObfuscateLiterals]
	void Start(){
		actionTabContent = GameObject.FindGameObjectWithTag ("ActionsTabContentPanel").gameObject;
	}
	[ObfuscateLiterals]
	void FixedUpdate(){
		if (first) {
			getActions ();
			first = false;
		}
		if (shoulderExtensionAction.function1 == "000") { // 120
			shoulderExtensionAction.angle1x = - shoulderFlexionAction.angle1x * shoulderFlexionAction.sliderValue + 0;
		}
		else if (shoulderExtensionAction.function1 == "001") { // 120:0
			shoulderExtensionAction.angle1x = - shoulderFlexionAction.angle1x * shoulderFlexionAction.sliderValue + 0;

		} else if (shoulderExtensionAction.function1 == "010") {// 120:-40
			shoulderExtensionAction.angle1x = - shoulderFlexionAction.angle1x * shoulderFlexionAction.sliderValue + 40;

		} else if (shoulderExtensionAction.function1 == "011") {// 120:-60
			shoulderExtensionAction.angle1x = - shoulderFlexionAction.angle1x * shoulderFlexionAction.sliderValue + 60;

		} else if (shoulderExtensionAction.function1 == "100") { // 120:0
			shoulderExtensionAction.angle1x = - shoulderFlexionAction.angle1x * shoulderFlexionAction.sliderValue + 10;

		}
	}
	[ObfuscateLiterals]
	void getActions (){
		int ch = actionTabContent.transform.childCount;
		for (int i = 0; i < ch; i++) {
			string t = actionTabContent.transform.GetChild (i).GetChild (TEXT_IN_ACTION_INDEX).GetComponent<Text> ().text;
			if (t == "shoulder flexion") {
				shoulderFlexionAction = actionTabContent.transform.GetChild (i).gameObject.GetComponent<actionObjectScript> ();
			} else if (t == "shoulder extension") {
				shoulderExtensionAction = actionTabContent.transform.GetChild (i).gameObject.GetComponent<actionObjectScript> ();
			}

			if (shoulderFlexionAction != null && shoulderExtensionAction != null)
				break;
		}
	}


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Beebyte.Obfuscator;
public class humerusRangeRotation : MonoBehaviour {

	private GameObject actionTabContent;
	private actionObjectScript upperRotationAction;
	private bool first = true;
	private const int TEXT_IN_ACTION_INDEX = 1;
	[ObfuscateLiterals]
	void Start(){
		actionTabContent = GameObject.FindGameObjectWithTag ("ActionsTabContentPanel").gameObject;
	}
	[ObfuscateLiterals]
	void FixedUpdate(){
		if (first) {
			getUpperRotationAction ();
			first = false;
		}
		Vector3 r = this.transform.localRotation.eulerAngles;
		if (upperRotationAction.function1 == "00") {
			//0
			r.x = 10;
		}
		else if (upperRotationAction.function1 == "01") {
			//40
			r.x = -40;
		} else if (upperRotationAction.function1 == "10") {
			//40
			r.x = -40;
		} else if (upperRotationAction.function1 == "11") {
			//60
			r.x = -60;
		}

		this.transform.localRotation = Quaternion.Euler(r);

	}
	[ObfuscateLiterals]
	void getUpperRotationAction (){
		int ch = actionTabContent.transform.childCount;
		for (int i = 0; i < ch; i++) {
			string t = actionTabContent.transform.GetChild (i).GetChild (TEXT_IN_ACTION_INDEX).GetComponent<Text> ().text;
			if (t == "scapula upper rotation") {
				upperRotationAction = actionTabContent.transform.GetChild (i).gameObject.GetComponent<actionObjectScript>();
				break;
			}
		}
	}
}

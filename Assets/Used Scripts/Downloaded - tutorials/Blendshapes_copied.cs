﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Blenshapes script.
/// 23-3-2017
/// --
/// this script was downloaded as a guide to deal with Unity/Blender blendshapes using
/// the scripting API 
/// it just contains the basic functions but it is not used as a script in the project till now
/// (it is not referenced so it can be deleted anytime)
/// </summary>

public class blenshapes_script : MonoBehaviour {

	int blendShapeCount;
	SkinnedMeshRenderer skinnedMeshRenderer;
	Mesh skinnedMesh;
	float blendOne = 0f;
	float blendTwo = 0f;
	public float blendSpeed = 1f;
	bool blendOneFinished = false;

	void Awake ()
	{
		skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		skinnedMesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
	}

	// Use this for initialization
	void Start () {
		blendShapeCount = skinnedMesh.blendShapeCount; 
		Debug.Log (blendShapeCount);

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (blendShapeCount > 2) {

			if (blendOne < 100f) {
				skinnedMeshRenderer.SetBlendShapeWeight (0, blendOne);
				blendOne += blendSpeed;
			} else {
				blendOneFinished = true;
			}

			if (blendOneFinished == true && blendTwo < 100f) {
				skinnedMeshRenderer.SetBlendShapeWeight (1, blendTwo);
				blendTwo += blendSpeed;
			}

		}
	}
}

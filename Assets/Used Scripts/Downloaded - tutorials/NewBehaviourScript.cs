﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script that controls vertices edits in blender editor.
/// 25-3-2017
/// --
/// this script was downloaded as a tutorial to implement the in Unity editor vertix manipulation
/// I didn't implement that control yet and didn't as i didn't need to, so I don't yet fully
/// understand this code below, it's not referenced anywhere in the project so it can be deleted anytime
/// </summary>

[ExecuteInEditMode]

public class VertHandler : MonoBehaviour 
{
	Mesh mesh;
	Vector3[] verts;
	Vector3 vertPos;
	GameObject[] handles;

	void OnEnable()
	{
		mesh = GetComponent<MeshFilter>().mesh;
		verts = mesh.vertices;
		foreach(Vector3 vert in verts)
		{
			vertPos = transform.TransformPoint(vert);
			GameObject handle = new GameObject("handle");
			handle.transform.position = vertPos;
			handle.transform.parent = transform;
			handle.tag = "handle";
			//handle.AddComponent<Gizmo_Sphere>();
			print(vertPos);
		}
	}

	void OnDisable()
	{
		GameObject[] handles = GameObject.FindGameObjectsWithTag("handle");
		foreach(GameObject handle in handles)
		{
			DestroyImmediate(handle);    
		}
	}

	void Update()
	{
		handles = GameObject.FindGameObjectsWithTag ("handle");
		for(int i = 0; i < verts.Length; i++)
		{
			verts[i] = handles[i].transform.localPosition;    
		}
		mesh.vertices = verts;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
	}
}

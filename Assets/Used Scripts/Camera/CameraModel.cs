﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraModel {
	/// ORBITTING ///////////////////////////
	private float lookSensitivity = 5;
	private float yRotation;
	private float xRotation;
	private float currentYRotation;
	private float currentXRotation;
	private float yRotationV;
	private float xRotationV;
	private float lookSmoothDamp = 0.1f;
	private float distance; 
	private bool resetter = false;
	//==

	public void setCameraLocation(float xrot, float yrot, float dist){
		this.xRotation = xrot;
		this.yRotation = yrot;
		this.distance = dist;
	}

	private void calculateNewLocation(){
		xRotation = Mathf.Clamp (xRotation, -90, 90);

		currentXRotation = Mathf.SmoothDamp (currentXRotation, xRotation, ref xRotationV, lookSmoothDamp);
		currentYRotation = Mathf.SmoothDamp (currentYRotation, yRotation, ref yRotationV, lookSmoothDamp);

	}
}

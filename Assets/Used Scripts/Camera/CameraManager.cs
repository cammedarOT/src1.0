﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class cam{
	public Transform cameraContainer;
	private Transform cameraobject;
	private Camera camera;
	private newCameraScript newcamerascript;
	private newCameraRotation newcamerarotation;

	public void setcameraobject(Transform co){
		this.cameraobject = co;
	}
	public void setcamera(){
		this.camera = this.cameraobject.gameObject.GetComponent<Camera> ();
	}
	public void setnewcamerascript(){
		this.newcamerascript = this.camera.GetComponent<newCameraScript> ();
	}
	public void setnewcamerarotation(){
		newcamerarotation = cameraContainer.GetComponent<newCameraRotation>();
	}
	public void setRenderTextureRect(Rect t){
		this.newcamerarotation.setRenderTextureRect (t);
		this.newcamerascript.setRenderTextureRect (t);
	}
	public void useRenderTexture(RenderTexture rt){
		this.newcamerarotation.useRenderTexture (rt);
	}
	public void freeRenderTexture(){
		this.newcamerarotation.freeRenderTexture ();
	}
	public void setParameters(float rotx, float roty, float distance){
		this.newcamerarotation.setRot (rotx, roty);
		this.newcamerascript.setDistance (distance);
	}
	public void setPos(Vector3 pos){
		this.cameraContainer.position = pos;
	}
}

[System.Serializable]
public class CameraManager : MonoBehaviour{

	public RawImage doubleview_L;
	public RawImage doubleview_R;

	public RawImage quadview_LT;
	public RawImage quadview_RT;
	public RawImage quadview_LB;
	public RawImage quadview_RB;
	 
	public List <cam> cameraList = new List <cam>();
	private const int CAMERA_1_INDEX = 0;
	private const int CAMERA_2_INDEX = 1;
	private const int CAMERA_3_INDEX = 2;
	private const int CAMERA_4_INDEX = 3;
	public Camera dummycameratoclearscreen;

	void Start(){
		for (int i = 0; i < cameraList.Count; i++) {
			Transform t = cameraList [i].cameraContainer.GetChild (0).transform;
			cameraList[i].setcameraobject(cameraList[i].cameraContainer.GetChild(0).transform);
			cameraList [i].setcamera ();
			cameraList [i].setnewcamerascript ();
			cameraList [i].setnewcamerarotation ();
		}
	}

	public void setCameraPosition_oldWay(int CAMERA_INDEX,float rotx, float roty, float distance, float pos_x = 0, float pos_y = 0, float pos_z = 0){
		cameraList [CAMERA_INDEX-1].setParameters (rotx, roty, distance);
		if (!(pos_x == 0 && pos_y == 0 && pos_z == 0))
			cameraList [CAMERA_INDEX - 1].setPos (new Vector3 (pos_x, pos_y, pos_z));
	}
	
	public void setSingleView(){
		dummycameratoclearscreen.gameObject.SetActive (false);
		doubleview_L.gameObject.SetActive (false);
		doubleview_R.gameObject.SetActive (false);
		quadview_LT.gameObject.SetActive (false);
		quadview_RT.gameObject.SetActive (false);
		quadview_LB.gameObject.SetActive (false);
		quadview_RB.gameObject.SetActive (false);

		cameraList [CAMERA_1_INDEX].cameraContainer.gameObject.SetActive (true);
		cameraList [CAMERA_1_INDEX].setRenderTextureRect (new Rect (0,0,Screen.width, Screen.height));
		cameraList [CAMERA_1_INDEX].freeRenderTexture ();

		cameraList [CAMERA_2_INDEX].cameraContainer.gameObject.SetActive (false);
		cameraList [CAMERA_3_INDEX].cameraContainer.gameObject.SetActive (false);
		cameraList [CAMERA_4_INDEX].cameraContainer.gameObject.SetActive (false);
	}

	public void setDoubleView(){
		dummycameratoclearscreen.gameObject.SetActive (true);
		doubleview_L.gameObject.SetActive (true);
		doubleview_R.gameObject.SetActive (true);
		quadview_LT.gameObject.SetActive (false);
		quadview_RT.gameObject.SetActive (false);
		quadview_LB.gameObject.SetActive (false);
		quadview_RB.gameObject.SetActive (false);

		cameraList [CAMERA_1_INDEX].cameraContainer.gameObject.SetActive (true);
		Rect r = doubleview_L.rectTransform.rect;
		r.position = doubleview_L.rectTransform.anchoredPosition;
		//r.xMin = 0; r.xMax = Screen.width / 2;
		//((RenderTexture)doubleview_L.texture).width = Screen.width/2;
		//((RenderTexture)doubleview_L.texture).height = Screen.height;
		cameraList [CAMERA_1_INDEX].setRenderTextureRect (r);
		cameraList [CAMERA_1_INDEX].useRenderTexture((RenderTexture)doubleview_L.texture);


		r = doubleview_R.rectTransform.rect;
		r.position = doubleview_R.rectTransform.anchoredPosition;
		cameraList [CAMERA_2_INDEX].cameraContainer.gameObject.SetActive (true);
		cameraList [CAMERA_2_INDEX].setRenderTextureRect (r);
		cameraList [CAMERA_2_INDEX].useRenderTexture((RenderTexture)doubleview_R.texture);

		cameraList [CAMERA_3_INDEX].cameraContainer.gameObject.SetActive (false);
		cameraList [CAMERA_4_INDEX].cameraContainer.gameObject.SetActive (false);
	}

	public void setQuadView(){
		dummycameratoclearscreen.gameObject.SetActive (true);
		doubleview_L.gameObject.SetActive (false);
		doubleview_R.gameObject.SetActive (false);
		quadview_LT.gameObject.SetActive (true);
		quadview_RT.gameObject.SetActive (true);
		quadview_LB.gameObject.SetActive (true);
		quadview_RB.gameObject.SetActive (true);

		cameraList [CAMERA_1_INDEX].cameraContainer.gameObject.SetActive (true);
		Rect r = quadview_LT.rectTransform.rect;
		r.position = quadview_LT.rectTransform.anchoredPosition;
		cameraList [CAMERA_1_INDEX].setRenderTextureRect (r);
		cameraList [CAMERA_1_INDEX].useRenderTexture((RenderTexture)quadview_LT.texture);

		r = quadview_RT.rectTransform.rect;
		r.position = quadview_RT.rectTransform.anchoredPosition;
		cameraList [CAMERA_2_INDEX].cameraContainer.gameObject.SetActive (true);
		cameraList [CAMERA_2_INDEX].setRenderTextureRect (r);
		cameraList [CAMERA_2_INDEX].useRenderTexture((RenderTexture)quadview_RT.texture);

		r = quadview_LB.rectTransform.rect;
		r.position = quadview_LB.rectTransform.anchoredPosition;
		cameraList [CAMERA_3_INDEX].cameraContainer.gameObject.SetActive (true);
		cameraList [CAMERA_3_INDEX].setRenderTextureRect (r);
		cameraList [CAMERA_3_INDEX].useRenderTexture((RenderTexture)quadview_LB.texture);

		r = quadview_RB.rectTransform.rect;
		r.position = quadview_RB.rectTransform.anchoredPosition;
		cameraList [CAMERA_4_INDEX].cameraContainer.gameObject.SetActive (true);
		cameraList [CAMERA_4_INDEX].setRenderTextureRect (r);
		cameraList [CAMERA_4_INDEX].useRenderTexture((RenderTexture)quadview_RB.texture);
	}


}
